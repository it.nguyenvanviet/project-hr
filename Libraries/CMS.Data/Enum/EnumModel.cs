﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CMS.Data.Enum
{
    public enum ListController
    {
        DashBoard_Controller,
        Menu_Controller,
        Account_Controller
    }
    public enum GenderEnum
    {

        [Display(Name = "Nữ")]
        Female,
        [Display(Name = "Nam")]
        Male,
        [Display(Name = "Không xác định")]
        Unspecified
    }
    public enum StatusEnum
    {
       
        [Display(Name = "Bình thường")]
        Active = 1,
        [Display(Name = "Không hoạt động")]
        InActive = 0,
        [Display(Name = "Tạm dừng")]
        Pending = 2,
        [Display(Name = "Đóng")]
        Close = 3
    }
    public enum PriorityEnum
    {
        [Display(Name = "Thấp")]
        Low = 1,
        [Display(Name = "Trung bình")]
        Medium = 2,
        [Display(Name = "Cao")]
        High = 3
    }
    public enum MenuTypeEnum
    {
        [Display(Name = "Menu HR")]
        Menu_Hr,
        [Display(Name = "Menu Project")]
        Menu_Project,
        [Display(Name = "Menu Cấu hình")]
        Menu_Setting
    }
    public enum EducationDegree
    {
        [Display(Name = "Lao động phổ thông")]
        Unskilled_labor,
        [Display(Name = "Cao học")]
        HighSchool,
        [Display(Name = "Trung học")]
        Postgraduate,
        [Display(Name = "Chứng chỉ")]
        Certificate,
        [Display(Name = "Trung cấp")]
        Intermediate,
        [Display(Name = "Cao đẳng")]
        College,
        [Display(Name = "Đại học")]
        University,

    }
    public enum Rank
    {
        [Display(Name = "Nhân viên")]
        Staff,
        [Display(Name = "Cộng tác viên")]
        Collaborators,
        [Display(Name = "Trưởng nhóm")]
        Lead,
        [Display(Name = "Chuyên gia")]
        Expert,
        [Display(Name = "Phó phòng")]
        Deputyhead,
        [Display(Name = "Quản lý cấp cao")]
        SeniorManager
    }
    public enum StatusEmp
    {

        [Display(Name = "Không hoạt động")]
        InActive = 0,
        [Display(Name = "Đang thử việc")]
        Working = 1,
        [Display(Name = "Đã nhận việc")]
        Active = 2,
        [Display(Name = "Tạm dừng công việc")]
        Pending = 3,
        [Display(Name = "Đã nghỉ việc")]
        Close = 4,
    }
    public enum SocialType
    {

        [Display(Name = "Facebook")]
        Facebook,
        [Display(Name = "Twitter")]
        Twitter,
        [Display(Name = "Instagram")]
        Instagram,
        [Display(Name = "Zalo")]
        Zalo
    }
    public enum LeaveTypeEnum
    {

        [Display(Name = "Nghỉ không phép")]
        WithoutPermission = 0,
        [Display(Name = "Nghỉ phép năm")]
        AnnualLeave = 1,
        [Display(Name = "Nghỉ ốm")]
        SickLeave = 2,
        [Display(Name = "Nghỉ thai sản")]
        Pregnant = 3,
        [Display(Name = "Làm việc tại nhà")]
        WorkFromHome = 4,
        [Display(Name = "Nghỉ lễ")]
        Holidays = 5
    }
    public enum LeaveStatusEnum
    {

        [Display(Name = "Chưa xử lý")]
        Notyet = 0,
        [Display(Name = "Đang xử lý")]
        Processing = 1,
        [Display(Name = "Đã xử lý")]
        Finished = 2,
    }
    public enum ApproveStatusEnum
    {

        [Display(Name = "Chưa duyệt")]
        NotApproved = 0,
        [Display(Name = "Đã duyệt")]
        Approved = 1,
        [Display(Name = "Từ chối")]
        Refuse = 2
    }
    public enum TypeSkillEnum
    {

        [Display(Name = "Skill phục vụ làm việc")]
        SkillforWork = 0,
        [Display(Name = "Kĩ năng mềm")]
        SoftSkill = 1,
        [Display(Name = "Khác")]
        Anothers = 2
    }

}
