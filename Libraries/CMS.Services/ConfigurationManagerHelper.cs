﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace CMS.Services
{
    public enum GetDatabaseType
    {
        /// <summary>
        /// Database Write Inside - Internet
        /// </summary>
        MSSQLServerInside = 1,
        /// <summary>
        /// Database Partner Oracle
        /// </summary>
        Job_Connection = 3,

        /// <summary>
        /// Database DatawareHouse Oracle
        /// </summary>
        PLSQLServerDatawarehouse = 4
    }

    public class ConfigurationManagerHelper
    {
        private readonly string _connectionString = string.Empty;

        public ConfigurationManagerHelper(GetDatabaseType databaseType)
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, true);
            var root = configurationBuilder.Build();
            var appsettings = root.GetSection("ConnectionStrings");

            _connectionString = databaseType switch
            {
                GetDatabaseType.MSSQLServerInside => appsettings.GetSection("DefaultConnection").Value,
                GetDatabaseType.Job_Connection => appsettings.GetSection("Job_Connection").Value,
                GetDatabaseType.PLSQLServerDatawarehouse => appsettings.GetSection("PLSQLServerDatawareHouse").Value,
                _ => string.Empty,
            };
        }

        /// <summary>
        /// Get ConnectionString from Appsettings.json
        /// </summary>
        public string GetConnectionString
        {
            get => _connectionString;
        }

    }
}
