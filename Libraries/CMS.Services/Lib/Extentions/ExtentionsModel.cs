﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;

namespace CMS.Services.Lib.Extentions
{
     
    public class ReflectionPopulator<T>
    {
        /// <summary>
        /// Convert IDataReader to List<T>
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public virtual List<T> CreateList(IDataReader reader)
        {
            var results = new List<T>();
            _ = typeof(T).GetProperties();

            while (reader.Read())
            {
                var item = Activator.CreateInstance<T>();
                foreach (var property in typeof(T).GetProperties())
                {
                    if (!reader.IsDBNull(reader.GetOrdinal(property.Name)))
                    {
                        Type convertTo = Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType;
                        property.SetValue(item, Convert.ChangeType(reader[property.Name], convertTo), null);
                    }
                }
                results.Add(item);
            }
            return results;
        }
    }

    public static class ExtentionsModel
    {
        /// <summary>
        /// Check datatable is empty
        /// </summary>
        /// <param name="table">DataTable</param>
        /// <returns></returns>
        public static bool IsEmpty(this DataTable table) => (table == null || table.Rows.Count <= 0) ? true : false;

        /// <summary>
        /// Check dataset is empty
        /// </summary>
        /// <param name="dataSet"></param>
        /// <returns></returns>
        public static bool IsEmpty(this DataSet dataSet) => (dataSet == null || dataSet.Tables.Count <= 0) ? true : false;

        /// <summary>
        /// Add item list in datarow
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="row"></param>
        /// <param name="columnsName"></param>
        /// <returns></returns>
        private static T GetObject<T>(DataRow row, List<string> columnsName) where T : new()
        {
            T obj = new T();
            try
            {
                string columnname = "";
                string value = "";
                PropertyInfo[] Properties;
                Properties = typeof(T).GetProperties();
                foreach (PropertyInfo objProperty in Properties)
                {
                    columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
                    if (!string.IsNullOrEmpty(columnname))
                    {
                        value = row[columnname].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                            {
                                value = row[columnname].ToString().Replace("$", "").Replace(",", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
                            }
                            else
                            {
                                value = row[columnname].ToString().Replace("%", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
                            }
                        }
                    }
                }
                return obj;
            }
            catch
            {
                return obj;
            }
        }

        /// <summary>
        /// Convert DataTable to List define class
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="datatable"></param>
        /// <returns></returns>
        public static List<T> ToList<T>(this DataTable datatable) where T : new()
        {
            List<T> Temp = new List<T>();
            try
            {
                List<string> columnsNames = new List<string>();
                foreach (DataColumn DataColumn in datatable.Columns)
                    columnsNames.Add(DataColumn.ColumnName);
                Temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => GetObject<T>(row, columnsNames));
                return Temp;
            }
            catch
            {
                return Temp;
            }

        }

        /// <summary>
        /// IDataReader to Json 
        /// </summary>
        /// <param name="rdr">IDataReader</param>
        /// <returns> string </returns>
        public static string ConvertToJson(this IDataReader rdr)
        {
            StringBuilder sb = new StringBuilder();
            System.IO.StringWriter sw = new System.IO.StringWriter(sb);
            JsonWriter jsonWriter = new JsonTextWriter(sw);
           
            jsonWriter.WriteStartArray();
            while (rdr.Read())
            {
                jsonWriter.WriteStartObject();
                int fields = rdr.FieldCount;
                for (int i = 0; i < fields; i++)
                {
                    jsonWriter.WritePropertyName(rdr.GetName(i));
                    jsonWriter.WriteValue(rdr[i]);
                }
                jsonWriter.WriteEndObject();
            }
            jsonWriter.WriteEndArray();
            return sw.ToString();
            
        }

        /// <summary>
        /// Convert DataReader To List By Model Class
        /// </summary>
        /// <typeparam name="T">Class Name</typeparam>
        /// <param name="dr">IDataReader</param>
        /// <returns></returns>
        public static List<T> MapToList<T>(this IDataReader dr)
        {
            List<T> list = new List<T>();
             
            while (dr.Read())
            {
                var obj = System.Activator.CreateInstance<T>();
                foreach (PropertyInfo prop in obj.GetType().GetProperties())
                {
                    if (!object.Equals(dr[prop.Name], System.DBNull.Value))
                    {
                        Type convertTo = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                        prop.SetValue(obj, Convert.ChangeType(dr[prop.Name], convertTo), null);
                        //prop.SetValue(obj, dr[prop.Name], null);
                    }
                }
                list.Add(obj);
            }
            return list;
        }
    }

}
