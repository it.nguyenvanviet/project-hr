﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CMS.Services.Lib.Crypt
{
    public class CryptServices
    {
        #region Password
        public static string HashMD5(String str)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            // Convert the input string to a byte array and compute the hash.
            byte[] b = System.Text.Encoding.UTF8.GetBytes(str);
            b = md5.ComputeHash(b);
            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder s = new StringBuilder();
            // Loop through each byte of the hashed data
            // and format each one as a hexadecimal string.
            foreach (byte by in b)
            {
                s.Append(by.ToString("x2").ToLower());
            }
            // Return the hexadecimal string.
            return s.ToString();
        }

        // Verify a hash against a string.
        public static bool verifyMd5Hash(string input, string hash)
        {
            // Hash the input.
            string hashOfInput = HashMD5(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string HashSHA1(string value)
        {
            //var sha1 = System.Security.Cryptography.SHA1.Create();
            SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
            var inputBytes = Encoding.UTF8.GetBytes(value);
            //var inputBytes = Encoding.ASCII.GetBytes(value);
            var hash = sha1.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2").ToLower());
            }
            return sb.ToString();
        }

        #endregion Password

        #region Base64

        /// <summary>
        /// The method create a Base64 encoded string from a normal string.
        /// </summary>
        /// <param name="toEncode">The String containing the characters to encode.</param>
        /// <returns>The Base64 encoded string.</returns>
        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.Unicode.GetBytes(toEncode);

            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;
        }

        /// <summary>
        /// The method to Decode your Base64 strings.
        /// </summary>
        /// <param name="encodedData">The String containing the characters to decode.</param>
        /// <returns>A String containing the results of decoding the specified sequence of bytes.</returns>
        public static string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(encodedData);

            string returnValue = System.Text.Encoding.Unicode.GetString(encodedDataAsBytes);

            return returnValue;
        }


        public void EncodeWithString(string inputFileName)
        {
            System.IO.FileStream inFile;
            byte[] binaryData;
            try
            {
                inFile = new System.IO.FileStream(inputFileName,
                                          System.IO.FileMode.Open,
                                          System.IO.FileAccess.Read);
                binaryData = new Byte[inFile.Length];
                long bytesRead = inFile.Read(binaryData, 0,
                                     (int)inFile.Length);
                inFile.Close();
            }
            catch (Exception ex)
            {
                // Error creating stream or reading from it.
                //System.Console.WriteLine("{0}", exp.Message);
                string error = ex.Message;
                return;
            }

            // Convert the binary input into Base64 UUEncoded output.
            string base64String;
            try
            {
                base64String =
                  System.Convert.ToBase64String(binaryData,
                                         0,
                                         binaryData.Length);
            }
            catch (System.ArgumentNullException)
            {
                System.Console.WriteLine("Binary data array is null.");
                return;
            }

            // Write the UUEncoded version to the output file.
            //System.IO.StreamWriter outFile;
            //try
            //{
            //    outFile = new System.IO.StreamWriter(outputFileName,
            //                         false,
            //                         System.Text.Encoding.ASCII);
            //    outFile.Write(base64String);
            //    outFile.Close();
            //}
            //catch (System.Exception exp)
            //{
            //    // Error creating stream or writing to it.
            //    System.Console.WriteLine("{0}", exp.Message);
            //}
        }

        public static string EncodeFileToBase64(string fileName)
        {
            FileStream fs = new FileStream(fileName,
                                   FileMode.Open,
                                   FileAccess.Read);
            byte[] filebytes = new byte[fs.Length];
            fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
            string encodedData =
                Convert.ToBase64String(filebytes);
            //Base64FormattingOptions.InsertLineBreaks);
            return encodedData;
        }

        public static void DecodeFileFromBase64(string encodeData, string fileName)
        {

            byte[] filebytes = Convert.FromBase64String(encodeData);
            FileStream fs = new FileStream(fileName,
                                           FileMode.CreateNew,
                                           FileAccess.Write,
                                           FileShare.None);
            fs.Write(filebytes, 0, filebytes.Length);
            fs.Close();

            byte[] file = System.Convert.FromBase64String(encodeData);
            //File.WriteAllBytes(fileName + ".zip", file);
            File.WriteAllBytes(fileName, file);
        }

        //http://msdn.microsoft.com/en-us/library/system.security.cryptography.frombase64transform.aspx

        public static void DecodeFromFile(string inFileName, string outFileName)
        {
            using (FromBase64Transform myTransform = new FromBase64Transform(FromBase64TransformMode.IgnoreWhiteSpaces))
            {

                byte[] myOutputBytes = new byte[myTransform.OutputBlockSize];

                //Open the input and output files. 
                using (FileStream myInputFile = new FileStream(inFileName, FileMode.Open, FileAccess.Read))
                {
                    using (FileStream myOutputFile = new FileStream(outFileName, FileMode.Create, FileAccess.Write))
                    {

                        //Retrieve the file contents into a byte array.  
                        byte[] myInputBytes = new byte[myInputFile.Length];
                        myInputFile.Read(myInputBytes, 0, myInputBytes.Length);

                        //Transform the data in chunks the size of InputBlockSize.  
                        int i = 0;
                        while (myInputBytes.Length - i > 4/*myTransform.InputBlockSize*/)
                        {
                            int bytesWritten = myTransform.TransformBlock(myInputBytes, i, 4/*myTransform.InputBlockSize*/, myOutputBytes, 0);
                            i += 4/*myTransform.InputBlockSize*/;
                            myOutputFile.Write(myOutputBytes, 0, bytesWritten);
                        }

                        //Transform the final block of data.
                        myOutputBytes = myTransform.TransformFinalBlock(myInputBytes, i, myInputBytes.Length - i);
                        myOutputFile.Write(myOutputBytes, 0, myOutputBytes.Length);

                        //Free up any used resources.
                        myTransform.Clear();
                    }
                }
            }

        }

        // Read in the specified source file and write out an encoded target file. 
        public static void EncodeFromFile(string sourceFile, string targetFile)
        {
            // Verify members.cs exists at the specified directory. 
            if (!File.Exists(sourceFile))
            {
                //Console.Write("Unable to locate source file located at ");
                //Console.WriteLine(sourceFile + ".");
                //Console.Write("Please correct the path and run the ");
                //Console.WriteLine("sample again.");
                return;
            }

            // Retrieve the input and output file streams. 
            using (FileStream inputFileStream =
                new FileStream(sourceFile, FileMode.Open, FileAccess.Read))
            {
                using (FileStream outputFileStream =
                    new FileStream(targetFile, FileMode.Create, FileAccess.Write))
                {

                    // Create a new ToBase64Transform object to convert to base 64.
                    ToBase64Transform base64Transform = new ToBase64Transform();

                    // Create a new byte array with the size of the output block size. 
                    byte[] outputBytes = new byte[base64Transform.OutputBlockSize];

                    // Retrieve the file contents into a byte array. 
                    byte[] inputBytes = new byte[inputFileStream.Length];
                    inputFileStream.Read(inputBytes, 0, inputBytes.Length);

                    // Verify that multiple blocks can not be transformed. 
                    if (!base64Transform.CanTransformMultipleBlocks)
                    {
                        // Initializie the offset size. 
                        int inputOffset = 0;

                        // Iterate through inputBytes transforming by blockSize. 
                        int inputBlockSize = base64Transform.InputBlockSize;

                        while (inputBytes.Length - inputOffset > inputBlockSize)
                        {
                            base64Transform.TransformBlock(
                                inputBytes,
                                inputOffset,
                                inputBytes.Length - inputOffset,
                                outputBytes,
                                0);

                            inputOffset += base64Transform.InputBlockSize;
                            outputFileStream.Write(
                                outputBytes,
                                0,
                                base64Transform.OutputBlockSize);
                        }

                        // Transform the final block of data.
                        outputBytes = base64Transform.TransformFinalBlock(
                            inputBytes,
                            inputOffset,
                            inputBytes.Length - inputOffset);

                        outputFileStream.Write(outputBytes, 0, outputBytes.Length);
                        //Console.WriteLine("Created encoded file at " + targetFile);
                    }

                    // Determine if the current transform can be reused. 
                    if (!base64Transform.CanReuseTransform)
                    {
                        // Free up any used resources.
                        base64Transform.Clear();
                    }
                }
            }

        }

        #endregion Base64

        #region AES

        private static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an Aes object 
            // with the specified key and IV. 
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                //aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream. 
            return encrypted;

        }

        private static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold 
            // the decrypted text. 
            string plaintext = null;

            // Create an Aes object 
            // with the specified key and IV. 
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                //aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }

        public static string EncryptText(string input, string password)
        {
            // Get the bytes of the string
            byte[] bytesToBeEncrypted = Encoding.UTF8.GetBytes(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            // Hash the password with SHA256
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] bytesEncrypted = EncryptStringToBytes_Aes(input, passwordBytes, passwordBytes);

            string result = Convert.ToBase64String(bytesEncrypted);

            return result;
        }
        public static string DecryptText(string input, string password)
        {
            // Get the bytes of the string
            byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            //byte[] bytesDecrypted = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            string result = DecryptStringFromBytes_Aes(bytesToBeDecrypted, passwordBytes, passwordBytes);

            return result;
        }

        #endregion AES 

        #region AES1

        //http://www.c-sharpcorner.com/UploadFile/a85b23/text-encrypt-and-decrypt-with-a-specified-key/

        private static byte[] _salt = Encoding.ASCII.GetBytes("uxyotp873@$%");

        public static string EncryptStringAES(string plainText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(plainText))
                throw new ArgumentNullException("plainText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            string outStr = null;                 // Encrypted string to return  
            RijndaelManaged aesAlg = null;        // RijndaelManaged object used to encrypt the data.  

            try
            {
                // generate the key from the shared secret and the salt  
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

                // Create a RijndaelManaged object  
                aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);

                // Create a decryptor to perform the stream transform.  
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.  
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    // prepend the IV  
                    msEncrypt.Write(BitConverter.GetBytes(aesAlg.IV.Length), 0, sizeof(int));
                    msEncrypt.Write(aesAlg.IV, 0, aesAlg.IV.Length);
                    using (CryptoStream csEncrypt =
                       new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.  
                            swEncrypt.Write(plainText);
                        }
                    }
                    outStr = Convert.ToBase64String(msEncrypt.ToArray());
                }
            }
            catch { }
            finally
            {
                // Clear the RijndaelManaged object.  
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            // Return the encrypted bytes from the memory stream.  
            return outStr;
        }

        public static string DecryptStringAES(string cipherText, string sharedSecret)
        {
            if (string.IsNullOrEmpty(cipherText))
                throw new ArgumentNullException("cipherText");
            if (string.IsNullOrEmpty(sharedSecret))
                throw new ArgumentNullException("sharedSecret");

            // Declare the RijndaelManaged object  
            // used to decrypt the data.  
            RijndaelManaged aesAlg = null;

            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext = null;

            try
            {
                // generate the key from the shared secret and the salt  
                Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(sharedSecret, _salt);

                // Create the streams used for decryption.  
                byte[] bytes = Convert.FromBase64String(cipherText);
                using (MemoryStream msDecrypt = new MemoryStream(bytes))
                {
                    // Create a RijndaelManaged object  
                    // with the specified key and IV.  
                    aesAlg = new RijndaelManaged();
                    aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                    // Get the initialization vector from the encrypted stream  
                    aesAlg.IV = ReadByteArray(msDecrypt);
                    // Create a decrytor to perform the stream transform.  
                    ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                    using (CryptoStream csDecrypt =
                        new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))

                            // Read the decrypted bytes from the decrypting stream  
                            // and place them in a string.  
                            plaintext = srDecrypt.ReadToEnd();
                    }
                }
            }
            catch { }
            finally
            {
                // Clear the RijndaelManaged object.  
                if (aesAlg != null)
                    aesAlg.Clear();
            }

            return plaintext;
        }
        private static byte[] ReadByteArray(Stream s)
        {
            byte[] rawLength = new byte[sizeof(int)];
            if (s.Read(rawLength, 0, rawLength.Length) != rawLength.Length)
            {
                throw new SystemException("Stream did not contain properly formatted byte array");
            }

            byte[] buffer = new byte[BitConverter.ToInt32(rawLength, 0)];
            if (s.Read(buffer, 0, buffer.Length) != buffer.Length)
            {
                throw new SystemException("Did not read byte array properly");
            }

            return buffer;
        }


        #endregion AES1

    }
}
