﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Services.Lib.Configuration
{ 
    public class ConfigurationConfigurationProvider : ConfigurationProvider
    {
        public ConfigurationConfigurationProvider(IConfiguration source)
            : this(null, source)
        {

        }

        public ConfigurationConfigurationProvider(string prefix, IConfiguration source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (prefix == null)
            {
                foreach (var pair in source.GetChildren())
                {
                    Data.Add(pair.Key, pair.Value);
                }
            }
            else
            { 
                foreach (var pair in source.GetChildren())
                {
                    if (pair.Key.StartsWith(prefix, StringComparison.OrdinalIgnoreCase))
                    {
                        Data.Add(pair.Key.Substring(prefix.Length), pair.Value);
                    }
                }
            }
        }
    }
     
    
}
