﻿using System.Threading.Tasks;

namespace CMS.Services.Lib.Email
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message, string UsernameEmail, string UsernamePassword, string host, int port, bool isssl);
    }
}
