﻿using CMS.Core.Domain.Account;
using Microsoft.Extensions.Options;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;



namespace CMS.Services.Lib.Email
{
    public class EmailSender : IEmailSender
    {
        public EmailSender(IOptions<AuthMessageSenderOptions> optionsAccessor)
        {
            _emailSettings = optionsAccessor.Value;
        }
        public AuthMessageSenderOptions _emailSettings { get; } //set only via Secret Manager

        public Task SendEmailAsync(string email, string subject, string message, string UsernameEmail, string UsernamePassword, string host, int port, bool isssl)
        {
            return Execute(_emailSettings.UsernameEmail, subject, message, email, UsernameEmail, UsernamePassword, host, port, isssl);
        }

        public async Task Execute(string apiKey, string subject, string message, string email, string UsernameEmail, string UsernamePassword, string host, int port, bool isssl)
        {
            try
            {
                string toEmail = string.IsNullOrEmpty(email)
                                 ? UsernameEmail
                                 : email;
                MailMessage mail = new MailMessage()
                {
                    From = new MailAddress(UsernameEmail, "Administrator")
                };
                mail.To.Add(new MailAddress(toEmail));
                //mail.CC.Add(new MailAddress(_emailSettings.CcEmail));

                mail.Subject = "Hệ thống - " + subject;
                mail.Body = message;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                using (SmtpClient smtp = new SmtpClient(host, port))
                {
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(UsernameEmail, UsernamePassword);
                    smtp.EnableSsl = isssl;

                    await smtp.SendMailAsync(mail);
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
    }
}
