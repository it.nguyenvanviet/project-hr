﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Services.Lib.Extentions;
using Dapper;
//using Oracle.ManagedDataAccess.Client;

namespace CMS.Services.Lib.Dapper
{
    public enum DapperDatabaseType
    {
        SQLServer = 1,
        PLSQLServer = 2
    }

    public class SQLDapperHelper : DapperHelper
    {
        private static readonly SQLDapperHelper _instance = null;
        public static SQLDapperHelper Instance => _instance ?? (new SQLDapperHelper());
        public SQLDapperHelper() : base(GetDatabaseType.MSSQLServerInside)
        {

        }
    }
    public class DapperHelper : IDisposable
    {
        #region Variable

        private readonly IDbConnection _dbConnection;
        private readonly ConfigurationManagerHelper configurationManager;

        #endregion

        #region Constructor

        public DapperHelper(GetDatabaseType dbType)
        {
            configurationManager = new ConfigurationManagerHelper(dbType);
            switch (dbType)
            {
                case GetDatabaseType.MSSQLServerInside:
                    this._dbConnection = new SqlConnection(configurationManager.GetConnectionString);
                    break;
                case GetDatabaseType.Job_Connection:
                    this._dbConnection = new SqlConnection(configurationManager.GetConnectionString);
                    break;
                case GetDatabaseType.PLSQLServerDatawarehouse:
                    break;
                default:
                    this._dbConnection = null;
                    break;
            }
        }


        public DapperHelper(string connectionString, DapperDatabaseType type)
        {
            switch (type)
            {
                case DapperDatabaseType.SQLServer:
                    this._dbConnection = new SqlConnection(connectionString);
                    break;
                case DapperDatabaseType.PLSQLServer:
                    break;
                //case DapperDatabaseType.PLSQLServer:
                //    this._dbConnection = new OracleConnection(connectionString);
                //    break;
                default: this._dbConnection = null; break;
            }
        }
        #endregion

        #region Open & Close

        private void Open()
        {
            if (this._dbConnection.State != ConnectionState.Open)
                this._dbConnection.Open();
        }


        public void Close()
        {
            this.Dispose();
        }

        #endregion

        #region ExecuteQuery & ExecuteQueryAsync
        /// <summary>
        /// ExecuteQuery
        /// </summary>
        /// <param name="commandText">Query String / Stored Procedure</param>
        /// <param name="commandType">CommandType.Text/CommandType.StoredProcedure</param>
        /// <param name="parameters">SqlDynamicParameter/OracleDynamicParameter</param>
        /// <returns></returns>
        public object ExecuteQuery(string commandText, CommandType commandType = CommandType.StoredProcedure, object parameters = null)
        {
            try
            {
                this.Open();
                return this._dbConnection.Query(commandText, parameters, transaction: null, buffered: true,
                    commandTimeout: 120, commandType: commandType);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                this.Dispose();
            }
        }

        /// <summary>
        /// ExecuteQuery
        /// </summary>
        /// <param name="commandText">Query String / Stored Procedure</param>
        /// <param name="commandType">CommandType.Text/CommandType.StoredProcedure</param>
        /// <param name="parameters">SqlDynamicParameter/OracleDynamicParameter</param>
        /// <returns></returns>
        public async void ExecuteScalar(string commandText, CommandType commandType = CommandType.StoredProcedure, object parameters = null)
        {
            try
            {
                this.Open();
                await this._dbConnection.ExecuteScalarAsync(commandText, parameters, transaction: null,
                    commandTimeout: 120, commandType: commandType);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                this.Dispose();
            }
        }

        public object ExecuteTransaction(string commandText, CommandType commandType = CommandType.StoredProcedure, object parameters = null)
        {
            var data = new object();
            using (var trans = _dbConnection.BeginTransaction())
            {
                try
                {
                    this.Open();
                    data = this._dbConnection.Execute(commandText, parameters, transaction: trans, commandType: commandType);
                    trans.Commit();
                }
                catch (Exception exception)
                {
                    trans.Rollback();
                    throw exception;
                }
                finally
                {
                    trans.Dispose();
                    this.Dispose();
                }
            }
            return data;
        }

        /// <summary>
        /// ExecuteQueryAsync
        /// </summary>
        /// <param name="commandText">Query String / Stored Procedure</param>
        /// <param name="commandType">CommandType.Text/CommandType.StoredProcedure</param>
        /// <param name="parameters">SqlDynamicParameter/OracleDynamicParameter</param>
        /// <para name="continueOnCaptureContext">ConfigureAwait</para>
        /// <returns></returns>
        public async Task<object> ExecuteQueryAsync(string commandText, CommandType commandType = CommandType.StoredProcedure, object parameters = null, bool continueOnCaptureContext = false)
        {
            try
            {
                this.Open();
                return await this._dbConnection.QueryAsync(commandText, parameters, null, 120, commandType).ConfigureAwait(continueOnCaptureContext);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                this.Dispose();
            }
        }


        /// <summary>
        /// ExecuteQuery To Class
        /// </summary>
        /// <typeparam name="T">Class define</typeparam>
        /// <param name="commandType">Query/StoredProcedure Name</param>
        /// <param name="commandText">CommandType.Text/CommandType.StoredProcedure</param>
        /// <param name="parameters">SqlDynamicParameters/OracleDynamicParameters</param>
        /// <returns></returns>
        public IList<T> ExecuteQuery<T>(string commandText, CommandType commandType = CommandType.StoredProcedure, object parameters = null)
        {
            try
            {
                this.Open();
                return this._dbConnection.Query<T>
                (
                    commandText, parameters, transaction: null, buffered: true, commandTimeout: 120, commandType: commandType
                ).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
        }
        public async Task<T> ExecuteScalar<T>(string commandText, object parameters, CommandType commandType = CommandType.StoredProcedure)
        {
            try
            {
                this.Open();
                return await this._dbConnection.ExecuteScalarAsync<T>
                (
                    commandText, parameters, transaction: null, commandTimeout: 120, commandType: commandType
                );
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
        }


        /// <summary>
        /// ExecuteQueryAsync To Class
        /// </summary>
        /// <typeparam name="T">Class define</typeparam>
        /// <param name="commandType">Query/StoredProcedure Name</param>
        /// <param name="commandText">CommandType.Text/CommandType.StoredProcedure</param>
        /// <param name="parameters">SqlDynamicParameters/OracleDynamicParameters</param>
        /// <param name="continueOnCaptureContext">ConfigureAwait(bool)</param>
        /// <returns></returns>
        public async Task<IList<T>> ExecuteQueryAsync<T>(string commandText, object parameters, CommandType commandType = CommandType.StoredProcedure, bool continueOnCaptureContext = false)
        {
            try
            {
                this.Open();
                var result = await this._dbConnection.QueryAsync<T>
                (
                    commandText, parameters, null, 120, commandType
                ).ConfigureAwait(continueOnCaptureContext);

                return result.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
        }

        /// <summary>
        /// ExecuteQuery:
        /// - Default StoredProcedure
        /// - Convert object to class
        /// </summary>
        /// <typeparam name="T">Class define</typeparam>
        /// <param name="commandText">Query/StoredProcedure Name</param>
        /// <param name="parameters">Parameters</param>
        /// <returns></returns>
        public IList<T> ExecuteQuery<T>(string commandText, object parameters)
        => this.ExecuteQuery<T>(commandText, CommandType.StoredProcedure, parameters);

        public async Task<T> ExecuteQueryFirstOrDefault<T>(string commandText, object parameters, CommandType commandType = CommandType.StoredProcedure)
        {
            try
            {
                this.Open();
                var result = await this._dbConnection.QueryFirstOrDefaultAsync<T>
                (
                    commandText, parameters, null, 120, commandType
                );
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
        }


        #endregion

        #region ExecuteMultiQuery & ExecuteMultiQueryAsync
        /// <summary>
        /// ExecuteMultiQuery return multi output
        /// </summary>
        /// <param name="commandText">Query/StoredProcedure</param>
        /// <param name="commandType">CommandType.Text/CommandType.StoredProcedure</param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private SqlMapper.GridReader ExecuteMultiQuery(string commandText, CommandType commandType, object parameters = null)
        {
            try
            {
                this.Open();
                return this._dbConnection.QueryMultiple(commandText, parameters, transaction: null, commandTimeout: null, commandType: commandType);
            }
            catch (Exception ex)
            {
                this.Close();
                throw ex;
            }
        }
        public IEnumerable<T1> QueryMultipleMapping<T1, T2>(string commandText, Func<T1, T2, T1> func, string splitOn, object parameters, CommandType commandType = CommandType.StoredProcedure)
        {
            try
            {
                this.Open();
                return this._dbConnection.Query<T1, T2, T1>(commandText, func, parameters, null, true, splitOn: splitOn, null, commandType).Distinct();
            }
            catch (Exception ex)
            {
                this.Close();
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandText">Query/StoredProcedure</param>
        /// <param name="commandType">CommandType.Text/CommandType.StoredProcedure</param>
        /// <param name="parameters"></param>
        /// <param name="continueOnCapturedContext">ConfigureAwait(bool)</param>
        /// <returns></returns>
        private async Task<SqlMapper.GridReader> ExecuteMultiQueryAsync(string commandText, CommandType commandType, object parameters = null, bool continueOnCapturedContext = false)
        {
            try
            {
                this.Open();
                return await this._dbConnection.QueryMultipleAsync(commandText, parameters, transaction: null, commandTimeout: null, commandType: commandType).ConfigureAwait(continueOnCapturedContext);
            }
            catch (Exception ex)
            {
                this.Close();
                throw ex;
            }
        }

        /// <summary>
        /// ExecuteMultiQuery return multi output
        /// </summary>
        /// <param name="commandText">Query/StoredProcedure</param>
        /// <param name="commandType">CommandType.Text/CommandType.StoredProcedure</param>
        /// <param name="parameters"></param>
        /// <param name="parameterOutput">Count number output</param>
        /// <returns></returns>
        public List<object> ExecuteMultiQuery(string commandText, CommandType commandType, object parameters, int parameterOutput)
        {
            try
            {
                List<object> list = new List<object>();
                var grid = this.ExecuteMultiQuery(commandText, commandType, parameters);
                for (int i = 0; i < parameterOutput; i++)
                {
                    var result = grid.Read<object>().ToList();
                    list.Add(result);
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
        }

        /// <summary>
        /// ExecuteMultiQueryAsync
        /// </summary>
        /// <param name="commandText">Query/StoredProcedure</param>
        /// <param name="commandType">CommandType.Text/CommandType.StoredProcedure</param>
        /// <param name="parameters"></param>
        /// <param name="parameterOutput"></param>
        /// <param name="continueOnCapturedContext">ConfigureAwait(bool)</param>
        /// <returns></returns>
        public async Task<List<object>> ExecuteMultiQueryAsync(string commandText, CommandType commandType, object parameters, int parameterOutput, bool continueOnCapturedContext = false)
        {
            try
            {
                List<object> list = new List<object>();
                var grid = await this.ExecuteMultiQueryAsync(commandText, commandType, parameters, continueOnCapturedContext);
                for (int i = 0; i < parameterOutput; i++)
                {
                    var result = grid.Read<object>().ToList();
                    list.Add(result);
                }
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
        }


        /// <summary>
        /// ExecuteMultiQuery return multi output v2
        /// </summary>
        /// <param name="commandText">Query/StoredProcedure</param>
        /// <param name="commandType">CommandType.Text/CommandType.StoredProcedure</param>
        /// <param name="parameters"></param>
        /// <param name="parameterOutput">Count number output</param>
        /// <returns></returns>
        public List<dynamic> ExecuteMultiQuery_MultiClass<T1, T2, T3, T4>(string commandText, object parameters, CommandType commandType = CommandType.StoredProcedure)
        {
            try
            {

                var grid = this.ExecuteMultiQuery(commandText, commandType, parameters);
                var rs = new List<dynamic>
                {
                    grid.Read<T1>().ToList()
                };
                try
                {
                    rs.Add(grid.Read<T2>().ToList());
                    rs.Add(grid.Read<T3>().ToList());
                    rs.Add(grid.Read<T4>().ToList());
                }
                catch { }
                return rs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.Dispose();
            }
        }


        /// <summary>
        /// ExecuteMultiQuery: User default StoredProcedure
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <param name="parameterOutput"></param>
        /// <returns></returns>
        public List<object> ExecuteMultiQuery(string commandText, object parameters, int parameterOutput)
        => this.ExecuteMultiQuery(commandText, CommandType.StoredProcedure, parameters, parameterOutput);

        /// <summary>
        /// ExecuteMultiQuery: User default StoredProcedure
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <param name="parameterOutput"></param>
        /// <param name="continueOnCapturedContext"></param>
        /// <returns></returns>
        public async Task<List<object>> ExecuteMultiQueryAsync(string commandText, object parameters, int parameterOutput, bool continueOnCapturedContext = false)
        => await this.ExecuteMultiQueryAsync(commandText, CommandType.StoredProcedure, parameters, parameterOutput, continueOnCapturedContext);


        #endregion

        #region ExecuteReader & ExecuteReaderAsync

        /// <summary>
        /// ExecuteReader
        /// </summary>
        /// <param name="commandText">Query/StoredProcedure</param>
        /// <param name="commandType">Text/StoredProcedure</param>
        /// <param name="parameters">Parameters</param>
        /// <param name="transaction">Transaction</param>
        /// <param name="commandTimeout">IDbCommand.CommandTimeout</param>
        /// <returns></returns>
        private IDataReader ExecuteReader(string commandText, CommandType commandType, object parameters = null, IDbTransaction transaction = null, int? commandTimeout = 90)
        {
            try
            {
                this.Open();
                return this._dbConnection.ExecuteReader(commandText, parameters, transaction, commandTimeout, commandType);
            }
            catch (Exception ex)
            {
                this.Close();
                throw ex;
            }

        }

        /// <summary>
        /// ExecuteReaderAsync
        /// </summary>
        /// <param name="commandText">Query/StoredProcedure</param>
        /// <param name="commandType">Text/StoredProcedure</param>
        /// <param name="parameters">Parameters</param>
        /// <param name="transaction">Transaction</param>
        /// <param name="commandTimeout">IDbCommand.CommandTimeout</param>
        /// <param name="continueOnCapturedContext"></param>
        /// <returns></returns>
        private async Task<IDataReader> ExecuteReaderAsync(string commandText, CommandType commandType, object parameters = null, IDbTransaction transaction = null, int? commandTimeout = 90, bool continueOnCapturedContext = false)
        {
            try
            {
                this.Open();
                return await this._dbConnection.ExecuteReaderAsync(commandText, parameters, transaction, commandTimeout, commandType).ConfigureAwait(continueOnCapturedContext);
            }
            catch (Exception ex)
            {
                this.Close();
                throw ex;
            }

        }

        /// <summary>
        /// ExecuteReader
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string commandText, CommandType commandType, object parameters = null)
            => this.ExecuteReader(commandText, commandType, parameters, null, 120);

        /// <summary>
        /// ExecuteReaderAsync
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <param name="continueOnCapturedContext"></param>
        /// <returns></returns>
        public async Task<IDataReader> ExecuteReaderAsync(string commandText, CommandType commandType, object parameters = null, bool continueOnCapturedContext = false)
            => await this.ExecuteReaderAsync(commandText, commandType, parameters, null, 120, continueOnCapturedContext);

        /// <summary>
        /// ExecuteReader to class
        /// </summary>
        /// <typeparam name="T">Class define</typeparam>
        /// <param name="commandText">Query/StoredProcedure</param>
        /// <param name="commandType">Text/StoredProcedure</param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public IList<T> ExecuteReader<T>(string commandText, CommandType commandType, object parameters)
        {
            IDataReader reader = null;
            try
            {
                reader = this.ExecuteReader(commandText, commandType, parameters, null, 120);
                ReflectionPopulator<T> reflectionPopulator = new ReflectionPopulator<T>();
                return reflectionPopulator.CreateList(reader);
            }
            catch (Exception ex)
            {
                this.Close();
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                this.Close();
            }
        }

        /// <summary>
        /// ExecuteReaderAsync convert to class
        /// </summary>
        /// <typeparam name="T">class define</typeparam>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <param name="continueOnCapturedContext"></param>
        /// <returns></returns>
        public async Task<IList<T>> ExecuteReaderAsync<T>(string commandText, CommandType commandType, object parameters, bool continueOnCapturedContext = false)
        {
            IDataReader reader = null;
            try
            {
                reader = await this.ExecuteReaderAsync(commandText, commandType, parameters, null, 120, continueOnCapturedContext);
                ReflectionPopulator<T> reflectionPopulator = new ReflectionPopulator<T>();
                return reflectionPopulator.CreateList(reader);
            }
            catch (Exception ex)
            {
                this.Close();
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                this.Close();
            }
        }

        /// <summary>
        /// ExecuteReader Convert Class - Default StoredProcedure
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public IList<T> ExecuteReader<T>(string commandText, object parameters)
            => this.ExecuteReader<T>(commandText, CommandType.StoredProcedure, parameters);

        /// <summary>
        /// ExecuteReaderAsync Convert Class - Default StoredProcedure
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public async Task<IList<T>> ExecuteReaderAsync<T>(string commandText, object parameters)
            => await this.ExecuteReaderAsync<T>(commandText, CommandType.StoredProcedure, parameters);

        /// <summary>
        /// ExecuteReader convert Json
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public string ExecuteReaderToJson(string commandText, CommandType commandType, object parameters)
        {
            IDataReader reader = null;
            try
            {
                reader = this.ExecuteReader(commandText, commandType, parameters, null, 90);
                return reader.ConvertToJson();
            }
            catch (Exception ex)
            {
                this.Close();
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                this.Close();
            }
        }

        /// <summary>
        /// ExecuteReader convert Json
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <param name="continueOnCapturedContext"></param>
        /// <returns></returns>
        public async Task<string> ExecuteReaderToJsonAsync(string commandText, CommandType commandType, object parameters, bool continueOnCapturedContext = false)
        {
            IDataReader reader = null;
            try
            {
                reader = await this.ExecuteReaderAsync(commandText, commandType, parameters, null, 90, continueOnCapturedContext);
                return reader.ConvertToJson();
            }
            catch (Exception ex)
            {
                this.Close();
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                this.Close();
            }
        }

        /// <summary>
        /// ExecuteReaderToJson: Default StoredProcedure
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public string ExecuteReaderToJson(string commandText, object parameters)
            => this.ExecuteReaderToJson(commandText, CommandType.StoredProcedure, parameters);

        /// <summary>
        /// ExecuteReaderToJsonAsync: Default StoredProcedure
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <param name="continueOnCapturedContext"></param>
        /// <returns></returns>
        public async Task<string> ExecuteReaderToJsonAsync(string commandText, object parameters, bool continueOnCapturedContext = false)
            => await this.ExecuteReaderToJsonAsync(commandText, CommandType.StoredProcedure, parameters, continueOnCapturedContext);

        #endregion

        #region ExecuteNonQuery & ExecuteNonQueryAsync
        /// <summary>
        /// ExecuteNonQuery
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string commandText, CommandType commandType, object parameters = null)
        {
            try
            {
                this.Open();
                return this._dbConnection.Execute(commandText, parameters, null, 120, commandType);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                this.Dispose();
            }
        }

        /// <summary>
        /// ExecuteNonQueryAsync
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <param name="continueOnCapturedContext"></param>
        /// <returns></returns>
        public async Task<int> ExecuteNonQueryAsync(string commandText, CommandType commandType, object parameters = null, bool continueOnCapturedContext = false)
        {
            try
            {
                this.Open();
                return await this._dbConnection.ExecuteAsync(commandText, parameters, null, 120, commandType).ConfigureAwait(continueOnCapturedContext);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                this.Dispose();
            }
        }

        /// <summary>
        /// ExecuteNonQuery: Default StoredProcedure
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns></returns> 

        /// <summary>
        /// ExecuteNonQueryAsync: default stored procedure
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <param name="continueOnCapturedContext"></param>
        /// <returns></returns> 
        #endregion

        public void GetColumnOfTable(string TableName, out IEnumerable<string> listfield)
        {
            try
            {
                this.Open();
                listfield = _dbConnection.Query<string>("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE  TABLE_NAME = '" + TableName + "'", commandType: CommandType.Text);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                this.Dispose();
            }
        }

        #region Insert, Update, Delete
        public bool Insert<T>(string tableName, T entity)
        {

            if (string.IsNullOrEmpty(tableName))
                throw new Exception($"Cannot save {tableName}. Database models should have [Table(\"tablename\")] attribute.");
            GetColumnOfTable(tableName.ToLower(), out var fields);
            var sql = $"INSERT INTO [{tableName}] (";
            foreach (var field in fields.Where(x => x.ToLower() != "id"))
            {
                sql += $"[{field}]" + ",";
            }
            sql = sql.TrimEnd(',');
            sql += ")";
            sql += " VALUES (";
            foreach (var field in fields.Where(x => x.ToLower() != "id"))
            {
                sql += "@" + field + ",";
            }
            sql = sql.TrimEnd(',');
            sql += ")";

            var affectedRows = _dbConnection.Execute(sql, entity, commandType: CommandType.Text);
            return affectedRows > 0;
        }

        #endregion


        #region Dispose
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing == true)
            {
                if (this._dbConnection.State == ConnectionState.Open || this._dbConnection.State == ConnectionState.Broken)
                {
                    this._dbConnection.Close();
                    //this._dbConnection.Dispose();
                }
            }
        }

        ~DapperHelper()
        {
            Dispose(false);
        }
        #endregion Dispose
    }
}
