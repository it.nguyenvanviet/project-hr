﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Services.Lib.Helper
{
    public class DatetimeHelper
    {
        /// <summary>
        /// UpdateDate
        /// </summary>
        /// <param name="date"></param>
        /// <returns>chuoi: dd/mm/yyy</returns>
        public static string FormatDate(DateTime? date)
        {
            //input: mm/dd/yyyy
            //output: dd/mm/yyy
            if (date.HasValue)
            {
                return String.Format("{0:dd/MM/yyyy}", date);
            }
            return string.Empty;
        }
        /// <summary>
        /// Chuyển chuổi định dạng dd/MM/yyyy thành DateTime
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static DateTime ConvertStringToDateTime(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                var array = input.Split('/');
                if (array != null && array.Length == 3)
                {
                    return new DateTime(int.Parse(array[2]), int.Parse(array[1]), int.Parse(array[0]));
                }
            }
            return DateTime.Now;
        }
        /// <summary>
        /// convert to datetime 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static DateTime? ConvertToDateTime(object obj)
        {
            if (obj != null)
            {
                DateTime.TryParse(obj.ToString(), out DateTime returnValue);
                return returnValue;
            }
            return null;
        }
        /// <summary>
        /// convert to date
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static DateTime? ConvertToDate(object obj)
        {
            if (obj != null)
            {
                DateTime? returnValue;
                try { returnValue = DateTime.Parse(obj.ToString()).Date; }
                catch { return null; }
                return returnValue;
            }
            return null;
        }

        public static string ConvertDateToFormatType(DateTime? date, string FormatType = "")
        {
            if (date.HasValue && !String.IsNullOrEmpty(FormatType))
            {
                return date.Value.ToString(FormatType);
            }
            return DateTime.Now.ToString("dd MMM, yyyy");
        }
    }
}
