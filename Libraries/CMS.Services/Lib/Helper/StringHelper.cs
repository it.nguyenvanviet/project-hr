﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Image = SixLabors.ImageSharp.Image;

namespace CMS.Services.Lib.Helper
{
    public struct StringHelper
    {
        public static string PasswordHash(string pass)
        { 
            // generate a 128-bit salt using a secure PRNG
            byte[] salt = new byte[128 / 8];
            //using (var rng = RandomNumberGenerator.Create())
            //{
            //    rng.GetBytes(salt);
            //}
            
            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: pass,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
            return hashed;
        }

        public static string CutString(string Fullstring, int length)
        {
            if (Fullstring.Length > length)
            {
                string tam = string.Format("{0}", Fullstring.Substring(0, length));
                int vitri = tam.LastIndexOf(" ");
                return string.Format("{0}...", tam.Substring(0, vitri));
            }

            return Fullstring;
        }
        public static string Seourl(string text)
        {
            for (int i = 33; i < 48; i++)
            {
                text = text.Replace(((char)i).ToString(), "");
            }

            for (int i = 58; i < 65; i++)
            {
                text = text.Replace(((char)i).ToString(), "");
            }

            for (int i = 91; i < 97; i++)
            {
                text = text.Replace(((char)i).ToString(), "");
            }

            for (int i = 123; i < 127; i++)
            {
                text = text.Replace(((char)i).ToString(), "");
            }

            text = text.ToLower();

            if (!string.IsNullOrEmpty(text))
            {
                if (text.Substring(text.Length - 1, 1) == " ")
                {
                    text = text.Remove(text.Length - 1);
                }
            }
            text = text.Replace(" ", "-");

            var regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string strFormD = text.Normalize(NormalizationForm.FormD);
            return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }
        public static string ToStringMoney(decimal number)
        {
            string s = number.ToString("#");
            string[] so = new string[] { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string[] hang = new string[] { "", "nghìn", "triệu", "tỷ" };
            int i, j, donvi, chuc, tram;
            string str = " ";
            bool booAm = false;
            decimal decS = 0;
            //Tung addnew
            try
            {
                decS = Convert.ToDecimal(s.ToString());
            }
            catch
            {
            }
            if (decS < 0)
            {
                decS = -decS;
                s = decS.ToString();
                booAm = true;
            }
            i = s.Length;
            if (i == 0)
                str = so[0] + str;
            else
            {
                j = 0;
                while (i > 0)
                {
                    donvi = Convert.ToInt32(s.Substring(i - 1, 1));
                    i--;
                    if (i > 0)
                        chuc = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        chuc = -1;
                    i--;
                    if (i > 0)
                        tram = Convert.ToInt32(s.Substring(i - 1, 1));
                    else
                        tram = -1;
                    i--;
                    if ((donvi > 0) || (chuc > 0) || (tram > 0) || (j == 3))
                        str = hang[j] + str;
                    j++;
                    if (j > 3) j = 1;
                    if ((donvi == 1) && (chuc > 1))
                        str = "một " + str;
                    else
                    {
                        if ((donvi == 5) && (chuc > 0))
                            str = "lăm " + str;
                        else if (donvi > 0)
                            str = so[donvi] + " " + str;
                    }
                    if (chuc < 0)
                        break;
                    else
                    {
                        if ((chuc == 0) && (donvi > 0)) str = "lẻ " + str;
                        if (chuc == 1) str = "mười " + str;
                        if (chuc > 1) str = so[chuc] + " mươi " + str;
                    }
                    if (tram < 0) break;
                    else
                    {
                        if ((tram > 0) || (chuc > 0) || (donvi > 0)) str = so[tram] + " trăm " + str;
                    }
                    str = " " + str;
                }
            }
            if (booAm) str = "Âm " + str;
            return str + "đồng chẵn";
        }
        public static int GenerateRandomInteger(int min = 0, int max = int.MaxValue)
        {
            var randomNumberBuffer = new byte[10];
            new RNGCryptoServiceProvider().GetBytes(randomNumberBuffer);
            return new Random(BitConverter.ToInt32(randomNumberBuffer, 0)).Next(min, max);
        }
    }
  
    public struct ImageHelper
    { 
        public class TextImageGenerator
        {
            private System.Drawing.Color TextColor { get; set; }
            private System.Drawing.Color BackgroundColor { get; set; }
            private Font Font { get; set; }
            private int Padding { get; set; } 

            public TextImageGenerator(System.Drawing.Color textColor, System.Drawing.Color backgroundColor, string font, int padding, int fontSize)
            {
                TextColor = textColor;
                BackgroundColor = backgroundColor;
                Font = new Font(font, fontSize, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel); ;
                Padding = padding; 
            }
            public TextImageGenerator()
            : this(System.Drawing.Color.Orange, System.Drawing.Color.LightGray, "Arial", 20, 20)
            { }
            public Bitmap CreateBitmap(string text)
            {
                // Create graphics for rendering 
                Graphics retBitmapGraphics = Graphics.FromImage(new Bitmap(1, 1));
                // measure needed width for the image
                var intWidth = (int)retBitmapGraphics.MeasureString(text, Font).Width;
                // measure needed height for the image
                var intHeight = (int)retBitmapGraphics.MeasureString(text, Font).Height;
                // Create the bitmap with the correct size and add padding
                Bitmap retBitmap = new Bitmap(intWidth + Padding, intHeight + Padding);
                // Add the colors to the new bitmap.
                retBitmapGraphics = Graphics.FromImage(retBitmap);
                // Set Background color
                retBitmapGraphics.Clear(BackgroundColor);
                retBitmapGraphics.SmoothingMode = SmoothingMode.AntiAlias;
                retBitmapGraphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                retBitmapGraphics.DrawString(text, Font, new SolidBrush(TextColor), Padding / 2, Padding / 2);
                // flush the changes
                retBitmapGraphics.Flush();

                return (retBitmap);
            }
            public string CreateBase64Image(string text, ImageFormat imageFormat)
            {
                var bitmap = CreateBitmap(text);
                var stream = new System.IO.MemoryStream();
                // save into stream
                bitmap.Save(stream, imageFormat);
                // convert to byte array
                var imageBytes = stream.ToArray();
                // convert to base64 string
                return "data:image/png;base64," + Convert.ToBase64String(imageBytes);
            }
            public void SaveAsJpg(string filename, string text)
            {
                var bitmap = CreateBitmap(text);
                bitmap.Save(filename, ImageFormat.Jpeg);
            }
            public void SaveAsPng(string filename, string text)
            {
                var bitmap = CreateBitmap(text);
                bitmap.Save(filename, ImageFormat.Png);
            }
            public void SaveAsGif(string filename, string text)
            {
                var bitmap = CreateBitmap(text);
                bitmap.Save(filename, ImageFormat.Gif);
            }
            public void SaveAsBmp(string filename, string text)
            {
                var bitmap = CreateBitmap(text);
                bitmap.Save(filename, ImageFormat.Bmp);
            }
        }
        public Bitmap CreateBitmapImage(string sImageText)
        {
            Bitmap objBmpImage = new Bitmap(1, 1);

            // Create the Font object for the image text drawing.
            Font objFont = new Font("Arial", 20, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);

            // Create a graphics object to measure the text's width and height.
            Graphics objGraphics = Graphics.FromImage(objBmpImage);

            // This is where the bitmap size is determined.
            var intWidth = (int)objGraphics.MeasureString(sImageText, objFont).Width;
            var intHeight = (int)objGraphics.MeasureString(sImageText, objFont).Height;

            int padding = 50;
            // Create the bmpImage again with the correct size for the text and font.
            objBmpImage = new Bitmap(objBmpImage, new System.Drawing.Size(intWidth + padding, intHeight + padding));

            // Add the colors to the new bitmap.
            objGraphics = Graphics.FromImage(objBmpImage);

            // Set Background color
            // objGraphics.Clear(Color.Transparent);
            objGraphics.Clear(System.Drawing.Color.White);
            objGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            //objGraphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            objGraphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            // objGraphics.DrawString(sImageText, objFont, new SolidBrush(Color.FromArgb(102, 102, 102)), 0, 0);
            objGraphics.DrawString(sImageText, objFont, new SolidBrush(System.Drawing.Color.LightGray), padding / 2, padding / 2);
            objGraphics.Flush();

            return (objBmpImage);
        }
        public static string TempImage(string url, string Name)
        { 
            var imageGenerator = new TextImageGenerator();
            if (!String.IsNullOrEmpty(url))
            { 
                return url; 
            }
            return imageGenerator.CreateBase64Image(Name, ImageFormat.Png);
        }
        public static void Image_resize(string input_Image_Path, string output_Image_Path, int width = 500, int height = 320)
        {
            using (Image<Rgba32> image = (Image<Rgba32>)Image.Load(input_Image_Path))
            {
                image.Mutate(x => x
                     .Resize(image.Width > width ? width : image.Width, image.Height > height ? height : image.Height));
                image.Save(output_Image_Path);
            }
        }
        public static string Image_CheckExist(string input_Image_Path,string webrooth)
        {
            var imagepath = "/Upload/avatar.png";
            string output_Image_Path = imagepath;
            if (input_Image_Path.Length > 0)
            {
                output_Image_Path = imagepath;
                var path = string.Concat(webrooth, input_Image_Path.Replace("/", "\\"));
                if (System.IO.File.Exists(path))
                {
                    output_Image_Path = input_Image_Path;
                } 
            }
            return output_Image_Path;
        }
        public static string Image_CheckExistAvatar(string input_Image_Path, string webrooth,string imagename)
        {
            string output_Image_Path = imagename;
            if (input_Image_Path.Length > 0)
            { 
                var path = string.Concat(webrooth, "\\upload\\min\\", input_Image_Path.Replace("/", "\\"));
                if (System.IO.File.Exists(path))
                {
                    output_Image_Path = string.Concat("/upload/min/",input_Image_Path);
                }
            }
            return output_Image_Path;
        }
    }
}
