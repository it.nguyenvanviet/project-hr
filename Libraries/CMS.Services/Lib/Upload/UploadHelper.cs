﻿using CMS.Services.Lib.Helper;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Net.Http.Headers;

namespace CMS.Services.Lib.Upload
{
    public static class UploadHelper
    {
        public static string UploadAvatar(string RootPath, IFormFile file,string name = "")
        { 
            
            var fileName = string.Empty;
            var fileName_resize = string.Empty;
            var fileCurrentName = string.Empty;
            string PathDB = string.Empty;
            string pathfolder = string.Empty;
            string pathfolder_imageresize = string.Empty;
            var newFileName = string.Empty;
             
            fileCurrentName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            //Assigning Unique Filename (Guid)
            var myUniqueFileName = Guid.NewGuid().ToString();
            //Getting file Extension
            var FileExtension = Path.GetExtension(fileCurrentName).ToLower();
            // concating  FileName + FileExtension
            newFileName = myUniqueFileName + "_" + FileExtension;
            // Combines two strings into a path.
            fileName =  Path.Combine(RootPath, "upload") + $@"\{newFileName}";
            fileName_resize = Path.Combine(RootPath, "upload\\min") + $@"\{newFileName}";
            // if you want to store path of folder in database
            PathDB = "/upload/" + newFileName;
            // create folder 
            pathfolder = Path.Combine(RootPath, "upload");
            pathfolder_imageresize = Path.Combine(RootPath, "upload\\min");
            if (!Directory.Exists(pathfolder))
                Directory.CreateDirectory(pathfolder);
            if (!Directory.Exists(pathfolder_imageresize))
                Directory.CreateDirectory(pathfolder_imageresize);
            using (FileStream fs = System.IO.File.Create(fileName))
            {
                file.CopyTo(fs);
                fs.Flush();
            }

            ImageHelper.Image_resize(fileName, fileName_resize, 140, 140);


            return PathDB;
        }


    }
}
