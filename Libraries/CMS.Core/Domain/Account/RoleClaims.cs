﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Account
{
    public class RoleClaims
    {
        public string ID { get; set; }
        public string RoleID { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
    }
}
