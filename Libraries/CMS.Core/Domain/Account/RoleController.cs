﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Account
{
    public class RoleController
    {
        public int ID { get; set; }
        public string RoleID { get; set; }
        public int ControllerID { get; set; }
        public int Status { get; set; }

    }
   
}
