﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CMS.Core.Domain.Account
{
    public class LoginModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        public string RequestPath { get; set; }
    }
    [DisplayName("login")]
    public class LoginModelApi
    {
        [Required]
        [SwaggerSchema("Tài khoản")]
        public string Username { get; set; }
        [Required]
        [SwaggerSchema("Mật khẩu")]
        public string Password { get; set; }
    }
    public class tokenmodel
    {
        public string token { get; set; }
        public LoginResult user { get; set; }
        public DateTime expired { get; set; }
    }
    public class StatusAccount
    {
        public enum RegisterResult
        {
            Fail = 0,
            Success = 1,
            Registed = 2,
            Locked = 3,
        }
        public enum LoginResult
        {
            WrongUserorPassword = 0,
            Success = 1,
        }
    }

    public class TokenRequest
    {
        public string Token { get; set; }
    }
}
