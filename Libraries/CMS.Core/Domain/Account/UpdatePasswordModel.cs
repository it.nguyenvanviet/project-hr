﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Account
{
    public class UpdatePasswordModel
    {
        public string UserName { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
