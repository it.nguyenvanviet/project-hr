﻿using CMS.Core.Domain.Employee;
using System;
using System.Collections.Generic;
using System.Text; 

namespace CMS.Core.Domain.Account
{
    public class LoginResult
    {
        public string UserID { get; set; }
        public string EmpID { get; set; }
        public string RoleID { get; set; }
        public string EmpName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string RoleName { get; set; }
        public string Avartar { get; set; }
        public string Avartar_Resize { get; set; }
        public int Experience { get; set; }

    }
}
