﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Account
{
    public class UserTokens
    {
        public string UserId { get; set; }
        public string LoginProvider { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
