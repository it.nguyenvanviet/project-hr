﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CMS.Core.Domain.Account
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Email không được để trống")]
        [EmailAddress(ErrorMessage = "The Email field is not a valid email address.")]
        [Display(Name = "Email")]
        [SwaggerSchema("Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password không được để trống")] 
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [SwaggerSchema("Mật khẩu")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "Password và xác nhận mật khẩu không giống nhau")]
        [SwaggerSchema("Mật khẩu xác nhận")]
        public string ConfirmPassword { get; set; }
    }
}
