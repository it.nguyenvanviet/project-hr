﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Account
{
    public class Roles
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string NormalizedName { get; set; }
        public string ConcurrencyStamp { get; set; }

    }
}
