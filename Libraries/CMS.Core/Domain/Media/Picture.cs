﻿using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Media
{
    public class Picture
    {
        [SwaggerSchema("id")]
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the picture mime type
        /// </summary>
        [SwaggerSchema("Multipurpose Internet Mail Extensions (Mime)")]
        public string MimeType { get; set; }

        /// <summary>
        /// Gets or sets the SEO friendly filename of the picture
        /// </summary>
        [SwaggerSchema("Tên hình không dấu")]
        public string SeoFilename { get; set; }

        /// <summary>
        /// Gets or sets the "alt" attribute for "img" HTML element. If empty, then a default rule will be used (e.g. product name)
        /// </summary>
        [SwaggerSchema("alt html ext: <img alt='hinh-abc'> ")]
        public string AltAttribute { get; set; }

        /// <summary>
        /// Gets or sets the "title" attribute for "img" HTML element. If empty, then a default rule will be used (e.g. product name)
        /// </summary>
        [SwaggerSchema("title hình ảnh ảnh")]
        public string TitleAttribute { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the picture is new
        /// </summary>
        [SwaggerSchema("tình trạng hình là mới nhất ?")]
        public bool IsNew { get; set; }

        /// <summary>
        /// Gets or sets the picture binary
        /// </summary>
        [SwaggerSchema("Binary hình")]
        public virtual PictureBinary PictureBinary { get; set; }

        /// <summary>
        /// Gets or sets the picture virtual path
        /// </summary>
        [SwaggerSchema("Đường dẫn ảo của hình")]
        public string VirtualPath { get; set; }
        /// <summary>
        /// Type Image
        /// 1: Avatar, 2 Blog, 3 Portfolio 
        /// </summary>
        [SwaggerSchema("Thể loại của hình ảnh ext: 1 Avatar, 2 Blog, 3 Portfolio ")]
        public int Type { get; set; }
        /// <summary>
        /// ID reference empid,blogid,portfolioid
        /// </summary>
        [SwaggerSchema("ID liên kết hình ảnh ext: empid, blogid, portfolioid")]
        public int ReferenceID { get; set; }
        /// <summary>
        /// Size : 1.thumb,2.normal, 3.large, 4.fullsize
        /// </summary>
        [SwaggerSchema("Size hình ảnh ext: 1.thumb,2.normal, 3.large, 4.fullsize")]
        public int SizeType { get; set; }
    }
 
}
