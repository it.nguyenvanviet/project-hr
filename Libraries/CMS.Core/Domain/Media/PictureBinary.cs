﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Media
{
    public class PictureBinary
    {
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the picture binary
        /// </summary>
        public byte[] BinaryData { get; set; }

        /// <summary>
        /// Gets or sets the picture identifier
        /// </summary>
        public int PictureId { get; set; }

        /// <summary>
        /// Gets or sets the picture
        /// </summary>
        public virtual Picture Picture { get; set; }
    }
}
