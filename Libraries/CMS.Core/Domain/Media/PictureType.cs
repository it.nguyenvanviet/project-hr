﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Media
{
    /// <summary>
    /// Represents a picture item type
    /// </summary>
    public enum PictureType
    {
        /// <summary>
        /// Entities (products, categories, manufacturers)
        /// </summary>
        Entity = 1,

        /// <summary>
        /// Avatar
        /// </summary>
        Avatar = 10
    }
}
