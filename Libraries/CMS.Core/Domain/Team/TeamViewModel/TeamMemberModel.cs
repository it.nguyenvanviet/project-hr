﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Team.TeamViewModel
{
    public class TeamMemberViewModel
    {
        public int ID { get; set; }
        public string Avartar { get; set; }
        public string Name { get; set; }
    }
    
}
