using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Team.TeamViewModel
{
    public class TeamSearchViewModel
    {
        public int ID { get;set; }
        public string Name{ get;set; }        
        public string Descriptions { get;set; }
        public int Status { get;set; }
        public int DepartID { get;set;  }        
        public string DepartmentName { get; set; }
        public List<TeamSearchModelMember> members { get;set; }
    }
    public class TeamSearchModelMember{
        public int TeamID { get; set; }
        public int EmpID { get;set; }
        public int MemberStatus { get;set; }
        public string MemberName { get;set; }
        public string Avartar { get;set; }
        public int TeamType { get; set; }
    }   
}
