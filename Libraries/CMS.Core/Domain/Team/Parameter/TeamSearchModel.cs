 using System.Collections.Generic;
namespace CMS.Core.Domain.Team.Parameter
{
    public class TeamSearchModel
    {
        public string TeamName{ get; set; }
        public int? EmpID {  get; set; }
        public int Status{  get; set; }
        public int? DepartID {  get; set; }
    }
}