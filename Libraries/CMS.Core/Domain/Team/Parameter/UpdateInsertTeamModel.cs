 using System.Collections.Generic;
 

namespace CMS.Core.Domain.Team.Parameter
{ 
    public class TeamInsertModel{
        public string TeamName{get;set; }
        public int LeaderEmpID{get;set; }
        public List<int> MemberEmpIDs{get;set;}
        public int Status{ get;set; }
        public string Description{ get; set; }
        public int DepartID { get; set; }
        public string CreateBy{ get;set; }
    }
    public class TeamUpdateModel{
        public int ID{ get;set; }
        public string TeamName{get;set; }
        public int LeaderEmpID{get;set; }
        public List<int> MemberEmpIDs{get;set;}
        public int Status{ get;set; }
        public string Description{ get; set; }
        public int DepartID { get; set; }
        public string UpdateBy{ get;set; }
    }
}