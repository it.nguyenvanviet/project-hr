﻿using CMS.Core.Domain.Employee;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Team
{
    public class Teams
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public int Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public List<TeamMember> Listmember { get; set; }
        public List<Specialism> Listspecialisms { get; set; }
    }
    
}
