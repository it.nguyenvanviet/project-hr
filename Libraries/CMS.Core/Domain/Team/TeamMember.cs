﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Team
{
    public class TeamMember
    {
        public long ID { get; set; }
        public int TeamID { get; set; }
        public int EmpID { get; set; }
        public int Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
