﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Clients
{
    public class ModulePermistion
    {
        [SwaggerSchema("Tên module/chức năng")]
        public string Name { get; set; }
        [SwaggerSchema("Tình trạng 0.hủy, 1.bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("Được phép Đọc")]
        public int IsRead { get; set; }
        [SwaggerSchema("Được phép Ghi")]
        public int IsWrite { get; set; }
        [SwaggerSchema("Được phép Xóa")]
        public int IsDelete { get; set; }
        [SwaggerSchema("Ngày tạo")]
        public DateTime CreateDate { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Ngày điều chỉnh")]
        public DateTime? UpdateDate { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
    }
}
