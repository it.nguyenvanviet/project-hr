﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Clients
{
    public class Client
    {
        [SwaggerSchema("id")]
        public int ID { get; set; }
        [SwaggerSchema("Họ")]
        public string FirstName { get; set; }
        [SwaggerSchema("Tên")]
        public string LastName { get; set; }
        [SwaggerSchema("Họ và tên")]
        public string FullName { get; set; }
        [SwaggerSchema("Email")]
        public string Email { get; set; }
        [SwaggerSchema("Hình đại diện")]
        public string Avatar { get; set; }
        [SwaggerSchema("Số điện thoại")]
        public string Phone { get; set; }
        [SwaggerSchema("Địa chỉ")]
        public string Address { get; set; }

        [SwaggerSchema("Ngày tạo")]
        public DateTime CreateDate { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Ngày điều chỉnh")]
        public DateTime? UpdateDate { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }

    }
}
