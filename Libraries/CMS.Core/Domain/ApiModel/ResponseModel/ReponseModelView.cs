﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.ApiModel.ResponseModel
{
    public class ReponseModelView
    {
        public int Status { get; set; }
        public string Error { get; set; }
        public object Result { get; set; }
        public long Elapsed { get; set; }

    }
    public class ResultApiModel
    {
        public string Message { get; set; }
        public object Result { get; set; }
    }
}
