﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Setting
{
    public class Setting
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public string CreateBy { get; set; }
        public DateTime ? CreateDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
        public string Contents { get; set; }
    }
}
