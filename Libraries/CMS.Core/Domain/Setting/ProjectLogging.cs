﻿using System;

namespace CMS.Core.Domain.Setting
{
    public class ProjectLogging
    {
        public int ID { get; set; }
        public DateTime? CreateDate { get; set; }
        public string Action { get; set; }
        public string UserID { get; set; }
        public string Data { get; set; }
    }
}
