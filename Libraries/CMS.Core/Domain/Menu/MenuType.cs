﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Menu
{
    public class MenuType
    {
        public int ID { get; set; }
        [SwaggerSchema("Loại Menu")]
        public string Name { get; set; }
        [SwaggerSchema("Tình trạng 0.hủy, 1 bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("Thể loại 0.hủy, 1 bình thường")]
        public int Type { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Ngày tạo")]
        public DateTime? CreateDate { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string ModifyBy { get; set; }
        [SwaggerSchema("Ngày điều chỉnh")]
        public DateTime? ModifyDate { get; set; }

    }
}
