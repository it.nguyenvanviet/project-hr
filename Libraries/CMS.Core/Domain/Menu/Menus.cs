﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
 

namespace CMS.Core.Domain.Menu
{
    public class Menus
    {
        [SwaggerSchema("ID menu")]
        public int ID { get; set; }
        [SwaggerSchema("ID cha")]
        public int ParentID { get; set; }
        [Display(Name = "Tên Menu")]
        [SwaggerSchema("Tên Menu")]
        public string Name { get; set; }
        [Display(Name = "Link menu")]
        [SwaggerSchema("link Menu")]
        public string Link { get; set; }
        [Display(Name = "Nội dung")]
        [SwaggerSchema("Nội dung")]
        public string Content { get; set; }
        [Display(Name = "Icon")]
        [SwaggerSchema("Icon Menu boostrap icon, theme icon")]
        public string IconClass { get; set; }
        [SwaggerSchema("Thứ tự")]
        public long Sort { get; set; }
        [Display(Name = "Loại Menu")]
        [SwaggerSchema("Loại menu")]
        public int Type { get; set; } 
        [Display(Name = "Trạng thái")]
        [SwaggerSchema("Trạng thái menu: 0.Không hoạt động, 1.Bình thường, 2.Tạm dựng, 3.Đóng")]
        public int Status { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Ngày tạo")]
        public DateTime? CreateDate { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string ModifyBy { get; set; }
        [SwaggerSchema("Ngày điều chỉnh")]
        public DateTime? ModifyDate { get; set; }
    } 
    public class MenuRoleParaUpdate
    {
        [SwaggerSchema("ID menu")]
        public int ID { get; set; }
        [SwaggerSchema("Danh sách quyền hạn")]
        public List<string> Listroleid { get; set; }
    }
    public class MenuViewModel
    {
        [SwaggerSchema("ID menu")]
        public int ID { get; set; }
        [SwaggerSchema("ID menu cha")]
        public int ParentID { get; set; }
        [SwaggerSchema("Tên menu")]
        public string Name { get; set; }
        [SwaggerSchema("Link menu")]
        public string Link { get; set; }
        [SwaggerSchema("Nội dung")]
        public string Content { get; set; }
        [SwaggerSchema("Icon Menu boostrap icon, theme icon")]
        public string IconClass { get; set; }
        [SwaggerSchema("Thứ tự")]
        public long Sort { get; set; }
        [SwaggerSchema("Loại menu Hr, Setting, Nhân viên")]
        public int Type { get; set; }
        [SwaggerSchema("Trạng thái menu: 0.Không hoạt động, 1.Bình thường, 2.Tạm dựng, 3.Đóng")]
        public int Status { get; set; }
    }
    public class RootMenu
    {
        public string List { get; set; }
        public List<ItemMenu> List_menu { get; set; }
    }
    public class ItemMenu
    {
        [SwaggerSchema("ID menu")]
        public int ID { get; set; }
        [SwaggerSchema("ID menu cha")]
        public int ParentID { get; set; }
        [SwaggerSchema("Số thứ tự")]
        public int Order { get; set; }
        [SwaggerSchema("Danh sách")]
        public List<ItemMenu> Children { get; set; }
    }
    
    public class MenuPosition
    {
        [SwaggerSchema("ID menu")]
        public int ID { get; set; }
        [SwaggerSchema("ID menu cha")]
        public int ParentID { get; set; }
        [SwaggerSchema("Tên menu")]
        public string Name { get; set; }
        [SwaggerSchema("Danh sách")]
        public List<MenuPosition> children { get; set; }
    }
}
