﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Menu
{
    public class MenuRole
    {
        public int ID { get; set; }
        [SwaggerSchema("ID quyền")]
        public string RoleID { get; set; }
        [SwaggerSchema("ID menu")]
        public int MenuID { get; set; }
        [SwaggerSchema("Tình trạng 0.hủy, 1 bình thường")]
        public int Status { get; set; } 
    }
    public class MenuRoleVM
    {
        public int MenuID { get; set; }
        [SwaggerSchema("Danh sách quyền")]
        public string ListRole { get; set; }
    }
}
