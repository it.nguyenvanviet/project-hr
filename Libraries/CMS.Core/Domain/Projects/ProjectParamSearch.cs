﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Projects
{
    public class ProjectParamSearch
    {
        public string Name { get; set; }
        public int? ProjectType { get; set; }
        public int? PriorityID { get; set; }
        public DateTime ? StartDate { get; set; }
        public DateTime ? EndDate { get; set; }
        

    }
}
