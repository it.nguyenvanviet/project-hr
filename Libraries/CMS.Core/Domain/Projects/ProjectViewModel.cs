﻿using CMS.Core.Domain.Team;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Projects
{
    public class ProjectViewModel
    {
        public int ID { get; set; } 
        public string Name { get; set; } 
        public string Descriptions { get; set; }   
        public int Status { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public ProjectTeamViewModel project_personnel { get; set; }
    }
    public class ProjectTeamViewModel
    {
        public int ProjectID { get; set; }
        public ProjectTeamMemberModel PM { get; set; }
        public List<ProjectTeamMemberModel> Teams { get; set; }
        //public string 
    }
    public class ProjectTeamMemberModel
    {
        public int EmpID { get; set; }
        public string Name { get; set; }
        public string Avatar { get; set; }
        public int PositionID { get; set; }
    }
}
