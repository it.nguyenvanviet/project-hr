﻿using CMS.Core.Domain.Team;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text; 
using CMS.Core.Domain.Clients;

namespace CMS.Core.Domain.Projects
{
    public class Projects
    {
        public int ID { get; set; }
        [Required(ErrorMessage ="Nhập tên dự án")]
        [Display(Name = "Tên dự án")]
        public string Name { get; set; }
        [Display(Name = "Mô tả")]
        public string Descriptions { get; set; }
        [Required(ErrorMessage = "Chọn ngày bắt đầu dự án")]
        [Display(Name = "Từ ngày")]
        public DateTime? StartDate { get; set; }
        [Display(Name = "Đến ngày")]
        [Required(ErrorMessage = "Chọn ngày kết thúc dự án")]
        public DateTime? EndDate { get; set; }
        [Display(Name = "Độ ưu tiên")]
        public int PriorityID { get; set; }
        [Display(Name = "Đánh giá")]
        public int Rate { get; set; }
        [Display(Name = "Loại dự án")]
        public int ProjectType { get; set; }
        [Display(Name = "Tiến độ")]
        public int Process { get; set; }
        [Display(Name = "Tình trạng")]
        public int Status { get; set; }
        [Display(Name = "Giá trị")]
        public decimal Cost { get; set; }
        [Display(Name = "Khách hàng")]
        public int ClientID { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }


        /// <summary>
        /// reference
        /// </summary>
        public IList<Teams> Teams { get; set; }
        public IList<Client> Clients { get; set; }
        public IList<ProjectType> ProjectTypes { get; set; }
    }
   
    
     
}
