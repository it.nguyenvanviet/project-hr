﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Security
{
    public class StandardPermissionProvider
    {
        public static readonly PermissionRecord AccessAdminPanel = new PermissionRecord { Name = "Access admin", SystemName = "AccessAdminPanel", Category = "Standard" };
        public static readonly PermissionRecord ManageSettings = new PermissionRecord { Name = "Admin. Manage Settings", SystemName = "ManageSettings", Category = "Configuration" };
    }
}
