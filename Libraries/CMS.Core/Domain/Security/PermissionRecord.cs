﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Security
{
    public class PermissionRecord
    {
        public int ID { get; set; }
        public string Name { get; set; }
 
        public string SystemName { get; set; }
 
        public string Category { get; set; }
    }
}
