﻿using CMS.Core.Domain.Account;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Security
{
    public class PermissionRecordRole
    {
        public int PermissionRecord_Id { get; set; }
        public string RoleId { get; set; }
        public virtual PermissionRecord PermissionRecord { get; set; }
        public virtual Roles Role { get; set; }
    }
}
