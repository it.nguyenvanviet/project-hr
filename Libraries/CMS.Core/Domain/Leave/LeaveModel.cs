﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Leave
{
    public class LeaveModel
    {
        public int ID { get; set; }
        [SwaggerSchema("Loại đơn nghỉ phép: 0.Chưa xử lý, 1.Đang xử lý, 2.Đã xử lý")]
        public int LeaveType { get; set; }
        [SwaggerSchema("Tình trạng đơn nghỉ phép: 0.Hủy, 1.Bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("ID Nhân viên")]
        public int EmpID { get; set; }
        [SwaggerSchema("Số ngày nghỉ ext: 0,5, 1, 2")]
        public float NoofDay { get; set; }
        [SwaggerSchema("Loại nghỉ phép: 0.một ngày,1.nhiều ngày")]
        public int LeaveTime { get; set; }
        [SwaggerSchema("Từ ngày")]
        public DateTime? FromDate { get; set; }
        [SwaggerSchema("Đến ngày")]
        public DateTime? ToDate { get; set; }
        [SwaggerSchema("Mô tả")]
        public string Description { get; set; }
        [SwaggerSchema("Người phê duyệt")]
        public string ApprovedBy { get; set; }
        [SwaggerSchema("Tình trạng phê duyệt: 0.chưa duyệt, 1. Đã duyệt, 2.Từ chối")]
        public int ApprovedStatus { get; set; }
        [SwaggerSchema("Ngày phê duyệt")]
        public DateTime? ApprovedDate { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Ngày tạo")]
        public DateTime? CreateDate { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
        [SwaggerSchema("Ngày điều chỉnh")]
        public DateTime? UpdateDate { get; set; }

     

    }
}
