﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Leave
{
    public class LeaveViewModel
    {
        public int ID { get; set; }
        [SwaggerSchema("Loại đơn nghỉ phép: 0.Chưa xử lý, 1.Đang xử lý, 2.Đã xử lý")]
        public int LeaveType { get; set; }
        [SwaggerSchema("Hình đại diện")]
        public string url_avatar { get; set; }
        [SwaggerSchema("Tên nhân viên")]
        public string Name { get; set; }
        [SwaggerSchema("Số điện thoại")]
        public string Phone { get; set; }
        [SwaggerSchema("Email")]
        public string Email { get; set; }
        [SwaggerSchema("Tên loại đơn")]
        public string TypeName { get; set; }
        [SwaggerSchema("Tình trạng đơn nghỉ phép: 0.Hủy, 1.Bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("ID Nhân viên")]
        public int EmpID { get; set; }
        [SwaggerSchema("Số ngày nghỉ ext: 0,5, 1, 2")]
        public int NoofDay { get; set; }
        [SwaggerSchema("Mô tả")]
        public string Description { get; set; }
        [SwaggerSchema("Từ ngày")]
        public DateTime? FromDate { get; set; }
        [SwaggerSchema("Đến ngày")]
        public DateTime? ToDate { get; set; }
        [SwaggerSchema("Người phê duyệt")]
        public string ApprovedBy { get; set; }
        [SwaggerSchema("Tình trạng phê duyệt: 0.chưa duyệt, 1. Đã duyệt, 2.Từ chối")]
        public int ApprovedStatus { get; set; }
        [SwaggerSchema("Ngày phê duyệt")]
        public DateTime? ApprovedDate { get; set; }
    }

    public class LeaveSearchParamModel
    {
        [SwaggerSchema("Từ khóa: số điện thoại, tên nhân viên, email")]
        public string key_search { get; set; }
        [SwaggerSchema("Loại đơn nghỉ phép: 0.Chưa xử lý, 1.Đang xử lý, 2.Đã xử lý")]
        public int? leaveType { get; set; }
        [SwaggerSchema("Tình trạng đơn nghỉ phép: 0.Hủy, 1.Bình thường")]
        public int? leaveStatus { get; set; }
        [SwaggerSchema("Tình trạng phê duyệt: 0.chưa duyệt, 1. Đã duyệt, 2.Từ chối")]
        public int? approvedStatus { get; set; }
        [SwaggerSchema("Từ ngày")]
        public DateTime? fromDate { get; set; }
        [SwaggerSchema("Đến ngày")]
        public DateTime? toDate { get; set; }
    }
    public class LeaveInsertModel
    {
        [SwaggerSchema("Loại đơn nghỉ phép: 0.Chưa xử lý, 1.Đang xử lý, 2.Đã xử lý")]
        public int LeaveType { get; set; }
        [SwaggerSchema("Tình trạng đơn nghỉ phép: 0.Hủy, 1.Bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("Danh sách nhân viên")]
        public int[] listEmpID { get; set; }
        [SwaggerSchema("Số ngày nghỉ ext: 0,5, 1, 2")]
        public float NoofDay { get; set; }
        [SwaggerSchema("Loại nghỉ phép: 0.một ngày,1.nhiều ngày")]
        public int LeaveTime { get; set; }
        [SwaggerSchema("Từ ngày")]
        public DateTime? FromDate { get; set; }
        [SwaggerSchema("Đến ngày")]
        public DateTime? ToDate { get; set; }
        [SwaggerSchema("Mô tả")]
        public string Description { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
    }

    
    public class LeaveUpdateModel
    {
        public int ID { get; set; }
        [SwaggerSchema("Loại đơn nghỉ phép: 0.Chưa xử lý, 1.Đang xử lý, 2.Đã xử lý")]
        public int LeaveType { get; set; }
        [SwaggerSchema("Tình trạng đơn nghỉ phép: 0.Hủy, 1.Bình thường")]
        public int Status { get; set; }
        public int EmpID { get; set; }
        [SwaggerSchema("Số ngày nghỉ ext: 0,5, 1, 2")]
        public float NoofDay { get; set; }
        [SwaggerSchema("Loại nghỉ phép: 0.một ngày,1.nhiều ngày")]
        public int LeaveTime { get; set; }
        [SwaggerSchema("Từ ngày")]
        public DateTime? FromDate { get; set; }
        [SwaggerSchema("Đến ngày")]
        public DateTime? ToDate { get; set; }
        [SwaggerSchema("Mô tả")]
        public string Description { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
        [SwaggerSchema("Người phê duyệt")]
        public string ApprovedBy { get; set; }
        [SwaggerSchema("Tình trạng phê duyệt: 0.chưa duyệt, 1. Đã duyệt, 2.Từ chối")]
        public int ApprovedStatus { get; set; }
        [SwaggerSchema("Ngày phê duyệt")]
        public DateTime? ApprovedDate { get; set; }
    }
    public class CountLeaveRequest 
    {
        [SwaggerSchema("Tổng ngày nghỉ phép năm")]
        public int count_annual_leave { get; set; }
        [SwaggerSchema("Tổng ngày nghỉ bệnh, nghỉ ốm")]
        public int count_leave_sick { get; set; }
        [SwaggerSchema("Tổng ngày nghỉ thai sản")]
        public int count_leave_pregnant { get; set; }
        [SwaggerSchema("Tổng ngày nghỉ không phép")]
        public int count_leave_without_permission { get; set; }
        [SwaggerSchema("Tổng ngày nghỉ làm việc tại nhà")]
        public int count_leave_wfh { get; set; }
        [SwaggerSchema("Tổng ngày nghỉ lễ")]
        public int count_holidays { get; set; }
    }
    public class ChangeLeaveApproved
    {
        [SwaggerSchema("ID đơn phép")]
        public int id { get; set; }
        [SwaggerSchema("Tình trạng phê duyệt")]
        public int ApprovedStatus { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
    }
}
