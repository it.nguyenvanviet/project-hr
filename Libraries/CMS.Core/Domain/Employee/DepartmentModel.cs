﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Employee
{
    public class Department
    {
        [SwaggerSchema("id phòng ban")]
        public int ID { get; set; }
        [SwaggerSchema("Mã phòng ban")]
        public string DepartmentCode { get; set; }
        [SwaggerSchema("Tên phòng ban")]
        public string DepartmentName { get; set; }
        [SwaggerSchema("Tình trạng phòng ban:  0.đã hủy, 1.bình thường,")]
        public int Status { get; set; }
        [SwaggerSchema("Ngày tạo")]
        public DateTime CreateDate { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Ngày điều chỉnh")]
        public DateTime? UpdateDate { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
    }
    public class DepartmentGetModel
    {
        [SwaggerSchema("Mã phòng ban")]
        public string DepartmentCode { get; set; }
        [SwaggerSchema("Tên phòng ban")]
        public string DepartmentName { get; set; }
    }
    public class DepartmentInsertModel
    {
        [SwaggerSchema("Mã phòng ban")]
        public string DepartmentCode { get; set; }
        [SwaggerSchema("Tên phòng ban")]
        public string DepartmentName { get; set; }
        [SwaggerSchema("Tình trạng phòng ban:  0.đã hủy, 1.bình thường,")]
        public int Status { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
    }
    public class DepartmentUpdateModel
    {
        [SwaggerSchema("ID Phòng ban")]
        public int ID { get; set; }
        [SwaggerSchema("Mã phòng ban")]
        public string DepartmentCode { get; set; }
        [SwaggerSchema("Tên phòng ban")]
        public string DepartmentName { get; set; }
        [SwaggerSchema("Tình trạng phòng ban:  0.đã hủy, 1.bình thường,")]
        public int Status { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
    }
}
