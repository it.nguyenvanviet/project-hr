﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMS.Core.Domain.Employee
{
    public class Employees
    {
        /// <summary>
        /// Basic info
        /// </summary>

        [SwaggerSchema("UserID")]
        public string UserID { get; set; }
        [SwaggerSchema("ID Nhân viên")]
        public int ID { get; set; }
        [Required(ErrorMessage = "Họ tên không được để trống")]
        [Display(Name = "Họ tên")]
        [SwaggerSchema("Họ tên")]
        public string Name { get; set; }
        [SwaggerSchema("Email nhân viên")]
        public string Email { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Ngày tạo")]
        public DateTime? CreateDate { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string ModifyBy { get; set; }
        [SwaggerSchema("Ngày điều chỉnh")]
        public DateTime? ModifyDate { get; set; }

        /// <summary>
        /// Info
        /// </summary>
        [Required(ErrorMessage = "Số điện thoại không được để trống")]
        [Display(Name = "Số điện thoại")]
        [SwaggerSchema("Số điện thoại")]
        public string Phone { get; set; }
        [SwaggerSchema("Tình trạng : 0.Không hoạt động, 1.Đang thử việc, 2.Đã nhận việc, 3.Tạm dừng công việc, 4.Đã nghỉ việc")]
        [Display(Name = "Trạng thái")] 
        public int Status { get; set; }
        [Display(Name = "Kinh nghiệm")]
        [SwaggerSchema("Số năm kinh nghiệm")]
        public int Experience { get; set; }
        [Required(ErrorMessage = "Ngày sinh không được để trống")]
        [Display(Name = "Ngày sinh")]
        [SwaggerSchema("Ngày sinh")]
        public DateTime? BirthDate { get; set; }

        [Display(Name = "Ngày gia nhập")]
        [SwaggerSchema("Ngày gia nhập")]
        public DateTime? JoinDate { get; set; }
        [Required(ErrorMessage = "Địa chỉ không được để trống")]
        [Display(Name = "Địa chỉ")]
        [SwaggerSchema("Địa chỉ")]
        public string Address { get; set; }
        [Display(Name = "Google Map")]
        [SwaggerSchema("Google Map")]
        public string GoogleMap { get; set; }
        [SwaggerSchema("Hình đại diện")]
        [Display(Name = "Hình đại diện")]
        public string Avartar { get; set; }
        [SwaggerSchema("Url Hình đại diện")]
        public string url_avatar { get; set; }
        [Display(Name = "Giới thiệu")]
        [SwaggerSchema("Lời giới thiệu")]
        public string Contract_Intro { get; set; }
        [Display(Name = "Giới tính")]
        [SwaggerSchema("Giới tính 0.Nữ, 1.Nam, 2.Không xác định")]
        public int Gender { get; set; }
        public string GenderValue { get; set; }
        /// <summary>
        /// Chức vụ hiện tại,Chuyên ngành, MXH, Trình độ học vấn, Kinh nghiệm,...
        /// </summary>
        [Display(Name = "Chức vụ hiện tại")]
        [SwaggerSchema("Rank chức vụ  0.Nhân viên, 1.Cộng tác viên, 2.Trưởng nhóm, 3.Chuyên gia, 4.Phó phòng, 5.Quản lý cấp cao")]
        public int Rank { get; set; }
        [Display(Name = "Bộ phận/Phòng ban")]
        [SwaggerSchema("ID Phòng ban")]
        public int DepartID { get; set; }
        public List<Specialism> Specialisms { get; set; }
        public string listspecialisms { get; set; }
        public List<Social> Socials { get; set; }
        public List<Education> Educations { get; set; }
        public List<Experience> Experiences { get; set; }
        public List<Portfolio> Portfolios { get; set; }

    }


    /// <summary>
    /// Mạng xã hội
    /// </summary>
    public class Social
    {
        [SwaggerSchema("ID Mxh")]
        public int ID { get; set; }
        [SwaggerSchema("Link MXH")]
        public string Link { get; set; }
        [SwaggerSchema("Tên MXH")]
        public string Name { get; set; }
        [SwaggerSchema("ID nhân viên")]
        public string EmpID { get; set; }
    }
    /// <summary>
    /// Trình độ học vấn
    /// </summary>
    public class Education
    {
        [SwaggerSchema("ID")]
        public int ID { get; set; }
        [SwaggerSchema("Tên trường, khóa học")]
        [Display(Name = "Tên trường")]
        public string Name { get; set; }
        [Display(Name = "Ghi chú")]
        [SwaggerSchema("Ghi chú")]
        public string Descriptions { get; set; }
        [Display(Name = "Chuyên môn")]
        [SwaggerSchema("Chuyên môn học")]
        public string Specialism { get; set; }
        [SwaggerSchema("Tình trạng 0.Hủy, 1.Bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("Ngày bắt đầu học")]
        public DateTime? FromDate { get; set; }
        [SwaggerSchema("Ngày kết thúc")]
        public DateTime? ToDate { get; set; }
        [Display(Name = "Loại học vấn")]
        [SwaggerSchema("Loại học vấn  0.Lao động phổ thông, 1.Cao học, 2.Trung học, 3.Chứng chỉ, 4.Trung cấp, 5.Cao đẳng, 6.Đại học,")]
        public int Degree { get; set; }
        public string degreename { get; set; }
        [SwaggerSchema("ID Nhân viên")]
        public int EmpID { get; set; }
    }
    /// <summary>
    /// Ngoại ngữ
    /// </summary>ư
    public class ForeignLanguage
    {
        [SwaggerSchema("ID")]
        public int ID { get; set; }
        [SwaggerSchema("Tên ngôn ngữ")]
        public string Name { get; set; }
        [SwaggerSchema("Tình trạng: 0.Hủy, 1.Bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("Mô tả")]
        public string Descriptions { get; set; }
        [SwaggerSchema("ID Nhân viên")]
        public int EmpID { get; set; }
        [SwaggerSchema("Phần trăm skill nghe")]
        public int ListenSkill { get; set; }
        [SwaggerSchema("Phần trăm skill nói")]
        public int SpeakSkill { get; set; }
        [SwaggerSchema("Phần trăm skill đọc")]
        public int ReadSkill { get; set; }
        [SwaggerSchema("Phần trăm skill viết")]
        public int WriteSkill { get; set; }
    }
    /// <summary>
    /// Kinh nghiệm
    /// </summary>
    public class Experience
    {
        [SwaggerSchema("ID")]
        public int ID { get; set; }
        [Display(Name = "Tên công việc")]
        [SwaggerSchema("Tên công việc, ngành nghề")]
        public string Name { get; set; }
        [SwaggerSchema("Nơi làm việc/trụ sở,...")]
        [Display(Name = "Nơi làm việc/trụ sở,...")]
        public string Office { get; set; }
        [SwaggerSchema("Số năm kinh nghiệm")]
        [Display(Name = "Số năm kinh nghiệm")]
        public float Year_experiences { get; set; }
        [Display(Name = "Mô tả")]
        [SwaggerSchema("Mô tả ngắn")]
        public string Descriptions { get; set; }
        [SwaggerSchema("Tình trạng: 0.Hủy, 1.Bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("ID Nhân viên")]
        public int EmpID { get; set; }
    }
    /// <summary>
    /// Tổng hợp các dự án, kinh nghiệm
    /// </summary>
    public class Portfolio
    {
        [SwaggerSchema("ID")]
        public int ID { get; set; }
        [Display(Name = "Tên PortFolio")]
        [SwaggerSchema("Tên PortFolio")]
        public string Name { get; set; } 
        [Display(Name = "Mô tả")]
        [SwaggerSchema("Mô tả ngắn")]
        public string Descriptions { get; set; }
        [SwaggerSchema("Tình trạng: 0.Hủy, 1.Bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("ID Nhân viên")]
        public int EmpID { get; set; }
    }
    /// <summary>
    /// Contract liên lạc
    /// </summary>
    public class Contact
    {
        public int ID { get; set; }
        [SwaggerSchema("Họ tên")]
        public string Name { get; set; }
        [SwaggerSchema("Email")]
        public string Email { get; set; }
        [SwaggerSchema("Lời nhắn")]
        public string Message { get; set; }
        [SwaggerSchema("Tình trạng: 0.Hủy, 1.Bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("ID Nhân viên")]
        public int EmpID { get; set; }
    }
    /// <summary>
    /// Skill kĩ năng, năng lực của nhân viên
    /// </summary>
    public class Skills
    {
        public int ID { get; set; }
        [SwaggerSchema("Tên kĩ năng, năng lực")]
        public string Name { get; set; }
        [SwaggerSchema("Mô tả ngắn")]
        public string Description { get; set; }
        [SwaggerSchema("Phần trăm thành thạo")]
        public float PointPercent { get; set; }
        [SwaggerSchema("Loại kĩ năng: 0: skill làm việc, 1: kĩ năng mềm, 2: ngoại ngữ, 3: Khác")]
        public int TypeSkill { get; set; }
        public string TypeSkillName { get; set; }
        [SwaggerSchema("Tình trạng: 0.Hủy, 1.Bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("ID Nhân viên")]
        public int EmpID { get; set; }
    }
    /// <summary>
    /// Chuyên môn
    /// </summary>
    public class Specialism
    {
        public int ID { get; set; }
        [SwaggerSchema("Tình trạng: 0.Hủy, 1.Bình thường")]
        public int Status { get; set; }
        [SwaggerSchema("Tên chuyên môn, ngành học")]
        public string Name { get; set; }
        [SwaggerSchema("Mô tả ngắn")]
        public string Descriptions { get; set; }
        [SwaggerSchema("ID Nhân viên")]
        public int EmpID { get; set; }
    }
}
