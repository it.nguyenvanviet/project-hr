﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CMS.Core.Domain.Employee
{
    public class SpecialismViewModel
    {
        
        public int ID { get; set; }
        [SwaggerSchema("Tên chuyên ngành")]
        public string Name { get; set; }
        [SwaggerSchema("Mô tả")]
        public string Descriptions { get; set; }
        [SwaggerSchema("ID Chuyên ngành ")]
        public int SpecialismID { get; set; }  
    }
    public class SpecialismInsertModel
    {
        [SwaggerSchema("Tên chuyên ngành ")]
        public string Name { get; set; }  
        [SwaggerSchema("Mô tả")]
        public string Descriptions { get; set; }  
        [SwaggerSchema("Tình trạng")]
        public int Status { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }  
         
    }
    public class SpecialismUpdateModel
    {
      
        public int ID { get; set; }
        [SwaggerSchema("Tên chuyên nghành")]
        public string Name { get; set; }
        [SwaggerSchema("Mô tả")]
        public string Descriptions { get; set; }
        [SwaggerSchema("Tình trạng")]
        public int Status { get; set; }
        [SwaggerSchema("Người diều chỉnh")]
        public string ModifyBy { get; set; }   
    }
    public class SpecialismDeleteModel
    {
        public int ID { get; set; } 
        [SwaggerSchema("Người thao tác")]
        public string ModifyBy { get; set; }
    }
}
