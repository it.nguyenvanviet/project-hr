﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Employee
{
    public class EmpBasicInfoViewModel
    {
        [SwaggerSchema("ID Nhân viên")]  
        public int EmpID { get; set; }
        [SwaggerSchema("Hình đại diện")]
        public string Avatar { get; set; }
        [SwaggerSchema("Tên nhân viên")]
        public string Name { get; set; }
        [SwaggerSchema("Địa chỉ")]
        public string Address { get; set; }
        [SwaggerSchema("GoogleMap")]
        public string GoogleMap { get; set; }
        [SwaggerSchema("Email")]
        public string Email { get; set; }
        [SwaggerSchema("Số điện thoại")]
        public string Phone { get; set; }
        [SwaggerSchema("Ngày sinh")]
        public DateTime? BirthDate { get; set; }
        [SwaggerSchema("Tình trạng : 0.Không hoạt động, 1.Đang thử việc, 2.Đã nhận việc, 3.Tạm dừng công việc, 4.Đã nghỉ việc")]
        public int Status { get; set; }
    }
}
