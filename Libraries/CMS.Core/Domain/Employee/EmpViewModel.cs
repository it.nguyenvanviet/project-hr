﻿using CMS.Core.Domain.Account;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CMS.Core.Domain.Employee
{
    public class EmpViewModel : Employees
    {
        public RegisterViewModel RegisterView { get; set; }
    }
    public class EmpSearchModel
    {
        [SwaggerSchema("Từ khóa tìm kiếm")]
        public string keysearch { get; set; }
        [SwaggerSchema("Tình trạng nhân viên: 0.Không hoạt động, 1.Đang thử việc, 2.Đã nhận việc, 3.Tạm dừng công việc, 4.Đã nghỉ việc")]
        public int? Status { get; set; }
        [SwaggerSchema("Rank chức vụ  0.Nhân viên, 1.Cộng tác viên, 2.Trưởng nhóm, 3.Chuyên gia, 4.Phó phòng, 5.Quản lý cấp cao")]
        public int? Rank { get; set; }
        [SwaggerSchema("ID Phòng ban")]
        public int? DepartID { get; set; }
    }


    public class EmpRegisViewModel
    {
        [SwaggerSchema("Họ tên")]
        public string Name { get; set; }
        [SwaggerSchema("Số điện thoại")]
        public string Phone { get; set; }
        [SwaggerSchema("Ngày sinh")]
        public string BirthDate { get; set; }
        [SwaggerSchema("Giới tính 0.Nữ, 1.Nam, 2.Không xác định")]
        public int Gender { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Địa chỉ")]
        public string Address { get; set; }
        [SwaggerSchema("Tình trạng : 0.Không hoạt động, 1.Đang thử việc, 2.Đã nhận việc, 3.Tạm dừng công việc, 4.Đã nghỉ việc")]
        public int Status { get; set; }
        [SwaggerSchema("Số năm kinh nghiệm")]
        public int Experience { get; set; }
        [SwaggerSchema("Ngày gia nhập")]
        public DateTime? JoinDate { get; set; }
        public string GoogleMap { get; set; }
        [SwaggerSchema("Hình đại diện")]
        public string Avartar { get; set; }
        [SwaggerSchema("Lời giới thiệu")]
        public string Contract_Intro { get; set; }
        [SwaggerSchema("Rank chức vụ  0.Nhân viên, 1.Cộng tác viên, 2.Trưởng nhóm, 3.Chuyên gia, 4.Phó phòng, 5.Quản lý cấp cao")]
        public int Rank { get; set; }
        [SwaggerSchema("ID Phòng ban")]
        public int DepartID { get; set; }
        public RegisterViewModel RegisterView { get; set; }
    }
    public class EmpUpdateViewModel
    {
        public int ID { get; set; }
        [SwaggerSchema("Họ tên")]
        public string Name { get; set; }
        [SwaggerSchema("Số điện thoại")]
        public string Phone { get; set; }
        [SwaggerSchema("Ngày sinh")]
        public string BirthDate { get; set; }
        [SwaggerSchema("Giới tính 0.Nữ, 1.Nam, 2.Không xác định")]
        public int Gender { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Địa chỉ")]
        public string Address { get; set; }
        [SwaggerSchema("Tình trạng : 0.Không hoạt động, 1.Đang thử việc, 2.Đã nhận việc, 3.Tạm dừng công việc, 4.Đã nghỉ việc")]
        public int Status { get; set; }
        [SwaggerSchema("Số năm kinh nghiệm")]
        public int Experience { get; set; }
        [SwaggerSchema("Rank chức vụ  0.Nhân viên, 1.Cộng tác viên, 2.Trưởng nhóm, 3.Chuyên gia, 4.Phó phòng, 5.Quản lý cấp cao")]
        public int Rank { get; set; }
        [SwaggerSchema("ID Phòng ban")]
        public int DepartID { get; set; }
        [SwaggerSchema("Hình đại diện")]
        public string Avartar { get; set; }
        [SwaggerSchema("Email")]
        public string Email { get; set; }
        [SwaggerSchema("Google Map")]
        public string GoogleMap { get; set; }
        [SwaggerSchema("Lời giới thiệu bản thân")]
        public string Contract_Intro { get; set; }
        public string UpdateBy { get; set; }
    }
    public class EmpUpdateAvatarViewModel
    {
        [SwaggerSchema("ID Nhân viên")]
        public int ID { get; set; }
        [SwaggerSchema("Hình đại diện")]
        public string Avartar { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
        [SwaggerSchema("Tên")]
        public string Name { get; set; }
        [SwaggerSchema("Email")]
        public string Email { get; set; }
    }
    public class EmpUpdateRoleViewModel
    {
        [SwaggerSchema("ID Nhân viên")]
        public int EmpID { get; set; }
        [SwaggerSchema("Danh sách quyền")]
        public List<string> Listroleid { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
    }

    public class EmpAdd_EducationViewModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string Name { get; set; }
        public string Specialism { get; set; }
        public int Degree { get; set; }
        public string Descriptions { get; set; }
        public string CreateBy { get; set; }
        public int EmpID { get; set; }
    }
    public class EmpAdd_ExperienceViewModel
    {
        public string Name { get; set; }
        public string Office { get; set; }
        public float Year_experiences { get; set; }
        public int EmpID { get; set; }
        public string CreateBy { get; set; }
    }
    public class EmpAdd_SocialViewModel
    {
        public string Name { get; set; }
        public string Link { get; set; }
        public int EmpID { get; set; }
        public string CreateBy { get; set; }
    }
    public class EmpAdd_SkillViewModel
    {
        public string Name { get; set; }
        public float PointPercent { get; set; }
        public string Description { get; set; }
        public int TypeSkill { get; set; } 
        public int EmpID { get; set; }
        public string CreateBy { get; set; }
    }
    public class EmpAdd_SpecialismViewModel
    {
        public int SpecialismID { get; set; }
        public int EmpID { get; set; }
        public string CreateBy { get; set; }
    }
    public class EmpAdd_PortfolioViewModel
    {
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public int EmpID { get; set; }
        public string CreateBy { get; set; }

    }


    public class EmpUpdate_EducationViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public string Specialism { get; set; }
        public int Status { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int Degree { get; set; }
        public int EmpID { get; set; }
        public string UpdateBy { get; set; }
    }
    public class EmpUpdate_ExperienceViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Office { get; set; }
        public float Year_experiences { get; set; }
        public string Descriptions { get; set; }
        public int Status { get; set; }
        public int EmpID { get; set; }
        public string UpdateBy { get; set; }
    }
    public class EmpUpdate_SocialViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public int Status { get; set; }
        public int EmpID { get; set; }
        public string UpdateBy { get; set; }
    }
    public class EmpUpdate_SkillViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float PointPercent { get; set; }
        public int TypeSkill { get; set; }
        public int Status { get; set; }
        public int EmpID { get; set; }
        public string UpdateBy { get; set; }
    }
    public class EmpUpdate_SpecialismViewModel
    {
        public int ID { get; set; }
        public int SpecialismID { get; set; }
        public int EmpID { get; set; }
        public string UpdateBy { get; set; }
    }
    public class EmpUpdate_PortfolioViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Descriptions { get; set; }
        public int Status { get; set; }
        public string UpdateBy { get; set; }

    }
}
