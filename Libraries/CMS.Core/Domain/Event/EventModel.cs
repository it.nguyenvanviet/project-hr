﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Event
{
    public class EventModel
    {
        public int ID { get; set; }
        [SwaggerSchema("Loại sự kiện :")]
        public int EventType { get; set; }
        [SwaggerSchema("Tiêu đề")]
        public string Subject { get; set; }
        [SwaggerSchema("Địa chỉ")]
        public string Location { get; set; }
        [SwaggerSchema("Ngày bắt đầu")]
        public DateTime? FromDate { get; set; }
        [SwaggerSchema("Ngày kết thúc")]
        public DateTime? ToDate { get; set; }
        [SwaggerSchema("Cả ngày")]
        public int IsAllDay { get; set; }
        [SwaggerSchema("Không thay đổi")]
        public int IsBlock { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Ngày tạo")]
        public DateTime? CreateDate { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
        [SwaggerSchema("Ngày điều chỉnh")]
        public DateTime? UpdateDate { get; set; }
    }
}
