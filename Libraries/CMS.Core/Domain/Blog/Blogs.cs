﻿using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Blog
{
    public class BlogsCategory
    {
        [SwaggerSchema("id")]
        public int ID { get; set; }
        [SwaggerSchema("Tiêu đề")]
        public string Title { get; set; }
        [SwaggerSchema("Tình trạng: 0.hủy 1.bình thường")]
        public string Status { get; set; }
    }
    public class Blogs
    {
        [SwaggerSchema("id bài viết")]
        public int ID { get; set; }
        [SwaggerSchema("id nhân viên")]
        public int EmpID { get; set; }
        [SwaggerSchema("Tiêu đề bài viết")]
        public string Title { get; set; }
        [SwaggerSchema("Mô tả bài viết")]
        public string Descriptions { get; set; }
        [SwaggerSchema("Nội dung bài viết")]
        public string Contents { get; set; }
        [SwaggerSchema("Tình trạng bài viết: 0.đã hủy, 1.bình thường, 2.chờ duyệt")]
        public int Status { get; set; }
        [SwaggerSchema("Ngày tạo")]
        public DateTime CreateDate { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Ngày điều chỉnh")]
        public DateTime ? UpdateDate { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
    }
    public class BlogComments
    {
        public int ID { get; set; }
        public int BlogID { get; set; }
        public string CommentsContents { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}
