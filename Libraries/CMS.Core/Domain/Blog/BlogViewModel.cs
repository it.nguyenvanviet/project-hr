﻿using CMS.Core.Domain.Media;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.Domain.Blog
{
    public class BlogViewModel :Blogs
    {
        public List<Picture> pictures { get; set; }
    }
    public class BlogInsertModel
    {
        [SwaggerSchema("ID nhân viên")]
        public int EmpID { get; set; }
        [SwaggerSchema("Tiêu đề bài viết")]
        public string Title { get; set; }
        [SwaggerSchema("Mô tả bài viết")]
        public string Descriptions { get; set; }
        [SwaggerSchema("Nội dung bài viết")]
        public string Contents { get; set; }
        [SwaggerSchema("Tình trạng bài viết 1: bình thường, 2: chờ duyệt, 6: đã hủy")]
        public int Status { get; set; }

        [SwaggerSchema("Ngày tạo")]
        public DateTime CreateDate { get; set; }
        [SwaggerSchema("Người tạo")]
        public string CreateBy { get; set; }
        [SwaggerSchema("Ngày điều chỉnh")]
        public DateTime? UpdateDate { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
    }
    public class BlogUpdateModel :Blogs
    { }
    public class BlogDeleteModel
    {
        [SwaggerSchema("ID bài viết")]
        public int ID { get; set; }
        [SwaggerSchema("Người điều chỉnh")]
        public string UpdateBy { get; set; }
    }
}
