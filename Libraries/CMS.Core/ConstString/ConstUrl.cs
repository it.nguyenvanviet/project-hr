﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.ConstString
{
   public struct BaseURL
    {
        public const string 
                SettingURL  = "/setting/",
                EmployeeURL = "/emp/", 
                ProjectUrl  = "/project/",
                AccountUrl  = "/account/",
                BlogUrl     = "/blog/",
                MenuUrl     = "/menu/";
                
    }
    public struct UrlApi
    {
        public const string ApiUrl                      = "https://apihr.karobean.com/";  ///"https://localhost:44310/"; */// // "https://apihr.karobean.com/"; //"";

        public const string Apiauthen                   = ApiUrl + "api/authenticate/";
        public const string ApiauthenLoginUrl           = Apiauthen + "login";
        public const string ApiauthenGetRole            = Apiauthen + "getrole";
        public const string MenuApiUrl                  = ApiUrl + "api/menu/",
                            SettingApiUrl               = ApiUrl + "api/setting/",
                            EmployeeApiUrl              = ApiUrl + "api/employee/",
                            ProjectApiUrl               = ApiUrl + "api/project/",
                            SpecialismApiUrl            = ApiUrl + "api/specialism/",
                            DepartmentApiUrl            = ApiUrl + "api/department/",
                            TeamApiUrl                  = ApiUrl + "api/team/",
                            LeaveApiUrl                 = ApiUrl + "api/leave/";
    } 

    public struct AccountUrl
    {
        public const string AccountLogin        = "/account/login";
        public const string AccountLogout       = "/account/Logout"; 
        public const string Account_Url_Profile = "account/MyProfile";
    }
    public struct MenuUrl
    {
        public const string ListMenu            = BaseURL.MenuUrl + "listmenu";
        public const string getmenutype         = UrlApi.MenuApiUrl + "getmenutype";
        public const string getmenutypebyuserid = UrlApi.MenuApiUrl + "getmenutypebyuseid";
        public const string getmenubyuserid     = UrlApi.MenuApiUrl + "getmenubyuseid";
    }
    public struct ProjectUrl
    {
        public const string ListProject = BaseURL.ProjectUrl + "list";
    }
    public struct EmployeeUrl
    {
        public const string ListEmp             = BaseURL.EmployeeURL + "list";
        public const string ListTeam            = BaseURL.EmployeeURL + "teams";
        public const string ListDepartments     = BaseURL.EmployeeURL + "departments";
        public const string ListLeaveRequest    = BaseURL.EmployeeURL + "leaverequest";
        public const string ListActivities      = BaseURL.EmployeeURL + "activities";
        public const string ListEvents          = BaseURL.EmployeeURL + "events";
    }
   

    public struct ProfileUrl
    {
        public const string myprofile = "/my-profile";
    }
    public struct CommunicationUrl
    {
        public const string Chat = "app-inbox";
    }
    public struct CVurl
    {
        public const string CVReview = "cvprofile";
    }
    public struct SecurityUrl
    {
        public const string AccountDenied = "/security/accessdenied";
    }
}
