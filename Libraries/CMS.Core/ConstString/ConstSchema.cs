﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.ConstString
{
    public class ConstSchema
    {
        public const string DBSchema = "ProjectHR.dbo.";
    }
    public class AuthenSchema
    {
        public const string Schema = "SecurityScheme",
                            CookieName = ".aspNetCore.Security.Cookie";
    }
    public class RoleName
    {
        public const string Administrator = "Administrator";
        public const string Manager = "Manager";
        public const string User = "User";
    }
    public class Policy
    {
        public const string AdminRole = "AdminRole"; 
        public const string UserRole = "UserRole";
    }
    public class Cor
    { }
    public class TokenConfig
    {
        public const int MinExpiredHour = 23;
        public const int MinExpiredMinustes = 60;
    }
}
