﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.ConstString
{
    public struct LoggingStore
    {
        public const string WriteLog = ConstSchema.DBSchema + "ProjectLogging_WriteLog";
    }

    public struct EmployeeStore
    {
        public const string
            GetByPaging         = ConstSchema.DBSchema + "Employee_GetByPaging",
            Search              = ConstSchema.DBSchema + "Employee_Search",
            GetByID             = ConstSchema.DBSchema + "Employee_GetByID",
            Insert              = ConstSchema.DBSchema + "Employee_Insert",
            Update              = ConstSchema.DBSchema + "Employee_Update",
            UpdateBasicInfo     = ConstSchema.DBSchema + "Employee_UpdateBasicInfo",
            Updateavartar       = ConstSchema.DBSchema + "Employee_Updateavartar",
            UpdateRole          = ConstSchema.DBSchema + "Employee_UpdateRole",
            Active              = ConstSchema.DBSchema + "Employee_Active",
            Delete              = ConstSchema.DBSchema + "Employee_Delete",
            GetByUserID         = ConstSchema.DBSchema + "Employee_GetByUserID";



        public const string
            AddEducation        = ConstSchema.DBSchema + "Employee_AddEducation",
            AddExperience       = ConstSchema.DBSchema + "Employee_AddExperience",
            AddSocial           = ConstSchema.DBSchema + "Employee_AddSocial",
            AddSkill            = ConstSchema.DBSchema + "Employee_AddSkill",
            AddSpecialism       = ConstSchema.DBSchema + "Employee_AddSpecialism",
            AddPortfolio        = ConstSchema.DBSchema + "Employee_AddPortfolio";

        public const string
            EditEducation        = ConstSchema.DBSchema + "Employee_EditEducation",
            EditExperience       = ConstSchema.DBSchema + "Employee_EditExperience",
            EditSocial           = ConstSchema.DBSchema + "Employee_EditSocial",
            EditSkill            = ConstSchema.DBSchema + "Employee_EditSkill",
            EditSpecialism       = ConstSchema.DBSchema + "Employee_EditSpecialism",
            EditPortfolio        = ConstSchema.DBSchema + "Employee_EditPortfolio";

        public const string
            DeleteEducation     = ConstSchema.DBSchema + "Employee_DeleteEducation",
            DeleteExperience    = ConstSchema.DBSchema + "Employee_DeleteExperience",
            DeleteSocial        = ConstSchema.DBSchema + "Employee_DeleteSocial",
            DeleteSkill         = ConstSchema.DBSchema + "Employee_DeleteSkill",
            DeleteSpecialism    = ConstSchema.DBSchema + "Employee_DeleteSpecialism",
            DeletePortfolio     = ConstSchema.DBSchema + "Employee_DeletePortfolio";


        public const string
            GetAllLeader       = ConstSchema.DBSchema + "Employee_GetAllLeader",
            GetAllMember       = ConstSchema.DBSchema + "Employee_GetAllMember";
    }
    public struct SpecialismsStore
    {
        public const string
            GetAll      = ConstSchema.DBSchema + "Specialisms_GetAll",
            GetByEmpID  = ConstSchema.DBSchema + "Specialisms_GetByEmpID",
            GetByUserID = ConstSchema.DBSchema + "Specialisms_GetByUserID";

        public const string
            Insert = ConstSchema.DBSchema + "Specialisms_Insert",
            Update = ConstSchema.DBSchema + "Specialisms_Update",
            Delete = ConstSchema.DBSchema + "Specialisms_Delete";
    }
    public struct EducationStore
    {
        public const string
            GetAll      = ConstSchema.DBSchema + "Education_GetAll",
            GetByEmpID  = ConstSchema.DBSchema + "Education_GetByEmpID",
            GetByUserID = ConstSchema.DBSchema + "Education_GetByUserID";

    }
    public struct SocialStore {
        public const string 
            GetByEmpID  = ConstSchema.DBSchema + "Social_GetByEmpID",
            GetByUserID = ConstSchema.DBSchema + "Social_GetByUserID";
    } 

    public struct SkillStore
    {
        public const string
            GetByEmpID  = ConstSchema.DBSchema + "Skill_GetbyEmpID",
            GetByUserID = ConstSchema.DBSchema + "Skill_GetByUserID";
    }
    public struct ExperienceStore
    {
        public const string
            GetAll      = ConstSchema.DBSchema + "Experience_GetAll",
            GetByEmpID  = ConstSchema.DBSchema + "Experience_GetByEmpID",
            GetByUserID = ConstSchema.DBSchema + "Experience_GetByUserID";

    }

    public struct AccountStore
    {
        public const string
            AccountRegister     = ConstSchema.DBSchema + "AccountRegister",
            CheckLogin          = ConstSchema.DBSchema + "CheckLogin",
            AccountFindByName   = ConstSchema.DBSchema + "AccountFindByName",
            AccountFindByID     = ConstSchema.DBSchema + "AccountFindByID";
    }
    public struct RolesStore
    {
        public const string GetRoleByMenuID     = ConstSchema.DBSchema + "Role_GetlistRolebyMenuID",
                            GetlistRolebyUserID = ConstSchema.DBSchema + "Role_GetlistRolebyUserID",
                            GetlistRolebyEmpID  = ConstSchema.DBSchema + "Role_GetlistRolebyEmpID";
    }
    public struct MenuStore
    {
        public const string GetMenuByRoleID = ConstSchema.DBSchema + "Menu_GetByRoleID",
                            GetMenuByUserID = ConstSchema.DBSchema + "Menu_GetByUserID",
                            GetAllMenu      = ConstSchema.DBSchema + "Menu_Getall",
                            GetByID         = ConstSchema.DBSchema + "Menu_GetByID", 
                            Insert          = ConstSchema.DBSchema + "Menu_Insert",
                            InsertDetail    = ConstSchema.DBSchema + "Menu_InsertDetail",
                            Update          = ConstSchema.DBSchema + "Menu_Update",
                            UpdateDetail    = ConstSchema.DBSchema + "Menu_UpdateDetail",
                            UpdatePosition  = ConstSchema.DBSchema + "Menu_UpdatePosition", 
                            Delete          = ConstSchema.DBSchema + "Menu_Delete",
                            DisableRole     = ConstSchema.DBSchema + "Menu_DisableRole" ;
                            

        public const string GetMenuType             = ConstSchema.DBSchema + "MenuType_Getall",
                            GetMenuTypeByUserID     = ConstSchema.DBSchema + "MenuType_GetByUserID";



    }
    public struct ProjectStore
    {
        public const string ProjectGetByName    = ConstSchema.DBSchema + "Project_GetByName",
                            ProjectSearch       = ConstSchema.DBSchema + "Project_Search",
                            GetByID             = ConstSchema.DBSchema + "Project_GetByID",
                            ProjectProjectType  = ConstSchema.DBSchema + "ProjectType_GetAll",
                            Insert              = ConstSchema.DBSchema + "Project_Insert",
                            InsertDetail        = ConstSchema.DBSchema + "Project_InsertDetail",
                            Update              = ConstSchema.DBSchema + "Project_Update",
                            UpdateDetail        = ConstSchema.DBSchema + "Project_UpdateDetail",
                            UpdatePosition      = ConstSchema.DBSchema + "Project_UpdatePosition",
                            Delete              = ConstSchema.DBSchema + "Project_Delete",
                            ChangeStatus        = ConstSchema.DBSchema + "Project_ChangeStatus";
    }
    public struct TeamStore
    {
        public const string 
            SearchTeam          = ConstSchema.DBSchema + "Team_Search",
            GetTeamsByProjectID = ConstSchema.DBSchema + "Team_GetByProjectID",
            GetByID             = ConstSchema.DBSchema + "Team_GetByID",
            Insert              = ConstSchema.DBSchema + "Team_Insert",
            InsertLeader        = ConstSchema.DBSchema + "Team_InsertLeader",
            InsertMember        = ConstSchema.DBSchema + "Team_InsertMember",
            Update              = ConstSchema.DBSchema + "Team_Update",
            UpdateLeader        = ConstSchema.DBSchema + "Team_UpdateLeader",
            UpdateMember        = ConstSchema.DBSchema + "Team_UpdateMember",
            Delete              = ConstSchema.DBSchema + "Team_Delete";
    }
    public struct ClientStore
    {
        public const string GetAll = ConstSchema.DBSchema + "Client_GetAll";
    }
    public struct SettingStore
    {
        public const string Getall = ConstSchema.DBSchema + "Setting_Getall";
        public const string Update = ConstSchema.DBSchema + "Setting_Update";
    }
    public struct SecurityStore
    {
        public const string GetPermissionRecord     = ConstSchema.DBSchema + "PermissionRecord_Getall";
        public const string GetPermissionRecordRole = ConstSchema.DBSchema + "PermissionRecordRole_Getall";
    }
    public struct PictureStore
    {
        public const string     Update  = ConstSchema.DBSchema + "Picture_Update",
                                Insert  = ConstSchema.DBSchema + "Picture_Insert",
                                Delete  = ConstSchema.DBSchema + "Picture_Delete",
                                GetbyID = ConstSchema.DBSchema + "Picture_GetbyID",
                                GetPicture_forBlog = ConstSchema.DBSchema + "Picture_GetByBlogID",
                                Getall  = ConstSchema.DBSchema + "Picture_Getall";
    }
    public struct BlogStore
    {
        public const string GetBlogByEmpID = ConstSchema.DBSchema + "Blog_GetByEmpID",
                            GetByID = ConstSchema.DBSchema + "Blog_GetByID", 
                            Insert = ConstSchema.DBSchema + "Blog_Insert",
                            Update = ConstSchema.DBSchema + "Blog_Update",
                            Delete = ConstSchema.DBSchema + "Blog_Delete";
    }
    public struct DepartmentStore
    {
        public const string
                GetDepartments = ConstSchema.DBSchema + "Department_GetByCodeName",
                GetDepartmentsByID = ConstSchema.DBSchema + "Department_GetByID",
                Insert  = ConstSchema.DBSchema + "Department_Insert",
                Update = ConstSchema.DBSchema + "Department_Update",
                Delete = ConstSchema.DBSchema + "Department_Delete";
    }
    public struct LeaveStore
    {
        public const string
                GetCountRequest = ConstSchema.DBSchema + "Leave_GetCountRequest",
                GetListRequest = ConstSchema.DBSchema + "Leave_GetListRequest",
                GetLeaveByID = ConstSchema.DBSchema + "Leave_GetByID",
                Insert = ConstSchema.DBSchema + "Leave_Create",
                Update = ConstSchema.DBSchema + "Leave_Edit",
                Delete = ConstSchema.DBSchema + "Leave_Delete",
                ChangeApproved = ConstSchema.DBSchema + "Leave_ChangeApproved";
    }

}
