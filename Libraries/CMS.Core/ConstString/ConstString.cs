﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Core.ConstString
{
    public struct ConstString
    {
        public const string UpdateFail = "Cập nhật thất bại";
        public const string UpdateSuccess = "Cập nhật thành công";
    }
    
}
