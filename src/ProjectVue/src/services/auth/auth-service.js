import axios from 'axios';
import * as apiconst from '../../../src/services/constants/apiconst.js';

axios.defaults.headers.common['Access-Control-Allow-Origin']  = '*';
axios.defaults.headers.common['Access-Control-Allow-Methods'] = apiconst.Access_Control_Allow_Methods;
axios.defaults.headers.common['Access-Control-Allow-Headers'] = apiconst.Access_Control_Allow_Headers;
axios.defaults.headers.common['Content-Type'] = apiconst.content_type;
class AuthService {
  login(user) {
    return axios
      .post(apiconst.apiurl + 'api/authenticate/login', {
        username: user.username,
        password: user.password
      })
      .then(response => {
        if (response.data !== null) {
            localStorage.setItem('user', JSON.stringify(response.data));
        }
        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user'); 
  }

  register(user) {
    return axios.post(apiconst.apiurl + 'api/authenticate/signup', {
      username: user.username,
      email: user.email,
      password: user.password
    });
  }
}

export default new AuthService();