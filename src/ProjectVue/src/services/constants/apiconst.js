export const apiurl         = "https://apihr.karobean.com/";
export const content_type   = 'application/json; charset=utf-8';
export const Access_Control_Allow_Headers = 'Origin, Accept, Content-Type, Authorization, Access-Control-Allow-Origin';
export const Access_Control_Allow_Methods = 'GET, POST, PUT, DELETE';