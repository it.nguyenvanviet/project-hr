import axios from 'axios';
import * as apiconst from '../../../src/services/constants/apiconst.js';

const headertoken = JSON.parse(localStorage.getItem('user'));
axios.defaults.headers.common['Access-Control-Allow-Origin']  = '*';
axios.defaults.headers.common['Access-Control-Allow-Methods'] = apiconst.Access_Control_Allow_Methods
axios.defaults.headers.common['Access-Control-Allow-Headers'] = apiconst.Access_Control_Allow_Headers;
axios.defaults.headers.common['Content-Type']  = apiconst.content_type;
class MenuService {
  async getMenuType() {
    if(headertoken !== null){ 
      axios.defaults.headers.common['Authorization'] = 'Bearer '+ headertoken.token;
      const response = await axios
        .get(apiconst.apiurl + 'api/menu/getmenutype');
      if (response !== null) {
        localStorage.setItem('menutype', JSON.stringify(response));
      } 
      return response.data;
    }
    else return null;
  }
  async getMenus() {
    if(headertoken !== null){
      axios.defaults.headers.common['Authorization'] = 'Bearer '+ headertoken.token;
      const response = await axios
      .get(apiconst.apiurl + 'api/menu/getmenus');
      if (response !== null) {
        localStorage.setItem('menu', JSON.stringify(response));
      }
      return response.data;
    }
  }
}

export default new MenuService();