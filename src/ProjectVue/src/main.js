import Vue from 'vue'
import App from './App.vue'
import { router } from './router';
import { LMap, LTileLayer, LMarker } from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css';

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);

import "@/plugins/echarts"
import FullCalendar from 'vue-full-calendar'
import 'fullcalendar/dist/fullcalendar.css'
import 'v-tooltip/dist/v-tooltip.css'
import VTooltip from 'v-tooltip'
import VueToast from 'vue-toast-notification';
import VeeValidate from 'vee-validate';
import 'vue-toast-notification/dist/theme-sugar.css'
import VueCarousel from 'vue-carousel'
import VueCircleControlSlider from 'vue-circle-control-slider'
import 'vue-circle-control-slider/dist/VueCircleControlSlider.css'
import store from './store';
import VueDropify from 'vue-dropify';

Vue.use(VueDropify);
Vue.use(VeeValidate);
Vue.component('VueCircleControlSlider', VueCircleControlSlider);

Vue.use(VueCarousel)
Vue.use(VueToast)
Vue.use(VTooltip)
Vue.use(FullCalendar) 
window.Event = new Vue();
Vue.use(VeeValidate);
router.afterEach((to) => {
    Vue.nextTick( () => {
        document.title = 'HR -- '+ to.meta.title;
    });
  })
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');