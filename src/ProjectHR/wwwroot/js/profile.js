var ddlstatus;
var ddltype;
var ddlrank;
var ddlgender;
var birthdate;
var experience;
var profile;
var countloading = 0;

 

$(document).ready(function () {
    loadcontrol();
});
 
function loadprofile()
{
    $('.page-loader-wrapper').show();
    var option_data_emp = {
        url: "/account/getbasicinfocontact/",
        method: "GET",
        success: function (data) {
            birthdate.value(data.birthDate);
            $('#Empid').val(data.id);
            //$('#Empavatar').empty().append('<img src="' + data.avartar + '"  class="rounded-circle" alt="">'); 
            $("#id_service_image").addClass('dropify');
            $("#id_service_image").attr("data-height", 300);
            $("#id_service_image").attr("data-default-file", "/Upload/avatar.png");

            if (imageExists(data.avartar)) {
                $("#id_service_image").attr("data-default-file", data.avartar);
            }
            $('.dropify').dropify(); 
             
            $('#_Info_Address').empty().append(data.address);
            $('#_Info_Name').empty().append(data.name);
            $('#_Info_GoogleMap').empty().append(data.googleMap).find("iframe").css("width", "100%");
            $('#_Info_Phone').empty().append(data.phone);
            $('#_Info_Email').empty().append(data.email);
            $('#_Info_BirthDate').empty().append(kendo.toString(birthdate.value(), 'dd-MM-yyyy'));

            $('.page-loader-wrapper').hide();
        }
    };
    AjaxCaller(null, option_data_emp); 


}
function loadcontrol() {
    $('.page-loader-wrapper').show();
     
    // load status
    var optionajaxstatusemp = {
        url: "/account/getstatusemp",
        method: "GET",
        success: function (data) {
            ddlstatus = $('#Basic_Info_Status').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxstatusemp);
    

    var optionajaxrank = {
        url: "/account/getranks",
        method: "GET",
        success: function (data) {
            ddlrank = $('#Basic_Info_Rank').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
             
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxrank);

    birthdate = $("#Basic_Info_BirthDate").kendoDatePicker({
        value: kendo.date.today(),
        format: "dd/MM/yyyy",
        culture: "vi-VN",
        animation: {
            open: {
                effects: "zoom:in",
                duration: 500
            }
        }
    }).data("kendoDatePicker");

    var optionajaxgender = {
        url: "/account/getgender",
        method: "GET",
        success: function (data) {
            ddlgender = $('#Basic_Info_Gender').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
            
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxgender);

    experience = $('#Experience').kendoNumericTextBox({
        format: "##,#",
        decimals: 0,
        MinCount: 0,
        value: 0,
        MaxCount: 1000000000
    });
    $('.page-loader-wrapper').hide();
}
// checkdataloading
function checkdataloading() {
    countloading++;
    if (countloading === 3) {
        $('.page-loader-wrapper').hide();
        countloading = 0;
        loadprofile(); 
    }
    console.log(countloading);
}
function loadinput() {  
    var option_data_emp = {
        url: "/account/getprofile/",
        method: "GET",
        success: function (data) {

            // basic info
            ddlstatus.value(data.status);
            ddlrank.value(data.rank);
            ddlgender.value(data.gender);
            birthdate.value(data.birthDate);
            $('#Basic_Info_Address').val(data.address);
            $('#Basic_Info_Name').val(data.name);
            $('#Basic_Info_GoogleMap').val(data.googleMap);
            $('#Basic_PhoneNumber').val(data.phone);
            $('#Basic_Experience').val(data.experience);
            $('#Basic_Email').val(data.email);
        }
    };
    AjaxCaller(null, option_data_emp); 
  
}

// confirm delete
function confirmdelete(option) {
    swal({
        title: "Bạn có chắc?",
        text: "Xóa " + option.title,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            Delete(option);
        } else {
            swal.close();
        }
    });
}
// delete menu
function Delete(option) {
    var optionajax = {
        url: option.url,
        type: "GET",
        success: function (data) {
            swal({
                title: "Success!",
                text: "Xóa thành công",
                type: "success",
                timer: 2000,
                cancelButtonText: "Close",
            }, function () {
                loadData();
            });

        }
    };
    AjaxCaller("deletebtn", optionajax);
}

function reload_info() {
    //loadcontrol();
    loadinput();
}

// change avatar 
function UpdateAvartar() {
    var btn_ladda = document.getElementById('btn-updateavartar');
    if (btn_ladda !== undefined) {
        $('#btn-updateavartar').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-updateavartar'));
        ladda.start();
    }
    var formData = new FormData();
    var inputfile = document.getElementById('id_service_image');
    if (inputfile !== undefined) {
        var re = inputfile.files;
        if (re.length > 0) {
            for (var e = 0; e !== re.length; e++) {
                formData.append("files", re[e]);
            }
        }
        else {
            shownotify("update avartar", "warning", "Chọn hình mới !!!!");
            setTimeout(function () {
                ladda.stop();
            }, 1000);
            return;
        }
    }
    formData.append("email", $("#AccountData_Email").val());
    formData.append("name", $("#Basic_Info_Name").val());
    formData.append("id", $("#Empid").val());
    var optionajax = {
        url: "/account/updateavartar",
        data: formData,
        success: function (data) {
            defaultSuccess(data);
            setTimeout(function () {
                ladda.stop();
            }, 1000);
        }
    };
    AjaxFormData(optionajax);
}
// update basic info name, birthdate, status, ...
function UpdateBasicInfo() {
    var btn_ladda = document.getElementById('btn-updatebasicinfo');
    if (btn_ladda !== undefined) {
        $('#btn-updatebasicinfo').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-updatebasicinfo'));
        ladda.start();
    }
    var model = {
        ID: parseInt($('#Empid').val()),
        Phone: $('#Basic_PhoneNumber').val(),
        BirthDate: kendo.toString(birthdate.value(), "yyyy-MM-dd"),
        Rank: parseInt(ddlrank.value()),
        Gender: parseInt(ddlgender.value()),
        Status: parseInt(ddlstatus.value()),
        Experience: parseInt($("#Basic_Experience").val()),
        Name: $("#Basic_Info_Name").val(),
        Address: $("#Basic_Info_Address").val(),
        GoogleMap: $("#Basic_Info_GoogleMap").val(),
        Email: $("#Basic_Email").val()
    };
    var optionajax = {
        url: "/Account/UpdateBasicInfo",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
        },
        complete: function () {
            setTimeout(function () {
                ladda.stop();
                loadprofile();
            }, 1000);
        }
    };
    AjaxCaller("btn-updatebasicinfo", optionajax);
}

 
var window_education, FromDateEdu, ToDateEdu, ddlDegreeEdu;
$(document).ready(function () { 
    init_kendogrid_education(); 
});
// kendowindow form update education
function init_kendogrid_education() {
    var control = {
        ID: "grid_edu", 
        columns: [ 
            { field: "name", title: "Trường", width: '100px', filterable: false },
            { field: "specialism", width: '200px', filterable: false, title: "Chuyên ngành", template: function (v) { return v.specialism + " (" + Kendogetyear(v.fromDate) + " - " + Kendogetyear(v.toDate) + ")"; } },
            { field: "degree", title: "Chuyên ngành", width: "180px", editor: degreeDropDownEditor, template: "#=degreename#" },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }
        ],
        editable: "inline"
    };
    grid_edu = initkendogrid(control);
}
// education
function openwindow_education() {
    window_education = $("#window_education"), undo = $("#undo");
    undo.click(function () {
        window_education.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }

    var wd = window_education.kendoWindow({
        width: "310px",
        title: "Bổ sung Học vấn",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow");
    wd.center();
    wd.open();
}
function loadcontrol_edu() {
    FromDateEdu = $("#FromDateEdu").kendoDatePicker({
        value: kendo.date.today(),
        format: "yyyy",
        depth: "year",
        start: "year",
        culture: "vi-VN",
        animation: {
            open: {
                effects: "zoom:in",
                duration: 500
            }
        }
    }).data("kendoDatePicker");
    ToDateEdu = $("#ToDateEdu").kendoDatePicker({
        value: kendo.date.today(),
        format: "yyyy",
        depth: "year",
        start: "year",
        culture: "vi-VN",
        animation: {
            open: {
                effects: "zoom:in",
                duration: 500
            }
        }
    }).data("kendoDatePicker");
    var optionajax_edudegree = {
        url: "/account/geteducationdegree",
        method: "GET",
        success: function (data) {
            ddlDegreeEdu = $('#DegreeEdu').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");

        }
    };
    AjaxCaller(null, optionajax_edudegree);
    kendo.ui.progress(grid_edu.element, true);
}
function reload_edu() {
    loadcontrol_edu();
    loaddata_edu();
}
function loaddata_edu() {
    var datasource = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/account/geteducation",
                },
                update: {
                    url: "/account/EditEducation",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_edu();
                    }
                },
                destroy: {
                    url: "/account/DeleteEducation",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_edu();
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        name: { validation: { required: true } },
                        degree: {
                            defaultValue: { id: 0, text: "Lao động phổ thông" }
                        },
                        specialism: { validation: { required: true } }
                    }
                }
            },
            pageSize: 10
        }
    ); 
    grid_edu.setDataSource(datasource);
    grid_edu.refresh();
    kendo.ui.progress(grid_edu.element, false);
}
function canceledu() {
    window_education.data("kendoWindow").close();
}
// add edu
function addeducation() {
    var btn_ladda = document.getElementById('btn-add-edu');
    if (btn_ladda !== undefined) {
        $('#btn-add-edu').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add-edu'));
        ladda.start();
    }
    var model = {
        FromDate: kendo.toString(FromDateEdu.value(), "yyyy-MM-dd"),
        ToDate: kendo.toString(ToDateEdu.value(), "yyyy-MM-dd"),
        Name: $('#NameEdu').val(),
        Specialism: $('#SpecialismEdu').val(),
        Degree: parseInt(ddlDegreeEdu.value()),
        CreateBy: $('#Basic_Email').val(), 
        EmpID: parseInt($('#Empid').val())
    };
    var optionajax = {
        url: "/account/addeducation",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loaddata_edu();
            setTimeout(function () { ladda.stop(); window_education.data("kendoWindow").close(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-add-edu", optionajax);
}
function degreeDropDownEditor(container, options) {
    $('<input required name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            index: 0,
            value: options.model.id,
            dataTextField: "text",
            dataValueField: "id",
            dataSource: {  transport: { read: {  url: "/account/geteducationdegree"  }  }  }
        });
        
}
var window_experiences;
$(document).ready(function () { 
    init_kendogrid_experience(); 
});
// kendowindow form update experience
function init_kendogrid_experience() {
    var control = {
        ID: "grid_exp",  
        columns: [
            //{ field: "degreename", title: "Loại",width: '100' },
            { field: "name", title: "Công việc", filterable: false, width: '100px', media: "(min-width: 450px)" },
            { field: "office", filterable: false, title: "Nơi làm việc", width: '100px', media: "(min-width: 450px)" }, 
            { field: "year_experiences", title: "Thời gian", filterable: false, width: '100px', template: function (v) { return v.year_experiences + " năm"; }, media: "(min-width: 450px)" },
            { field: "descriptions", filterable: false, title: "Mô tả", width: '100px', media: "(min-width: 450px)" },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }
        ],
        editable: "inline"
    };
    grid_exp = initkendogrid(control);
}
// kendowindow form update experience
function initkendo_window_experience() {
    window_experience = $('#window_experience').kendoWindow({
        width: "500px",
        height: "450px",
        title: "Bổ sung thông tin kinh nghiệm làm việc",
        visible: false,
        actions: [
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow");
    var undo = $("#undo");
    undo.click(function () {
        window_experience.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
}

// experience
function openwindow_experience() {
    window_experiences = $("#window_experiences"), undo = $("#undo");
    undo.click(function () {
        window_experiences.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
    var wd = window_experiences.kendoWindow({
        width: "310px",
        title: "Thêm kinh nghiệm",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow")
    wd.center();
    wd.open();
}
function reload_exp() { 
    var datasource = new kendo.data.DataSource(
        {
            pageSize: 10,
            transport: {
                read: {
                    url: "/account/getexperience",
                },
                update: {
                    url: "/account/EditExperience",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_exp();
                    }
                },
                destroy: {
                    url: "/account/DeleteExperience",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_exp();
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        name: { validation: { required: true } },
                        office: { validation: { required: false } },
                        year_experiences: {
                            type: "number", validation: { min: 0, required: true }
                        },
                        descriptions: { validation: { required: false } },
                    }
                }
            }
        }
    ); 
    grid_exp.setDataSource(datasource);
    grid_exp.refresh();
    kendo.ui.progress(grid_exp.element, false);
} 
function cancel_experiences() {
    window_experiences.data("kendoWindow").close();
}
// add exp
function add_experiences() {
    var btn_ladda = document.getElementById('btn-add-exp');
    if (btn_ladda !== undefined) {
        $('#btn-add-exp').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add-exp'));
        ladda.start();
    }
    var model = { 
        Name: $('#NameExp').val(),
        Office: $('#OfficeExp').val(),
        Descriptions: $('#DescriptionsExp').val(),
        Year_experiences: parseFloat($('#Year_ExperienceExp').val()),
        CreateBy: $('#Basic_Email').val(), 
        EmpID: parseInt($('#Empid').val())
    };
    var optionajax = {
        url: "/account/addexperience",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            reload_exp();
            setTimeout(function () { ladda.stop(); window_experiences.data("kendoWindow").close(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-add-exp", optionajax);
}
 
var window_social, ddlSocialType;
$(document).ready(function () { 
    init_kendogrid_social(); 
    loaddata_social();
});
// kendowindow form update social
function initkendo_window_social() {
    window_social = $('#window_social').kendoWindow({
        width: "500px",
        height: "450px",
        title: "Bổ sung thông tin mạng xã hội",
        visible: false,
        actions: [
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow");
    var undo = $("#undo");
    undo.click(function () {
        window_experience.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
}

// kendowindow form update social
function init_kendogrid_social() {
    var control = {
        ID: "grid_social", 
        columns: [
            //{ field: "degreename", title: "Loại",width: '100' },
            { field: "name", title: "MXH", width: '140px', filterable: false },
            { field: "link", title: "link", width: '200px', filterable: false },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }
        ],
        editable: "inline"
    };
    grid_social = initkendogrid(control);
}
// social
function openwindow_social() {
    window_social = $("#window_social"), undo = $("#undo");
    undo.click(function () {
        window_social.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
    var wd = window_social.kendoWindow({
        width: "320px",
        title: "Bổ sung MXH",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ],
        position: {
            top: 100
        },
        close: onClose
    }).data("kendoWindow");
    wd.center();
    wd.open();
}
function loadcontrol_social() {
    kendo.ui.progress(grid_social.element, true);
    var optionajax_socialdegree = {
        url: "/account/getsocialtype",
        method: "GET",
        success: function (data) {
            ddlSocialType = $('#SocialType').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");

        }
    };
    AjaxCaller(null, optionajax_socialdegree);
}
function reload_social() { 
    loadcontrol_social();
    loaddata_social();
} 
function loaddata_social() {
    var datasource = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/account/getsocial",
                    type: "GET",
                    dataType: "json",
                    complete: function (data) {
                        $('#_Info_Socials').empty();
                        $.each(data.responseJSON, function (i, item) {
                            $('#_Info_Socials').append('<p><a href="' + item.link + '">' + item.link + '</a></p>');
                        });
                    }
                },
                update: {
                    url: "/account/EditSocial",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_social();
                    }
                },
                destroy: {
                    url: "/account/DeleteSocial",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_social();
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        name: { validation: { required: true } },
                        link: { validation: { required: true } }
                    }
                }
            },
            pageSize: 10
        }
    );
     
    grid_social.setDataSource(datasource);
    grid_social.refresh();
    kendo.ui.progress(grid_social.element, false);
 
}
 
function cancel_social() {
    window_social.data("kendoWindow").close();
}
// add social
function add_social() {
    var btn_ladda = document.getElementById('btn-add-social');
    if (btn_ladda !== undefined) {
        $('#btn-add-social').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add-social'));
        ladda.start();
    }
    var model = {
        Name: ddlSocialType.text(),
        Link: $('#LinkSocial').val(), 
        CreateBy: $('#Basic_Email').val(), 
        EmpID: parseInt($('#Empid').val())
    };
    var optionajax = {
        url: "/account/addsocial",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loaddata_social();
            setTimeout(function () { ladda.stop(); window_social.data("kendoWindow").close(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-add-social", optionajax);
}
function removesocial(e) {
    var option = {
        title: e.dataset.title,
        id: e.dataset.id,
        url: "/Account/DeleteSocial?id=" + e.dataset.id
    };
    confirmdelete(option);
}

var window_skill, grid_skill, ddlTypeSkill
$(document).ready(function () { 
    init_kendogrid_skill(); 
});
// kendowindow form update skill
function init_kendogrid_skill() {
    var control = {
        ID: "grid_skill", 
        columns: [ 
            { field: "name", title: "Kĩ năng", width: '150px', filterable: false },
            { field: "pointPercent", title: "% ", width: '100px', filterable: false, template: function (v) { return v.pointPercent + " %"; } },
            { field: "typeSkill", title: "Loại kĩ năng", width: "180px", editor: typeSkillDropDownEditor, template: "#=typeSkillName#" },
            { field: "description", title: "Mô tả ngắn", width: '150px', filterable: false },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
        ],
        editable: "inline"
    };
    grid_skill = initkendogrid(control);
}
// kendowindow form update skill
function initkendo_window_skill() {
    window_skill = $('#window_skill').kendoWindow({
        width: "500px",
        height: "450px",
        title: "Bổ sung thông tin kĩ năng",
        visible: false,
        actions: [
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow");
    var undo = $("#undo");
    undo.click(function () {
        window_skill.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
}

// skill
function openwindow_skill() {
    window_skill = $("#window_skill"), undo = $("#undo");
    undo.click(function () {
        window_skill.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
    var wd = window_skill.kendoWindow({
        width: "320px",
        title: "Bổ sung kĩ năng",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ], 
        close: onClose
    }).data("kendoWindow");
    wd.center();
    wd.open();
}
function loadcontrol_skill() {
    kendo.ui.progress(grid_skill.element, true);
    PointPercent = $('#PointPercent').kendoNumericTextBox({
        format: "n0",
        min: 0,
        max: 100, 
    }).data("kendoNumericTextBox");
    var optionajax_typeSkill = {
        url: "/account/getTypeSkill",
        method: "GET",
        success: function (data) {
            ddlTypeSkill = $('#TypeSkill').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");

        }
    };
    AjaxCaller(null, optionajax_typeSkill);
}
function reload_skill() { 
    loadcontrol_skill()
    loaddata_skill();
}
function loaddata_skill() {
    var datasource = new kendo.data.DataSource(
        { 
            pageSize: 10,
            transport: {
                read: {
                    url: "/account/getskill", 
                },
                update: {
                    url: "/account/EditSkill", 
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_skill();
                    }
                },
                destroy: {
                    url: "/account/deleteskill", 
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_skill();
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        name: { validation: { required: true } },
                        description: { validation: { required: false } },
                        pointPercent: {
                            type: "number", validation: { min: 0, required: true }
                        },
                    }
                }
            }
        }
    );
   
    grid_skill.setDataSource(datasource);
    grid_skill.refresh();
    kendo.ui.progress(grid_skill.element, false);
}
function cancel_skill() {
    window_skill.data("kendoWindow").close();
}
// add skill
function add_skill() {
    var btn_ladda = document.getElementById('btn-add-skill');
    if (btn_ladda !== undefined) {
        $('#btn-add-skill').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add-skill'));
        ladda.start();
    }
    var model = {
        Name: $('#NameSkill').val(),
        PointPercent: parseFloat($('#PointPercent').val()),
        CreateBy: $('#Basic_Email').val(),
        TypeSkill: parseInt(ddlTypeSkill.value()),
        Description: $('#DescriptionSkill').val(),
        EmpID: parseInt($('#Empid').val())
    };
    var optionajax = {
        url: "/account/addskill",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loaddata_skill();
            setTimeout(function () { ladda.stop(); window_skill.data("kendoWindow").close(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-add-skill", optionajax);
}
function customBoolEditor(container, options) {
    $('<input class="k-checkbox" type="checkbox" name="Discontinued" data-type="boolean" data-bind="checked:Discontinued">').appendTo(container);
}
function typeSkillDropDownEditor(container, options) {
    $('<input required name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            index: 0,
            value: options.model.id,
            dataTextField: "text",
            dataValueField: "id",
            dataSource: { transport: { read: { url: "/account/getTypeSkill" } } }
        });

}

var window_specialism, ddlSpecialismID;
$(document).ready(function () { 
    init_kendogrid_specialism(); 
});
// kendowindow form update specialism
function init_kendogrid_specialism() {
    var control = {
        ID: "grid_specialism", 
        columns: [
            { field: "specialismID", title: "Chuyên ngành", width: "180px", editor: specialismDropDownEditor, template: "#=name#" },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }
        ],
        editable: "inline"
    };
    grid_specialism = initkendogrid(control);
}
// specialism
function openwindow_specialism() {
    window_specialism = $("#window_specialism"), undo = $("#undo");
    undo.click(function () {
        window_specialism.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
    var wd = window_specialism.kendoWindow({
        width: "320px",
        title: "Bổ sung chuyên ngành",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow");
    wd.center();
    wd.open();

}
function loadcontrol_specialism() {
     
    var optionajax_ddlspecialism = {
        url: "/account/getlistspecialism",
        method: "GET",
        success: function (data) {
            ddlSpecialismID = $('#ddlSpecialismID').kendoDropDownList({
                dataSource: data,
                filter:"contains",
                dataTextField: "name",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");

        }
    };
    AjaxCaller(null, optionajax_ddlspecialism);
}
function reload_specialism() {
    loadcontrol_specialism();
    loaddata_specialism();
}
function loaddata_specialism() {
      
    var datasource = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/account/getspecialism",
                },
                update: {
                    url: "/account/EditSpecialism",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_specialism();
                    }
                },
                destroy: {
                    url: "/account/DeleteSpecialism",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_specialism();
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        specialismID: {
                            defaultValue: { id: 0, text: "Chọn ngành" }
                        } 
                    }
                }
            },
            pageSize: 10
        }
    );
    //datasource.read();
    grid_specialism.setDataSource(datasource);
    grid_specialism.refresh(); 
    kendo.ui.progress(grid_specialism.element, false);
}
function cancelspecialism() {
    window_specialism.data("kendoWindow").close();
}
// add specialism
function addspecialism() {
    var btn_ladda = document.getElementById('btn-add-specialism');
    if (btn_ladda !== undefined) {
        $('#btn-add-specialism').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add-specialism'));
        ladda.start();
    }
    var model = {
        SpecialismID: parseInt(ddlSpecialismID.value()),
        CreateBy: $('#Basic_Email').val(), 
        EmpID: parseInt($('#Empid').val())
    };
    var optionajax = {
        url: "/account/addspecialism",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loaddata_specialism();
            setTimeout(function () { ladda.stop(); window_specialism.data("kendoWindow").close(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-add-specialism", optionajax);
}
function specialismDropDownEditor(container, options) {
    $('<input required name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            index: 0,
            value: options.model.id,
            dataTextField: "name",
            dataValueField: "id",
            dataSource: { transport: { read: { url: "/account/getlistspecialism" } } }
        });

}