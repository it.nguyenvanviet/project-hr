﻿var grammar = '#JSGF V1.0;'
 
var SpeechRecognition = SpeechRecognition || webkitSpeechRecognition
var SpeechGrammarList = SpeechGrammarList || webkitSpeechGrammarList
var SpeechRecognitionEvent = SpeechRecognitionEvent || webkitSpeechRecognitionEvent
var recognition = new SpeechRecognition();
var txt_inputRecornize = 'input';
var diagnostic = document.querySelector(txt_inputRecornize);
var speechRecognitionList = new SpeechGrammarList();
speechRecognitionList.addFromString(grammar, 1);
recognition.grammars = speechRecognitionList;
recognition.continuous = false;
recognition.lang = 'vi-VN';
recognition.interimResults = false;
recognition.maxAlternatives = 1;


 
recognition.onresult = function (event) {
    var text_rs = event.results[0][0].transcript;
    diagnostic.value = text_rs;
    // console.log(text_rs)
}; 
function recognizeName(txt_input) {
    recognition.start();
    diagnostic = document.querySelector(txt_input);
    // console.log('Ready to receive a command.');
};
recognition.onspeechend = function () {
    recognition.stop();
};