﻿var GridlistMenu;
var WindowFormAddorUpdate;
var dataListMenu = [];
var datatypemenu = [];
var ddlStatus;
var ddlType;
var listroleid; 
$(document).ready(function () {

    // loaddata();  
    loadControl();
    initKendoGridListMenu();
    loadRole();

   
}); 

// 
function initKendoGridListMenu() {

    var control = {
        ID: "GridlistMenu",
        columns: [
            {
                field: "name",
                title: "Danh mục",width:200
            },
            {
                field: "type",
                title: "Loại", width: 120,
                template: function (val) {
                    var html = "";
                    switch (val.type) {
                        case 0: html = "HR"; break;
                        case 1: html = "Project"; break;
                        case 2: html = "Setting"; break;
                    }
                    return html;
                }
            }, 
            {
                field: "link", width: 200,
                title: "Link"
            },
            {
                field: "status", width: 150,
                title: "Tình trạng", 
                template: function (val) {
                    var htmlstatus = "";
                    switch (val.status) {
                        case 0: htmlstatus = "<span class=\"badge badge-danger\">Ẩn</span> "; break;
                        case 1: htmlstatus = "<span class=\"badge badge-success\">Kích hoạt</span> "; break;
                        case 2: htmlstatus = "<span class=\"badge badge-warning\">Không hoạt động</span> "; break;
                    }
                    return htmlstatus;
                }
            },
            {
                field: "id", width: 170,
                title: "Thao tác",
                attributes: {
                    style: "text-align: center;"
                },
                headerAttributes: {
                    style: "text-align: center;"
                },
                template: function (val) {
                    var htmlaction = "";
                    htmlaction += '<a href="javascript:void(0);" onclick="ViewRole(' + val.id + ')" id="viewrole_' + val.id + '" data-toggle="modal" data-target="#RoleModal" class="btn btn-sm btn-outline-primary"><i class="icon-users"></i></a> ';
                    htmlaction += '<a href="javascript:void(0);" onclick="ViewUpdate(' + val.id + ')" id="edit_' + val.id + '" data-toggle="modal" data-target="#Editmodel" class="ladda-button btn btn-sm btn-outline-warning"><i class="icon-pencil"></i></a> ';
                    htmlaction += '<a href="javascript:void(0);" id="remove_' + val.id + '" class="ladda-button btn btn-sm btn-outline-danger deletebtn"  title="Delete" onclick="Viewdelete(this)" data-type="confirm" data-title="' + val.name + '" data-id="' + val.id + '"> <i class="icon-trash"></i></a>';
                    return htmlaction;
                }
            }
        ]
    };
    GridlistMenu = initkendogrid(control);
    loadData();
}
// load data list menu
function loadData() {
   
    var optionajax = {
        url: "/Menu/Getall",
        method: "GET",
        success: function (rs) {
            var datasource = new kendo.data.DataSource({ data: rs, pageSize: 8 });
            datasource.read();
            GridlistMenu.setDataSource(datasource);
            GridlistMenu.refresh();
        }
    };
    AjaxCaller(null, optionajax);
}
// load control
function loadControl() {
    // load status
    var dataStatus = [
        { "ID": 0, "Text": "Không hoạt động" },
        { "ID": 1, "Text": "Bình thường" },
        { "ID": 2, "Text": "Tạm dừng" },
        { "ID": 3, "Text": "Đóng" }];
    ddlStatus = $('#Status').kendoDropDownList({
        dataSource: dataStatus,
        dataTextField: "Text",
        dataValueField: "ID",
        index: 0
    }).data("kendoDropDownList");

    var optionajax = {
        url: "/Menu/Getmenutypes",
        method: "GET",
        success: function (rs) {
            ddlType = $('#Type').kendoDropDownList({
                dataSource: rs,
                dataTextField: "name",
                dataValueField: "type",
                index: 0
            }).data("kendoDropDownList");
        }
    };
    AjaxCaller(null, optionajax);
}

function loadRole() {
    var optionajax = {
        url: "/Menu/Getlistrole",
        method: "GET",
        success: function (data) {
            if (data.length > 0) {
                listroleid = $('#listroleid').kendoMultiSelect({
                    dataTextField: "name",
                    dataValueField: "id",
                    dataSource: data
                }).data("kendoMultiSelect");
                 
                return false;
            }
        }
    };
    AjaxCaller(null, optionajax);
}
// update postionmenu
function UpdatePosition() {
    var btn_ladda = document.getElementById('btn_updateposition');
    if (btn_ladda !== undefined) {
        $('#btn_updateposition').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn_updateposition'));
        ladda.start();
    }
    var model = {
        list: $('#list_menu_position').val()
    };
    var optionajax = {
        url: "/Manager/Menu/UpdatePosition",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            setTimeout(function () { ladda.stop(); }, data.elapsed);
        }
    };
    AjaxCaller("btn_updateposition", optionajax);

}
// view update
function ViewUpdate(id) {
    clearform();
    var btn_ladda = document.getElementById('#edit_' + id);
    if (btn_ladda !== undefined) {
        $('#edit_' + id).attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#edit_' + id));
        ladda.start();
    }

    var optionajax = {
        url: "/Menu/Getbyid/" + id,
        method: "GET",
        success: function (data) {
            WindowFormAddorUpdate.data("kendoWindow").setOptions({ title: "Điều chỉnh Menu" });
            $('#ID').val(data.id);
            $('#Name').val(data.name);
            $('#Link').val(data.link);
            $('.note-editable').html(data.content);
            $('#Content').text(data.content);
            ddlType.value(data.type);
            ddlStatus.value(data.status);
            setTimeout(function () { ladda.stop(); }, 2000);
            $('.k-window-titlebar').html(function (i, h) {
                return h.replace(/&nbsp;/g, '');
            });
        }
    };
    AjaxCaller('#edit_' + id, optionajax);
    $('#btn-add').hide();
    $('#btn-update').show();
    openAddorUpdateView();
}
// AddMenu
function AddMenu() {
    var btn_ladda = document.getElementById('btn-add');
    if (btn_ladda !== undefined) {
        $('#btn-add').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add'));
        ladda.start();
    }
    var model = { 
        ID: 0,
        Name: $('#Name').val(),
        Link: $('#Link').val(),
        Content: $('.note-editable').html(),  
        Type: parseInt(ddlType.value()),
        IconClass: "",
        Sort: 0,
        ParentID: 0,
        Status: parseInt(ddlStatus.value())
    };
    var optionajax = {
        url: "/Menu/Create", 
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loadData(); 
            setTimeout(function () { ladda.stop(); WindowFormAddorUpdate.data("kendoWindow").close(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-add", optionajax);
}
// Update 
function UpdateMenu() {
    var btn_ladda = document.getElementById('btn-update');
    if (btn_ladda !== undefined) {
        $('#btn-update').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-update'));
        ladda.start();
    }
    var model = {
        ID: parseInt($('#ID').val()),
        Name :$('#Name').val(),
        Link :$('#Link').val(),
        Content: $('.note-editable').html(), // $('#Content').text(),
        Type: parseInt(ddlType.value()),
        IconClass: "",
        Sort: 0,
        ParentID:0,
        Status: parseInt(ddlStatus.value())
    };
    var optionajax = {
        url: "/Menu/Update",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loadData();
            
            setTimeout(function () {
                ladda.stop();
                WindowFormAddorUpdate.data("kendoWindow").close();
            }, data.elapsed);
        }
    };
    AjaxCaller("btn-update", optionajax);
}
// cancel add or update, close modal
function clearform() {
    $('input').val('');
    $('textarea').val('');
}
// get id menu to update
function Viewdelete(e) {
    var option = {
        title: e.dataset.title,
        id: e.dataset.id
    };
    confirmdelete(option);
}

// view listrole menu
function ViewRole(id) {
    $('#MenuID').val(id);
    var optionajax = {
        url: "/Menu/Getlistrolenbyid/"+id,
        method: "GET",
        success: function (data) {
            if (data.status > 0) {
                var listroledata = new Array();
                $.each(data.result, function (id, name) { listroledata.push(name); });
                listroleid.value(listroledata);
                return false;
            }
        }
    };
    AjaxCaller(null, optionajax);

}

// update role
function UpdateRoles() {
    var btn_ladda = document.getElementById('btn-updaterole');
    if (btn_ladda !== undefined) {
        $('#btn-updaterole').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-updaterole'));
        ladda.start();
    }
    var model = {
        ID: parseInt($('#MenuID').val()),
        listroleid: listroleid.value()
    };
    var optionajax = {
        url: "/Menu/UpdateRole",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data); 
            $('#RoleModal').modal('hide');
            setTimeout(function () { ladda.stop(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-updaterole", optionajax);
}

// confirm delete
function confirmdelete(option) {
    swal({
        title: "Bạn có chắc?",
        text: "Xóa !" + option.title,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            Delete(option);
        } else {
            swal.close();
        }
    });
}

// delete menu
function Delete(option) {
    var optionajax = {
        url: "/Menu/Delete?id=" + parseInt(option.id),
        type: "GET",
        success: function (data) {
            swal({
                title: "Success!",
                text: "Xóa thành công",
                type: "success",
                timer: 2000,
                cancelButtonText: "Close",
            }, function () {
                 loadData();
            });

        }
    };
    AjaxCaller("deletebtn", optionajax);
}

// open Add or Update form
function openAddorUpdateView() {
    
    WindowFormAddorUpdate = $("#window"), undo = $("#undo");
    undo.click(function () {
        WindowFormAddorUpdate.data("kendoWindow").open();
        undo.fadeOut();
    });

    function onClose() {
        undo.fadeIn();
    }

    WindowFormAddorUpdate.kendoWindow({
        title: "Thêm mới Menu",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow").center().open();
}

function ViewAdd() {
    $('#btn-add').show();
    $('#btn-update').hide();

    $('#Name').val('');
    $('#Link').val('');
    $('#Content').val('');
    $('.note-editable').html(''); 
    openAddorUpdateView();
    $('.k-window-titlebar').html(function (i, h) {
        return h.replace(/&nbsp;/g, '');
    });
}

function loadNestable() {
    var tarr = [{ "id": 4, "children": [{ "id": 3 }] }];
    var list = listify(tarr);
    $("#test").append(list);
}

function initNestable(strarr) {
    var l = $("<ol>").addClass("dd-list");
    $.each(strarr, function (i, v) {
        var c = $("<li>").addClass("dd-item"),
            h = $("<div>").addClass("dd-handle").text("Item " + v["id"]);
        l.append(c.append(h));
        if (v["children"] !== null)
            c.append(listify(v["children"]));
    });
    return l;
}

function closeform() {
    WindowFormAddorUpdate.data("kendoWindow").close();
    clearform();
};