﻿var WindowNesttable;


function expandall() {
    $('.dd').nestable('expandAll');
}
function collapseall() {
    $('.dd').nestable('collapseAll');
}

// update postionmenu
function UpdatePosition() {
    var btn_ladda = document.getElementById('btn_updateposition');
    if (btn_ladda !== undefined) {
        $('#btn_updateposition').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn_updateposition'));
        ladda.start();
    }
    var datalistmenu = $('#list_menu_position').val();
    if (datalistmenu === "") {
        shownotify("", "warning", "Không có sự thay đổi vị trí của menu");
        ladda.stop();
        return; 
    }
    var model = {
        list: datalistmenu
    };
    var optionajax = {
        url: "/Menu/UpdatePosition",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            setTimeout(function () {
                ladda.stop();
            }, data.elapsed);
            WindowNesttable.data("kendoWindow").close();
        }
    };
    AjaxCaller("btn_updateposition", optionajax); 
}

// AjaxCaller to submit data
function LoadPosition() {
    var optionajax = {
        url: "/Menu/Getposition",
        method: "GET",
        success: function (data) {
            $('.dd-list').empty(); 
            $('.dd-list').append(nestablebuilder(data.result, ''));
            collapseall();
        }
    };
    AjaxCaller("btn_updateposition", optionajax); 
}

// Use AjaxCaller to get listdata
function nestablebuilder(data) {

    var html = "";
    $.each(data, function (i, item) {
        if (item.children.length > 0) {
            html += '<li class="dd-item" data-id="' + item.id + '"> <div class="dd-handle">' + item.name + '</div> <ol class="dd-list">';
            html +=  nestablebuilder(item.children);
            html += '</ol> </li>';
        }
        else {
            html += '<li class="dd-item" data-id="' + item.id + '" > <div class="dd-handle">' + item.name + ' </div></li>';
        }
    });  
    return html;
}

// open Add or Update form
function openPositionForm() {
    LoadPosition();
    
    WindowNesttable = $("#nestable"), undo = $("#undo");
    undo.click(function () {
        WindowNesttable.data("kendoWindow").open();
        undo.fadeOut();
    });

    function onClose() {  undo.fadeIn(); }

    WindowNesttable.kendoWindow({
        width: "350px",
        height:"450px",
        title: "Điều chỉnh vị trí Menu",
        visible: false,
        actions: [ 
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow").center().open();
    collapseall();

    $('.k-window-titlebar').html(function (i, h) {
        return h.replace(/&nbsp;/g, '');
    });
}

