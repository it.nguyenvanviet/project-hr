﻿var ddldepartmentSearch, ddlStatusSearch, ddlEmpSearch;
var teamStatus, teamDepart, teamLeader, teamMember;

$(document).ready(function () {
    loadOptionTeamFunction();
    $('#showddl_leader').hide();
    $('#showddl_teammember').hide();
    $('#showddl_formexample').hide();
});
// load data 
function loadOptionTeamFunction() {
    // load danh sách nhân viên theo phòng ban để tìm kiếm team
    ddlEmpSearch = $('#ddlEmpSearch').kendoDropDownList({
        optionLabel: "----- Chọn nhân viên -----",
        dataTextField: "name",
        dataValueField: "id"
    }).data("kendoDropDownList");
    // load ds nhân viên theo phòng ban để làm leader trong team 
    teamLeader = $('#teamLeader').kendoDropDownList({
        optionLabel: "----- Chọn nhân viên -----",
        dataTextField: "name",
        dataValueField: "id",
        change: onchange_teamlead,
        index: 0
    }).data("kendoDropDownList");
    // load danh sách nhân viên theo phòng ban để làm member trong team
    teamMember = $('#teamMember').kendoMultiSelect({
        dataSource: [],
        optionLabel: "----- Chọn nhân viên -----",
        change: onchange_addteammember,
        dataTextField: "name",
        dataValueField: "id",
        footerTemplate: 'Total #: instance.dataSource.total() # items found',
        template: '<span class="k-state-default" style="background-image: url(\'#:data.avartar#\')"></span>' +
            '<span class="k-state-default">#: data.name #</span>'
    }).data("kendoMultiSelect");


    // load danh sách tình trạng
    var optionAjaxLoadStatus = {
        url: "/team/getstatus",
        method: "GET",
        success: function (rs) {
            ddlStatusSearch = $('#ddlStatusSearch').kendoDropDownList({
                dataSource: rs,
                dataTextField: "text",
                dataValueField: "id",
                index: 1
            }).data("kendoDropDownList");

            teamStatus = $('#teamStatus').kendoDropDownList({
                dataSource: rs,
                dataTextField: "text",
                dataValueField: "id",
                index: 1
            }).data("kendoDropDownList");
        }
    };
    AjaxCaller(null, optionAjaxLoadStatus);

    // load danh sách phòng ban
    var optionAjaxLoadDepartment = {
        url: "/department/getlist",
        method: "GET",
        success: function (rs) {
            ddldepartmentSearch = $('#ddlDepartmentSearch').kendoDropDownList({
                dataSource: rs,
                optionLabel: "-----Chọn bộ phận ---",
                dataTextField: "departmentName",
                dataValueField: "id",
                index: 0,
                change: onchange_ddldepartment,
                filter: 'contains'
            }).data("kendoDropDownList");
            teamDepart = $('#teamDepart').kendoDropDownList({
                dataSource: rs,
                optionLabel: "-----Chọn bộ phận---",
                dataTextField: "departmentName",
                dataValueField: "id",
                index: 0,
                change: onchange_teamDepart,
                filter: 'contains'
            }).data("kendoDropDownList");
        }
    };
    AjaxCaller(null, optionAjaxLoadDepartment);
}
function onchange_ddldepartment(e) {
    var value = this.value();
    var optionAjaxLeader = {
        url: "/team/getlistleader?departid=" + value,
        method: "GET",
        success: function (rs) {
            var dataSource = new kendo.data.DataSource({ data: rs });
            ddlEmpSearch.setDataSource(dataSource);
        }
    };
    AjaxCaller(null, optionAjaxLeader);
}
function onchange_teamDepart(e) {
    var value = this.value();
    var optionAjaxLeader = {
        url: "/team/getlistleader?departid=" + value,
        method: "GET",
        success: function (rs) {
            var dataSource = new kendo.data.DataSource({ data: rs });
            teamLeader.setDataSource(dataSource);
            if (rs.length > 0) {
                $('#showddl_leader').show();
            }
        }
    };
    AjaxCaller(null, optionAjaxLeader);

    var optionAjaxMember = {
        url: "/team/getlistmember?departid=" + value,
        method: "GET",
        success: function (rs) {
            var dataSource = new kendo.data.DataSource({ data: rs });
            teamMember.setDataSource(dataSource);
            if (rs.length > 0) {
                $('#showddl_teammember').show();
            }
        }
    };
    AjaxCaller(null, optionAjaxMember);
}
function onchange_teamlead(e) {
    var value = parseInt(this.value());
    var item = teamLeader.dataSource._data.find(x => x.id === value);
    $('#leader_image').attr('src', item.avartar);
    $('#leader_name').empty().text(item.name);
    teamcount();

}
function onchange_addteammember(e) {
    var value = this.value();
    var html = '';
    $('#list_team_member').empty();
    for (var i = 0; i < value.length; i++) {
        var item = teamMember.dataSource._data.find(x => x.id === parseInt(value[i]));
        $('#list_team_member').append('<li><img src="' + item.avartar + '" data-toggle="tooltip" data-placement="top" title="" alt="Avatar" data-original-title="' + item.name + '"></li>');
    }
    $('[data-toggle="tooltip"]').tooltip();
    teamcount();
}
function teamcount() {
    $('#showddl_formexample').show();
    $('#teammember_count').empty().text('1 Leader, ' + teamMember.value().length + ' member');
}
// search team
function SearchTeam() {
    var btn_ladda = document.getElementById('btn-searchteam');
    if (btn_ladda !== undefined) {
        $('#btn-searchteam').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-searchteam'));
        ladda.start();
    }
    var model = {
        TeamName: $('#TeamNameSearch').val(),
        EmpID: parseInt(ddlEmpSearch.value()),
        Status: parseInt(ddlStatusSearch.value()),
        DepartID: parseInt(ddldepartmentSearch.value())
    };

    var optionAjaxSearchTeam = {
        url: "/Team/Search",
        data: JSON.stringify(model),
        success: function (data) {
            // defaultSuccess(data);
            loadteams(data);
        },
    };
    AjaxCaller('btn-searchteam', optionAjaxSearchTeam);
    ladda.stop();
}
function loadteams(data) {
    $('#listteams').empty();
    for (var i = 0; i < data.length; i++) {
        var item = data[i];
        var html = '<div class="col-lg-3 col-md-6 col-sm-12">';
        html += '<div class="card" >';
        html += '<div class="header"><h2>' + item.name + '</h2></div > ';
        html += '    <div class="body text-left">';
        html += '         <div class="row align-items-center team-lead">';
        for (var y = 0; y < item.members.length; y++)
        {
            var t = item.members[y];
            if (t.teamType === 2) {
                html += '<div class="col-lg-5 col-md-6 col-sm-12">';
                html += '<img class="rounded-circle img-thumbnail mx-auto d-block mb-2 leader_image" src="' + t.avartar + '" alt="">';
                html += '</div>';
                html += '<div class="col-lg-7 col-md-6 col-sm-12">';
                html += '<h6 class="project-title text-primary mb-2 font-13">' + t.memberName + '</h6>';
                html += '<p class="font-11">Leader</p>';
                html += '</div>';
            }
        }

        html += '          </div>';
        html += ' <div class="mb-3 mt-3 team-language">  <span class="badge badge-default">' + item.departmentName + '</span>  </div>';
        html += '   <div class="align-items-center d-flex">';
        html += '   <span class="mb-0 mr-2">Nhân viên :</span>';
        html += '   <ul class="list-unstyled team-info margin-0">';
        var countmem = 0;
        for (var j = 0; j < item.members.length; j++) {
            var x = item.members[j];
            if (x.teamType === 1 && x.memberStatus === 1) {
                html += '<li><img src="' + x.avartar + '" data-toggle="tooltip" data-placement="top" title="" alt="Avatar" data-original-title="' + x.memberName + '"></li>';
                countmem = countmem + 1;
            }
        }
        html += '   </ul>';
        html += '   </div><p class="pt-3 mb-0 font-16"><small>1 Lead, ' + countmem + ' Employees</small></p>';
        html += '<div class="modify_team">';
        html += '  <button type="button" class="btn btn-warning" onclick="loadviewEditTeam(' + item.id +')" data-toggle="tooltip" data-placement="top" data-original-title="Điều chỉnh" ><span class="sr-only">Edit</span> <i class="fa fa-cogs"></i></button > ';
        //html += '  <button type="button" class="btn btn-danger" onclick="loadviewEditTeam(' + item.id +')" title="Delete" ><span class="sr-only">Delete</span> <i class="fa fa-trash-o"></i></button>';
        html += '</div>';
        html += '</div>';

        html += '</div>';
        html += '</div>';
        $('#listteams').append(html);
    }
    $('[data-toggle="tooltip"]').tooltip();
}
// function
function OpenFormAddteam() {
    $('#btn-edit').hide();
}
function AddTeam() {

    var btn_ladda = document.getElementById('btn-add');
    if (btn_ladda !== undefined) {
        $('#btn-add').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add'));
        ladda.start();
    }
    var model = {
        LeaderEmpID: parseInt(teamLeader.value()),
        MemberEmpIDs: teamMember.value(),
        TeamName: $('#teamName').val(),
        Description: $('#Description').val(),
        Status: parseInt(teamStatus.value()),
        DepartID: parseInt(teamDepart.value()),
        CreateBy: "",
    };
    var optionajax = {
        url: "/team/Add",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loaddata();
            $('#openformadd').modal('hide');
        },
    };
    console.log(model)
    AjaxCaller("btn-add", optionajax);
    ladda.stop();
}
function CloseFormAddTeam() {
    $('#TeamName').val('');
    teamStatus.value(1);
    teamDepart.value(0);
    teamLeader.value(0);
    teamMember.value([]);
    $('#showddl_leader').hide();
    $('#showddl_teammember').hide();
    $('#showddl_formexample').hide();
}
function loadviewEditTeam(id) {
    CloseFormAddTeam();
    $('#btn-edit').show();
    $('#btn-add').hide();
    var optionAjaxGetTeamByID = {
        url: "/Team/GetByID/"+id,
        method:"Get",
        success: function (data) { 
            loadteambyid(data);
        },
    };
    AjaxCaller(null, optionAjaxGetTeamByID);
}
function loadteambyid(data) {

    $('#IDTeam').val(data.id);
    $('#openformadd').modal('show');
    $('#teamName').val(data.name);
    $('#Description').val(data.descriptions);
    teamStatus.value(data.status);
    teamDepart.value(data.departID);
    teamDepart.trigger("change");
    $('#showddl_leader').show();
    $('#showddl_teammember').show();
    $('#showddl_formexample').show();
    $('#list_team_member').empty();
    var countmem = 0;
    var listmemid = [];
    setTimeout(function () {
        for (var y = 0; y < data.members.length; y++) {

            var t = data.members[y];
            if (t.teamType === 2) {
                $('#leader_image').attr('src', t.avartar);
                $('#leader_name').empty().text(t.memberName);
                teamLeader.value(t.empID);
            }
        }

        for (var j = 0; j < data.members.length; j++) {
            var x = data.members[j];
            if (x.teamType === 1) {
                $('#list_team_member').append('<li><img src="' + x.avartar + '" data-toggle="tooltip" data-placement="top" title="" alt="Avatar" data-original-title="' + x.memberName + '"></li>');
                countmem = countmem + 1;
                listmemid.push(x.empID);
            }
        }
        teamMember.value(listmemid);
        $('[data-toggle="tooltip"]').tooltip();
        $('#teammember_count').empty().append('<p class="pt-3 mb-0 font-16"><small>1 Lead, ' + countmem + ' Employees</small></p>');
    }, 1000);
}
function EditTeam() {
    var btn_ladda = document.getElementById('btn-edit');
    if (btn_ladda !== undefined) {
        $('#btn-edit').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-edit'));
        ladda.start();
    }
    var model = {
        ID: parseInt($('#IDTeam').val()),
        LeaderEmpID: parseInt(teamLeader.value()),
        MemberEmpIDs: teamMember.value(),
        TeamName: $('#teamName').val(),
        Description: $('#Description').val(),
        Status: parseInt(teamStatus.value()),
        DepartID: parseInt(teamDepart.value()),
        UpdateBy: "",
    };
    var optionajax = {
        url: "/team/Update",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            // loaddata();
            $('#openformadd').modal('hide');
        },
    };
    console.log(model)
    AjaxCaller("btn-edit", optionajax);
    ladda.stop();
}
function Delete()
{
    var btn_ladda = document.getElementById('btn-delete');
    if (btn_ladda !== undefined) {
        $('#btn-delete').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-delete'));
        ladda.start();
    }
    var model = {
        LeaderEmpID: parseInt(teamLeader.value()),
        MemberEmpIDs: teamMember.value(),
        TeamName: $('#teamName').val(),
        Description: $('#Description').val(),
        Status: parseInt(teamStatus.value()),
        DepartID: parseInt(teamDepart.value()),
        CreateBy: "",
    };
    var optionajax = {
        url: "/team/delete",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loaddata();
            $('#openformadd').modal('hide');
        },
    };
    console.log(model)
    AjaxCaller("btn-delete", optionajax);
    ladda.stop();
}