﻿var ddlProjectPriority, ddlProjectType, ddlProjectStatus;

$(document).ready(function () {
    loadOptionProject();
}); 
// load data list menu
function loadOptionProject() {
    var optionajax = {
        url: "/Project/getOptionProjectSearch",
        method: "GET",
        success: function (rs) {
            ddlProjectPriority = $('#ddlProjectPriority').kendoDropDownList({
                dataSource: rs.projectPriority,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
            ddlProjectType = $('#ddlProjectType').kendoDropDownList({
                dataSource: rs.projectType,
                dataTextField: "name",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
            ddlProjectStatus = $('#ddlProjectStatus').kendoDropDownList({
                dataSource: rs.projectStatus,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        }
    };
    AjaxCaller(null, optionajax);
}
function SearchProject() {
    $('#formSearch').hide();
    $('#listProject').show();
}