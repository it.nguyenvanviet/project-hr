﻿var griddepartment;

$(document).ready(function () {
    init_kendogrid_department();
    loaddata();
});
function loaddata() {
    var datasource = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/department/getlist",
                },
                update: {
                    url: "/Department/Edit",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        defaultSuccess(data.responseJSON);
                        loaddata();
                    }
                },
                destroy: {
                    url: "/department/delete",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                         
                        defaultSuccess(data.responseJSON);
                        loaddata();
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        departmentCode: { validation: { required: true } },
                        departmentName: { validation: { required: true } },
                        status: { validation: { required: true } }
                    }
                }
            },
            pageSize: 10
        }
    );
    griddepartment.setDataSource(datasource);
    griddepartment.refresh();
    kendo.ui.progress(griddepartment.element, false);
}
// kendowindow form update education
function init_kendogrid_department() {
    var control = {
        ID: "griddepartment",
        columns: [
            { field: "departmentName", title: "Tên phòng ban", width: "200px" },
            { field: "departmentCode", width: '100px', filterable: false, title: "Mã phòng ban" },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }
        ],
        editable: "inline"
    };
    griddepartment = initkendogrid(control);
}
function AddDepart() {
    var btn_ladda = document.getElementById('btn-add');
    if (btn_ladda !== undefined) {
        $('#btn-add').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add'));
        ladda.start();
    }
    var model = {
        departmentName: $('#departmentName').val(),
        departmentCode: $('#departmentCode').val(),
        Status: 1,
        CreateBy : ""
    };
    var optionajax = {
        url: "/Department/Add",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loaddata();
            $('#openformadd').modal('hide');
        }, 
        
    };
    AjaxCaller("btn-add", optionajax);
    ladda.stop();
}