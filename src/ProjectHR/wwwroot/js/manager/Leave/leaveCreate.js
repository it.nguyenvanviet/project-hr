﻿
// Add
function OpenViewAdd() {
    $('#FormModal').modal('show');
}
function CreateLeaveRequest() {
    var check = validateFormAddLeave();
    if (check === true) {
        var btn_ladda = document.getElementById('btn-add');
        if (btn_ladda !== undefined) {
            $('#btn-add').attr('disabled', 'disabled');
            var ladda = Ladda.create(document.querySelector('#btn-add'));
            ladda.start();
        }
        swal({
            title: "Bạn có chắc !!!",
            text: "Tạo đơn nghỉ phép?",
            showCancelButton: true,
            confirmButtonColor: "#dc3545",
            confirmButtonText: "Đồng ý",
            cancelButtonText: "Không đồng ý",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {


                var noofday = 0;
                var _f = DatePicker.value(), _t = DatePicker.value();
                switch (choosetime) {
                    case 1: { noofday = 0; _f = DateRange._range.start; _t = DateRange._range.end }; break;
                    case 2: { noofday = 1;  }; break;
                    case 3: { noofday = 0.5 };; break;
                    case 4: { noofday = 0.5 };; break;
                }
                var model = {
                    ListEmpID: ddlEmpID.value(),
                    Status: 1,
                    NoofDay: noofday,
                    LeaveTime: 0,
                    LeaveType: parseInt(ddlLeaveType.value()),
                    fromDate: _f,
                    toDate: _t,
                    Description: $('#Description').val(),
                    CreateBy: ""
                };
                var optionajax = {
                    url: "/Leave/Add",
                    method: "POST",
                    data: JSON.stringify(model),
                    success: function (data) {
                        //defaultSuccess(data);
                        if (data.status > 0) {
                            swal("Tạo thành công!", "Đã tạo đơn phép thành công", "success");
                            setTimeout(function () { ladda.stop(); }, data.elapsed);
                            $('#FormModal').modal('show');
                        }
                        else {
                            shownotify("Thêm mới đơn phép", "warning", "Lỗi hệ thống !");
                        }
                    }
                };
                AjaxCaller("btn-add", optionajax);


                //

            } else {
                swal.close();
            }
        });
    }
}
function validateFormAddLeave() {
    var isCheck = true;
    if (ddlEmpID.value().length === 0) { shownotify("Thêm mới đơn phép", "warning", "Chưa chọn nhân viên "); isCheck = false; }
    if (choosetime === 1) {
        var range = DateRange._range;
        if (range === null || range === undefined) { shownotify("Thêm mới đơn phép", "warning", "Chưa chọn ngày nghỉ"); isCheck = false };
    }
    if (ddlLeaveType.value() === "") { shownotify("Thêm mới đơn phép", "warning", "Chưa chọn loại đơn phép "); isCheck = false; }
    if ($('#Description').val().trim() === "") { shownotify("Thêm mới đơn phép", "warning", "Chưa nhập lý do "); isCheck = false; }

    return isCheck;

}
function CloseForm() {

}
function CallBackChange() {
    switch (this.element.attr("id")) {
        case "DateRange": {
            break;
        }
    }
}

function choosetimeleave(type) {
    $('#few_day').addClass("btn-outline-primary").removeClass("btn-primary");
    $('#one_day').addClass("btn-outline-primary").removeClass("btn-primary");
    $('#morning').addClass("btn-outline-primary").removeClass("btn-primary");
    $('#afternoon').addClass("btn-outline-primary").removeClass("btn-primary");
    $('#chooseinday').hide();
    choosetime = type;
    switch (type) {
        case 1: {
            $('#few_day').removeClass("btn-outline-primary").addClass("btn-primary");
            $('#choosefewday').show();
            $('#chooseinday').hide();

        }; break;
        case 2: {
            $('#one_day').removeClass("btn-outline-primary").addClass("btn-primary");
            $('#choosefewday').hide();
            $('#chooseinday').show();
        }; break;
        case 3: {
            $('#morning').removeClass("btn-outline-primary").addClass("btn-primary");
            $('#choosefewday').hide();
            $('#chooseinday').show();
        }; break;
        case 4: {
            $('#afternoon').removeClass("btn-outline-primary").addClass("btn-primary");
            $('#choosefewday').hide();
            $('#chooseinday').show();
        } break;
    }
}
function selectallEmpID() {
    var values = $.map(ddlEmpID.dataSource.data(), function (dataItem) {
        return dataItem.id;
    });
    ddlEmpID.value(values);
};
function deselectallEmpID() {
    ddlEmpID.value([]);
};