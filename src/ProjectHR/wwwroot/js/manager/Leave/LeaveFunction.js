﻿var ddlLeaveTypeSearch, ddlleaveStatusSearch, ddlApprovedStatusSearch, ddlEmpID, choosetime, ddlApprovedStatusData;
var DatePicker, DateRangeSearch, DateRange;
var currentDate = kendo.date.today();
var url_loadDepartment          = "/department/getlist",
    url_loadEmpByDepartment     = "/team/getlistmember",
    url_SearchLeaveRequest      = "/leave/searchrequest",
    url_LoadLeaveStatus         = "/leave/getLeaveStatus",
    url_LoadLeaveType           = "/leave/getLeaveType",
    url_LoadLeaveApprovedStatus = "/leave/getApproveStatus",
    url_LeaveCountLeave         = "/leave/getCountLeaveRequest",
    url_changeApproved          = "/leave/changeApproved";

$(document).ready(function () {
    initKendoGridList();
    loadDatacountLeave();
    loadcontrol(); 
    choosetimeleave(1);
    $('#showddl_employee').hide();
  
});
var listroleid = [];
function initKendoGridList() {
    var control = {
        ID: "Gridlist",
        columns: [
            {
                template: function (val) {
                    var tempimage = val.avartar;
                    return "<div class='emp-photo'" +
                        "style='background-image: url(" + val.url_avatar + ");'></div>" +
                        " <div class='emp-name'> " + val.name + "</div>" +
                        "<div class='emp-email'> <strong>email:</strong> " + val.email + "</div>" +
                        "<div class='emp-email'> <strong>phone: </strong> " + val.phone + "</div>";
                },
                field: "id",
                title: "Họ tên", width: 200
            }, {
                field: "fromDate",
                title: "ngày bắt đầu", width: 150,
                template: function (val) { return kendo.toString(kendo.parseDate(val.fromDate), 'dd/MM/yyyy'); }
            }, {
                field: "toDate",
                title: "ngày kết thúc", width: 150,
                template: function (val) { return kendo.toString(kendo.parseDate(val.toDate), 'dd/MM/yyyy'); }
            },  {
                field: "typeName",
                title: "Loại đơn", width: 150
            }, {
                field: "description",
                title: "Lý do", width: 150
            }, {
                field: "approvedStatus", width: 150,
                title: "Tình trạng",
                template: function (val) {
                    var htmlstatus = "<div class='ddlapprovedStatusAction_"+val.id+"'></div>";
                    
                    return htmlstatus;
                }
            }, {
                field: "id", width: 200,
                title: "Thao tác",
                attributes: {
                    style: "text-align: center;"
                },
                headerAttributes: {
                    style: "text-align: center;"
                },
                template: function (val) {
                    var htmlaction = "";
                    htmlaction += '<a href="javascript:void(0);" onclick="ViewRole(' + val.id + ')" id="viewrole_' + val.id + '" data-toggle="modal" data-target="#RoleModal" class="btn btn-sm btn-outline-primary"><i class="icon-users"></i></a> ';
                    htmlaction += '<a href="/emp/edit/' + val.id + '" target="_blank"  class="btn btn-sm btn-outline-warning"><i class="icon-pencil"></i></a> ';
                    htmlaction += '<a href="javascript:void(0);" id="remove_' + val.id + '" class="ladda-button btn btn-sm btn-outline-danger deletebtn"  title="Delete" onclick="Viewdelete(this)" data-type="confirm" data-title="' + val.name + '" data-id="' + val.id + '"> <i class="icon-trash"></i></a>';
                    return htmlaction;
                }
            }
        ],
        dataBound: function (e) {
            $.each(e.sender._data, function (e, item) {
                $('.ddlapprovedStatusAction_' + item.id).kendoDropDownList({
                    dataSource: ddlApprovedStatusData,
                    dataTextField: "text",
                    dataValueField: "id",
                    index: item.approvedStatus,
                    change: ChangeApproved
                }).data("kendoDropDownList");
            });
        }
    };
    Gridlist = initkendogrid(control);

    var datasource = new kendo.data.DataSource(
        {
            pageSize: 7,
        }
    );
    datasource.read();
    Gridlist.setDataSource(datasource);
    Gridlist.refresh();
}

function loadDatacountLeave() {
    var optionajax = {
        url: url_LeaveCountLeave,
        method: "GET",
        success: function (data) {
            $("#count_annual_leave").empty().text(data.count_annual_leave);
            $("#count_leave_sick").empty().text(data.count_leave_sick);
            $("#count_leave_pregnant").empty().text(data.count_leave_pregnant);
            $("#count_leave_without_permission").empty().text(data.count_leave_without_permission);
            $("#count_leave_wfh").empty().text(data.count_leave_wfh);
            $("#count_holidays").empty().text(data.count_holidays);
        }
    };
    AjaxCaller(null, optionajax);
}

// Load control 
function loadcontrol() {
    // load danh sách tình trạng
    var optionAjaxLoadStatus = {
        url: url_LoadLeaveStatus,
        method: "GET",
        success: function (rs) {
            ddlleaveStatusSearch = $('#ddlleaveStatusSearch').kendoDropDownList({
                dataSource: rs,
                optionLabel: "--- Tất cả ---",
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        }
    };
    AjaxCaller(null, optionAjaxLoadStatus);


    // load tình trạng duyệt
    var optionAjaxApprovedStatus = {
        url: url_LoadLeaveApprovedStatus,
        method: "GET",
        success: function (rs) {
            ddlApprovedStatusData = rs;
            ddlApprovedStatusSearch = $('#ddlApprovedStatusSearch').kendoDropDownList({
                dataSource: rs,
                optionLabel: "--- Tất cả ---",
                dataTextField: "text",
                dataValueField: "id",
                index: 0,
            }).data("kendoDropDownList");
        }
    };
    AjaxCaller(null, optionAjaxApprovedStatus);

    // load loại đơn
    var optionAjaxLeaveType = {
        url: url_LoadLeaveType,
        method: "GET",
        success: function (rs) {
            ddlLeaveTypeSearch = $('#ddlLeaveTypeSearch').kendoDropDownList({
                dataSource: rs,
                optionLabel: "--- Tất cả ---",
                dataTextField: "text",
                dataValueField: "id",
                index: 0,
            }).data("kendoDropDownList");
            ddlLeaveType = $('#ddlLeaveType').kendoDropDownList({
                dataSource: rs,
                optionLabel: "--- Chọn ---",
                dataTextField: "text",
                dataValueField: "id",
                index: 0,
            }).data("kendoDropDownList");
        }
    };
    AjaxCaller(null, optionAjaxLeaveType);

    DatePicker = $("#DatePicker").kendoDatePicker({
        value: kendo.date.today(),
        format: "dd/MM/yyyy",
        culture: "vi-VN",
        animation: {
            open: {
                effects: "zoom:in",
                duration: 500
            }
        }
    }).data("kendoDatePicker");
    var start = currentDate;
    var end = new Date(start.getFullYear(), start.getMonth() + 1, 1);
    DateRangeSearch = $("#DateRangeSearch").kendoDateRangePicker({
        value: currentDate,
        format: "dd/MM/yyyy",
        range: { start: start, end: end },
        change: CallBackChange
    }).data("kendoDateRangePicker");

    DateRange = $("#DateRange").kendoDateRangePicker({
        value: currentDate,
        format: "dd/MM/yyyy",
        change: CallBackChange
    }).data("kendoDateRangePicker");
    
    var optionAjaxLoadDepartment = {
        url: url_loadDepartment,
        method: "GET",
        success: function (rs) {
            ddlDepartID = $('#ddlDepartID').kendoDropDownList({
                dataSource: rs,
                optionLabel: "-----Chọn bộ phận ---",
                dataTextField: "departmentName",
                dataValueField: "id",
                index: 0,
                change: onchange_ddldepartment
            }).data("kendoDropDownList");
        }
    };
    AjaxCaller(null, optionAjaxLoadDepartment);

    ddlEmpID = $('#ddlEmpID').kendoMultiSelect({
        dataSource: [],
        optionLabel: "----- Chọn nhân viên -----",
        dataTextField: "name",
        dataValueField: "id",
        footerTemplate: 'Total #: instance.dataSource.total() # items found',
        itemTemplate: '<span class="k-state-default" style="background-image: url(\'#:data.avartar#\')"></span>' +
            '<span class="k-state-default">#: data.name #</span>',
        tagTemplate: '<span class="selected-value" style="background-image: url(\'#:data.avartar#\')"></span><span>#: data.name #</span>'
    }).data("kendoMultiSelect");
}

function onchange_ddldepartment(e) {
    var value = this.value();
    var optionAjaxMember = {
        url: url_loadEmpByDepartment + "?departid=" + value,
        method: "GET",
        success: function (rs) {
            var dataSource = new kendo.data.DataSource({ data: rs });
            ddlEmpID.setDataSource(dataSource);
            if (rs.length > 0) {
                $('#showddl_employee').show();
            }
            ddlEmpID.refresh();
        }
    };
    AjaxCaller(null, optionAjaxMember);
}

// Search
function SearchLeaveRequest() {
    var btn_ladda = document.getElementById('btn-search-leave');
    if (btn_ladda !== undefined) {
        $('#btn-search-leave').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-search-leave'));
        ladda.start();
    }

    var range = DateRangeSearch.range()

    var model = {
        key_search: $('#keysearch').val(),
        leaveType: parseInt(ddlLeaveTypeSearch.value()),
        leaveStatus: parseInt(ddlleaveStatusSearch.value()),
        approvedStatus: parseInt(ddlApprovedStatusSearch.value()),
        fromDate: kendo.toString(range.start, "yyyy-MM-dd"),
        toDate: kendo.toString(range.end, "yyyy-MM-dd")
    };
    var optionAjaxSearchTeam = {
        url: url_SearchLeaveRequest,
        data: JSON.stringify(model),
        success: function (data) {
            var datasource = new kendo.data.DataSource({ data: data, pageSize: 10 });
            datasource.read();
            Gridlist.setDataSource(datasource);
            Gridlist.refresh();

       
        },
    };
    AjaxCaller('btn-search-leave', optionAjaxSearchTeam);
    ladda.stop();
}

// change Approved
function ChangeApproved(e) {
    var element = e.sender.element;
    var row = element.closest("tr");
    var grid = $("#Gridlist").data("kendoGrid");
    var dataItem = grid.dataItem(row);
    dataItem.set("approvedStatus", e.sender.value());
    var model = {
        ID: dataItem.id,
        approvedStatus: e.sender.value()
    };
    var optionAjaxchangeApproved = {
        url: url_changeApproved,
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
        },
    };
    AjaxCaller(null, optionAjaxchangeApproved);
}