﻿$(document).ready(function () {
    loadData();
});
// load data list menu
function loadData() {

    var optionajax = {
        url: "/Setting/Getall",
        method: "GET",
        success: function (rs) {
            $.each(rs, function (i, item) {
                $('#' + item.name).val(item.contents);
                if (item.type === 5 || item.type === 1 || item.type === 2 || item.type === 3) {
                    $('#is' + item.name).prop('checked',true);
                }
            });       
        }
    };
    AjaxCaller(null, optionajax);
}

function UpdateSettings() {
    var btn_ladda = document.getElementById('btn-update');
    if (btn_ladda !== undefined) {
        $('#btn-update').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-update'));
        ladda.start();
    }
    var model = {
        ID: parseInt($('#ID').val()),
        Name: $('#Name').val(),
        Link: $('#Link').val(),
        Content: $('.note-editable').html(), // $('#Content').text(),
        Type: parseInt(ddlType.value()),
        IconClass: "",
        Sort: 0,
        ParentID: 0,
        Status: parseInt(ddlStatus.value())
    };
    var optionajax = {
        url: "/Setting/UpdateGeneralSetting",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loadData();

            setTimeout(function () {
                ladda.stop();
                WindowFormAddorUpdate.data("kendoWindow").close();
            }, data.elapsed);
        }
    };
    AjaxCaller("btn-update", optionajax);
}