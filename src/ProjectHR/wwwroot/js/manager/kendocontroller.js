﻿function GridDataBound(e,ishavechildrow) {
    var grid = e.sender;
    if (grid.dataSource.total() === 0) {
        var colCount = grid.columns.length + 1;
        $(e.sender.wrapper)
            .find('tbody')
            .append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data" style="text-align: center;">Không có dữ liệu</td></tr>');
    }
    if (ishavechildrow)
        $(e.sender.wrapper)
            .find('tbody')
            .append('<tr class="k-master-row"></tr>').first(); 
}


function controlkendogrid(control) {

    var filter = { 
        dataBound: function (e) {
            $('.k-icon').text('');
            GridDataBound(e, control.ishavechildrow !== null ? control.ishavechildrow : false);
            control.dataBound !== undefined ? control.dataBound(e) : null

        },
        dataBinding: control.dataBinding !== null ?  control.dataBinding : DataBindingKendoGridDefault(),
        sortable: true,
        filterable: {
            extra: false,
            messages: {
                info: "Filter by: ",
                clear: 'Xóa',
                filter: 'Lọc',
                checkAll: "Chọn tất cả"
            },
            operators: {
                number: {//STT
                    eq: "Bằng",
                    neq: "Không bằng",
                    gte: "Lớn hơn hoặc bằng",
                    gt: "Lớn hơn",
                    lte: "Nhỏ hơn hoặc bằng",
                    lt: "Nhỏ hơn"
                },
                string: {//Họ tên thí sinh
                    contains: "Chứa",
                    startswith: "Bắt đầu bằng",
                    eq: "Bằng",
                    neq: "Không bằng"
                },
            }
        },
        pageable: {
            refresh: false,
            pageSize: 5,
            pageSizes: [ 5, 10, 20, 50, 100, "All"],
            messages: {
                display: "Từ {0}-{1} trong {2} dòng",
                itemsPerPage: ""
            }
        },
        detailInit: control.detail !== null ? control.detail : null,
        detailTemplate: control.templatedetail !== null ? control.templatedetail : null,
        columnMenu: control.columnMenu !== null ? control.columnMenu : true,
        reorderable: control.reorderable !== null ? control.reorderable: true, scrollable: true,
        resizable: true,
        columns: control.columns,
        editable: control.editable
        
    };
    return filter;
}
function initkendogrid(control) {
    var gridkendo = $('#' + control.ID).kendoGrid(controlkendogrid(control)).data("kendoGrid");
   
    return gridkendo;
}

function DataBindingKendoGridDefault(e) {
    $('.k-icon').text('');
    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
}
