﻿
var ddlstatus; 
var ddlrank;
var ddlgender;
var ddldepartment;
var birthdate;
var experience;
var countloading;

$(document).ready(function () {
    loadControl();
    loadprofile();
});
// checkdataloading
function checkdataloading() {
    countloading++;
    if (countloading === 3) {
        $('.page-loader-wrapper').hide();
    }

}
// load control
function loadControl() {
    // load status
    var optionajaxstatusemp = {
        url: "/account/getstatusemp",
        method: "GET",
        success: function (data) {
            ddlstatus = $('#Status').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxstatusemp);


    var optionajaxrank = {
        url: "/Account/getranks",
        method: "GET",
        success: function (data) {
            ddlrank = $('#Rank').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxrank);

    birthdate = $("#BirthDate").kendoDatePicker({
        value: kendo.date.today(),
        format: "dd/MM/yyyy",
        culture: "vi-VN",
        animation: {
            open: {
                effects: "zoom:in",
                duration: 500
            }
        }
    }).data("kendoDatePicker");

    var optionajaxgender = {
        url: "/Account/getgender",
        method: "GET",
        success: function (data) {
            ddlgender = $('#Gender').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxgender);

    experience = $('#Experience').kendoNumericTextBox({
        format: "##,#",
        decimals: 0,
        MinCount: 0,
        value: 0,
        MaxCount: 1000000000
    }).data('kendoNumericTextBox');

    var optionAjaxGetDepartment = {
        url: "/department/getlist",
        method: "GET",
        success: function (rs) {
            ddldepartment = $('#DepartID').kendoDropDownList({
                dataSource: rs, 
                dataTextField: "departmentName",
                dataValueField: "id",
                index: 0,
                filter: 'contains'
            }).data("kendoDropDownList");
        }
    };
    AjaxCaller(null, optionAjaxGetDepartment);
}
// load profile
function loadprofile() {
    //$('.page-loader-wrapper').show();
    var option_data_emp = {
        url: "/emp/Getbyid/"+$('#EmpID').val(),
        method: "GET",
        success: function (data) {  
            $("#id_service_image").addClass('dropify');
            $("#id_service_image").attr("data-height", 140);
            $("#id_service_image").attr("data-default-file", data.avartar);
            $('.dropify').dropify();



            $("#Avartar").val(data.avartar);
            $('#Contract_Intro').text(data.contract_Intro);
            $('#Name').val(data.name);
            $('#Address').val(data.address);
            $('#RegisterView_Email').val(data.email);
            $('#Phone').val(data.phone);
            
            birthdate.value(data.birthDate);
            experience.value(data.experience);
            ddlgender.value(data.gender);
            ddlrank.value(data.rank); 
            ddlstatus.value(data.status);
            ddldepartment.value(data.departID);
            
        }
        , complete: function () {
            $('.dropify-wrapper').css("height", "140px");
            $('.dropify-render').find("img").css('max-height', '140px');
        }
    };
    AjaxCaller(null, option_data_emp); 
}
// AddEmp
function EditEmp() {
    var btn_ladda = document.getElementById('btn-edit');
    if (btn_ladda !== undefined) {
        $('#btn-edit').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-edit'));
        ladda.start();
    }

    var formData = new FormData();
    var inputfile = document.getElementById('id_service_image');
    if (inputfile !== undefined) {
        var re = inputfile.files;
        if (re.length > 0) {
            for (var e = 0; e !== re.length; e++) {
                formData.append("files", re[e]);
            }
        }
    }
    formData.append("ID", parseInt($("#EmpID").val())); 
    formData.append("Email", $("#RegisterView_Email").val()); 
    formData.append("Name", $("#Name").val());
    formData.append("Phone", $("#Phone").val());
    formData.append("BirthDate", kendo.toString(birthdate.value(), "yyyy-MM-dd")); 
    formData.append("Address", $("#Address").val());
    formData.append("Gender", kendo.toString(ddlgender.value())); 
    formData.append("Status", kendo.toString(ddlstatus.value()));
    formData.append("Experience", experience.value());
    formData.append("Rank", kendo.toString(ddlrank.value()));
    formData.append("DepartID", kendo.toString(ddldepartment.value()));
    formData.append("Avartar", $("#Avartar").val());
    formData.append("Contract_Intro", $("#Contract_Intro").val());
    formData.append("UpdateBy", "");
    var optionajax = {
        url: "/emp/editemp",
        data: formData,
        success: function (data) {
            defaultSuccess(data);
            setTimeout(function () {
                ladda.stop();
            }, data.elapsed);
        }
    };
    AjaxFormData(optionajax);
}
