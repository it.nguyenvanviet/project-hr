﻿
var ddlstatus;
var ddltype;
var ddlrank;
var ddlgender;
var ddlDepartment
var birthdate;
var experience;
var countloading;

$(document).ready(function () {
    loadControl();
    $('.page-loader-wrapper').show();
});

// checkdataloading
function checkdataloading() {
    countloading++;
    if (countloading === 3) {
        $('.page-loader-wrapper').hide();
    }

}

// load control
function loadControl() {
    // load status
    var optionajaxstatusemp = {
        url: "/account/getstatusemp",
        method: "GET",
        success: function (data) {
            ddlstatus = $('#Status').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxstatusemp);


    var optionajaxrank = {
        url: "/Account/getranks",
        method: "GET",
        success: function (data) {
            ddlrank = $('#Rank').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxrank);

    birthdate = $("#BirthDate").kendoDatePicker({
        value: kendo.date.today(),
        format: "dd/MM/yyyy",
        culture: "vi-VN",
        animation: {
            open: {
                effects: "zoom:in",
                duration: 500
            }
        }
    }).data("kendoDatePicker");

    var optionajaxgender = {
        url: "/Account/getgender",
        method: "GET",
        success: function (data) {
            ddlgender = $('#Gender').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxgender);

    experience = $('#Experience').kendoNumericTextBox({
        format: "##,#",
        decimals: 0,
        MinCount: 0,
        value: 0,
        MaxCount: 1000000000
    });

    var optionAjaxGetDepartment = {
        url: "/department/getlist",
        method: "GET",
        success: function (rs) {
            ddldepartment = $('#DepartID').kendoDropDownList({
                dataSource: rs,
                optionLabel: "-----Chọn bộ phận ---",
                dataTextField: "departmentName",
                dataValueField: "id",
                index: 0,
                filter: 'contains'
            }).data("kendoDropDownList"); 
        }
    };
    AjaxCaller(null, optionAjaxGetDepartment);
}
 
// AddEmp
function AddEmp() {
    var btn_ladda = document.getElementById('btn-add');
    if (btn_ladda !== undefined) {
        $('#btn-add').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add'));
        ladda.start();
    }

    var formData = new FormData();
    var inputfile = document.getElementById('FormFile');
    if (inputfile !== undefined) {
        var re = inputfile.files;
        if (re.length > 0) {
            for (var e = 0; e !== re.length; e++) {
                formData.append("files", re[e]);
            }
        }
    }
    formData.append("registerView.email", $("#RegisterView_Email").val());
    formData.append("registerView.password", $("#RegisterView_Password").val());
    formData.append("registerView.confirmPassword", $("#RegisterView_ConfirmPassword").val());
    formData.append("Name", $("#Name").val());
    formData.append("Phone", $("#Phone").val());
    formData.append("Contract_Intro", $("#Contract_Intro").val());
    formData.append("BirthDate", kendo.toString(birthdate.value(), "yyyy-MM-dd"));
    formData.append("Address", $("#Address").val());
    formData.append("Gender", kendo.toString(ddlgender.value()));
    formData.append("Experience", kendo.toString(experience.val()));
    formData.append("Rank", kendo.toString(ddlrank.value()));
    formData.append("DepartID", kendo.toString(ddldepartment.value()));

    var optionajax = {
        url: "/emp/createemp",
        data: formData,
        success: function (data) {
            defaultSuccess(data);
            setTimeout(function () {
                ladda.stop();
            }, data.elapsed);
        }
    };
    AjaxFormData(optionajax);
}
