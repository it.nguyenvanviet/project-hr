﻿var ddlDepartment, ddlStatusSearch, ddlRankSearch;
$(document).ready(function () {
    initKendoGridList();
    loadcontrol();
    loadrole();
    loadData();
});
var listroleid = [];
function initKendoGridList() {
    var control = {
        detail: detailinitemp,
        templatedetail: kendo.template($("#kendodetailtemplate").html()),
        ID: "Gridlist",
        columns: [
            {
                template: function (val) {
                    var tempimage = val.avartar;
                    return "<div class='customer-photo'" +
                        "style='background-image: url(" + val.url_avatar + ");'></div> <div class='customer-name'> " + val.name + "</div>";
                },
                field: "id",
                title: "Họ tên",
                width: 240
            }, {
                field: "email",
                title: "Email", width: 250
            }, {
                field: "phone",
                title: "Phone", width: 150
            }, {
                field: "genderValue",
                title: "Giới tính", width: 150
            }, {
                field: "status", width: 150,
                title: "Tình trạng",
                template: function (val) {
                    var htmlstatus = "";
                    switch (val.status) {
                        case 0: htmlstatus = "<span class=\"badge badge-danger\">Không hoạt động</span> "; break;
                        case 1: htmlstatus = "<span class=\"badge badge-info\">Đang thử việc</span> "; break;
                        case 2: htmlstatus = "<span class=\"badge badge-success\">Đã nhận việc</span> "; break;
                        case 3: htmlstatus = "<span class=\"badge badge-warning\">Tạm dừng công việc</span> "; break;
                        case 4: htmlstatus = "<span class=\"badge badge-default\">Đã nghỉ việc</span> "; break;
                    }
                    return htmlstatus;
                }
            }, {
                field: "id", width: 200,
                title: "Thao tác",
                attributes: {
                    style: "text-align: center;"
                },
                headerAttributes: {
                    style: "text-align: center;"
                },
                template: function (val) {
                    var htmlaction = "";
                    htmlaction += '<a href="javascript:void(0);" onclick="ViewRole(' + val.id + ')" id="viewrole_' + val.id + '" data-toggle="modal" data-target="#RoleModal" class="btn btn-sm btn-outline-primary"><i class="icon-users"></i></a> ';
                    htmlaction += '<a href="/emp/edit/' + val.id + '" target="_blank"  class="btn btn-sm btn-outline-warning"><i class="icon-pencil"></i></a> ';
                    htmlaction += '<a href="javascript:void(0);" id="remove_' + val.id + '" class="ladda-button btn btn-sm btn-outline-danger deletebtn"  title="Delete" onclick="Viewdelete(this)" data-type="confirm" data-title="' + val.name + '" data-id="' + val.id + '"> <i class="icon-trash"></i></a>';
                    return htmlaction;
                }
            }
        ],
        ishavechildrow: true
    };
    Gridlist = initkendogrid(control);
    
}

// confirm delete
function confirmdelete(option) {
    swal({
        title: "Bạn có chắc?",
        text: "Xóa !" + option.title,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            Delete(option);
        } else {
            swal.close();
        }
    });
}

// delete menu
function Delete(option) {
    var optionajax = {
        url: "Emp/Delete?id=" + parseInt(option.id),
        type: "Delete",
        success: function (data) {
            swal({
                title: "Success!",
                text: "Reloading in 2 seconds.",
                type: "success",
                timer: 2000,
                showConfirmButton: false
            }, function () {
                loadData();
            });

        }
    };
    AjaxCaller("deletebtn", optionajax);
}

// load data list emp
function loadData() {

    var datasource = new kendo.data.DataSource(
        {
            pageSize: 7,
        }
    );
    datasource.read();
    Gridlist.setDataSource(datasource);
    Gridlist.refresh();
}

// init detail
function detailinitemp(e) {
    e.detailRow.find("#Gridlist").kendoGrid({
        dataSource: e.data
    });
    var detailRow = e.detailRow;
    detailRow.find(".tabstrip").kendoTabStrip({
        animation: {
            open: { effects: "fadeIn" }
        }
    });

    var optionajax = {
        url: "/Emp/getspecialism?empid=" + e.data.id,
        method: "GET",
        success: function (rs) {
            var html = "";
            html += '<li><label> Chuyên ngành: </label> ' + stringjoin(rs, 'name') + ' <li>';
            html += '<li><label> Kinh nghiệm: </label> ' + e.data.experience + ' năm. <li>';
            html += '<li><label> Địa chỉ: </label> ' + e.data.address + ' <li>';
            detailRow.find('.emp_specialisms').append(html);
        }
    };
    AjaxCaller(null, optionajax);

    detailRow.find(".emp_education").kendoGrid({
        dataSource: {
            type: "json",
            transport: {
                read: "/Emp/geteducation?empid=" + e.data.id
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 7
        },
        dataBound: function (e) { GridDataBound(e, false); },
        scrollable: false,
        sortable: true,
        pageable: true,
        columns: [
            { field: "name", title: "Tên trường" },
            { field: "specialism", title: "Chuyên ngành học" },
            { field: "fromdate", template: function (v) { return Kendogetyear(v.fromDate) + " - " + Kendogetyear(v.toDate); }, title: "Thời gian" }
        ]
    });
    detailRow.find(".emp_experience").kendoGrid({
        dataSource: {
            type: "json",
            transport: {
                read: "/Emp/getexperiences?empid=" + e.data.id
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 7
        },
        dataBound: function (e) { GridDataBound(e, false); },
        scrollable: false,
        sortable: true,
        pageable: true,
        columns: [
            { field: "name", title: "Tên công ty" },
            { field: "year_experiences", title: "Số năm kinh nghiệm" }, 
            { field: "office", title: "Nơi làm việc" }
        ]
    });
    detailRow.find(".emp_socials").kendoGrid({
        dataSource: {
            type: "json",
            transport: {
                read: "/Emp/getsocial?empid=" + e.data.id
            },
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            pageSize: 7
        },
        dataBound: function (e) { GridDataBound(e, false); },
        scrollable: false,
        sortable: true,
        pageable: true,
        columns: [
            { field: "name", title: "MXH" },
            { field: "link", title: "Link", template: function (v) { return "<a href=" + v.link + ">" + v.link + "</a>"; } }
        ]
    });
}

// view listrole menu
function ViewRole(id) {
    $('#EmpID').val(id);
    var optionajax = {
        url: "/Emp/getlistrole/" + id,
        method: "GET",
        success: function (data) {
            if (data.length > 0) {
                var listroledata = new Array();
                $.each(data, function (id, name) { listroledata.push(name); });
                listroleid.value(listroledata);
                return false;
            }
        }
    };
    AjaxCaller(null, optionajax);

}

// load role
function loadrole() {
    var optionajax = {
        url: "/emp/listrole",
        method: "GET",
        success: function (data) {
            if (data.length > 0) {
                listroleid = $('#listroleid').kendoMultiSelect({
                    dataTextField: "name",
                    dataValueField: "id",
                    dataSource: data
                }).data("kendoMultiSelect");

                return false;
            }
        }
    };
    AjaxCaller(null, optionajax);
}

// update role
function UpdateRoles() {
    var btn_ladda = document.getElementById('btn-updaterole');
    if (btn_ladda !== undefined) {
        $('#btn-updaterole').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-updaterole'));
        ladda.start();
    }
    var model = {
        EmpID: parseInt($('#EmpID').val()),
        Listroleid: listroleid.value(),
        UpdateBy : ""
    };
    var optionajax = {
        url: "/Emp/EditRoleEmp",
        method:"POST",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            $('#RoleModal').modal('hide');
            setTimeout(function () { ladda.stop(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-updaterole", optionajax);
}

// Load control 
function loadcontrol()  {
    // load danh sách tình trạng
    var optionAjaxLoadStatus = {
        url: "/account/getstatusemp",
        method: "GET",
        success: function (rs) {
            ddlStatusSearch = $('#ddlStatusSearch').kendoDropDownList({
                dataSource: rs,
                optionLabel: "--- Chọn trạng thái ---",
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        }
    };
    AjaxCaller(null, optionAjaxLoadStatus);
    // load danh sách phòng ban
    var optionAjaxLoadDepartment = {
        url: "/department/getlist",
        method: "GET",
        success: function (rs) {
            ddlDepartment = $('#ddlDepartment').kendoDropDownList({
                dataSource: rs,
                optionLabel: "--- Chọn bộ phận ---",
                dataTextField: "departmentName",
                dataValueField: "id",
                index: 0,
                filter: 'contains'
            }).data("kendoDropDownList");
        }
    };
    AjaxCaller(null, optionAjaxLoadDepartment);
    //Chức vụ
    var optionajaxrank = {
        url: "/Account/getranks",
        method: "GET",
        success: function (data) {
            ddlRankSearch = $('#ddlRankSearch').kendoDropDownList({
                dataSource: data, 
                optionLabel: "--- Chọn chức vụ ---",
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        }, 
    };

    AjaxCaller(null, optionajaxrank);
}

// Search
function SearchEmp() {
    var btn_ladda = document.getElementById('btn-searchemp');
    if (btn_ladda !== undefined) {
        $('#btn-searchemp').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-searchemp'));
        ladda.start();
    }
    var model = {
        keysearch: $('#keysearch').val(),
        Rank: parseInt(ddlRankSearch.value()),
        Status: parseInt(ddlStatusSearch.value()),
        DepartID: parseInt(ddlDepartment.value())
    };

    var optionAjaxSearchTeam = {
        url: "/Emp/Search",
        data: JSON.stringify(model),
        success: function (data) {
            var datasource = new kendo.data.DataSource( { data: data, pageSize: 5 });
            datasource.read();
            Gridlist.setDataSource(datasource);
            Gridlist.refresh();
        },
    };
    AjaxCaller('btn-searchteam', optionAjaxSearchTeam);
    ladda.stop();
}