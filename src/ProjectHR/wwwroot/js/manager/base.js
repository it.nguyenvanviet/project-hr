
function stringjoin(list, nameElement) { 
    return $.map(list, function (v) { return v[nameElement]; }).join(', ');
}
function Kendogetyear(date) {
    if (date === null)
        date = new Date();
    return kendo.toString(kendo.parseDate(date).getFullYear(), 'yyyy');
}
function KendoformatYear(text,format) {
    return kendo.toString(kendo.parseDate(text), format); 
}