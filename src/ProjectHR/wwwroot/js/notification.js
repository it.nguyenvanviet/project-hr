"use strict";
var connection = new signalR.HubConnectionBuilder().withUrl("/NotificationHub").build();
connection.on("sendToUser", (articleType,articleUrl,articleHeading, articleContent) => {
    var html ='';
    html += '<li> <a href="' + articleUrl + '">';
    html += '<div class="media"><div class="media-left">';
    switch (articleType) {
        case 1: { html += '<i class="icon-check text-success"></i>' }; break;
        case 0: { html += '<i class="icon-info text-warning"></i>' }; break; 
    }
    html += '</div>';
    html += '<div class="media-body">';
    html += '<p class="text">' + articleContent+'</p>';
    html += '<span class="timestamp"></span>';
    html += '</div> </div>  </a> </li>';
    $('#notificationlist').append(html);
});
connection.start().catch(function (err) {
       return console.error(err.toString());
});