﻿function AjaxCaller(btn_id, option) {
    $.ajax({
        url: option.url,
        datatype: option.datatype ? option.datatype : "json",
        type: option.method ? option.method : "POST",
        enctype: option.enctype ? option.enctype : null,
        data: option.data ? option.data : null,
        beforeSend: option.beforeSend ? option.beforeSend : null,
        cache: option.cache ? option.cache : false,
        processData: option.processData ? option.processData : true,
        contentType: option.contentType ? option.contentType : "application/json; charset=utf-8",
        success: option.success ? option.success : function (data) { defaultSuccess(data); },
        complete: option.complete ? option.complete : null,
        error: option.error ? option.error : null
    });
}
function AjaxFormData(option) {

    $.ajax({
        url: option.url,
        data: option.data,
        processData: false,
        contentType: false,
        type: "POST",
        success: option.success ? option.success : function (data) { defaultSuccess(data); }
    });
}
function defaultSuccess(data) {
    switch (data.status) {
        case 0: shownotify(data.title, "danger", data.error); break;
        case 1: shownotify(data.title, "success", data.result); break;
        default: shownotify(data.title, "warning", data.error); break;
    }
}
function shownotify(title, type, message) {
    switch (type) {
        case "success": {
            notify('success', title, 'fa fa-check', message, '', '_blank');
        }; break;
        case "warning": {
            notify('warning', title, 'fa fa-exclamation-circle', message, '', '_blank');
        }; break;
        case "fail": {
            notify('danger', title, 'fa fa-exclamation-triangle', message, '', '_blank');
        }; break;

    }
}
function notify(type, title, icon, message, url, target) {
    $.notify({
        type: type,
        icon: icon,
        title: title,
        message: message,
        target: '_blank',
    }, {
        // settings
        element: 'body',
        position: null,
        type: type,
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 999999,
        delay: 5000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="notify col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" class="btn fa fa-trash" aria-hidden="true" class="close" data-notify="dismiss"></button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
    });
}
