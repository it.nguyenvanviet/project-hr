﻿
var window_specialism, ddlSpecialismID;
$(document).ready(function () { 
    init_kendogrid_specialism(); 
});
// kendowindow form update specialism
function init_kendogrid_specialism() {
    var control = {
        ID: "grid_specialism", 
        columns: [
            { field: "specialismID", title: "Chuyên ngành", width: "180px", editor: specialismDropDownEditor, template: "#=name#" },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }
        ],
        editable: "inline"
    };
    grid_specialism = initkendogrid(control);
}
// specialism
function openwindow_specialism() {
    window_specialism = $("#window_specialism"), undo = $("#undo");
    undo.click(function () {
        window_specialism.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
    var wd = window_specialism.kendoWindow({
        width: "320px",
        title: "Bổ sung chuyên ngành",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow");
    wd.center();
    wd.open();

}
function loadcontrol_specialism() {
     
    var optionajax_ddlspecialism = {
        url: "/account/getlistspecialism",
        method: "GET",
        success: function (data) {
            ddlSpecialismID = $('#ddlSpecialismID').kendoDropDownList({
                dataSource: data,
                filter:"contains",
                dataTextField: "name",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");

        }
    };
    AjaxCaller(null, optionajax_ddlspecialism);
}
function reload_specialism() {
    loadcontrol_specialism();
    loaddata_specialism();
}
function loaddata_specialism() {
      
    var datasource = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/account/getspecialism",
                },
                update: {
                    url: "/account/EditSpecialism",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_specialism();
                    }
                },
                destroy: {
                    url: "/account/DeleteSpecialism",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_specialism();
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        specialismID: {
                            defaultValue: { id: 0, text: "Chọn ngành" }
                        } 
                    }
                }
            },
            pageSize: 10
        }
    );
    //datasource.read();
    grid_specialism.setDataSource(datasource);
    grid_specialism.refresh(); 
    kendo.ui.progress(grid_specialism.element, false);
}
function cancelspecialism() {
    window_specialism.data("kendoWindow").close();
}
// add specialism
function addspecialism() {
    var btn_ladda = document.getElementById('btn-add-specialism');
    if (btn_ladda !== undefined) {
        $('#btn-add-specialism').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add-specialism'));
        ladda.start();
    }
    var model = {
        SpecialismID: parseInt(ddlSpecialismID.value()),
        CreateBy: $('#Basic_Email').val(), 
        EmpID: parseInt($('#Empid').val())
    };
    var optionajax = {
        url: "/account/addspecialism",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loaddata_specialism();
            setTimeout(function () { ladda.stop(); window_specialism.data("kendoWindow").close(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-add-specialism", optionajax);
}
function specialismDropDownEditor(container, options) {
    $('<input required name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            index: 0,
            value: options.model.id,
            dataTextField: "name",
            dataValueField: "id",
            dataSource: { transport: { read: { url: "/account/getlistspecialism" } } }
        });

}