﻿var window_social, ddlSocialType;
$(document).ready(function () { 
    init_kendogrid_social(); 
    loaddata_social();
});
// kendowindow form update social
function initkendo_window_social() {
    window_social = $('#window_social').kendoWindow({
        width: "500px",
        height: "450px",
        title: "Bổ sung thông tin mạng xã hội",
        visible: false,
        actions: [
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow");
    var undo = $("#undo");
    undo.click(function () {
        window_experience.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
}

// kendowindow form update social
function init_kendogrid_social() {
    var control = {
        ID: "grid_social", 
        columns: [
            //{ field: "degreename", title: "Loại",width: '100' },
            { field: "name", title: "MXH", width: '140px', filterable: false },
            { field: "link", title: "link", width: '200px', filterable: false },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }
        ],
        editable: "inline"
    };
    grid_social = initkendogrid(control);
}
// social
function openwindow_social() {
    window_social = $("#window_social"), undo = $("#undo");
    undo.click(function () {
        window_social.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
    var wd = window_social.kendoWindow({
        width: "320px",
        title: "Bổ sung MXH",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ],
        position: {
            top: 100
        },
        close: onClose
    }).data("kendoWindow");
    wd.center();
    wd.open();
}
function loadcontrol_social() {
    kendo.ui.progress(grid_social.element, true);
    var optionajax_socialdegree = {
        url: "/account/getsocialtype",
        method: "GET",
        success: function (data) {
            ddlSocialType = $('#SocialType').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");

        }
    };
    AjaxCaller(null, optionajax_socialdegree);
}
function reload_social() { 
    loadcontrol_social();
    loaddata_social();
} 
function loaddata_social() {
    var datasource = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/account/getsocial",
                    type: "GET",
                    dataType: "json",
                    complete: function (data) {
                        $('#_Info_Socials').empty();
                        $.each(data.responseJSON, function (i, item) {
                            $('#_Info_Socials').append('<p><a href="' + item.link + '">' + item.link + '</a></p>');
                        });
                    }
                },
                update: {
                    url: "/account/EditSocial",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_social();
                    }
                },
                destroy: {
                    url: "/account/DeleteSocial",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_social();
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        name: { validation: { required: true } },
                        link: { validation: { required: true } }
                    }
                }
            },
            pageSize: 10
        }
    );
     
    grid_social.setDataSource(datasource);
    grid_social.refresh();
    kendo.ui.progress(grid_social.element, false);
 
}
 
function cancel_social() {
    window_social.data("kendoWindow").close();
}
// add social
function add_social() {
    var btn_ladda = document.getElementById('btn-add-social');
    if (btn_ladda !== undefined) {
        $('#btn-add-social').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add-social'));
        ladda.start();
    }
    var model = {
        Name: ddlSocialType.text(),
        Link: $('#LinkSocial').val(), 
        CreateBy: $('#Basic_Email').val(), 
        EmpID: parseInt($('#Empid').val())
    };
    var optionajax = {
        url: "/account/addsocial",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loaddata_social();
            setTimeout(function () { ladda.stop(); window_social.data("kendoWindow").close(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-add-social", optionajax);
}
function removesocial(e) {
    var option = {
        title: e.dataset.title,
        id: e.dataset.id,
        url: "/Account/DeleteSocial?id=" + e.dataset.id
    };
    confirmdelete(option);
}
