﻿var window_education, FromDateEdu, ToDateEdu, ddlDegreeEdu;
$(document).ready(function () { 
    init_kendogrid_education(); 
});
// kendowindow form update education
function init_kendogrid_education() {
    var control = {
        ID: "grid_edu", 
        columns: [ 
            { field: "name", title: "Trường", width: '100px', filterable: false },
            { field: "specialism", width: '200px', filterable: false, title: "Chuyên ngành", template: function (v) { return v.specialism + " (" + Kendogetyear(v.fromDate) + " - " + Kendogetyear(v.toDate) + ")"; } },
            { field: "degree", title: "Chuyên ngành", width: "180px", editor: degreeDropDownEditor, template: "#=degreename#" },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }
        ],
        editable: "inline"
    };
    grid_edu = initkendogrid(control);
}
// education
function openwindow_education() {
    window_education = $("#window_education"), undo = $("#undo");
    undo.click(function () {
        window_education.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }

    var wd = window_education.kendoWindow({
        width: "310px",
        title: "Bổ sung Học vấn",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow");
    wd.center();
    wd.open();
}
function loadcontrol_edu() {
    FromDateEdu = $("#FromDateEdu").kendoDatePicker({
        value: kendo.date.today(),
        format: "yyyy",
        depth: "year",
        start: "year",
        culture: "vi-VN",
        animation: {
            open: {
                effects: "zoom:in",
                duration: 500
            }
        }
    }).data("kendoDatePicker");
    ToDateEdu = $("#ToDateEdu").kendoDatePicker({
        value: kendo.date.today(),
        format: "yyyy",
        depth: "year",
        start: "year",
        culture: "vi-VN",
        animation: {
            open: {
                effects: "zoom:in",
                duration: 500
            }
        }
    }).data("kendoDatePicker");
    var optionajax_edudegree = {
        url: "/account/geteducationdegree",
        method: "GET",
        success: function (data) {
            ddlDegreeEdu = $('#DegreeEdu').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");

        }
    };
    AjaxCaller(null, optionajax_edudegree);
    kendo.ui.progress(grid_edu.element, true);
}
function reload_edu() {
    loadcontrol_edu();
    loaddata_edu();
}
function loaddata_edu() {
    var datasource = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/account/geteducation",
                },
                update: {
                    url: "/account/EditEducation",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_edu();
                    }
                },
                destroy: {
                    url: "/account/DeleteEducation",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_edu();
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        name: { validation: { required: true } },
                        degree: {
                            defaultValue: { id: 0, text: "Lao động phổ thông" }
                        },
                        specialism: { validation: { required: true } }
                    }
                }
            },
            pageSize: 10
        }
    ); 
    grid_edu.setDataSource(datasource);
    grid_edu.refresh();
    kendo.ui.progress(grid_edu.element, false);
}
function canceledu() {
    window_education.data("kendoWindow").close();
}
// add edu
function addeducation() {
    var btn_ladda = document.getElementById('btn-add-edu');
    if (btn_ladda !== undefined) {
        $('#btn-add-edu').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add-edu'));
        ladda.start();
    }
    var model = {
        FromDate: kendo.toString(FromDateEdu.value(), "yyyy-MM-dd"),
        ToDate: kendo.toString(ToDateEdu.value(), "yyyy-MM-dd"),
        Name: $('#NameEdu').val(),
        Specialism: $('#SpecialismEdu').val(),
        Degree: parseInt(ddlDegreeEdu.value()),
        CreateBy: $('#Basic_Email').val(), 
        EmpID: parseInt($('#Empid').val())
    };
    var optionajax = {
        url: "/account/addeducation",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loaddata_edu();
            setTimeout(function () { ladda.stop(); window_education.data("kendoWindow").close(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-add-edu", optionajax);
}
function degreeDropDownEditor(container, options) {
    $('<input required name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            index: 0,
            value: options.model.id,
            dataTextField: "text",
            dataValueField: "id",
            dataSource: {  transport: { read: {  url: "/account/geteducationdegree"  }  }  }
        });
        
}