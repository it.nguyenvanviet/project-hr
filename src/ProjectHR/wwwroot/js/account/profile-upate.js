﻿// change avatar 
function UpdateAvartar() {
    var btn_ladda = document.getElementById('btn-updateavartar');
    if (btn_ladda !== undefined) {
        $('#btn-updateavartar').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-updateavartar'));
        ladda.start();
    }
    var formData = new FormData();
    var inputfile = document.getElementById('id_service_image');
    if (inputfile !== undefined) {
        var re = inputfile.files;
        if (re.length > 0) {
            for (var e = 0; e !== re.length; e++) {
                formData.append("files", re[e]);
            }
        }
        else {
            shownotify("update avartar", "warning", "Chọn hình mới !!!!");
            setTimeout(function () {
                ladda.stop();
            }, 1000);
            return;
        }
    }
    formData.append("email", $("#AccountData_Email").val());
    formData.append("name", $("#Basic_Info_Name").val());
    formData.append("id", $("#Empid").val());
    var optionajax = {
        url: "/account/updateavartar",
        data: formData,
        success: function (data) {
            defaultSuccess(data);
            setTimeout(function () {
                ladda.stop();
            }, 1000);
        }
    };
    AjaxFormData(optionajax);
}
// update basic info name, birthdate, status, ...
function UpdateBasicInfo() {
    var btn_ladda = document.getElementById('btn-updatebasicinfo');
    if (btn_ladda !== undefined) {
        $('#btn-updatebasicinfo').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-updatebasicinfo'));
        ladda.start();
    }
    var model = {
        ID: parseInt($('#Empid').val()),
        Phone: $('#Basic_PhoneNumber').val(),
        BirthDate: kendo.toString(birthdate.value(), "yyyy-MM-dd"),
        Rank: parseInt(ddlrank.value()),
        Gender: parseInt(ddlgender.value()),
        Status: parseInt(ddlstatus.value()),
        Experience: parseInt($("#Basic_Experience").val()),
        Name: $("#Basic_Info_Name").val(),
        Address: $("#Basic_Info_Address").val(),
        GoogleMap: $("#Basic_Info_GoogleMap").val(),
        Email: $("#Basic_Email").val()
    };
    var optionajax = {
        url: "/Account/UpdateBasicInfo",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
        },
        complete: function () {
            setTimeout(function () {
                ladda.stop();
                loadprofile();
            }, 1000);
        }
    };
    AjaxCaller("btn-updatebasicinfo", optionajax);
}

 