﻿var window_experiences;
$(document).ready(function () { 
    init_kendogrid_experience(); 
});
// kendowindow form update experience
function init_kendogrid_experience() {
    var control = {
        ID: "grid_exp",  
        columns: [
            //{ field: "degreename", title: "Loại",width: '100' },
            { field: "name", title: "Công việc", filterable: false, width: '100px', media: "(min-width: 450px)" },
            { field: "office", filterable: false, title: "Nơi làm việc", width: '100px', media: "(min-width: 450px)" }, 
            { field: "year_experiences", title: "Thời gian", filterable: false, width: '100px', template: function (v) { return v.year_experiences + " năm"; }, media: "(min-width: 450px)" },
            { field: "descriptions", filterable: false, title: "Mô tả", width: '100px', media: "(min-width: 450px)" },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "200px" }
        ],
        editable: "inline"
    };
    grid_exp = initkendogrid(control);
}
// kendowindow form update experience
function initkendo_window_experience() {
    window_experience = $('#window_experience').kendoWindow({
        width: "500px",
        height: "450px",
        title: "Bổ sung thông tin kinh nghiệm làm việc",
        visible: false,
        actions: [
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow");
    var undo = $("#undo");
    undo.click(function () {
        window_experience.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
}

// experience
function openwindow_experience() {
    window_experiences = $("#window_experiences"), undo = $("#undo");
    undo.click(function () {
        window_experiences.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
    var wd = window_experiences.kendoWindow({
        width: "310px",
        title: "Thêm kinh nghiệm",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow")
    wd.center();
    wd.open();
}
function reload_exp() { 
    var datasource = new kendo.data.DataSource(
        {
            pageSize: 10,
            transport: {
                read: {
                    url: "/account/getexperience",
                },
                update: {
                    url: "/account/EditExperience",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_exp();
                    }
                },
                destroy: {
                    url: "/account/DeleteExperience",
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_exp();
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        name: { validation: { required: true } },
                        office: { validation: { required: false } },
                        year_experiences: {
                            type: "number", validation: { min: 0, required: true }
                        },
                        descriptions: { validation: { required: false } },
                    }
                }
            }
        }
    ); 
    grid_exp.setDataSource(datasource);
    grid_exp.refresh();
    kendo.ui.progress(grid_exp.element, false);
} 
function cancel_experiences() {
    window_experiences.data("kendoWindow").close();
}
// add exp
function add_experiences() {
    var btn_ladda = document.getElementById('btn-add-exp');
    if (btn_ladda !== undefined) {
        $('#btn-add-exp').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add-exp'));
        ladda.start();
    }
    var model = { 
        Name: $('#NameExp').val(),
        Office: $('#OfficeExp').val(),
        Descriptions: $('#DescriptionsExp').val(),
        Year_experiences: parseFloat($('#Year_ExperienceExp').val()),
        CreateBy: $('#Basic_Email').val(), 
        EmpID: parseInt($('#Empid').val())
    };
    var optionajax = {
        url: "/account/addexperience",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            reload_exp();
            setTimeout(function () { ladda.stop(); window_experiences.data("kendoWindow").close(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-add-exp", optionajax);
}
 