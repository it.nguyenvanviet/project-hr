﻿var ddlstatus;
var ddltype;
var ddlrank;
var ddlgender;
var birthdate;
var experience;
var profile;
var countloading = 0;

 

$(document).ready(function () {
    loadcontrol();
});
 
function loadprofile()
{
    $('.page-loader-wrapper').show();
    var option_data_emp = {
        url: "/account/getbasicinfocontact/",
        method: "GET",
        success: function (data) {
            birthdate.value(data.birthDate);
            $('#Empid').val(data.id);
            //$('#Empavatar').empty().append('<img src="' + data.avartar + '"  class="rounded-circle" alt="">'); 
            $("#id_service_image").addClass('dropify');
            $("#id_service_image").attr("data-height", 300);
            $("#id_service_image").attr("data-default-file", "/Upload/avatar.png");

            if (imageExists(data.avartar)) {
                $("#id_service_image").attr("data-default-file", data.avartar);
            }
            $('.dropify').dropify(); 
             
            $('#_Info_Address').empty().append(data.address);
            $('#_Info_Name').empty().append(data.name);
            $('#_Info_GoogleMap').empty().append(data.googleMap).find("iframe").css("width", "100%");
            $('#_Info_Phone').empty().append(data.phone);
            $('#_Info_Email').empty().append(data.email);
            $('#_Info_BirthDate').empty().append(kendo.toString(birthdate.value(), 'dd-MM-yyyy'));

            $('.page-loader-wrapper').hide();
        }
    };
    AjaxCaller(null, option_data_emp); 


}
function loadcontrol() {
    $('.page-loader-wrapper').show();
     
    // load status
    var optionajaxstatusemp = {
        url: "/account/getstatusemp",
        method: "GET",
        success: function (data) {
            ddlstatus = $('#Basic_Info_Status').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxstatusemp);
    

    var optionajaxrank = {
        url: "/account/getranks",
        method: "GET",
        success: function (data) {
            ddlrank = $('#Basic_Info_Rank').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
             
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxrank);

    birthdate = $("#Basic_Info_BirthDate").kendoDatePicker({
        value: kendo.date.today(),
        format: "dd/MM/yyyy",
        culture: "vi-VN",
        animation: {
            open: {
                effects: "zoom:in",
                duration: 500
            }
        }
    }).data("kendoDatePicker");

    var optionajaxgender = {
        url: "/account/getgender",
        method: "GET",
        success: function (data) {
            ddlgender = $('#Basic_Info_Gender').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");
            
        },
        complete: function () {
            checkdataloading();
        }
    };
    AjaxCaller(null, optionajaxgender);

    experience = $('#Experience').kendoNumericTextBox({
        format: "##,#",
        decimals: 0,
        MinCount: 0,
        value: 0,
        MaxCount: 1000000000
    });
    $('.page-loader-wrapper').hide();
}
// checkdataloading
function checkdataloading() {
    countloading++;
    if (countloading === 3) {
        $('.page-loader-wrapper').hide();
        countloading = 0;
        loadprofile(); 
    }
    console.log(countloading);
}
function loadinput() {  
    var option_data_emp = {
        url: "/account/getprofile/",
        method: "GET",
        success: function (data) {

            // basic info
            ddlstatus.value(data.status);
            ddlrank.value(data.rank);
            ddlgender.value(data.gender);
            birthdate.value(data.birthDate);
            $('#Basic_Info_Address').val(data.address);
            $('#Basic_Info_Name').val(data.name);
            $('#Basic_Info_GoogleMap').val(data.googleMap);
            $('#Basic_PhoneNumber').val(data.phone);
            $('#Basic_Experience').val(data.experience);
            $('#Basic_Email').val(data.email);
        }
    };
    AjaxCaller(null, option_data_emp); 
  
}

// confirm delete
function confirmdelete(option) {
    swal({
        title: "Bạn có chắc?",
        text: "Xóa " + option.title,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            Delete(option);
        } else {
            swal.close();
        }
    });
}
// delete menu
function Delete(option) {
    var optionajax = {
        url: option.url,
        type: "GET",
        success: function (data) {
            swal({
                title: "Success!",
                text: "Xóa thành công",
                type: "success",
                timer: 2000,
                cancelButtonText: "Close",
            }, function () {
                loadData();
            });

        }
    };
    AjaxCaller("deletebtn", optionajax);
}

function reload_info() {
    //loadcontrol();
    loadinput();
}
