﻿var window_skill, grid_skill, ddlTypeSkill
$(document).ready(function () { 
    init_kendogrid_skill(); 
});
// kendowindow form update skill
function init_kendogrid_skill() {
    var control = {
        ID: "grid_skill", 
        columns: [ 
            { field: "name", title: "Kĩ năng", width: '150px', filterable: false },
            { field: "pointPercent", title: "% ", width: '100px', filterable: false, template: function (v) { return v.pointPercent + " %"; } },
            { field: "typeSkill", title: "Loại kĩ năng", width: "180px", editor: typeSkillDropDownEditor, template: "#=typeSkillName#" },
            { field: "description", title: "Mô tả ngắn", width: '150px', filterable: false },
            { command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }
        ],
        editable: "inline"
    };
    grid_skill = initkendogrid(control);
}
// kendowindow form update skill
function initkendo_window_skill() {
    window_skill = $('#window_skill').kendoWindow({
        width: "500px",
        height: "450px",
        title: "Bổ sung thông tin kĩ năng",
        visible: false,
        actions: [
            "Minimize",
            "Maximize",
            "Close"
        ],
        close: onClose
    }).data("kendoWindow");
    var undo = $("#undo");
    undo.click(function () {
        window_skill.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
}

// skill
function openwindow_skill() {
    window_skill = $("#window_skill"), undo = $("#undo");
    undo.click(function () {
        window_skill.data("kendoWindow").open();
        undo.fadeOut();
    });
    function onClose() {
        undo.fadeIn();
    }
    var wd = window_skill.kendoWindow({
        width: "320px",
        title: "Bổ sung kĩ năng",
        visible: false,
        actions: [
            "Pin",
            "Minimize",
            "Maximize",
            "Close"
        ], 
        close: onClose
    }).data("kendoWindow");
    wd.center();
    wd.open();
}
function loadcontrol_skill() {
    kendo.ui.progress(grid_skill.element, true);
    PointPercent = $('#PointPercent').kendoNumericTextBox({
        format: "n0",
        min: 0,
        max: 100, 
    }).data("kendoNumericTextBox");
    var optionajax_typeSkill = {
        url: "/account/getTypeSkill",
        method: "GET",
        success: function (data) {
            ddlTypeSkill = $('#TypeSkill').kendoDropDownList({
                dataSource: data,
                dataTextField: "text",
                dataValueField: "id",
                index: 0
            }).data("kendoDropDownList");

        }
    };
    AjaxCaller(null, optionajax_typeSkill);
}
function reload_skill() { 
    loadcontrol_skill()
    loaddata_skill();
}
function loaddata_skill() {
    var datasource = new kendo.data.DataSource(
        { 
            pageSize: 10,
            transport: {
                read: {
                    url: "/account/getskill", 
                },
                update: {
                    url: "/account/EditSkill", 
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_skill();
                    }
                },
                destroy: {
                    url: "/account/deleteskill", 
                    type: "POST",
                    dataType: "json",
                    complete: function (data) {
                        reload_skill();
                    }
                }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        name: { validation: { required: true } },
                        description: { validation: { required: false } },
                        pointPercent: {
                            type: "number", validation: { min: 0, required: true }
                        },
                    }
                }
            }
        }
    );
   
    grid_skill.setDataSource(datasource);
    grid_skill.refresh();
    kendo.ui.progress(grid_skill.element, false);
}
function cancel_skill() {
    window_skill.data("kendoWindow").close();
}
// add skill
function add_skill() {
    var btn_ladda = document.getElementById('btn-add-skill');
    if (btn_ladda !== undefined) {
        $('#btn-add-skill').attr('disabled', 'disabled');
        var ladda = Ladda.create(document.querySelector('#btn-add-skill'));
        ladda.start();
    }
    var model = {
        Name: $('#NameSkill').val(),
        PointPercent: parseFloat($('#PointPercent').val()),
        CreateBy: $('#Basic_Email').val(),
        TypeSkill: parseInt(ddlTypeSkill.value()),
        Description: $('#DescriptionSkill').val(),
        EmpID: parseInt($('#Empid').val())
    };
    var optionajax = {
        url: "/account/addskill",
        data: JSON.stringify(model),
        success: function (data) {
            defaultSuccess(data);
            loaddata_skill();
            setTimeout(function () { ladda.stop(); window_skill.data("kendoWindow").close(); }, data.elapsed);
        }
    };
    AjaxCaller("btn-add-skill", optionajax);
}
function customBoolEditor(container, options) {
    $('<input class="k-checkbox" type="checkbox" name="Discontinued" data-type="boolean" data-bind="checked:Discontinued">').appendTo(container);
}
function typeSkillDropDownEditor(container, options) {
    $('<input required name="' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            index: 0,
            value: options.model.id,
            dataTextField: "text",
            dataValueField: "id",
            dataSource: { transport: { read: { url: "/account/getTypeSkill" } } }
        });

}