﻿var menutype = [];
$(document).ready(function () {
    //loadtypemenu(); 
});
function loadtypemenu() {
    var i = 1;
    var ii = 1;
    var active = "";
    var option_ = {
        url: "/account/getmenutypebyuserid/",
        method: "GET",
        success: function (data) {
            if (data.length > 0) {
                menutype = data;
                $('._menutype').empty();
                $.each(data, function (i, item) {
                    active = "";
                    if (i === 0) { active = "active"; }
                    $('._menutype').append(' <li class="nav-item"><a class="nav-link ' + active + ' " data-toggle="tab" href="#menutab_' + item.id + '">' + item.name + '</a></li>');
                    i = i + 1;
                });
            }
        }, complete: function () { loadmenu(); }
    };
    AjaxCaller(null, option_);
}
function loadmenu() {
    var i = 1;
    var ii = 1;
    var active = "";
    var option_ = {
        url: "/account/getmenubyuserid/",
        method: "GET",
        success: function (data) {
            if (data.length > 0) {
                $('._menu').empty();

                $.each(menutype, function (i, item) {
                    active = "";
                    if (i === 0) { active = "active"; }
                    var html = '';
                    html += '<div class="tab-pane animated fadeIn ' + active + '" id="menutab_' + item.id + '" data-id="">';
                    html += '<nav class="sidebar-nav" >';
                    html += '<ul class="main-menu metismenu menu__' + item.id + '">';
                    html += '</ul>';
                    html += '</nav>';
                    html += '</div>';
                    $('._menu').append(html);


                    i = i + 1;
                });
                $.each(menutype, function (i, item) {
                    loadmenuitem(data, item);
                });
            }
        }, complete: function () { }
    };
    AjaxCaller(null, option_);
}
function loadmenuitem(data, i) {
    var listmenu = [];
    var listmenu_child = [];
    $.each(data, function (ii, item) {
        if (item.type === i.type && item.parentID === 0)
            listmenu.push(item);
        else
            listmenu_child.push(item);
    });
    $.each(listmenu, function (iii, item) {
        var _ischeckhavechild = 0;
        var html2 = '';
        $.each(data, function (_data, item_data) {
            if (item_data.parentID === item.id) {
                _ischeckhavechild = 1;
            }
        });
        if (_ischeckhavechild === 0)  
            html2 += '<li class="navar"><a href="' + item.link + '"><i class="icon-list"></i>' + item.name + '</a></li>';
        else {
            if (!$('.menuparent').hasClass('menupr_' + item.id))
            {
                html2 += '<li class="menuparent menupr_' + item.id + ' navar" data-name="' + item.name + '" onclick = "reloadnav(' + item.id + ')" >  <a id="menuparent_' + item.id + '" href="javascript:void(0);" class="has-arrow" aria-expanded="false" ><i class="icon-users"></i><span>' + item.name + '</span></a>';
                html2 += '     <ul id="menuchild_' + item.id + '" class="collapse" style="height: 0px;">';
                html2 += '     </ul>';
                html2 += '</li>';
            }
        }
        $('.menu__' + i.id).append(html2);
    });
    $.each(listmenu, function (ii, item) {
        loadmenu_child(listmenu_child, item);
    });
}
function loadmenu_child(data, i) {
    var child = [];
    $.each(data, function (ii, childitem) {
        if (childitem.parentID === i.id)
            child.push(childitem);
    });

    $.each(child, function (iii, item) {
        var html2 = '';
        if (!$('.menuchild').hasClass('_menuchild_' + item.id)) {
            html2 += '<li data-name="' + item.name +'" class="menuchild _menuchild_' + item.id + '"><a href="' + item.link + '">' + item.name + '</a></li>';
        }
        $('#menuchild_' + i.id).append(html2);
    });
}
function reloadnav(id) {
    $('.menuparent').removeClass('active');
    $('.has-arrow').attr('aria-expanded', false);
    $('.collapse').removeClass('in');

    $('ul #menuchild_' + id).toggleClass('in');

    if ($('ul #menuchild_' + id).hasClass('in')) {
        $('#menuparent_' + id).attr('aria-expanded', true);
        $('.menupr_' + id).addClass('active');
        $('ul #menuchild_' + id).css('height', 'auto');
    }
    else {
        $('.menupr_' + id).removeClass('active');
        $('#menuparent_' + id).attr('aria-expanded', false);
    }
}
 
function activemenu(parentname, name) {
    var parent = $('.metismenu').find('li').filter("[data-name='" + parentname + "']");
    var child = $('.metismenu').find('li').filter("[data-name='" + name + "']");
    parent.find("ul").addClass("in").attr('aria-expanded', true).css('height', 'auto');
    parent.addClass("active");
    child.addClass("active");
}

function imageExists(image_url) {

    var http = new XMLHttpRequest();

    http.open('HEAD', image_url, false);
    http.send();

    return http.status !== 404;

}