﻿$(function () {
    $('.js-sweetalert').on('click', function () {
        var type = $(this).data('type');
        var title = $(this).data('title');
        var option = { title: title };
        if (type === 'basic') {
            showBasicMessage(option);
        }
        else if (type === 'with-title') {
            showWithTitleMessage(option);
        }
        else if (type === 'success') {
            showSuccessMessage(option);
        }
        else if (type === 'confirm') {
            showConfirmMessage(option);
        }
        else if (type === 'cancel') {
            showCancelMessage(option);
        }
        else if (type === 'with-custom-icon') {
            showWithCustomIconMessage(option);
        }
        else if (type === 'html-message') {
            showHtmlMessage(option);
        }
        else if (type === 'autoclose-timer') {
            showAutoCloseTimerMessage(option);
        }
        else if (type === 'prompt') {
            showPromptMessage(option);
        }
        else if (type === 'ajax-loader') {
            showAjaxLoaderMessage(option);
        }
    });
});

//These codes takes from http://t4t5.github.io/sweetalert/
function showBasicMessage(option) {
    swal("Here's a message!");
}

function showWithTitleMessage(option) {
    swal("Here's a message!", "It's pretty, isn't it?");
}

function showSuccessMessage(option) {
    swal("Good job!", "You clicked the button!", "success");
}

function showConfirmMessage(option) {
    swal({
        title: "Bạn có chắc??",
        text: "Xóa " + option.title,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: "Xác nhận!",
        cancelButtonText: "Hủy bỏ",
        closeOnConfirm: false
    }, function () {
        swal("Xác nhận xóa!", option.title+" đã được xóa.", "success");
    });
}

function showCancelMessage(option) {
    swal({
        title: "Bạn có chắc?",
        text: "Xóa !" +option.title,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#dc3545",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            swal("Xác nhận xóa!", option.title+" đã được xóa.", "success");
        } else {
            //swal("Hủy bỏ", option.title + "", "error");
        }
    });
}

function showWithCustomIconMessage(option) {
    swal({
        title: "Sweet!",
        text: "Here's a custom image.",
        imageUrl: "../assets/images/sm/avatar2.jpg"
    });
}

function showHtmlMessage(option) {
    swal({
        title: "HTML <small>Title</small>!",
        text: "A custom <span style=\"color: #CC0000\">html<span> message.",
        html: true
    });
}

function showAutoCloseTimerMessage(option) {
    swal({
        title: "Auto close alert!",
        text: "I will close in 2 seconds.",
        timer: 2000,
        showConfirmButton: false
    });
}

function showPromptMessage(option) {
    swal({
        title: "An input!",
        text: "Write something interesting:",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Write something"
    }, function (inputValue) {
        if (inputValue === false) return false;
        if (inputValue === "") {
            swal.showInputError("You need to write something!"); return false
        }
        swal("Nice!", "You wrote: " + inputValue, "success");
    });
}

function showAjaxLoaderMessage(option) {
    swal({
        title: "Ajax request example",
        text: "Submit to run ajax request",
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function () {
        setTimeout(function () {
            swal("Ajax request finished!");
        }, 2000);
    });
}