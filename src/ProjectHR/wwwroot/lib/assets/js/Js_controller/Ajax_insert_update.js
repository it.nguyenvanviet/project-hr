﻿//hàm update và insert
var status = 0;
function insert_update(title, url, model) {
    if (confirm("Bạn có chắc " + title) == true) {

        //them moi Package
        var btnaddnew = document.getElementById('btn-addnew');
        if (btnaddnew != undefined) {
            $('#btn-addnew').attr('disabled', 'disabled');
            var btnagp = Ladda.create(document.querySelector('#btn-addnew'));
            btnagp.start();
        }
        var btnupdate = document.getElementById('btn-update');
        if (btnupdate != undefined) {
            $('#btn-update').attr('disabled', 'disabled');
            var btnup = Ladda.create(document.querySelector('#btn-update'));
            btnup.start();
        }
        $.ajax({
            url: url,
            contentType: 'application/json; charset=utf-8',
            traditional: true,
            data: JSON.stringify(model),
            dataType: 'json',
            type: "POST",
            success: function (data) {
                status = data.status;
                switch (data.status) {
                    case -1: {
                        shownotify(title, "FAIL", data.result);

                    }; break;
                    case 0: {
                        shownotify(title, "WARNING", data.result);

                    }; break
                    case 1: {
                        shownotify(title, "SUCCESS", data.result);
                        if (GetlistPackage != undefined) {
                            GetlistPackage();
                        }
                        //
                        cancel();
                    }; break
                }
                if (btnaddnew != undefined) {
                    $('#btn-addnew').removeAttr('disabled');
                    btnagp.stop();
                }
                if (btnupdate != undefined) {
                    $('#btn-update').removeAttr('disabled');
                    btnup.stop();
                }
            }
        });
    }
    return parseInt(status);
};
function insert_update_noconfirm(title, url, model) {
    //them moi Package
    var btnaddnew = document.getElementById('btn-addnew');
    if (btnaddnew != undefined) {
        $('#btn-addnew').attr('disabled', 'disabled');
        var btnagp = Ladda.create(document.querySelector('#btn-addnew'));
        btnagp.start();
    }
    var btnupdate = document.getElementById('btn-update');
    if (btnupdate != undefined) {
        $('#btn-update').attr('disabled', 'disabled');
        var btnup = Ladda.create(document.querySelector('#btn-update'));
        btnup.start();
    }
    $.ajax({
        url: url,
        contentType: 'application/json; charset=utf-8',
        traditional: true,
        data: JSON.stringify(model),
        dataType: 'json',
        type: "POST",
        success: function (data) {
            switch (data.status) {
                case -1: {
                    shownotify(title, "FAIL", data.result);
                }; break;
                case 0: {
                    shownotify(title, "WARNING", data.result)
                }; break
                case 1: {
                    shownotify(title, "SUCCESS", data.result);
                    cancel();
                }; break
            }
            if (btnaddnew != undefined) {
                $('#btn-addnew').removeAttr('disabled');
                btnagp.stop();
            }
            if (btnupdate != undefined) {
                $('#btn-update').removeAttr('disabled');
                btnup.stop();
            }
            return data.status;
        }
    });
};
function insert_update_v3(title, url, model) {
    if (confirm("Bạn có chắc " + title) == true) {


        var btn = document.getElementById('btn_AddNewPromotion');
        if (btn != undefined) {
            $('#btn_AddNewPromotion').attr('disabled', 'disabled');
            var btnladda = Ladda.create(document.querySelector('#btn_AddNewPromotion'));
            btnladda.start();
        }


        $.ajax({
            url: url,
            dataType: "json",
            type: "POST",
            data: $.param({ list: JSON.stringify(model) }),
            success: function (data) {
                status = data.status;
                switch (data.status) {
                    case -1: {
                        shownotify(title, "FAIL", data.result)
                    }; break;
                    case 1: {
                        shownotify(title, "SUCCESS", data.result);
                    }; break;
                    default: {
                        shownotify(title, "WARNING", data.result)
                    }; break;
                }
            }
        });
        if (btn != undefined) {
            $('#btn_AddNewPromotion').removeAttr('disabled');
            btnladda.stop();
        }
    }
    return parseInt(status);
};
function insert_update_shownotify(title, url, model) {
    if (confirm("Bạn có chắc " + title) == true) {

        //them moi Package
        var btnaddnew = document.getElementById('btn-addnew');
        if (btnaddnew != undefined) {
            $('#btn-addnew').attr('disabled', 'disabled');
            var btnagp = Ladda.create(document.querySelector('#btn-addnew'));
            btnagp.start();
        }
        var btnupdate = document.getElementById('btn-update');
        if (btnupdate != undefined) {
            $('#btn-update').attr('disabled', 'disabled');
            var btnup = Ladda.create(document.querySelector('#btn-update'));
            btnup.start();
        }
        $.ajax({
            url: url,
            contentType: 'application/json; charset=utf-8',
            traditional: true,
            data: JSON.stringify(model),
            dataType: 'json',
            type: "POST",
            success: function (data) {
                status = data.status;
                switch (data.status) {
                    case -1: {
                        shownotify(title, "FAIL", data.result);

                    }; break;
                    case 0: {
                        shownotify(title, "WARNING", data.result);

                    }; break
                    case 1: {
                        shownotify(title, "SUCCESS", data.result);
                        cancel();
                    }; break
                }
                if (data.status > 0)
                {
                    shownotify(title, "SUCCESS", data.result);
                    cancel();
                }
                if (btnaddnew != undefined) {
                    $('#btn-addnew').removeAttr('disabled');
                    btnagp.stop();
                }
                if (btnupdate != undefined) {
                    $('#btn-update').removeAttr('disabled');
                    btnup.stop();
                }
            }
        });
    }
    return parseInt(status);
};
//hàm thông báo
function shownotify(title, type, message) {
    switch (type) {
        case "SUCCESS": {
            notify('success', title, 'fa fa-check-circle', message, '', '_blank');
        }; break;
        case "WARNING": {
            notify('warning', title, 'fa fa-exclamation-circle', message, '', '_blank');
        }; break;
        case "FAIL": {
            notify('danger', title, 'fa fa-exclamation-triangle', message, '', '_blank');
        }; break;
        
    }
}
function notify(type, title, icon, message, url, target) {
    $.notify({
        type: type,
        icon: icon,
        title: title,
        message: message,
        target: '_blank',
    }, {
        // settings
        element: 'body',
        position: null,
        type: type,
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "top",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 999999,
        delay: 5000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInDown',
            exit: 'animated fadeOutUp'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
        template: '<div data-notify="container" class="notify col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
    });
}
  
function ajax_getData(btn, url, model, callback) {
    var btn_submit = document.getElementById(btn);
    if (btn_submit != undefined) {
        $('#' + btn).attr('disabled', 'disabled');
        var btnload = Ladda.create(document.querySelector('#' + btn));
        btnload.start();
    }
    $.ajax({ 
        url: url,
        contentType: 'application/json; charset=utf-8',
        //traditional: true,
        data: JSON.stringify(model),
        dataType: 'json',
        type: "POST",
        success: function (data) {
            btnload.stop();
            if (typeof callback === 'function')
                callback(data);
        },

    });
}
 