﻿using CMS.Core.ConstString;
using CMS.Infrastructure.HttpRequest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using ProjectHR.Hubs;

namespace ProjectHR.Controllers
{
    [Authorize]
    [Route(BaseURL.BlogUrl + "[action]")]
    public class BlogController : BaseController
    {
        public BlogController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext)
           : base(httprequest, notificationHubContext) { }
        public IActionResult Index()
        {
            var us = User;
            if (us == null)
                return RedirectToAction("Login", "Account");
            if (!Checktoken())
            {
                foreach (var cookie in Request.Cookies.Keys)
                {
                    Response.Cookies.Delete(cookie);
                }
                return RedirectToAction("Login", "Account");
            }
            ViewData["PageParentName"] = "Tài khoản";
            ViewData["PageName"] = "Blog";
            return View();
        }
        public IActionResult Create()
        {
            var us = User;
            if (us == null)
                return RedirectToAction("Login", "Account");
            if (!Checktoken())
            {
                foreach (var cookie in Request.Cookies.Keys)
                {
                    Response.Cookies.Delete(cookie);
                }
                return RedirectToAction("Login", "Account");
            }
            ViewData["PageParentName"] = "Tài khoản";
            ViewData["PageName"] = "Blog";
            return View();

        }
    }
}
    