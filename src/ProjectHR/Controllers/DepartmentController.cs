﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Employee;
using CMS.Infrastructure.HttpRequest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using ProjectHR.Hubs;
using System.Security.Claims;

namespace ProjectHR.Controllers
{
    [Authorize(Policy = Policy.AdminRole)] 
    public class DepartmentController : BaseController
    {
        #region Repository 
        private readonly string MenuType = "HR";
        private readonly string PageParentName = "Quản lý nhân viên";

        public DepartmentController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext)
        : base(httprequest, notificationHubContext) { }
        #endregion
        [HttpGet(EmployeeUrl.ListDepartments)]
        public IActionResult Index()
        {
            ViewData["MenuType"] = MenuType;
            ViewData["PageParentName"] = PageParentName;
            ViewData["PageName"] = "Quản lý phòng ban";
            return View();
        }
        [HttpGet()]
        public IActionResult getlist(string DepartmentCode = "", string DepartmentName = "")
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.DepartmentApiUrl + "GetDepartments", "Post",
                JsonConvert.SerializeObject(new { DepartmentCode, DepartmentName }), token: token);
            return Content(responseApi);
        }

        #region Function
        [HttpPost]
        public IActionResult Add([FromBody] DepartmentInsertModel model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            model.Status = 1;
            if (!ModelState.IsValid)
            { return Content(model); }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(model);
            var responseApi = _httprequest.GetData(UrlApi.DepartmentApiUrl, "POST", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult Edit(DepartmentUpdateModel model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            model.UpdateBy = User.FindFirst(ClaimTypes.Email).Value;
            if (!ModelState.IsValid)
            { return Content(model); }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(model);
            var responseApi = _httprequest.GetData(UrlApi.DepartmentApiUrl + model.ID,"PUT", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult Delete(int ID)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.DepartmentApiUrl + ID, "DELETE", null, token: token);
            return Content(responseApi);
        }
        #endregion


    }
}
