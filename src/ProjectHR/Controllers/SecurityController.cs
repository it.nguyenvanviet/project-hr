﻿using System.Diagnostics;
using CMS.Core.ConstString;
using CMS.Infrastructure.HttpRequest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using ProjectHR.Hubs;
using ProjectHR.Models;

namespace ProjectHR.Controllers
{
    [Authorize]
    public class SecurityController : BaseController
    {
        public SecurityController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext)
             : base(httprequest, notificationHubContext) { }

        [AllowAnonymous]
        [HttpGet(SecurityUrl.AccountDenied)]
        public IActionResult AccessDenied()
        {
            var error = new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                ErrorCode = 401,
                ErrorString = $"<p> Access denied to this page</p>"
            }; 
            return View(error);
        }


        /// <summary>
        /// Return Error
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("error/{code:int}")]
        public IActionResult Error(int code = 0)
        {
            var error = new ErrorViewModel
            {
                RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                ErrorCode = code,
            };
            switch (code)
            {
                case 403: error.ErrorString = "<p>You don't have permission to access / on this server.</p>"; break;
                case 404: error.ErrorString = "<p>The page you were looking for could not be found, please <a href=\"javascript: void(0); \">contact us</a> to report this issue.</p>"; break;
                case 500: error.ErrorString = "<p>Apparently we're experiencing an error. But don't worry, we will solve it shortly.<br > Please try after some time.</p> "; break;
                case 503: error.ErrorString = "<p>This site is getting up in few minutes.</p>"; break;
            }
            return View(error);
        }
    }
}