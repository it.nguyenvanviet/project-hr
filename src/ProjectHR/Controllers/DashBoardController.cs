﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ProjectHR.Controllers
{

    [Authorize]
    public class DashBoardController : Controller
    {


        public IActionResult Index()
        { 
            ViewData["Title"] = "Trang quản trị";
            ViewData["User"] = User;

            ViewData["PageParentName"] = "Trang quản trị";
            ViewData["PageName"] = "Home";
            return View();
        }


         
       
        
        /// <summary>
        /// PageMaintenance
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult PageMaintenance()
        {
            return View();
        }
   
     
    }
}