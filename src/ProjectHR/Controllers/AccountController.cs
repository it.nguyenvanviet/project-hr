﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CMS.Core.Domain.Employee;
using CMS.Core.Domain.Account;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using CMS.Core.ConstString;
using CMS.Infrastructure.HttpRequest;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using CMS.Services.Lib.Upload;
using Microsoft.AspNetCore.Hosting;
using CMS.Data.Enum;
using CMS.Services.Lib.Helper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.SignalR;
using ProjectHR.Hubs;

namespace ProjectHR.Controllers
{

    [Authorize]
    [Route(BaseURL.AccountUrl + "[action]")]
    public class AccountController : BaseController
    {
        #region Repository
        private readonly string MenuType = "HR";

        private readonly IWebHostEnvironment _env;
        public AccountController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext, IWebHostEnvironment env)
           : base(httprequest, notificationHubContext)
        {
            _env = env;
        }

        #endregion
        #region Login & Register 

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string RequestPath = "")
        { 
            var us = User.Claims.Count();

            if (us > 0)
                return RedirectToAction("Profile", "Account");
             
            ViewData["Title"] = "Đăng nhập";
            var model = new LoginModel
            {
                RequestPath = RequestPath
            };
            return View(model);
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (!ModelState.IsValid)
                return View(model);
            else
            {
                try
                {
                    var datalogin = await _httprequest.Login(model.Username, model.Password);
                    if (datalogin != null)
                    { 
                        var imagepath = ImageHelper.TempImage(null, datalogin.user.EmpName.Substring(0, 1));
                        if (datalogin.user.Avartar_Resize != null)
                        {
                            imagepath = ImageHelper.Image_CheckExistAvatar(datalogin.user.Avartar_Resize,  _env.WebRootPath,  ImageHelper.TempImage(null, datalogin.user.EmpName.Substring(0, 1)));
                        }   

                        List<Claim> claims = new List<Claim>
                        {
                            new Claim(ClaimTypes.Name, datalogin.user.EmpName),
                            new Claim(ClaimTypes.Email, model.Username),
                            new Claim(ClaimTypes.Role, datalogin.user.RoleName),
                            new Claim(ClaimTypes.Sid, datalogin.user.UserID),
                            new Claim(ClaimTypes.NameIdentifier, datalogin.user.RoleName),
                            new Claim("avartar",imagepath),
                            new Claim("empid",datalogin.user.EmpID != null ? datalogin.user.EmpID: ""),
                            new Claim("Experience",datalogin.user.Experience.ToString()),
                            new Claim("Token",datalogin.token),
                            new Claim("Tokenexpired", datalogin.expired.ToString())
                        };
                        // create identity
                        ClaimsIdentity identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                        var props = new AuthenticationProperties();
                        props.IsPersistent = model.RememberMe;
                        props.AllowRefresh = true;
                        props.ExpiresUtc = DateTime.UtcNow.AddHours(TokenConfig.MinExpiredHour);
                        // create principal
                        ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,  principal, props);
                        return Redirect(model.RequestPath ?? nameof(Profile));
                    }
                    else
                    {
                        return View(model);
                    }
                }
                catch 
                {
                    return View(model);
                } 
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
      
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(scheme: CookieAuthenticationDefaults.AuthenticationScheme);
            foreach (var cookie in Request.Cookies.Keys)
            {
                Response.Cookies.Delete(cookie);
            }
            return RedirectToAction("Login");
        }
         
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }
        #endregion

        #region Profile 
        /// <summary>
        /// Profile 
        /// </summary>
        /// <returns></returns> 
        public IActionResult Profile()
        {
            if (!Checktoken())
            {
                return RedirectToAction(nameof(Logout));
            }

            ViewData["PageParentName"] = "Tài khoản";
            ViewData["PageName"] = "Thông tin cá nhân";
            ViewData["MenuType"] = MenuType;
            return View();
        }

        #region get data

        
        /// <summary>
        /// load profile
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public IActionResult getprofile()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var userid = User.FindFirst(ClaimTypes.Sid).Value;
            var token = User.FindFirst("Token").Value;
            var emp = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getinfobyuserid?userid=" + userid, "GET", token: token);
            if(emp != null)
            {

            }    
            return Content(emp);
        }
        [HttpGet]
        public IActionResult getbasicinfocontact()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var userid = User.FindFirst(ClaimTypes.Sid).Value;
            var token = User.FindFirst("Token").Value;
            var emp = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getinfobyuserid?userid=" + userid, "GET", token: token);
            if (emp != null)
            {

            }
            return Content(emp);
        }
        [HttpGet]
        public IActionResult geteducation()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var userid = User.FindFirst(ClaimTypes.Sid).Value;
            var token = User.FindFirst("Token").Value;
            var emp = _httprequest.GetData(UrlApi.EmployeeApiUrl + "geteducationbyuserid?userid=" + userid, "GET", token: token);

            return Content(emp);
        }
        [HttpGet]
        public IActionResult getexperience()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var userid = User.FindFirst(ClaimTypes.Sid).Value;
            var token = User.FindFirst("Token").Value;
            var emp = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getexperiencesbyuserid?userid=" + userid, "GET", token: token);

            return Content(emp);
        }
        [HttpGet]
        public IActionResult getsocial()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var userid = User.FindFirst(ClaimTypes.Sid).Value;
            var token = User.FindFirst("Token").Value;
            var emp = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getsocialbyuserid?userid=" + userid, "GET", token: token);

            return Content(emp);
        }
        [HttpGet]
        public IActionResult getskill()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var userid = User.FindFirst(ClaimTypes.Sid).Value;
            var token = User.FindFirst("Token").Value;
            var emp = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getskillbyuserid?userid=" + userid, "GET", token: token);

            return Content(emp);
        }
        [HttpGet]
        public IActionResult getspecialism()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var userid = User.FindFirst(ClaimTypes.Sid).Value;
            var token = User.FindFirst("Token").Value;
            var emp = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getspecialismbyuserid?userid=" + userid, "GET", token: token);

            return Content(emp);
        }

        #region enum and databasic
        [AllowAnonymous]
        [HttpGet]
        public IActionResult getranks()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var data = Enum.GetValues(typeof(Rank))
            .Cast<Rank>()
            .Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) })
            .ToList();
            return Content(data);
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult geteducationdegree()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var data = Enum.GetValues(typeof(EducationDegree))
            .Cast<EducationDegree>()
            .Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) })
            .ToList();
            return Content(data);
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult getsocialtype()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var data = Enum.GetValues(typeof(SocialType))
            .Cast<SocialType>()
            .Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) })
            .ToList();
            return Content(data);
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult getTypeSkill()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var data = Enum.GetValues(typeof(TypeSkillEnum))
            .Cast<TypeSkillEnum>()
            .Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) })
            .ToList();
            return Content(data);
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult getgender()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var data = Enum.GetValues(typeof(GenderEnum))
            .Cast<GenderEnum>()
            .Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) })
            .ToList();
            return Content(data);
        }
        [AllowAnonymous]
        [HttpGet]
        public IActionResult getstatusemp()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var data = Enum.GetValues(typeof(StatusEmp))
            .Cast<StatusEmp>()
            .Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) })
            .ToList();
            return Content(data);
        }
        [HttpGet]
        public IActionResult getlistspecialism()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseapi = _httprequest.GetData(UrlApi.SpecialismApiUrl, "GET", token: token);
            return Content(responseapi);
        } 
        #endregion

        [HttpGet]
        public IActionResult getmenutypebyuserid()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var userid = User.FindFirst(ClaimTypes.Sid).Value;
            var responseapi = _httprequest.GetData(MenuUrl.getmenutypebyuserid + "/" + userid, "GET", token: token);
            return Content(responseapi);
        }
        [HttpGet]
        public IActionResult getmenubyuserid()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var userid = User.FindFirst(ClaimTypes.Sid).Value;
            var responseapi = _httprequest.GetData(MenuUrl.getmenubyuserid + "/" + userid, "GET", token: token);
            return Content(responseapi);
        }
        #endregion

        #region UpdateDataBasic


        /// <summary>
        /// UpdateAvartar
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult UpdateAvartar(List<IFormFile> files, EmpUpdateAvatarViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            emp.Avartar = null;
            emp.UpdateBy = emp.Email;
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            try
            {
                foreach (IFormFile file in files.Where(file => file.Length > 0).Select(file => file))
                {
                    emp.Avartar = UploadHelper.UploadAvatar(_env.WebRootPath, file, emp.Name);
                }
            }
            catch
            { }
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "updateavartar", "PUT", datapost, token: token);
            return Content(responseApi);
        }
        
        /// <summary>
        /// UpdateBasicInfo
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult UpdateBasicInfo([FromBody] EmpUpdateViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            emp.UpdateBy = emp.Email;
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "updatebasicinfo", "PUT", datapost, token: token);
            return Content(responseApi);
        }

        [HttpPost]
        public IActionResult ChangePassword([FromBody] UpdatePasswordModel model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(model);
            }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(model);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "changepassword", "PUT", datapost, token: token);
            return Content(responseApi);
        }


        #endregion

        #region education
        [HttpPost]
        public IActionResult AddEducation([FromBody] EmpAdd_EducationViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "addeducation", "POST", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult EditEducation(EmpUpdate_EducationViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "editeducation", "PUT", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult DeleteEducation(EmpUpdate_EducationViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "education/" + emp.ID, "DELETE", null, token: token);
            return Content(responseApi);
        }
        #endregion

        #region experience
        [HttpPost]
        public IActionResult AddExperience([FromBody] EmpAdd_ExperienceViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "addexperiences", "POST", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult EditExperience(EmpUpdate_ExperienceViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "editexperience", "PUT", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult DeleteExperience(EmpUpdate_ExperienceViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "experience/" + emp.ID, "DELETE", null, token: token);
            return Content(responseApi);
        }
        #endregion

        #region social
        [HttpPost]
        public IActionResult AddSocial([FromBody] EmpAdd_SocialViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "addsocial", "POST", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult EditSocial(EmpUpdate_SocialViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "editsocial", "PUT", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult DeleteSocial(EmpUpdate_SocialViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "social/" + emp.ID, "DELETE", null, token: token);
            return Content(responseApi);
        }
        #endregion

        #region skill
        [HttpPost]
        public IActionResult AddSkill([FromBody] EmpAdd_SkillViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "addskill", "POST", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult EditSkill(EmpUpdate_SkillViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "editskill", "PUT", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult DeleteSkill(EmpUpdate_SkillViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "skill/" + emp.ID, "DELETE", null, token: token);
            return Content(responseApi);
        }
        #endregion

        #region Specialism

        [HttpPost]
        public IActionResult AddSpecialism([FromBody] EmpAdd_SpecialismViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "addspecialism", "POST", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult EditSpecialism(SpecialismViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var modelupdate = new EmpUpdate_SpecialismViewModel();
            modelupdate.EmpID =  int.Parse(User.FindFirst("empid").Value);
            modelupdate.UpdateBy = User.FindFirst(ClaimTypes.Email).Value;
            modelupdate.ID = emp.ID;    
            modelupdate.SpecialismID = emp.SpecialismID; 
            var token = User.FindFirst("Token").Value;
            var datapost = JsonConvert.SerializeObject(modelupdate);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "editspecialism", "PUT", datapost, token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult DeleteSpecialism(SpecialismViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "specialism/" + emp.ID, "DELETE", null, token: token);
            return Content(responseApi);
        }
        #endregion



        #endregion


    }
}