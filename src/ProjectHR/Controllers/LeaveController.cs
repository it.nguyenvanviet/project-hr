﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Leave;
using CMS.Data.Enum;
using CMS.Infrastructure.HttpRequest;
using CMS.Services.Lib.Helper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using ProjectHR.Hubs;
using System;
using System.Linq;
using System.Security.Claims;

namespace ProjectHR.Controllers
{
    [Authorize(Policy = Policy.AdminRole)]
    public class LeaveController : BaseController
    {
        private readonly string PageParentName = "Đơn phép";
        private readonly string MenuType = "HR";
        public LeaveController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext)
          : base(httprequest, notificationHubContext) { }
        [HttpGet]
        [Route(EmployeeUrl.ListLeaveRequest)]
        public IActionResult Index()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            ViewData["PageParentName"] = "Quản lý nhân viên";
            ViewData["PageName"] = PageParentName;
            ViewData["MenuType"] = MenuType;

            return View();
        }
        [HttpGet]
        public IActionResult getCountLeaveRequest()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.LeaveApiUrl + "GetCountLeaveRequest", "GET", null, token: token);
            return Content(responseApi);
        }
        
        [HttpPost]
        public IActionResult searchrequest([FromBody] LeaveSearchParamModel model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.LeaveApiUrl + "GetListLeaveRequest", "POST", JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }

        #region get enum
        [HttpGet]
        public IActionResult getApproveStatus()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var data = Enum.GetValues(typeof(ApproveStatusEnum))
            .Cast<ApproveStatusEnum>()
            .Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) }).ToList();
            return Content(data);
        }
        [HttpGet]
        public IActionResult getLeaveStatus()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var data = Enum.GetValues(typeof(LeaveStatusEnum))
            .Cast<LeaveStatusEnum>()
            .Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) })
            .ToList();
            return Content(data);
        }
        [HttpGet]
        public IActionResult getLeaveType()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var data = Enum.GetValues(typeof(LeaveTypeEnum))
            .Cast<LeaveTypeEnum>()
            .Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) })
            .ToList();
            return Content(data);
        }
        #endregion

        #region Function 
        [HttpPost]
        public IActionResult Add([FromBody] LeaveInsertModel model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            model.CreateBy = User.FindFirst(ClaimTypes.Email).Value;
            var responseApi = _httprequest.GetData(UrlApi.LeaveApiUrl, "POST", JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult Update([FromBody] LeaveUpdateModel model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            model.UpdateBy = User.FindFirst(ClaimTypes.Email).Value;
            var responseApi = _httprequest.GetData(UrlApi.LeaveApiUrl + model.ID, "PUT", JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult Delete([FromRoute] int ID)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.LeaveApiUrl + ID, "Delete", token: token);
            return Content(responseApi);
        }

        [HttpPost]
        public IActionResult changeApproved([FromBody] ChangeLeaveApproved model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            model.UpdateBy = User.FindFirst(ClaimTypes.Email).Value;
            var responseApi = _httprequest.GetData(UrlApi.LeaveApiUrl + "ChangeApproved", "PUT", JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
        #endregion
    }
}
