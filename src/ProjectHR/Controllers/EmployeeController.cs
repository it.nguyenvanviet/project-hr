﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Account;
using CMS.Core.Domain.Employee;
using CMS.Data.Enum;
using CMS.Infrastructure.HttpRequest;
using CMS.Services.Lib.Helper;
using CMS.Services.Lib.Upload;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using ProjectHR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace ProjectHR.Controllers
{
    [Authorize(Policy = Policy.AdminRole)]
    [Route(BaseURL.EmployeeURL + "[action]")]
    public class EmployeeController : BaseController
    {

        #region Repository 

        private readonly IWebHostEnvironment _env;

        private readonly string PageParentName = "Quản lý nhân viên";
        private readonly string MenuType = "HR";
        public EmployeeController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext, IWebHostEnvironment env)
           : base(httprequest, notificationHubContext)
        {
            _env = env;
        } 
       
        #endregion

       

        /// <summary>
        /// List 
        /// </summary>
        /// <param name="searchString"></param>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        [HttpGet(EmployeeUrl.ListEmp)]
        public IActionResult Index()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            ViewData["MenuType"] = MenuType;
            ViewData["PageParentName"] = PageParentName;
            ViewData["PageName"] = "Danh sách nhân viên";
            var menu = new Employees();
            return View(menu);
        }

        #region API

      
        
        [HttpGet("{ID}")]
        public IActionResult Getbyid(int ID)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + ID, token: token);
            return Content(responseApi);
        }
        [HttpGet]
        public IActionResult listrole()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseapi = _httprequest.GetData(UrlApi.ApiauthenGetRole, token: token);
            return Content(responseapi);
        }
        [HttpGet("{ID}")]
        public IActionResult getlistrole(int ID)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getrole?empid=" + ID, token: token);
            return Content(responseApi);
        }

        [HttpGet]
        public IActionResult getstatusemp()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var data = Enum.GetValues(typeof(StatusEmp))
            .Cast<StatusEmp>()
            .Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) })
            .ToList();
            return Content(data);
        }
        [HttpGet]
        public IActionResult getsocial(int empid)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getsocial?empid=" + empid, token: token);
            return Content(responseApi);
        }
        [HttpGet]
        public IActionResult getspecialism(int empid)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getspecialism?empid=" + empid, token: token);
            return Content(responseApi);
        }
        [HttpGet]
        public IActionResult geteducation(int empid)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "geteducation?empid=" + empid, token: token);
            return Content(responseApi);
        }
        [HttpGet]
        public IActionResult getexperiences(int empid)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getexperiences?empid=" + empid, token: token);
            return Content(responseApi);
        }
        #endregion

        #region Functions 

        [HttpPost]
        public IActionResult Search([FromBody] EmpSearchModel model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "Search","POST",JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
        /// <summary>
        /// Tạo mới nhân viên
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Create()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            ViewData["PageParentName"] = PageParentName;
            ViewData["MenuType"] = MenuType;
            ViewData["PageName"] = "Danh sách nhân viên";
            var emp = new EmpViewModel
            {
                RegisterView = new RegisterViewModel()
            };
            return View(emp);
        }
        [HttpPost]
        public IActionResult CreateEmp(List<IFormFile> files, EmpRegisViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            emp.Avartar = null;
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            try
            {
                foreach (IFormFile file in files.Where(file => file.Length > 0).Select(file => file))
                {
                    emp.Avartar = UploadHelper.UploadAvatar(_env.WebRootPath, file, emp.Name);
                }
            }
            catch
            { }
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "create", "POST", datapost, token: token);
            return Content(responseApi);
        }

        /// <summary>
        /// Điều chỉnh thông tin nhân viên
        /// </summary>
        /// <returns></returns>
        [HttpGet("{ID}")]
        public IActionResult Edit(int ID)
        {
            ViewData["PageParentName"] = PageParentName;
            ViewData["PageName"] = "Điều chỉnh bổ sung";
            ViewData["MenuType"] = MenuType;
            ViewData["EmpID"] = ID;
            var emp = new EmpViewModel
            {
                RegisterView = new RegisterViewModel()
            };
            return View(emp);
        }
        
        [HttpPost] 
        public IActionResult EditEmp(List<IFormFile> files, EmpUpdateViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            if (files.Count() > 0)
                emp.Avartar = null;
            if (!ModelState.IsValid)
            {
                return Content(emp);
            }
            var token = User.FindFirst("Token").Value;
            try
            {
                foreach (IFormFile file in files.Where(file => file.Length > 0).Select(file => file))
                {
                    emp.Avartar = UploadHelper.UploadAvatar(_env.WebRootPath, file, emp.Name);
                }
            }
            catch
            { }
            emp.UpdateBy = User.FindFirst(ClaimTypes.Email).Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + emp.ID, "PUT", datapost, token: token);
            return Content(responseApi); 
        }

        [HttpPost] 
        public IActionResult EditRoleEmp([FromBody] EmpUpdateRoleViewModel emp)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            emp.UpdateBy = User.FindFirst(ClaimTypes.Email).Value;
            var datapost = JsonConvert.SerializeObject(emp);
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "updaterole", "PUT", datapost, token: token);
            return Content(responseApi);
        } 
        #endregion
    }
}