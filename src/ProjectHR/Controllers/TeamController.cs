﻿using CMS.Core.ConstString;
using CMS.Infrastructure.HttpRequest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using CMS.Services.Lib.Helper;
using Microsoft.AspNetCore.SignalR;
using ProjectHR.Hubs;
using CMS.Data.Enum;
using CMS.Core.Domain.Team.Parameter;
using Newtonsoft.Json;
using System.Security.Claims;

namespace ProjectHR.Controllers
{
    [Authorize(Policy = Policy.AdminRole)]
    public class TeamController : BaseController
    {
        private readonly IWebHostEnvironment _env;
        private readonly string PageParentName = "Quản lý nhóm";
        private readonly string MenuType = "HR";
        public TeamController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext,
            IWebHostEnvironment env) : base(httprequest, notificationHubContext)
        {
            _env = env;
        } 
        [HttpGet(EmployeeUrl.ListTeam)]
        public IActionResult Index()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            ViewData["PageParentName"] = "Quản lý nhân viên";
            ViewData["PageName"] = PageParentName;
            ViewData["MenuType"] = MenuType; 
            
            return View();
        }

        [HttpPost()]
        public IActionResult Search([FromBody] TeamSearchModel model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.TeamApiUrl + "GetTeams", "POST", JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
        
        [HttpGet]
        public IActionResult GetByID([FromRoute] int ID)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.TeamApiUrl + ID, "GET",null, token: token);
            return Content(responseApi);
        }    
        
        #region enum
        [AllowAnonymous]
        [HttpGet]
        public IActionResult getstatus()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var data = Enum.GetValues(typeof(StatusEnum))
            .Cast<StatusEnum>()
            .Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) })
            .ToList();
            return Content(data);
        }
        [HttpGet]
        public IActionResult getlistleader(int departid = 0)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getteamleader/" + departid, "GET", null, token: token);
            return Content(responseApi);
        }
        [HttpGet]
        public IActionResult getlistmember(int departid = 0)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.EmployeeApiUrl + "getteammember/" + departid, "GET", null, token: token);
            return Content(responseApi);
        }
        #endregion

        #region Function 
        [HttpPost]
        public IActionResult Add([FromBody] TeamInsertModel model )
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            model.CreateBy = User.FindFirst(ClaimTypes.Email).Value;
            var responseApi = _httprequest.GetData(UrlApi.TeamApiUrl , "POST", JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult Update([FromBody] TeamUpdateModel model )
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            model.UpdateBy = User.FindFirst(ClaimTypes.Email).Value;
            var responseApi = _httprequest.GetData(UrlApi.TeamApiUrl +model.ID, "PUT", JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult Delete([FromRoute]  int ID)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value; 
            var responseApi = _httprequest.GetData(UrlApi.TeamApiUrl + ID , "Delete",  token: token);
            return Content(responseApi);
        }
        #endregion
    }
}
