﻿using CMS.Core.ConstString;
using CMS.Infrastructure.HttpRequest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using ProjectHR.Hubs;


namespace ProjectHR.Controllers
{
    [Authorize(Policy = Policy.AdminRole)]
    public class EventController : BaseController
    {
        private readonly string PageParentName = "Sự kiện";
        private readonly string MenuType = "HR";
        public EventController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext)
          : base(httprequest, notificationHubContext) { }
        [HttpGet]
        [Route(EmployeeUrl.ListEvents)]
        public IActionResult Index()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            ViewData["PageParentName"] = "Quản lý nhân viên";
            ViewData["PageName"] = PageParentName;
            ViewData["MenuType"] = MenuType;

            return View();
        }
    }
}
