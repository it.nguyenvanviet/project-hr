﻿using CMS.Core.ConstString;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ProjectHR.Areas.Manager.Controllers
{

    [Authorize(Policy = Policy.AdminRole)]
    public class CommunicationController : Controller
    {
        [HttpGet(CommunicationUrl.Chat)]
        public IActionResult Appchat()
        {
            return View();
        }
    }
}