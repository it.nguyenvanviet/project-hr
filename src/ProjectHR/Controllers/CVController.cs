﻿using System;
using CMS.Infrastructure.HttpRequest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using ProjectHR.Hubs;

namespace ProjectHR.Controllers
{
    public class CVController : BaseController
    {
        public CVController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext)
           : base(httprequest, notificationHubContext) { }
        [AllowAnonymous]
        [HttpGet("cv/{name}-{empid}")]
        public IActionResult CVReview(string name, string empid, string page = "")
        {
            if(String.IsNullOrEmpty(empid))
            {
                return NotFound();
            }    
            return View();
        }

    }
}
