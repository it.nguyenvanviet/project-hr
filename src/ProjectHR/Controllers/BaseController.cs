﻿using CMS.Infrastructure.HttpRequest;
using CMS.Infrastructure.Pattern;
using CMS.Services.Lib.Helper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using ProjectHR.Hubs;
using System;
using System.IO;
using System.Text;

namespace ProjectHR.Controllers
{
    public abstract class BaseController : Controller
    {

        protected readonly IHubContext<NotificationsHub> _notificationHubContext;
        protected readonly IHttpRequest _httprequest;
        protected BaseController(IHttpRequest httprequest,IHubContext<NotificationsHub> notificationHubContext)
        {  
            _notificationHubContext = notificationHubContext;
            _httprequest = httprequest;
        }
        protected virtual ContentResult Content(object obj)
        {
            return Content(JsonConvert.SerializeObject(obj), "application/json", Encoding.UTF8);
        }

        /// <summary>
        /// Render partial view to string
        /// </summary>
        /// <param name="viewName">View name</param>
        /// <param name="model">Model</param>
        /// <returns>Result</returns>
        protected virtual string RenderPartialViewToString(string viewName, object model)
        {
            //get Razor view engine
            var razorViewEngine = EngineContext.Current.Resolve<IRazorViewEngine>();

            //create action context
            var actionContext = new ActionContext(HttpContext, RouteData, ControllerContext.ActionDescriptor, ModelState);

            //set view name as action name in case if not passed
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.ActionDescriptor.ActionName;

            //set model
            ViewData.Model = model;

            //try to get a view by the name
            var viewResult = razorViewEngine.FindView(actionContext, viewName, false);
            if (viewResult.View == null)
            {
                //or try to get a view by the path
                viewResult = razorViewEngine.GetView(null, viewName, false);
                if (viewResult.View == null)
                    throw new ArgumentNullException($"{viewName} view was not found");
            }
            using (var stringWriter = new StringWriter())
            {
                var viewContext = new ViewContext(actionContext, viewResult.View, ViewData, TempData, stringWriter, new HtmlHelperOptions());

                var t = viewResult.View.RenderAsync(viewContext);
                t.Wait();
                return stringWriter.GetStringBuilder().ToString();
            }
        }
        protected bool Checktoken()
        {
            var token = User.FindFirst("Token").Value;
            var expire = User.FindFirst("Tokenexpired").Value;
            if (token.Length == 0)
                return false;
            if (expire != null)
                if (DateTime.Now > DatetimeHelper.ConvertToDateTime(expire))
                    return false;
            return true;
        }

        protected async void sendnotify(string head = "", string url = "", string content = "", int type = 0, string method = "sendToUser")
        {
            await _notificationHubContext.Clients.All.SendAsync(method, type, url, head, content);
        }

        /// <summary>
        /// Access denied view
        /// </summary>
        /// <returns>Access denied view</returns>
        protected virtual IActionResult AccessDeniedView()
        {
            //var webHelper = EngineContext.Current.Resolve<IWebHelper>();

            return RedirectToAction("AccessDenied", "Security");
        } 

    }   
}