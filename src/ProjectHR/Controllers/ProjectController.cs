﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Projects;
using CMS.Infrastructure.HttpRequest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using CMS.Data.Enum;
using CMS.Services.Lib.Helper;
using System;
using Microsoft.AspNetCore.SignalR;
using ProjectHR.Hubs;

namespace ProjectHR.Controllers
{

    [Authorize(Policy = Policy.AdminRole)]
    public class ProjectController : BaseController
    {
 
         
        #region Repository

        private readonly string PageParentName = "Quản lý dự án";
        private readonly string MenuType = "Project"; 
        private readonly IWebHostEnvironment _env;

        public ProjectController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext, 
            IWebHostEnvironment env)
           : base(httprequest, notificationHubContext)
        {
            _env = env;
        }
        #endregion

        #region Action

        [HttpGet]
        public IActionResult getOptionProjectSearch()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var listprojecttype = _httprequest.GetData(UrlApi.ProjectApiUrl + "getProjectType", "GET", token: token);
            var listPriority = Enum.GetValues(typeof(PriorityEnum)).Cast<PriorityEnum>().Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) }).ToList();
            var listProjectStatus = Enum.GetValues(typeof(StatusEnum)).Cast<StatusEnum>().Select(v => new { id = v, text = EnumHelper.ToDisplayName(v) }).ToList();
            var data = new
            {
                projectType = listprojecttype,
                projectPriority = listPriority,
                projectStatus = listProjectStatus
            };
            return Content(data);
        }
        /// <summary>
        /// Danh sách Dự án
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet(ProjectUrl.ListProject)]
        public IActionResult ListProject()
        {
            ViewData["PageParentName"] = PageParentName;
            ViewData["PageName"] = ViewData["Title"] = "Danh sách dự án";
            ViewData["MenuType"] = MenuType;

            return View();
        }


        /// <summary>
        /// tìm kiếm project
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SearchProject(ProjectParamSearch model)
        {
            ViewData["PageParentName"] = PageParentName;
            ViewData["PageName"] = ViewData["Title"] = "Tìm kiếm dự án";
            return View();
        } 

        /// <summary>
        /// Create Project
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        } 
        
        [HttpPost] 
        [ValidateAntiForgeryToken]
        public IActionResult Create(Projects projects)
        { 
            return RedirectToAction(nameof(ListProject));
        }

        /// <summary>
        /// Edit Project
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Edit(int ID)
        { 
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Projects projects)
        {
            return RedirectToAction(nameof(ListProject));
        }
        
        /// <summary>
        /// Review Project
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Review(int ID)
        {
            ViewData["PageParentName"] = PageParentName;
            ViewData["PageName"] = ViewData["Title"] = "Điều chỉnh dự án"; 
            return View( );
        }
        #endregion
    }
}