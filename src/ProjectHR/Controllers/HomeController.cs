﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace ProjectHR.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        //[HttpGet(ProfileUrl.myprofile)]
        public IActionResult Index()
        {
             return RedirectToAction("Profile", "Account");
        }   
        [AllowAnonymous]
        public IActionResult Index2()
        {
            try
            { ViewData["UserID"] = User.FindFirst(ClaimTypes.Sid).Value; ; }
            catch { ViewData["UserID"] = null; }
            return View();
        }
    }
    
}
