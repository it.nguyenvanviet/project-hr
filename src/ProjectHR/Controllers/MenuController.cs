﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Menu;
using CMS.Infrastructure.HttpRequest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using ProjectHR.Hubs;

namespace ProjectHR.Controllers
{

    [Authorize(Policy = Policy.AdminRole)]
    [Route(BaseURL.MenuUrl + "[action]")]
    public class MenuController : BaseController
    {
        #region Repository 
         
        private readonly string MenuType = "Setting";
        public MenuController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext)
           : base(httprequest, notificationHubContext) { }
        #endregion

        [HttpGet(MenuUrl.ListMenu)]
        public IActionResult Index()
        {
            if(!Checktoken())
                return RedirectToAction("Login", "Account");
            ViewData["PageParentName"] = "Cấu hình hệ thống";
            ViewData["PageName"] = "Quản lý menu";
            ViewData["MenuType"] = MenuType;
            var menu = new Menus();
            return View(menu);
        }
        [HttpGet]
        public IActionResult Getall()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.MenuApiUrl + "GetMenus", token: token); 
            return Content(responseApi);
        }
        [HttpGet]
        public IActionResult getmenutypes()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseapi = _httprequest.GetData(MenuUrl.getmenutype , "GET", token: token); 
            return Content(responseapi);
        }
        [HttpGet("{id}")]
        public IActionResult Getbyid([FromRoute] int id )
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.MenuApiUrl + id, token: token);
            return Content(responseApi);
        }
        [HttpGet]
        public IActionResult Getlistrole()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseapi = _httprequest.GetData(UrlApi.ApiauthenGetRole,token:token);
            return Content(responseapi);
        }
        [HttpGet("{ID}")]
        public IActionResult Getlistrolenbyid([FromRoute] int id)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseapi = _httprequest.GetData(UrlApi.MenuApiUrl+id+ "/role", token: token);
            return Content(responseapi);
        }
        [HttpGet]
        public IActionResult Getposition()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseapi = _httprequest.GetData(UrlApi.MenuApiUrl + "position", token: token);
            return Content(responseapi);
        }
        /// <summary>
        /// Create New Menu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create([FromBody] MenuViewModel model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.MenuApiUrl + "create", "POST", JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
        /// <summary>
        /// Update menu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Update([FromBody] MenuViewModel model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.MenuApiUrl + model.ID , "PUT",JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
        /// <summary>
        /// Update menu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult UpdateRole([FromBody] MenuRoleParaUpdate model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.MenuApiUrl+ "updaterole", "PUT", JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult UpdatePosition([FromBody] RootMenu model)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.MenuApiUrl + "updateposition", "PUT", JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
        [HttpPost]
        public IActionResult Delete(int ID)
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.MenuApiUrl + ID, "DELETE", token: token);
            return Content(responseApi);
        }

    }
}