﻿using CMS.Core.ConstString;
using CMS.Infrastructure.HttpRequest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using ProjectHR.Hubs;

namespace ProjectHR.Controllers
{
    [Authorize]
    public class ActivitiesController : BaseController
    {
        private readonly string PageParentName = "Hoạt động";
        private readonly string MenuType = "HR";
        public ActivitiesController(IHttpRequest httprequest, IHubContext<NotificationsHub> notificationHubContext)
          : base(httprequest, notificationHubContext) { }
        [HttpGet]
        [Route(EmployeeUrl.ListActivities)]
        public IActionResult Index()
        {
            if (!Checktoken())
                return RedirectToAction("Login", "Account");
            ViewData["PageParentName"] = "Quản lý nhân viên";
            ViewData["PageName"] = PageParentName;
            ViewData["MenuType"] = MenuType;

            return View();
        }
    }
}
