﻿using System.Security.Claims;
using System.Threading.Tasks;
using CMS.Core.ConstString;
using CMS.Core.Domain.Security;
using CMS.Core.Domain.Setting;
using CMS.Infrastructure.HttpRequest;
using CMS.Infrastructure.Mapping.Interfaces.Permission;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using ProjectHR.Hubs;

namespace ProjectHR.Controllers
{
    public class SettingController : BaseController
    {
   
        private readonly IPermissionService _permissionService;
        private readonly string MenuType = "Setting";
        private readonly string PageParentName = "Setting";
        public SettingController(
            IHttpRequest httprequest, 
            IHubContext<NotificationsHub> notificationHubContext,
            IPermissionService permissionService)
           : base(httprequest, notificationHubContext)
        {
            _permissionService = permissionService;
        }
      
        [HttpGet]
        public async Task<IActionResult> GeneralSettingAsync()
        {
            if (! await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageSettings, User.FindFirst(ClaimTypes.Sid).Value))
                return AccessDeniedView();
            ViewData["PageParentName"] = "Cấu hình hệ thống";
            ViewData["MenuType"] = MenuType;
            ViewData["PageName"] = PageParentName;
            return View();
        }
        [HttpGet]
        public IActionResult MailSetting()
        {
            ViewData["PageParentName"] = "Cấu hình hệ thống";
            ViewData["PageName"] = PageParentName;
            ViewData["MenuType"] = MenuType;
            return View();
        }
        [HttpPost]
        public ContentResult UpdateGeneralSetting(Setting model)
        {
            var token = User.FindFirst("Token").Value;
            var responseApi = _httprequest.GetData(UrlApi.SettingApiUrl + "update", "PUT", JsonConvert.SerializeObject(model), token: token);
            return Content(responseApi);
        }
    }
}