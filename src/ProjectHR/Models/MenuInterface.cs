﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Menu;
using CMS.Infrastructure.HttpRequest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectHR.Models
{
    public interface IMenuInterface
    {
        List<MenuType> GetMenuTypes(string userid, string token);
        List<Menus> GetMenu(string userid, string token);

    }
    public class MenuInterface : IMenuInterface
    {
        private readonly IHttpRequest _httprequest;
        public MenuInterface(IHttpRequest httprequest)
        {
            _httprequest = httprequest;
        }
        public List<MenuType> GetMenuTypes(string userid,string token)
        {
            var responseapi = _httprequest.GetDataString(MenuUrl.getmenutypebyuserid + "/" + userid, "GET", token: token);
            //return JObject.Parse(responseapi).ToObject<List<MenuType>>();
            return JsonConvert.DeserializeObject<List<MenuType>>(responseapi);
        }
        public List<Menus> GetMenu(string userid, string token)
        {
            var responseapi = _httprequest.GetDataString(MenuUrl.getmenubyuserid + "/" + userid, "GET", token: token);
            return JsonConvert.DeserializeObject<List<Menus>>(responseapi);
        }
    }
}
