
namespace ProjectHR.Models
{
    public class Article
    {
        public int articleType { get; set; }
        public string articleUrl { get; set; }
        public string articleHeading { get; set; }
        public string articleContent { get; set; }
        public string userID { get; set; }
    }
}