﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Account;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.HttpRequest
{
    public interface IHttpRequest
    {
        Task<tokenmodel> Login(string _user = "", string _pass = "");
        object GetData(string url, string _method = "GET", string _data = null, string contentType = "application/json; charset=utf-8", string token = "");
        Task<object> GetDataAsync(string _url, string _method = "GET", string _data = null, string contentType = "application/json; charset=utf-8", string token = "");
        string GetDataString(string url, string _method = "GET", string _data = null, string contentType = "application/json; charset=utf-8", string token = "");
    }
    public class HttpRequestApi : IHttpRequest
    {
        public async Task<tokenmodel> Login(string _user = "", string _pass = "")
        {
            var modeltoken = new tokenmodel();
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(UrlApi.ApiauthenLoginUrl);
            wr.Method = "POST";
            var model = new { username = _user, password = _pass }; 

            wr.ContentType = "application/json; charset=utf-8"; 
            using (var streamWriter = new StreamWriter(wr.GetRequestStream()))
            {
                streamWriter.Write(JsonConvert.SerializeObject(model));
            }

            var httpResponse = (HttpWebResponse)wr.GetResponse();
            using var streamReader = new StreamReader(httpResponse.GetResponseStream());
            var rs = await streamReader.ReadToEndAsync();
            HttpStatusCode statusCode = httpResponse.StatusCode;
            switch (statusCode)
            {
                case HttpStatusCode.OK: { 
                        modeltoken.token = JObject.Parse(rs)["token"].ToString(); 
                        modeltoken.expired = JObject.Parse(rs)["expiration"].ToObject<DateTime>();
                        modeltoken.user = JObject.Parse(rs)["user"].ToObject<LoginResult>();
                    } break;
                case HttpStatusCode.BadRequest: { modeltoken = null; } break;
                case HttpStatusCode.Unauthorized: { modeltoken.token = ""; } break;
            }
             
            return modeltoken;
        }
        public object GetData(string _url, string _method = "GET", string _data = null, string contentType = "application/json; charset=utf-8", string token = "")
        {
            try
            {
                HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(_url);
                if (token.Length > 0)
                {
                    var headers = wr.Headers;
                    headers.Add("Authorization","Bearer " + token);
                }
                wr.Method = _method;
                if (_method.ToUpper().Equals("POST")|| _method.ToUpper().Equals("PUT"))
                {
                    //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    wr.ContentType = contentType;
                    // Set the data to send.
                    using (var streamWriter = new StreamWriter(wr.GetRequestStream()))
                    {
                        streamWriter.Write(_data);
                    }
                }
                var httpResponse = (HttpWebResponse)wr.GetResponse();

                using var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var rs = streamReader.ReadToEnd(); 
                HttpStatusCode statusCode = httpResponse.StatusCode;
                switch (statusCode)
                { 
                    case HttpStatusCode.Unauthorized: { rs = "";  } break;
                }
                var jsonapi = JsonConvert.DeserializeObject(rs);
                return jsonapi;
            }
            catch (Exception e)
            { throw e; }
        }
        public async Task<object> GetDataAsync(string _url, string _method = "GET", string _data = null, string contentType = "application/json; charset=utf-8", string token = "")
        {
            try
            {
                HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(_url);
                if (token.Length > 0)
                {
                    var headers = wr.Headers;
                    headers.Add("Authorization", "Bearer " + token);
                }
                wr.Method = _method;
                if (_method.ToUpper().Equals("POST") || _method.ToUpper().Equals("PUT"))
                {
                    //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    wr.ContentType = contentType;
                    // Set the data to send.
                    using (var streamWriter = new StreamWriter(wr.GetRequestStream()))
                    {
                        streamWriter.Write(_data);
                    }
                }
                var httpResponse = (HttpWebResponse)wr.GetResponse();

                using var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var rs = await streamReader.ReadToEndAsync();
                HttpStatusCode statusCode = httpResponse.StatusCode;
                switch (statusCode)
                {
                    case HttpStatusCode.Unauthorized: { rs = null; } break;
                }
                var jsonapi = JsonConvert.DeserializeObject(rs);
                return jsonapi; 
            }
            catch (Exception e)
            { throw e; }
        }
        public string GetDataString(string _url, string _method = "GET", string _data = null, string contentType = "application/json; charset=utf-8", string token = "")
        {
            try
            {
                HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(_url);
                if (token.Length > 0)
                {
                    var headers = wr.Headers;
                    headers.Add("Authorization", "Bearer " + token);
                }
                wr.Method = _method;
                if (_method.ToUpper().Equals("POST") || _method.ToUpper().Equals("PUT"))
                {
                    //ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

                    wr.ContentType = contentType;
                    // Set the data to send.
                    using (var streamWriter = new StreamWriter(wr.GetRequestStream()))
                    {
                        streamWriter.Write(_data);
                    }
                }
                var httpResponse = (HttpWebResponse)wr.GetResponse();

                using var streamReader = new StreamReader(httpResponse.GetResponseStream());
                var rs = streamReader.ReadToEnd();
                HttpStatusCode statusCode = httpResponse.StatusCode;
                switch (statusCode)
                {
                    case HttpStatusCode.Unauthorized: { rs = ""; } break;
                }
                var jsonapi =  rs;
                return jsonapi;
            }
            catch (Exception e)
            { throw e; }
        }
    }
}
