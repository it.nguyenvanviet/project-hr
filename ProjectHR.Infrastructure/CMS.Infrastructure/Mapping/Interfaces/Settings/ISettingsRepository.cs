﻿using CMS.Core.Domain.Setting;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Infrastructure.Mapping.Interfaces.Settings
{
    public interface ISettingsRepository
    {
        IEnumerable<Setting> GetSettings();
        int UpdateSettings(Setting model);
    }
}
