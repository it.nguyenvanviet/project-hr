﻿using CMS.Core.Domain.Projects;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Infrastructure.Interfaces.Project
{
    public interface IProjectsRepository
    {

        IList<Projects> GetByName(string Name = "");
        IList<Projects> Search(ProjectParamSearch model);
        IList<ProjectType> GetProjectType();
        Projects GetByID(int ID);
       


        int Insert(Projects par);
        int Update(Projects par);
        int Delete(int ID);

        IEnumerable<ProjectViewModel> getListProjects(string userid);

    }
}
