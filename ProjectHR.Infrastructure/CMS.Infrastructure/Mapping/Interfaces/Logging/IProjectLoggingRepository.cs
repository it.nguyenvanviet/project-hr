﻿using CMS.Core.Domain.Setting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Interfaces.Logging
{
    public interface IProjectLoggingRepository
    {

        Task<int> Writelog(string Action = "", string UserID = "", string Data = "");
       
    }
}
