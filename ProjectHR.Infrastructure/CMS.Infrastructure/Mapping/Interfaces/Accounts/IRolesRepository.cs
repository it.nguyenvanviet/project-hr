﻿using CMS.Core.Domain.Account;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Interfaces.Accounts
{
    public interface IRolesRepository
    {
        IEnumerable<Roles> getall();
        Task<IEnumerable<Roles>> getAllRolesbyUserIDAsync(string UserID);
        Task<IEnumerable<Roles>> getAllRolesbyEmpIDAsync(int EmpID);
        IEnumerable<Roles> getByMenuID(int ID);
        int updateUserRole(UserRoles model);
        
    }
}
