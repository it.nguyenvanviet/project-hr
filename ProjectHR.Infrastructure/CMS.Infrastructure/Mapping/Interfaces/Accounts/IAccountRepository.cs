﻿using CMS.Core.Domain.Account;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Interfaces.Account
{
    public interface IAccountRepository
    {
        Task<string> CreateAccount(RegisterViewModel model); 
        Task<string> UpdateAccount(string userID,RegisterViewModel model);
        Task<StatusAccount.RegisterResult> Register(RegisterViewModel model);
        Task<LoginResult> Login(string user = "", string password = "");
        Task<Users> FindByNameAsync(string UserName);
        Task<Users> FindByID(string UserID);
    }
}
