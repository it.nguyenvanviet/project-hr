﻿using CMS.Core.Domain.Employee;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Interfaces.Accounts.Departs
{
    public interface IDepartmentRepository
    {
        Task<IList<Department>>  getDepartments(DepartmentGetModel model);
        Task<Department> getDepartmentByID(int id);
        Task<int> insert(DepartmentInsertModel model);
        Task<int> update(DepartmentUpdateModel model);
        Task<int> delete(int id);
    }
}
