﻿using CMS.Core.Domain.Menu;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Interfaces.Menu
{
    public interface IMenuRepository
    {
        IEnumerable<Menus> Getall();
        IEnumerable<Menus> GetMenusByUserID(string UserID);
        IEnumerable<Menus> GetChildrenMenu(IList<Menus> menuList, int parentId = 0);
        IEnumerable<MenuPosition> GetMenuPosition();
        IEnumerable<MenuType> GetMenuTypes();
        IEnumerable<MenuType> GetMenuTypeByUserID(string UserID);
       
        Menus GetMenuItem(IList<Menus> menuList, int id);
        Menus GetByID(int ID);


        int Insert(Menus model);
        bool InsertApi(Menus model);
        int UpdateRoleMenu(MenuRoleVM role);
        int Update(Menus model); 
        int ChangePositionAsync(RootMenu model);
        int Delete(int ID);
        
    }
}
