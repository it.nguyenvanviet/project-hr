﻿using CMS.Core.Domain.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Infrastructure.Mapping.Interfaces.Permission
{
    public interface IPermissionRecordRepository
    {
        IEnumerable<PermissionRecord> getall();
    }
}
