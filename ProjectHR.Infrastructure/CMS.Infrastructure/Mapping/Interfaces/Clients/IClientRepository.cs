﻿using CMS.Core.Domain.Clients;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Infrastructure.Interfaces.Clients
{
    public interface IClientRepository
    {
        IList<Client> GetClients(string Name = "");
    }
}
