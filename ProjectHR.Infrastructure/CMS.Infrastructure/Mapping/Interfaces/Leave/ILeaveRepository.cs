﻿using CMS.Core.Domain.Leave;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Interfaces.Leave
{
    public interface ILeaveRepository
    {
        /// <summary>
        /// Get Count Leave Request 3 month until now 
        /// </summary>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        Task<CountLeaveRequest> getCountLeaveRequest(DateTime? FromDate, DateTime? ToDate);
        Task<IList<LeaveViewModel>> getListLeaveRequest(LeaveSearchParamModel model);
        Task<LeaveModel> getByID(int ID);
        Task<int> createLeaveRequest(LeaveInsertModel model);
        Task<int> updateLeaveRequest(LeaveUpdateModel model);
        Task<int> changeApproved(ChangeLeaveApproved model);
        Task<int> deleteLeaveRequest(int ID, string UpdateBy);
    }
}
