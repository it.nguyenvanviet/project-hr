﻿using CMS.Core.Domain.Team;
using CMS.Core.Domain.Team.Parameter;
using CMS.Core.Domain.Team.TeamViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Interfaces.Team
{
    public interface ITeamRepository
    {
        Task<IList<Teams>> GetTeamsByProjectID(int ProjectID); 
        IList<TeamSearchViewModel> GetTeamsByFilter(TeamSearchModel model); 
        TeamSearchViewModel GetByID(int ID);
        Task<int> Insert(TeamInsertModel model);
        Task<int> Update(TeamUpdateModel model);
        Task<int> Delete(int ID);
    }
}
