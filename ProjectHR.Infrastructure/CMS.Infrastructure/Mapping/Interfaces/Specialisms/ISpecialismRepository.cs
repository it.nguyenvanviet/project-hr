﻿using CMS.Core.Domain.Employee;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Interfaces.Specialisms
{
    public interface ISpecialismRepository
    {
        IEnumerable<Specialism> GetSpecialisms();
        Task<int> Create(SpecialismInsertModel model);
        Task<int> Update (SpecialismUpdateModel model);
        Task<int> Delete(SpecialismDeleteModel model);
    }
}
