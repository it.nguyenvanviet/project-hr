﻿using CMS.Core.Domain.Blog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Interfaces.Blog
{
    public interface IBlogRepository
    {
        Task<IList<BlogViewModel>> GetBlogs(int EmpID);
        Task<BlogViewModel> GetByID(int ID);
        Task<int> Insert(BlogInsertModel model);
        Task<int> Update(BlogUpdateModel model);
        Task<int> Delete(BlogDeleteModel model);
    }
}
