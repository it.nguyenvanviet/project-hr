﻿using CMS.Core;
using CMS.Core.Domain.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Interfaces.Media
{
    public interface IPictureRepository
    {
        Task<int> UpdateAsync(Picture picture);
        Task<int> InsertAsync(Picture picture);
        Picture GetById(int ID);
        IQueryable<Picture> Table { get; }

    }
}
