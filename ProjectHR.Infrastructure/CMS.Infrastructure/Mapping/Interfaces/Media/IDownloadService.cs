﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Infrastructure.Mapping.Interfaces.Media
{
    public interface IDownloadService
    {
        byte[] GetDownloadBits(IFormFile file);
    }
}
