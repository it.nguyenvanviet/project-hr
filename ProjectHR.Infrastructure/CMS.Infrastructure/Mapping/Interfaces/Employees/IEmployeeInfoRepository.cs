﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CMS.Core.Domain.Employee;

namespace CMS.Infrastructure.Interfaces.Employee
{
    public interface IEmployeeInfoRepository
    {
        Task<int> addEducation(EmpAdd_EducationViewModel model);
        Task<int> addExperience(EmpAdd_ExperienceViewModel model);
        Task<int> addSocial(EmpAdd_SocialViewModel model);
        Task<int> addSkill(EmpAdd_SkillViewModel model);
        Task<int> addSpecialism(EmpAdd_SpecialismViewModel model);
        Task<int> addPortfolio(EmpAdd_PortfolioViewModel model);

        Task<int> editEducation(EmpUpdate_EducationViewModel model);
        Task<int> editExperience(EmpUpdate_ExperienceViewModel model);
        Task<int> editSocial(EmpUpdate_SocialViewModel model);
        Task<int> editSkill(EmpUpdate_SkillViewModel model);
        Task<int> editSpecialism(EmpUpdate_SpecialismViewModel model);
        Task<int> editPortfolio(EmpUpdate_PortfolioViewModel model);

        Task<int> deleteEducation(int ID);
        Task<int> deleteExperience(int ID);
        Task<int> deleteSocial(int ID);
        Task<int> deleteSkill(int ID);
        Task<int> deleteSpecialism(int ID);
        Task<int> deletePortfolio(int ID);
    }
}
