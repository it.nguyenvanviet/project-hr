﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CMS.Core.Domain.Employee;
using CMS.Core.Domain.Team;
using CMS.Core.Domain.Team.TeamViewModel;

namespace CMS.Infrastructure.Interfaces.Employee
{
    public interface IEmployeeRepository
    {
        Employees getByID(int ID);
        //IEnumerable<Employees> GetPaging(int PageNumber = 0, int RowsofPage = 0, string sortingcol = "", string sorttype = "DESC");

        IEnumerable<Employees> search(EmpSearchModel model);

        IEnumerable<TeamMemberViewModel> getteamleader(int departid);
        IEnumerable<TeamMemberViewModel> getteammember(int departid);

        /// <summary>
        /// get by empid 
        /// </summary>
        /// <param name="EmpID"></param>
        /// <returns></returns>
        IEnumerable<Education> getEducationsByEmpID(int EmpID = 0);
        IEnumerable<SpecialismViewModel> getSpecialismByEmpID(int EmpID = 0);
        IEnumerable<Social> getSocialsByEmpID(int EmpID = 0);
        IEnumerable<Experience> getExperiencesByEmpID(int EmpID = 0);
        IEnumerable<Skills> getSkillsByEmpID(int EmpID = 0);
 

        /// <summary>
        /// get by userid 
        /// </summary>
        /// <param name="EmpID"></param>
        /// <returns></returns>
        IEnumerable<Education> getEducationsByUserID(string EmpID);
        IEnumerable<SpecialismViewModel> getSpecialismByUserID(string EmpID);
        IEnumerable<Social> getSocialsByUserID(string UserID);
        IEnumerable<Experience> getExperiencesByUserID(string UserID);
        IEnumerable<Skills> getSkillsByUserID(string UserID);

        Task<Employees> getByUserIDAsync(string UserID);
        Task<EmpBasicInfoViewModel> getBasicInfoUser(string UserID);
        Task<int> insertAsync(EmpRegisViewModel emp);
        Task<int> updateBasicInfoAsync(EmpUpdateViewModel emp);
        Task<int> updateAvatar(EmpUpdateAvatarViewModel emp);
        Task<int> updateRole(EmpUpdateRoleViewModel emp);
        Task<bool> insertv2Async(EmpViewModel emp);
        Task<int> updateAsync(EmpUpdateViewModel emp);
        Task<int> delete(int empID);
    }
}
