﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Leave;
using CMS.Data.Enum;
using CMS.Infrastructure.Mapping.Interfaces.Leave;
using CMS.Services.Lib.Dapper;
using CMS.Services.Lib.Helper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Repositories.Leave
{
    public class LeaveRepository : ILeaveRepository
    {
        public async Task<CountLeaveRequest> getCountLeaveRequest(DateTime? FromDate, DateTime? ToDate)
        {
            var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<CountLeaveRequest>(LeaveStore.GetCountRequest, 
                new {
                    FromDate = FromDate == null ? new DateTime(DateTime.Now.Year,DateTime.Now.Month,1) : FromDate,
                    ToDate  = ToDate == null ? new DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(1).Month, 1) : ToDate
                });
            return data;
        }
        public async Task<IList<LeaveViewModel>> getListLeaveRequest(LeaveSearchParamModel model)
        {
            var data = await SQLDapperHelper.Instance.ExecuteQueryAsync<LeaveViewModel>(LeaveStore.GetListRequest, new
            {
                model.key_search,
                model.leaveStatus,
                model.leaveType,
                model.approvedStatus,
                model.fromDate, 
                model.toDate
            });
            for(int i = 0; i < data.Count; i++)
            {
                setTypeName(data[i]);
            }
            return data;
        }

        private static void setTypeName(LeaveViewModel item)
        {
            switch (item.LeaveType)
            {
                case 0: { item.TypeName = EnumHelper.ToDisplayName(LeaveTypeEnum.WithoutPermission); } break;
                case 1: { item.TypeName = EnumHelper.ToDisplayName(LeaveTypeEnum.AnnualLeave); } break;
                case 2: { item.TypeName = EnumHelper.ToDisplayName(LeaveTypeEnum.SickLeave); } break;
                case 3: { item.TypeName = EnumHelper.ToDisplayName(LeaveTypeEnum.Pregnant); } break;
                case 4: { item.TypeName = EnumHelper.ToDisplayName(LeaveTypeEnum.WorkFromHome); } break;
                case 5: { item.TypeName = EnumHelper.ToDisplayName(LeaveTypeEnum.Holidays); } break;
            }
        }

        public async Task<LeaveModel> getByID(int ID)
        {
            var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<LeaveModel>(LeaveStore.GetLeaveByID, new { ID });
            return data;
        }
        public async Task<int> createLeaveRequest(LeaveInsertModel model)
        {
            try
            {
                var id = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(
                      LeaveStore.Insert,
                      new
                      {
                          listEmpID = string.Join(",", model.listEmpID),
                          model.LeaveType,
                          model.NoofDay,
                          model.FromDate,
                          ToDate = new DateTime(model.ToDate.Value.Year, model.ToDate.Value.Month, model.ToDate.Value.Day, 23, 59, 59),
                          model.Description,
                          model.CreateBy,
                          model.LeaveTime
                      }); ;
                return id;
            }
            catch //(Exception e)
            {
                return 0;
            }
        }
        public async Task<int> updateLeaveRequest(LeaveUpdateModel model)
        {
            var id = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(
                   LeaveStore.Update,
                   new
                   {
                       model.EmpID,
                       model.LeaveType,
                       model.Status,
                       model.FromDate,
                       model.ToDate,
                       model.UpdateBy,
                       model.ApprovedBy,
                       model.ApprovedStatus,
                       model.ApprovedDate
                   });
            return id;
        }
        public async Task<int> deleteLeaveRequest(int ID, string UpdateBy)
        {
            return await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(LeaveStore.Delete, new { ID, UpdateBy });
        }

        public async Task<int> changeApproved(ChangeLeaveApproved model)
        {
            var id = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(
                   LeaveStore.ChangeApproved,
                   new
                   {
                       model.id,
                       model.UpdateBy,
                       model.ApprovedStatus, 
                   });
            return id;
        }
    }
}
