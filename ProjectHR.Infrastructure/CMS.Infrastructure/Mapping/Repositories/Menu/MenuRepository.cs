﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Menu;
using CMS.Infrastructure.Interfaces.Menu;
using CMS.Services.Lib.Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Repositories.Menu
{
    public class MenuRepository : IMenuRepository
    {
         
        /// <summary>
        /// get menu by UserID
        /// </summary>
        /// <param name="RoleID"></param>
        /// <returns></returns>
        public IEnumerable<Menus> GetMenusByUserID(string UserID)
        { 
            return SQLDapperHelper.Instance.ExecuteQuery<Menus>(MenuStore.GetMenuByUserID, new { UserID }).ToList();
        }
        /// <summary>
        /// GetChildMenu
        /// </summary>
        /// <param name="menuList"></param>
        /// <param name="parentId"></param>
        /// <returns></returns>
        public IEnumerable<Menus> GetChildrenMenu(IList<Menus> menuList, int parentId = 0)
        {
            return menuList.Where(x => x.ParentID == parentId).OrderBy(x => x.Sort).ToList();
        }
        public Menus GetMenuItem(IList<Menus> menuList, int id)
        {
            return menuList.FirstOrDefault(x => x.ID == id);
        }
        /// <summary>
        /// get all menu
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Menus> Getall()
        {

            var listmenus = SQLDapperHelper.Instance.ExecuteQuery<Menus>(MenuStore.GetAllMenu,
                   CommandType.StoredProcedure).ToList().OrderBy(a => a.ID).ToList();
            return listmenus;
        }
        /// <summary>
        /// get all menu type
        /// </summary>
        /// <returns></returns>
        public IEnumerable<MenuType> GetMenuTypes()
        {

            var listmenus = SQLDapperHelper.Instance.ExecuteQuery<MenuType>(MenuStore.GetMenuType,
                   CommandType.StoredProcedure).ToList().OrderBy(a => a.ID).ToList();
            return listmenus;
        }
        public IEnumerable<MenuType> GetMenuTypeByUserID(string UserID)
        {

            var listmenus = SQLDapperHelper.Instance.ExecuteQuery<MenuType>(MenuStore.GetMenuTypeByUserID,
                   CommandType.StoredProcedure, new { UserID });
            return listmenus;
        }
        public IEnumerable<MenuPosition> GetMenuPosition()
        {
            var listmenus = SQLDapperHelper.Instance.ExecuteQuery<MenuPosition>(MenuStore.GetAllMenu);
            var parent = listmenus.Where(a => a.ParentID == 0).ToList();
            if(parent.Count()>0)
                foreach(var item in parent)
                {
                    item.children = listmenus.Where(a => a.ParentID == item.ID).ToList();
                    if(item.children.Count()>0)
                    {
                        foreach(var itemchild in item.children)
                        {
                            itemchild.children = listmenus.Where(a => a.ParentID == itemchild.ID).ToList();
                            if(itemchild.children.Count()>0)
                            {
                                foreach(var itemchild2 in itemchild.children)
                                {
                                    itemchild2.children = new List<MenuPosition>();
                                }
                            }
                        }
                    }
                }
            return parent;
        }
        /// <summary>
        /// get menu by id
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Menus GetByID(int ID)
        {
            try
            {
                var data = SQLDapperHelper.Instance.ExecuteQuery<Menus>(MenuStore.GetByID,
                       CommandType.StoredProcedure, new { ID }).FirstOrDefault();
                return data;
            }
            catch(Exception e)
            { throw e; }
        }
        /// <summary>
        /// Insert Menu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Insert(Menus model)
        {
            var data = SQLDapperHelper.Instance.ExecuteQuery<int>(MenuStore.Insert,
                   CommandType.StoredProcedure, new { model.ParentID, model.Link, model.Name, model.Sort, model.Status, model.Type, model.IconClass, model.Content, model.CreateBy }).FirstOrDefault();
            return data;
        }
        /// <summary>
        /// Insert Menu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool InsertApi(Menus model)
        {
            var data = SQLDapperHelper.Instance.Insert<Menus>(typeof(Menus).Name,model);
            return data;
        }
        /// <summary>
        /// Update Menu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Update(Menus model)
        {
            var data = SQLDapperHelper.Instance.ExecuteQuery<int>(MenuStore.Update,
                   CommandType.StoredProcedure, new { model.ID, model.Link, model.Name, model.Status, model.Type, model.IconClass, model.Content, model.CreateBy }).FirstOrDefault();
            return data;
        }
        /// <summary>
        /// Update Position and ParentID
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int ChangePositionAsync(RootMenu model)
        {
            int count = 0;
            int order = 0;
            if (model != null)
            {

                foreach (var item in model.List_menu)
                {
                    order += 1;
                    UpdatePosition(item.ID, item.ParentID, order);
                    if (item.Children != null)
                        if (item.Children.Count() > 0)
                            foreach (var itemchild in item.Children)
                            {
                                order += 1;
                                UpdatePosition(itemchild.ID, item.ID, order);
                                if (itemchild.Children != null)
                                    if (itemchild.Children.Count() > 0)
                                        foreach (var itemchild2 in itemchild.Children)
                                        {
                                            order += 1;
                                            UpdatePosition(itemchild2.ID, itemchild.ID, order);
                                            if (itemchild2.Children != null)
                                                if (itemchild2.Children.Count() > 0)
                                                    foreach (var itemchild3 in itemchild2.Children)
                                                    {
                                                        order += 1;
                                                        UpdatePosition(itemchild3.ID, itemchild2.ID, order);
                                                        if (itemchild3.Children != null)
                                                            if (itemchild3.Children.Count() > 0)
                                                                foreach (var itemchild4 in itemchild3.Children)
                                                                {
                                                                    order += 1;
                                                                    UpdatePosition(itemchild4.ID, itemchild3.ID, order);
                                                                }
                                                    }
                                        }

                            }
                }
                count++;
            }
            return count;
        }
        /// <summary>
        /// Update postion
        /// </summary>
        /// <param name="id"></param>
        /// <param name="parentid"></param>
        /// <param name="sort"></param>
        /// <returns></returns>
        public int UpdatePosition(int id, int parentid, int sort)
        {

            var data = SQLDapperHelper.Instance.ExecuteQuery<int>(MenuStore.UpdatePosition,
                   CommandType.StoredProcedure, new { id, parentid, sort }).FirstOrDefault();
            return data;
        }
        /// <summary>
        /// Delete Menu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Delete(int ID)
        {

            var data = SQLDapperHelper.Instance.ExecuteQuery<int>(MenuStore.Delete,
                   CommandType.StoredProcedure, new { ID }).FirstOrDefault();
            return data;
        }
        /// <summary>
        /// Add or Update Menurole
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public int UpdateRoleMenu(MenuRoleVM role)
        {
            var data = SQLDapperHelper.Instance.ExecuteQuery<int>(MenuStore.InsertDetail, new { role.MenuID, role.ListRole }).FirstOrDefault();
            return data;
        }

      

    }
}
