﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Account;
using CMS.Infrastructure.Interfaces.Accounts;
using CMS.Services.Lib.Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Repositories.Accounts
{
    
    public class RolesRepository : IRolesRepository
    { 
        
        public IEnumerable<Roles> getall()
        {
          
            var data = SQLDapperHelper.Instance.ExecuteQuery<Roles>("SELECT Id,Name,NormalizedName,ConcurrencyStamp FROM dbo.Roles(nolock)",CommandType.Text, null);
            return  data; 
        }
        public async Task<IEnumerable<Roles>> getAllRolesbyUserIDAsync(string UserID)
        {
          
            var data = await SQLDapperHelper.Instance.ExecuteQueryAsync<Roles>(RolesStore.GetlistRolebyUserID, new { UserID = UserID });
            return data;
        }
        public async Task<IEnumerable<Roles>> getAllRolesbyEmpIDAsync(int EmpID)
        {

            var data = await SQLDapperHelper.Instance.ExecuteQueryAsync<Roles>(RolesStore.GetlistRolebyEmpID, new { EmpID = EmpID });
            return data;
        }
        public IEnumerable<Roles> getByMenuID(int ID)
        {
          
            var data = SQLDapperHelper.Instance.ExecuteQuery<Roles>(RolesStore.GetRoleByMenuID,new { MenuID = ID });
            return data;
        }
        public int updateUserRole(UserRoles model)
        {
          
            var sp = "UPDATE dbo.UserRoles SET RoleId = '" + model.RoleId + "' WHERE UserId = '" + model.UserId + "'";
            var data = int.Parse(SQLDapperHelper.Instance.ExecuteTransaction(sp, CommandType.Text, null).ToString());
            return data;
        }
    }
}
