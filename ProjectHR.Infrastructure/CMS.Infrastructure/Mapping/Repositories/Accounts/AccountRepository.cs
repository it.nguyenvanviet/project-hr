﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Account;
using CMS.Core.Domain.Employee;
using CMS.Infrastructure.Interfaces.Account;
using CMS.Services.Lib.Dapper;
using CMS.Services.Lib.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Repositories.Accounts
{
    public class AccountRepository : IAccountRepository
    {
        /// <summary>
        /// Create Account
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<string> CreateAccount(RegisterViewModel model)
        {
           
            var ID = Guid.NewGuid().ToString(); 
            var SecurityStamp = Guid.NewGuid().ToString();
            var ConcurrencyStamp = Guid.NewGuid().ToString();
            var Passwordhash = StringHelper.PasswordHash(model.Password);
            _ = SQLDapperHelper.Instance.ExecuteScalar<int>(
                AccountStore.AccountRegister, 
                new
                {
                    ID,
                    model.Email, 
                    Passwordhash,
                    SecurityStamp,
                    ConcurrencyStamp
                }); 
            return await Task.FromResult(ID); 
        }

        /// <summary>
        /// Đăng ký 
        /// </summary>
        /// <param name="model"></param>
        /// <returns> return 
        ///                 RegisterResult.Fail, 
        ///                 RegisterResult.Success
        ///                 RegisterResult.Locked
        ///                 RegisterResult.Registed
        /// </returns>
        public async Task<StatusAccount.RegisterResult> Register(RegisterViewModel model)
        {
            var response = await Task.FromResult(StatusAccount.RegisterResult.Fail);
            try
            {
                var ID = Guid.NewGuid().ToString();
                var SecurityStamp = Guid.NewGuid().ToString();
                var ConcurrencyStamp = Guid.NewGuid().ToString();
                var Passwordhash = StringHelper.PasswordHash(model.Password);
                var data = await SQLDapperHelper.Instance.ExecuteScalar<int>(
                    AccountStore.AccountRegister,
                    new
                    {
                        ID,
                        model.Email,
                        model.Password,
                        Passwordhash,
                        SecurityStamp,
                        ConcurrencyStamp
                    });
                
                switch (data)
                {
                    case 0: { response = await Task.FromResult(StatusAccount.RegisterResult.Fail); }; break;         //đăng ký thất bại
                    case 1: { response = await Task.FromResult(StatusAccount.RegisterResult.Success); }; break;     //đăng ký thành công     
                    case 2: { response = await Task.FromResult(StatusAccount.RegisterResult.Locked); }; break;      //tk đã bị khóa
                    case 3: { response = await Task.FromResult(StatusAccount.RegisterResult.Registed); }; break;    //tk đã tồn tại
                }
            }
            catch(Exception e)
            {
                throw e;
            }
            return response; 
        }
       
        /// <summary>
        /// Update Account
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<string> UpdateAccount(string userID, RegisterViewModel model)
        {
          
            var SecurityStamp = Guid.NewGuid().ToString();
            var ConcurrencyStamp = Guid.NewGuid().ToString();
            var Passwordhash = StringHelper.PasswordHash(model.Password);
            _ = SQLDapperHelper.Instance.ExecuteScalar<int>(
                AccountStore.AccountRegister, 
                new
                {
                    ID = userID,
                    model.Email,
                    model.Password,
                    Passwordhash,
                    SecurityStamp,
                    ConcurrencyStamp
                }); 
            return await Task.FromResult(userID); 
        }

        /// <summary>
        /// FindByName
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        public async Task<Users> FindByNameAsync(string UserName)
        {
            try
            { 
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<Users>(
                    AccountStore.AccountFindByName,  new  {  UserName });
                return data;
            }
            catch(Exception e)
            { throw e; }
        }
       
        /// <summary>
        /// Login 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <returns>Return
        ///     LoginResult.WrongUserorPassword;
        ///     LoginResult.Success;
        /// </returns>
        public async Task<LoginResult> Login(string user = "", string password = "")
        {
            try
            { 
                var Passwordhash = StringHelper.PasswordHash(password);
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<LoginResult>(
                    AccountStore.CheckLogin, new { Email = user, Passwordhash });
                return data;
            }
            catch (Exception e)
            {
                throw e;
            } 
        }

        public async Task<Users> FindByID(string UserID)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<Users>(
                    AccountStore.AccountFindByID, new { UserID });
                return data;
            }
            catch (Exception e)
            { throw e; }
        }

          
    }
}
