﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Employee;
using CMS.Infrastructure.Mapping.Interfaces.Accounts.Departs;
using CMS.Services.Lib.Dapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Repositories.Accounts.Departs  
{
    public class DepartmentRepository : IDepartmentRepository
    {
        public async Task<int> insert(DepartmentInsertModel model)
        {
            var id = await SQLDapperHelper.Instance.ExecuteScalar<int>(
                  DepartmentStore.Insert,
                  new
                  {
                      model.DepartmentCode,
                      model.DepartmentName,
                      model.Status,
                      model.CreateBy
                  });
            return await Task.FromResult(id);
        }

        public async Task<int> delete(int id)
        {
            var rs = await SQLDapperHelper.Instance.ExecuteScalar<int>(DepartmentStore.Delete, new { id  });
            return await Task.FromResult(rs);
        }

        public async Task<Department> getDepartmentByID(int id)
        {
            var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<Department>(
                 DepartmentStore.Insert, new { id });
            return data;
        }

        public async Task<IList<Department>> getDepartments(DepartmentGetModel model)
        {
            var data = await SQLDapperHelper.Instance.ExecuteQueryAsync<Department>(
                  DepartmentStore.GetDepartments, new { model.DepartmentCode, model.DepartmentName });
            return data;
        }

        public async Task<int> update(DepartmentUpdateModel model)
        {
            var id = await SQLDapperHelper.Instance.ExecuteScalar<int>(
                 DepartmentStore.Update,
                 new
                 {
                     model.ID,
                     model.DepartmentCode,
                     model.DepartmentName,
                     model.Status,
                     model.UpdateBy
                 });
            return await Task.FromResult(id);
        }
    }
}
