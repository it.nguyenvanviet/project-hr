﻿using CMS.Core.Domain.Employee;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data;
using CMS.Services.Lib.Dapper;
using CMS.Core.ConstString;
using CMS.Infrastructure.Interfaces.Employee;
using System.Threading.Tasks;
using CMS.Infrastructure.Interfaces.Account;
using CMS.Infrastructure.Repositories.Accounts;
using CMS.Infrastructure.Mapping.Interfaces.Logging;
using Newtonsoft.Json;
using CMS.Services.Lib.Helper;
using CMS.Data.Enum;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace CMS.Infrastructure.Repositories.Employee
{
    public class EmployeeInfoRepository : IEmployeeInfoRepository
    {

        private readonly IAccountRepository _accountRepository;
        private readonly IProjectLoggingRepository _projectLoggingRepository;
        private readonly IWebHostEnvironment _env;


        public EmployeeInfoRepository(IAccountRepository accountRepository,
            IProjectLoggingRepository projectLoggingRepository,
            IWebHostEnvironment env)
        {
            _accountRepository = accountRepository;
            _projectLoggingRepository = projectLoggingRepository;
            _env = env;
        }

        public async Task<int> addEducation(EmpAdd_EducationViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.AddEducation,
                new
                {
                    model.FromDate,
                    model.ToDate,
                    model.Name,
                    model.Specialism,
                    model.Degree,
                    model.EmpID,
                    model.CreateBy
                });
                if (data > 0)
                  await _projectLoggingRepository.Writelog("Add Education Profile", model.CreateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> addExperience(EmpAdd_ExperienceViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.AddExperience,
                new
                {
                    model.Name,
                    model.Office,
                    model.Year_experiences,
                    model.EmpID,
                    model.CreateBy
                });
                if (data > 0)
                   await _projectLoggingRepository.Writelog("Add Experience Profile", model.CreateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> addSocial(EmpAdd_SocialViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.AddSocial,
                new
                {
                    model.Name,
                    model.Link,
                    model.EmpID,
                    model.CreateBy
                });
                if (data > 0)
                  await  _projectLoggingRepository.Writelog("Add Social Profile", model.CreateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> addSkill(EmpAdd_SkillViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.AddSkill,
                new
                {
                    model.Name,
                    model.PointPercent,
                    model.EmpID,
                    model.TypeSkill,
                    model.Description,
                    model.CreateBy
                });
                if (data > 0)
                  await  _projectLoggingRepository.Writelog("Add Skill Profile", model.CreateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> addSpecialism(EmpAdd_SpecialismViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.AddSpecialism,
                new
                {
                    model.SpecialismID,
                    model.EmpID,
                    model.CreateBy
                });
                if (data > 0)
                  await _projectLoggingRepository.Writelog("Add Specialism Profile", model.CreateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> addPortfolio(EmpAdd_PortfolioViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.AddPortfolio,
                new
                {
                    model.Name,
                    model.Descriptions,
                    model.EmpID,
                    model.CreateBy
                });
                if (data > 0)
                   await _projectLoggingRepository.Writelog("Add Portfolio Profile", model.CreateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<int> editEducation(EmpUpdate_EducationViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.EditEducation,
                new
                {
                    model.ID,
                    model.FromDate,
                    model.ToDate,
                    model.Name,
                    model.Specialism,
                    model.Degree,
                    model.EmpID,
                    model.UpdateBy
                });
                if (data > 0)
                   await _projectLoggingRepository.Writelog("Edit Education Profile", model.UpdateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> editExperience(EmpUpdate_ExperienceViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.EditExperience,
                new
                {
                    model.ID,
                    model.Name,
                    model.Office,
                    model.Year_experiences,
                    model.EmpID,
                    model.Descriptions,
                    model.UpdateBy
                });
                if (data > 0)
                   await _projectLoggingRepository.Writelog("Edit Experience Profile", model.UpdateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> editSocial(EmpUpdate_SocialViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.EditSocial,
                new
                {
                    model.ID,
                    model.Name,
                    model.Link,
                    model.EmpID,
                    model.UpdateBy
                });
                if (data > 0)
                   await _projectLoggingRepository.Writelog("Edit Social Profile", model.UpdateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> editSkill(EmpUpdate_SkillViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.EditSkill,
                new
                {
                    model.ID,
                    model.Name,
                    model.PointPercent,
                    model.EmpID,
                    model.TypeSkill,
                    model.Description,
                    model.UpdateBy
                });
                if (data > 0)
                 await _projectLoggingRepository.Writelog("Edit Skill Profile", model.UpdateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> editSpecialism(EmpUpdate_SpecialismViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.EditSpecialism,
                new
                {
                    model.ID,
                    model.SpecialismID,
                    model.EmpID,
                    model.UpdateBy
                });
                if (data > 0)
                    await _projectLoggingRepository.Writelog("Edit Specialism Profile", model.UpdateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> editPortfolio(EmpUpdate_PortfolioViewModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.EditPortfolio,
                new
                {
                    model.ID,
                    model.Name,
                    model.Descriptions,
                    model.Status,
                    model.UpdateBy
                });
                if (data > 0)
                    await _projectLoggingRepository.Writelog("Edit Portfolio Profile", model.UpdateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<int> deleteEducation(int ID)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.DeleteEducation, new { ID });
                if (data > 0)
                    await _projectLoggingRepository.Writelog("Delete Education Profile", "system", JsonConvert.SerializeObject(ID));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> deleteExperience(int ID)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.DeleteExperience, new { ID });
                if (data > 0)
                    await _projectLoggingRepository.Writelog("Delete Experience Profile", "system", JsonConvert.SerializeObject(ID));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> deleteSocial(int ID)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.DeleteSocial, new { ID });
                if (data > 0)
                    await _projectLoggingRepository.Writelog("Delete Social Profile", "system", JsonConvert.SerializeObject(ID));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> deleteSkill(int ID)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.DeleteSkill, new { ID });
                if (data > 0)
                    await _projectLoggingRepository.Writelog("Delete Skill Profile", "system", JsonConvert.SerializeObject(ID));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> deleteSpecialism(int ID)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.DeleteSpecialism, new { ID });
                if (data > 0)
                    await _projectLoggingRepository.Writelog("Delete Specialism Profile", "system", JsonConvert.SerializeObject(ID));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> deletePortfolio(int ID)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.DeletePortfolio, new { ID });
                if (data > 0)
                   await _projectLoggingRepository.Writelog("Delete Portfolio Profile", "system", JsonConvert.SerializeObject(ID));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
