﻿using CMS.Core.Domain.Employee;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Data;
using CMS.Services.Lib.Dapper;
using CMS.Core.ConstString;
using CMS.Infrastructure.Interfaces.Employee;
using System.Threading.Tasks;
using CMS.Infrastructure.Interfaces.Account;
using CMS.Infrastructure.Repositories.Accounts;
using CMS.Infrastructure.Mapping.Interfaces.Logging;
using Newtonsoft.Json;
using CMS.Services.Lib.Helper;
using CMS.Data.Enum;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using CMS.Core.Domain.Team;
using CMS.Core.Domain.Team.TeamViewModel;

namespace CMS.Infrastructure.Repositories.Employee
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IProjectLoggingRepository _projectLoggingRepository;
        private readonly IWebHostEnvironment _env;


        public EmployeeRepository(IAccountRepository accountRepository,
            IProjectLoggingRepository projectLoggingRepository, 
            IWebHostEnvironment env)
        {
            _accountRepository = accountRepository;
            _projectLoggingRepository = projectLoggingRepository;
            _env = env;
        }

        /// <summary>
        /// GetPaging
        /// </summary>
        /// <returns></returns>

        //public IEnumerable<Employees> GetPaging(int PageNumber = 0, int RowsofPage = 0, string sortingcol = null, string sorttype = "DESC")
        //{
        //    var data = SQLDapperHelper.Instance.ExecuteQuery<Employees>(EmployeeStore.GetByPaging, new { PageNumber, RowsofPage, sortingcol, sorttype });
            
        //    return data;
        //}

        public IEnumerable<Employees> search(EmpSearchModel model)
        {
            var data = SQLDapperHelper.Instance.ExecuteQuery<Employees>(EmployeeStore.Search, model);
            foreach (var item in data)
            {
                item.url_avatar = ImageHelper.TempImage(item.Avartar, item.Name.Substring(0, 1).ToUpper());
                switch (item.Gender)
                {
                    case 0: { item.GenderValue = EnumHelper.ToDisplayName(GenderEnum.Female); } break;
                    case 1: { item.GenderValue = EnumHelper.ToDisplayName(GenderEnum.Male); } break;
                    case 2: { item.GenderValue = EnumHelper.ToDisplayName(GenderEnum.Unspecified); } break;
                }
            }
            return data;
        }

        /// <summary>
        /// Get by ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Employees getByID(int ID)
        {
            try
            {
                var data = SQLDapperHelper.Instance.ExecuteQuery<Employees>(EmployeeStore.GetByID, new { ID }).FirstOrDefault();
                if (data != null)
                {
                    data.Socials = getSocialsByEmpID(data.ID).ToList();
                }
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get by User ID
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public async Task<Employees> getByUserIDAsync(string UserID)
        {
            var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<Employees>(EmployeeStore.GetByUserID, new { UserID });
            return data;
        }
        public async Task<EmpBasicInfoViewModel> getBasicInfoUser(string UserID)
        {
            var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<EmpBasicInfoViewModel>(EmployeeStore.GetByUserID, new { UserID });
            return data;
        }
        
        /// <summary>
        /// Insert emp
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public async Task<int> insertAsync(EmpRegisViewModel emp)
        {
            try
            {
                var UserID = await _accountRepository.CreateAccount(emp.RegisterView);
                if (UserID.Length > 0)
                {
                    var data = await SQLDapperHelper.Instance.ExecuteScalar<int>(EmployeeStore.Insert,// nameof(Employees) 
                    new
                    {
                        emp.Name,
                        emp.RegisterView.Email,
                        emp.Phone,
                        emp.Experience,
                        emp.BirthDate,
                        emp.CreateBy,
                        UserID,
                        emp.Address,
                        emp.GoogleMap,
                        emp.Avartar,
                        emp.Contract_Intro,
                        emp.Rank,
                        emp.DepartID
                    });
                    if (data > 0)
                      await _projectLoggingRepository.Writelog("Thêm mới Nhân viên", emp.CreateBy, JsonConvert.SerializeObject(emp));
                    return data;
                }
                return 0;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        /// <summary>
        /// Update basic info
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public async Task<int> updateAvatar(EmpUpdateAvatarViewModel emp)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.Updateavartar,
                new
                {
                    emp.ID,
                    emp.Avartar,
                    emp.UpdateBy,
                });
                if (data > 0)
                   await _projectLoggingRepository.Writelog("Update avartar", emp.UpdateBy, JsonConvert.SerializeObject(emp));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        /// <summary>
        /// Update avartar
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public async Task<int> updateBasicInfoAsync(EmpUpdateViewModel emp)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.UpdateBasicInfo,
                new
                {
                    emp.ID,
                    emp.Name,
                    emp.Phone,
                    emp.Email,
                    emp.Experience,
                    emp.Gender,
                    emp.Rank,
                    emp.BirthDate,
                    emp.Status,
                    emp.Address, 
                    emp.GoogleMap,
                    emp.UpdateBy
                });
                if (data > 0)
                   await _projectLoggingRepository.Writelog("Update Basic info", emp.UpdateBy, JsonConvert.SerializeObject(emp));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public async Task<int> updateRole(EmpUpdateRoleViewModel emp)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.UpdateRole,
                new
                {
                    emp.EmpID,
                    Listroleid = string.Join(",", emp.Listroleid),
                    emp.UpdateBy
                });
                if (data > 0)
                  await  _projectLoggingRepository.Writelog("Update Role Emp", emp.UpdateBy, JsonConvert.SerializeObject(emp));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            } 
        }

        /// <summary>
        /// Insert emp
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public async Task<bool> insertv2Async(EmpViewModel emp)
        {

            var UserID = await _accountRepository.CreateAccount(emp.RegisterView);
            if (UserID.Length > 0)
            {
                var data = SQLDapperHelper.Instance.Insert<Employees>(typeof(Employees).Name, emp);
                if (data)
                   await _projectLoggingRepository.Writelog("Thêm mới Nhân viên", emp.CreateBy, JsonConvert.SerializeObject(emp));
                return data;
            }
            return false;
        }

        /// <summary>
        /// Update emp
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public async Task<int> updateAsync(EmpUpdateViewModel emp)
        { 
            var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(EmployeeStore.Update,
                new
                {
                    emp.ID,
                    emp.Name,
                    emp.Email,
                    emp.Phone,
                    emp.Status,
                    emp.Gender,
                    emp.Experience,
                    emp.BirthDate,   
                    emp.Address, 
                    emp.Avartar,
                    emp.Contract_Intro,
                    emp.Rank,
                    emp.DepartID,
                    emp.UpdateBy
                });
            return data;
        }

        /// <summary>
        /// Delete emp
        /// </summary>
        /// <param name="empID"></param>
        /// <returns></returns>
        public async Task<int> delete(int empID)
        {
            try
            {
                var rs = await SQLDapperHelper.Instance.ExecuteScalar<int>(EmployeeStore.Delete, new { ID = empID });
                return rs;
            }
            catch(Exception e)
            {
                throw e;
            }
        }


        /// <summary>
        /// get by empid
        /// </summary>
        /// <param name="EmpID"></param>
        /// <returns></returns>
        public IEnumerable<Education> getEducationsByEmpID(int EmpID = 0)
        {
            var list = SQLDapperHelper.Instance.ExecuteQuery<Education>(EducationStore.GetByEmpID, new { EmpID }).ToList();
            foreach (var item in list)
            {
                switch (item.Degree)
                {
                    case 0: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.Unskilled_labor); } break;
                    case 1: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.HighSchool); } break;
                    case 2: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.Postgraduate); } break;
                    case 3: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.Certificate); } break;
                    case 4: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.Intermediate); } break;
                    case 5: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.College); } break;
                    case 6: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.University); } break;
                }
            }
            return list;
        }
        public IEnumerable<SpecialismViewModel> getSpecialismByEmpID(int EmpID = 0)
        {
            return SQLDapperHelper.Instance.ExecuteQuery<SpecialismViewModel>(SpecialismsStore.GetByEmpID, new { EmpID }).ToList();
        }
        public IEnumerable<Experience> getExperiencesByEmpID(int EmpID = 0)
        {
            return SQLDapperHelper.Instance.ExecuteQuery<Experience>(ExperienceStore.GetByEmpID, new { EmpID }).ToList();
        }
        public IEnumerable<Social> getSocialsByEmpID(int EmpID = 0)
        {
            return SQLDapperHelper.Instance.ExecuteQuery<Social>(SocialStore.GetByEmpID, new { EmpID }).ToList();
        }
        public IEnumerable<Skills> getSkillsByEmpID(int EmpID = 0)
        {
            var list = SQLDapperHelper.Instance.ExecuteQuery<Skills>(SkillStore.GetByEmpID, new { EmpID }).ToList();
            foreach (var item in list)
            {
                switch (item.TypeSkill)
                {
                    case 0: { item.TypeSkillName = EnumHelper.ToDisplayName(TypeSkillEnum.SkillforWork); } break;
                    case 1: { item.TypeSkillName = EnumHelper.ToDisplayName(TypeSkillEnum.SoftSkill); } break;
                    case 2: { item.TypeSkillName = EnumHelper.ToDisplayName(TypeSkillEnum.Anothers); } break; 
                }
            }
            return list;
        }



        /// <summary>
        /// get by UserID
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public IEnumerable<Education> getEducationsByUserID(string UserID)
        {
            var list = SQLDapperHelper.Instance.ExecuteQuery<Education>(EducationStore.GetByUserID, new { UserID }).ToList();
            foreach (var item in list)
            {
                switch (item.Degree)
                {
                    case 0: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.Unskilled_labor); } break;
                    case 1: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.HighSchool); } break;
                    case 2: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.Postgraduate); } break;
                    case 3: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.Certificate); } break;
                    case 4: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.Intermediate); } break;
                    case 5: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.College); } break;
                    case 6: { item.degreename = EnumHelper.ToDisplayName(EducationDegree.University); } break;
                }
            }
            return list;
        }
        public IEnumerable<SpecialismViewModel> getSpecialismByUserID(string UserID)
        {
            return SQLDapperHelper.Instance.ExecuteQuery<SpecialismViewModel>(SpecialismsStore.GetByUserID, new { UserID }).ToList();
        }
        public IEnumerable<Experience> getExperiencesByUserID(string UserID)
        {
            return SQLDapperHelper.Instance.ExecuteQuery<Experience>(ExperienceStore.GetByUserID, new { UserID }).ToList();
        }
        public IEnumerable<Social> getSocialsByUserID(string UserID)
        {
            return SQLDapperHelper.Instance.ExecuteQuery<Social>(SocialStore.GetByUserID, new { UserID }).ToList();
        }
        public IEnumerable<Skills> getSkillsByUserID(string UserID)
        {
            var list = SQLDapperHelper.Instance.ExecuteQuery<Skills>(SkillStore.GetByUserID, new { UserID }).ToList();
            foreach (var item in list)
            {
                switch (item.TypeSkill)
                {
                    case 0: { item.TypeSkillName = EnumHelper.ToDisplayName(TypeSkillEnum.SkillforWork); } break;
                    case 1: { item.TypeSkillName = EnumHelper.ToDisplayName(TypeSkillEnum.SoftSkill); } break;
                    case 2: { item.TypeSkillName = EnumHelper.ToDisplayName(TypeSkillEnum.Anothers); } break;
                }
            }
            return list;
        }
        public IEnumerable<TeamMemberViewModel> getteamleader(int departid)
        {
            return SQLDapperHelper.Instance.ExecuteQuery<TeamMemberViewModel>(EmployeeStore.GetAllLeader,new { departid }).ToList();
        }
        public IEnumerable<TeamMemberViewModel> getteammember(int departid)
        {
            return SQLDapperHelper.Instance.ExecuteQuery<TeamMemberViewModel>(EmployeeStore.GetAllMember, new { departid }).ToList();
        }
 
    }
}
