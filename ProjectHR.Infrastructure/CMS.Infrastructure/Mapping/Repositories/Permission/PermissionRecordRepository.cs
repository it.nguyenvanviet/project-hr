﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Security;
using CMS.Infrastructure.Mapping.Interfaces.Permission;
using CMS.Services.Lib.Dapper;
using System.Collections.Generic;
using System.Linq;

namespace CMS.Infrastructure.Mapping.Repositories.Permission
{
    public class PermissionRecordRepository : IPermissionRecordRepository
    {
        public IEnumerable<PermissionRecord> getall()
        {
            return SQLDapperHelper.Instance.ExecuteQuery<PermissionRecord>(SecurityStore.GetPermissionRecord).ToList();
        }
    }
}
