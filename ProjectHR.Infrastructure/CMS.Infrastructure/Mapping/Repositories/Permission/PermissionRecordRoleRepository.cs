﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Security;
using CMS.Infrastructure.Mapping.Interfaces.Permission;
using CMS.Services.Lib.Dapper;
using System.Collections.Generic;
using System.Linq;

namespace CMS.Infrastructure.Mapping.Repositories.Permission
{
    public class PermissionRecordRoleRepository : IPermissionRecordRoleRepository
    {
        public IEnumerable<PermissionRecordRole> Getall()
        {
           return SQLDapperHelper.Instance.ExecuteQuery<PermissionRecordRole>(SecurityStore.GetPermissionRecordRole).ToList();
        }
    }
}
