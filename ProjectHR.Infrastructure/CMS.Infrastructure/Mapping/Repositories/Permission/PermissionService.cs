﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Account;
using CMS.Core.Domain.Security;
using CMS.Infrastructure.Interfaces.Account;
using CMS.Infrastructure.Interfaces.Accounts;
using CMS.Infrastructure.Mapping.Interfaces.Permission;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Repositories.Permission
{
    public class PermissionService : IPermissionService
    {
        private readonly IPermissionRecordRepository _permissionRecordRepository;
        private readonly IPermissionRecordRoleRepository _permissionRecordRoleRepository;
        private readonly IRolesRepository _rolesRepository;
        private readonly IAccountRepository _accountRepository;
        public PermissionService(IPermissionRecordRepository permissionRecordRepository, 
            IPermissionRecordRoleRepository permissionRecordRoleRepository, 
            IRolesRepository rolesRepository,
             IAccountRepository accountRepository)
        {
            _permissionRecordRepository = permissionRecordRepository;
            _permissionRecordRoleRepository = permissionRecordRoleRepository;
            _rolesRepository = rolesRepository;
            _accountRepository = accountRepository;
        }


        public async Task<bool> authorizeAsync(PermissionRecord permission,string UserID)
        {
            if (String.IsNullOrEmpty(UserID))
                return false;
            var user = await _accountRepository.FindByID(UserID);
            return await authorizeAsync(permission, user);
        }

        public async Task<bool> authorizeAsync(PermissionRecord permission, Users user)
        {
            if (permission == null)
                return false;

            if (user == null)
                return false;
            return await authorizeAsync(permission.SystemName, user);
        }

        public bool authorize(string permissionRecordSystemName)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> authorizeAsync(string permissionRecordSystemName, Users user)
        {
            if (string.IsNullOrEmpty(permissionRecordSystemName))
                return false;


            var listRoles = await _rolesRepository.getAllRolesbyUserIDAsync(user.ID);
            foreach (var role in listRoles)
                if (authorize(permissionRecordSystemName, role.ID))
                    //yes, we have such permission
                    return true;

            //no permission found
            return false;
        }
        protected virtual bool authorize(string permissionRecordSystemName, string RoleId)
        {
            if (string.IsNullOrEmpty(permissionRecordSystemName))
                return false;
             
            var permissions = getPermissionRecordsByCustomerRoleId(RoleId);
            foreach (var permission1 in permissions)
                if (permission1.SystemName.Equals(permissionRecordSystemName, StringComparison.InvariantCultureIgnoreCase))
                    return true;

            return false;
          
        }
        protected virtual IList<PermissionRecord> getPermissionRecordsByCustomerRoleId(string RoleId)
        {
            var query = from pr in _permissionRecordRepository.getall().ToList()
                        join prcrm in _permissionRecordRoleRepository.Getall().ToList() on pr.ID equals prcrm.PermissionRecord_Id
                        where prcrm.RoleId == RoleId
                        orderby pr.ID
                        select pr;

            return query.ToList();
        }

        public void deletePermissionRecord(PermissionRecord permission)
        {
            throw new NotImplementedException();
        }

        public PermissionRecord getPermissionRecordById(int permissionId)
        {
            throw new NotImplementedException();
        }

        public PermissionRecord getPermissionRecordBySystemName(string systemName)
        {
            throw new NotImplementedException();
        }

        public IList<PermissionRecord> getAllPermissionRecords()
        {
            throw new NotImplementedException();
        }

        public void insertPermissionRecord(PermissionRecord permission)
        {
            throw new NotImplementedException();
        }

        public void updatePermissionRecord(PermissionRecord permission)
        {
            throw new NotImplementedException();
        }

        public void installPermissions(IPermissionProvider permissionProvider)
        {
            throw new NotImplementedException();
        }

        public void uninstallPermissions(IPermissionProvider permissionProvider)
        {
            throw new NotImplementedException();
        }
    }
}
