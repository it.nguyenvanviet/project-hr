﻿using CMS.Infrastructure.Mapping.Interfaces.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Infrastructure.Mapping.Repositories.Settings
{
    public class SettingService : ISettingService
    {
        public T GetSettingByKey<T>(string key, T defaultValue = default, int storeId = 0, bool loadSharedValueIfNotFound = false)
        {
            throw new NotImplementedException();
        }

        public void SetSetting<T>(string key, T value, int storeId = 0, bool clearCache = true)
        {
            throw new NotImplementedException();
        }

    }
}
