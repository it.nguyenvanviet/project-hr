﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Setting;
using CMS.Infrastructure.Mapping.Interfaces.Settings;
using CMS.Services.Lib.Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CMS.Infrastructure.Mapping.Repositories.Settings
{
    public class SettingsRepository : ISettingsRepository
    {
        public IEnumerable<Setting> GetSettings()
        {
            return SQLDapperHelper.Instance.ExecuteQuery<Setting>(SettingStore.Getall); ;
        }
        public int UpdateSettings(Setting model)
        {
            return SQLDapperHelper.Instance.ExecuteQuery<int>(SettingStore.Update, new { model.ID, model.Type, model.Status,model.Contents, model.ModifyBy }).FirstOrDefault();
        }
    }
}
