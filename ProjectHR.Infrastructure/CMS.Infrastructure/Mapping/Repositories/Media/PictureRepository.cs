﻿using CMS.Core;
using CMS.Core.ConstString;
using CMS.Core.Domain.Media;
using CMS.Infrastructure.Mapping.Interfaces.Logging;
using CMS.Infrastructure.Mapping.Interfaces.Media;
using CMS.Services.Lib.Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Repositories.Media
{
    public class PictureRepository : IPictureRepository
    {
        private readonly IProjectLoggingRepository _projectLoggingRepository;
        // private readonly IWebHostEnvironment _env;
        public PictureRepository(
              IProjectLoggingRepository projectLoggingRepository
              // ,IWebHostEnvironment env
              )
        {
             
            _projectLoggingRepository = projectLoggingRepository;
            //_env = env;
        }
        IQueryable<Picture> IPictureRepository.Table => SQLDapperHelper.Instance.ExecuteQuery<Picture>(PictureStore.Getall).AsQueryable();
        public Picture GetById(int ID)
        {
            return SQLDapperHelper.Instance.ExecuteQuery<Picture>(PictureStore.GetbyID, new { ID }).FirstOrDefault();
        }
        public async Task<int> InsertAsync(Picture picture)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(PictureStore.Insert, new
                { 
                    picture.IsNew,
                    picture.MimeType,
                    picture.AltAttribute,
                    picture.SeoFilename,
                    picture.TitleAttribute,
                    picture.VirtualPath,
                    picture.Type,
                    picture.ReferenceID,
                    picture.SizeType
                });
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> UpdateAsync(Picture picture)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(PictureStore.Update, new { 
                    picture.Id,
                    picture.IsNew,
                    picture.MimeType,
                    picture.PictureBinary, 
                    picture.SeoFilename,
                    picture.TitleAttribute,
                    picture.VirtualPath,
                    picture.Type,
                    picture.ReferenceID,
                    picture.SizeType
                });
                return data;
            }
            catch(Exception e)
            {
                throw e;
            }
        }
        public async Task<int> DeleteAsync(int ID, string UpdateBy )
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(PictureStore.Update, new
                { ID });
                if (data > 0)
                    await _projectLoggingRepository.Writelog("Delete Picture", UpdateBy, JsonConvert.SerializeObject(ID));
                return data; 
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}

