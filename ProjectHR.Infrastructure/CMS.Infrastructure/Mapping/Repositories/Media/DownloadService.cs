﻿using CMS.Infrastructure.Mapping.Interfaces.Media;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CMS.Infrastructure.Mapping.Repositories.Media
{
    public class DownloadService : IDownloadService
    {  
        public byte[] GetDownloadBits(IFormFile file)
        {
            var fileStream = file.OpenReadStream();
            var ms = new MemoryStream();
            fileStream.CopyTo(ms);
            var fileBytes = ms.ToArray();
            return fileBytes;
        }
    }
}
