﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Team;
using CMS.Core.Domain.Team.Parameter;
using CMS.Core.Domain.Team.TeamViewModel;
using CMS.Infrastructure.Interfaces.Team;
using CMS.Services.Lib.Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Repositories.Team
{
    public class TeamRepository : ITeamRepository
    {
        public IList<TeamSearchViewModel> GetTeamsByFilter(TeamSearchModel model)
        {
            var orderDictionary = new Dictionary<int, TeamSearchViewModel>();

            var func = new Func<TeamSearchViewModel, TeamSearchModelMember, TeamSearchViewModel>((order, orderDetail) =>
            {
                TeamSearchViewModel orderEntry;

                if (!orderDictionary.TryGetValue(order.ID, out orderEntry))
                {
                    orderEntry = order;
                    orderEntry.members = new List<TeamSearchModelMember>();
                    orderDictionary.Add(orderEntry.ID, orderEntry);
                }

                orderEntry.members.Add(orderDetail);
                return orderEntry;
            });
            var data = SQLDapperHelper.Instance.QueryMultipleMapping<TeamSearchViewModel, TeamSearchModelMember>
                (TeamStore.SearchTeam, func,"ID,TeamID", model);
            return data.ToList();
        }
        public async Task<IList<Teams>> GetTeamsByProjectID(int ProjectID) 
        {
            
            var list = await SQLDapperHelper.Instance.ExecuteQueryAsync<Teams>(TeamStore.GetTeamsByProjectID, new { ProjectID });
            return  list;
        }
        public TeamSearchViewModel GetByID(int ID)
        {
            var orderDictionary = new Dictionary<int, TeamSearchViewModel>();

            var func = new Func<TeamSearchViewModel, TeamSearchModelMember, TeamSearchViewModel>((order, orderDetail) =>
            {
                TeamSearchViewModel orderEntry;

                if (!orderDictionary.TryGetValue(order.ID, out orderEntry))
                {
                    orderEntry = order;
                    orderEntry.members = new List<TeamSearchModelMember>();
                    orderDictionary.Add(orderEntry.ID, orderEntry);
                }

                orderEntry.members.Add(orderDetail);
                return orderEntry;
            });
            var data = SQLDapperHelper.Instance.QueryMultipleMapping
                (TeamStore.GetByID, func, "ID,TeamID", new { ID });
            return data.FirstOrDefault();
        }
        public async Task<int> Insert(TeamInsertModel model)
        {
            var id = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(
                 TeamStore.Insert,
                 new
                 {
                     model.TeamName,
                     model.CreateBy,
                     model.Status,
                     model.Description,
                     model.DepartID
                 });

            await insertLeader(id, model);
            await insertMember(id,model);
            
            return id;
        }
        public async Task<int> insertLeader(int TeamID, TeamInsertModel model)
        {
            return await SQLDapperHelper.Instance.ExecuteScalar<int>(TeamStore.InsertLeader, new { TeamID, model.LeaderEmpID , model.CreateBy });
        }
        public async Task<int> insertMember(int TeamID, TeamInsertModel model)
        {
            return await SQLDapperHelper.Instance.ExecuteScalar<int>(TeamStore.InsertMember, new { TeamID, MemberEmpIDs = string.Join(",", model.MemberEmpIDs), model.CreateBy });
        }
        public async Task<int> Update(TeamUpdateModel model)
        { 
            var id = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(
               TeamStore.Update,
               new
               {
                   model.ID,
                   model.TeamName,
                   model.UpdateBy,
                   model.Status,
                   model.Description,
                   model.DepartID
               });

            await UpdateLeader(model);
            await UpdateMember(model);
            return id;
        }
        public async Task<int> UpdateLeader(TeamUpdateModel model)
        {
            return await SQLDapperHelper.Instance.ExecuteScalar<int>(TeamStore.UpdateLeader, new { TeamID = model.ID, model.LeaderEmpID, model.UpdateBy });
        }
        public async Task<int> UpdateMember(TeamUpdateModel model)
        {
            return await SQLDapperHelper.Instance.ExecuteScalar<int>(TeamStore.UpdateMember, new { TeamID = model.ID, MemberEmpIDs = string.Join(",", model.MemberEmpIDs), model.UpdateBy });
        }
        public async Task<int> Delete(int ID)
        {
            var id = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(TeamStore.Delete, new{ ID });
            return id;
        }
    }
}
