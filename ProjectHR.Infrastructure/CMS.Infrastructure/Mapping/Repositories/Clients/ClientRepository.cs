﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Clients;
using CMS.Infrastructure.Interfaces.Clients;
using CMS.Services.Lib.Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CMS.Infrastructure.Repositories.Clients
{
    public class ClientRepository : IClientRepository
    {
        public IList<Client> GetClients(string Name= "")
        {
            
            var listmenus = SQLDapperHelper.Instance.ExecuteQuery<Client>( ClientStore.GetAll,
                   CommandType.StoredProcedure, new { Name = Name }).ToList();
            return listmenus;
        }
    } 
}
