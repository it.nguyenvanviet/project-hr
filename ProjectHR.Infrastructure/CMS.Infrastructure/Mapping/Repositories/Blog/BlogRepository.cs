﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Blog;
using CMS.Core.Domain.Media;
using CMS.Infrastructure.Mapping.Interfaces.Blog;
using CMS.Infrastructure.Mapping.Interfaces.Logging;
using CMS.Services.Lib.Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Repositories.Blog
{
    public class BlogRepository : IBlogRepository
    {
        private readonly IProjectLoggingRepository _projectLoggingRepository; 
        public BlogRepository( IProjectLoggingRepository projectLoggingRepository  )
        {
            _projectLoggingRepository = projectLoggingRepository;
        }
        public async Task<IList<BlogViewModel>> GetBlogs(int EmpID)
        {
            var data = await SQLDapperHelper.Instance.ExecuteQueryAsync<BlogViewModel>(BlogStore.GetBlogByEmpID, new { EmpID });
            foreach(var item in data)
            {
                item.pictures = GetPictureByBlogID(item.ID);
            }    
            return data;
        }
        private List<Picture> GetPictureByBlogID(int BlogID)
        {
            return SQLDapperHelper.Instance.ExecuteQuery<Picture>(PictureStore.GetPicture_forBlog, new { BlogID }).ToList();
        }    
        public async Task<BlogViewModel> GetByID(int ID)
        {
            return await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<BlogViewModel>(BlogStore.GetByID, new { ID });
        }    
        public async Task<int> Insert(BlogInsertModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(BlogStore.Insert, 
                    new
                    {
                        model.EmpID,
                        model.Title,
                        model.Descriptions,
                        model.Contents,
                        model.Status,
                        model.CreateBy
                    });
                if (data > 0)
                   await _projectLoggingRepository.Writelog("Insert Blog", model.CreateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> Update(BlogUpdateModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(BlogStore.Update,
                    new
                    {
                        model.ID,
                        model.EmpID,
                        model.Title,
                        model.Descriptions,
                        model.Contents,
                        model.Status,
                        model.UpdateBy
                    });
                if (data > 0)
                    await _projectLoggingRepository.Writelog("Insert Blog", model.CreateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public async Task<int> Delete(BlogDeleteModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(BlogStore.Delete,
                    new
                    {
                        model.ID, 
                    });
                if (data > 0)
                  await  _projectLoggingRepository.Writelog("Insert Blog", model.UpdateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
