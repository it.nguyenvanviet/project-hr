﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Setting;
using CMS.Infrastructure.Mapping.Interfaces.Logging;
using CMS.Services.Lib.Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Repositories.Logging
{
    public class ProjectLoggingRepository : IProjectLoggingRepository
    {
        public async Task<int> Writelog(string Action, string UserID = "", string Data = "")
        {
            try
            {
                return await SQLDapperHelper.Instance.ExecuteScalar<int>(LoggingStore.WriteLog,
                 new { Action, UserID, Data }, CommandType.StoredProcedure);
            }

            catch(Exception e) { throw e; }
        } 
    
    }
}
