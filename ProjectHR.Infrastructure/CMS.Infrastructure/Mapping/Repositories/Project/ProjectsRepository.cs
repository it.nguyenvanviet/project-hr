﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Projects;
using CMS.Infrastructure.Interfaces.Project;
using CMS.Services.Lib.Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using CMS.Infrastructure.Repositories.Team;
using CMS.Infrastructure.Interfaces.Team;

namespace CMS.Infrastructure.Repositories.Project
{
    public class ProjectsRepository : IProjectsRepository
    {
        public int Delete(int ID)
        {
            throw new NotImplementedException();
        }

        public Projects GetByID(int ID)
        {
            throw new NotImplementedException();
        }

        public IList<Projects> GetByName(string Name = "")
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProjectViewModel> getListProjects(string userid)
        {
            throw new NotImplementedException();
        }

        public IList<ProjectType> GetProjectType()
        {
            throw new NotImplementedException();
        }

        public int Insert(Projects par)
        {
            throw new NotImplementedException();
        }

        public IList<Projects> Search(ProjectParamSearch model)
        {
            throw new NotImplementedException();
        }

        public int Update(Projects par)
        {
            throw new NotImplementedException();
        }
    }
}
