﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Employee;
using CMS.Infrastructure.Mapping.Interfaces.Logging;
using CMS.Infrastructure.Mapping.Interfaces.Specialisms;
using CMS.Services.Lib.Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMS.Infrastructure.Mapping.Repositories.Specialisms
{
    public class SpecialismRepository : ISpecialismRepository
    { 
        private readonly IProjectLoggingRepository _projectLoggingRepository;

        public SpecialismRepository( 
            IProjectLoggingRepository projectLoggingRepository)
        { 
            _projectLoggingRepository = projectLoggingRepository;  
        }

        public async Task<int> Create(SpecialismInsertModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(SpecialismsStore.Insert,
                new { model.Name,model.Descriptions,model.CreateBy,model.Status });
                if (data > 0)
                  await _projectLoggingRepository.Writelog("Add Specialism", model.CreateBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public IEnumerable<Specialism> GetSpecialisms()
        {
            return SQLDapperHelper.Instance.ExecuteQuery<Specialism>(SpecialismsStore.GetAll).ToList();
        }

        public async Task<int> Update(SpecialismUpdateModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(SpecialismsStore.Insert,
                new { model.ID, model.Name, model.Descriptions, model.ModifyBy, model.Status });
                if (data > 0)
                  await  _projectLoggingRepository.Writelog("Update Specialism", model.ModifyBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            { throw e; }
        }
        public async Task<int> Delete(SpecialismDeleteModel model)
        {
            try
            {
                var data = await SQLDapperHelper.Instance.ExecuteQueryFirstOrDefault<int>(SpecialismsStore.Delete,
                new { model.ID, model.ModifyBy });
                if (data > 0)
                   await _projectLoggingRepository.Writelog("Delete Specialism", model.ModifyBy, JsonConvert.SerializeObject(model));
                return data;
            }
            catch (Exception e)
            { throw e; }
        }
    }
}
