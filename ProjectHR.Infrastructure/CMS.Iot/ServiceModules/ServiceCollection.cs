﻿using CMS.Core;
using CMS.Core.ConstString;
using CMS.Infrastructure.HttpRequest;
using CMS.Infrastructure.Interfaces.Account;
using CMS.Infrastructure.Interfaces.Accounts;
using CMS.Infrastructure.Interfaces.Clients;
using CMS.Infrastructure.Interfaces.Employee;
using CMS.Infrastructure.Interfaces.Menu;
using CMS.Infrastructure.Interfaces.Project;
using CMS.Infrastructure.Mapping.Interfaces.Event;
using CMS.Infrastructure.Mapping.Interfaces.Logging;
using CMS.Infrastructure.Mapping.Interfaces.Media;
using CMS.Infrastructure.Mapping.Interfaces.Permission;
using CMS.Infrastructure.Mapping.Interfaces.Settings;
using CMS.Infrastructure.Mapping.Repositories.Event;
using CMS.Infrastructure.Mapping.Repositories.Logging;
using CMS.Infrastructure.Mapping.Repositories.Media;
using CMS.Infrastructure.Mapping.Repositories.Permission;
using CMS.Infrastructure.Mapping.Repositories.Settings;
using CMS.Infrastructure.Pattern;
using CMS.Infrastructure.Repositories.Accounts;
using CMS.Infrastructure.Repositories.Clients;
using CMS.Infrastructure.Repositories.Employee;
using CMS.Infrastructure.Repositories.Menu;
using CMS.Infrastructure.Repositories.Project;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using Microsoft.Extensions.DependencyInjection;
using CMS.Infrastructure.Mapping.Interfaces.Specialisms;
using CMS.Infrastructure.Mapping.Repositories.Specialisms;
using CMS.Infrastructure.Mapping.Interfaces.Blog;
using CMS.Infrastructure.Mapping.Repositories.Blog;
using CMS.Infrastructure.Mapping.Interfaces.Accounts.Departs;
using CMS.Infrastructure.Mapping.Repositories.Accounts.Departs;
using CMS.Infrastructure.Interfaces.Team;
using CMS.Infrastructure.Repositories.Team;
using CMS.Infrastructure.Mapping.Interfaces.Leave;
using CMS.Infrastructure.Mapping.Repositories.Leave;

namespace CMS.Iot.ServiceModules
{

    public static class ServiceCollectionIot
    {
        public static IServiceCollection ServiceCollection(this IServiceCollection services)
        {
            services.AddTransient<IAccountRepository, AccountRepository>();
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();
            services.AddTransient<IEmployeeInfoRepository, EmployeeInfoRepository>();
            services.AddTransient<IMenuRepository, MenuRepository>();
            services.AddTransient<IProjectsRepository, ProjectsRepository>();
            services.AddTransient<IClientRepository, ClientRepository>();
            services.AddTransient<IRolesRepository, RolesRepository>();
            services.AddTransient<IProjectLoggingRepository, ProjectLoggingRepository>();
            services.AddTransient<IHttpRequest, HttpRequestApi>();
            services.AddTransient<ISettingsRepository, SettingsRepository>();
            services.AddTransient<IPermissionRecordRepository, PermissionRecordRepository>();
            services.AddTransient<IPermissionRecordRoleRepository, PermissionRecordRoleRepository>();
            services.AddTransient<IPermissionService, PermissionService>();
            services.AddTransient<IWebHelper, WebHelper>();
            services.AddTransient<IEngine, Engine>();
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IPictureRepository, PictureRepository>();
            services.AddTransient<IPictureService, PictureService>();
            services.AddTransient<ICmsFileProvider, CmsFileProvider>();
            services.AddTransient<IDownloadService, DownloadService>();
            services.AddTransient<IEventPublisher, EventPublisher>();
            services.AddTransient<ISettingService, SettingService>();
            services.AddTransient<ISpecialismRepository, SpecialismRepository>();
            services.AddTransient<MediaSettings, MediaSettings>();
            services.AddTransient<IBlogRepository,BlogRepository >();
            services.AddTransient<IDepartmentRepository, DepartmentRepository>();
            services.AddTransient<ITeamRepository, TeamRepository>();
            services.AddTransient<ILeaveRepository, LeaveRepository>();


            return services;
        }
        public static IServiceCollection ServiceAuthenCollection(this IServiceCollection services, IConfiguration Configuration)
        {
            //role - authentication - login social
            services.AddAuthorization(options =>
            {
                options.AddPolicy(Policy.AdminRole, policy => policy.RequireRole(RoleName.Administrator)  );
                options.AddPolicy(Policy.UserRole, policy => policy.RequireRole(RoleName.User));
            });

             
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
            {
                //options.EventsType = typeof()
                options.AccessDeniedPath = new PathString(SecurityUrl.AccountDenied);
                options.Cookie = new CookieBuilder
                {
                    //Domain = "",
                    HttpOnly = true,
                    Name = AuthenSchema.CookieName,
                    Path = "/",
                    SameSite = SameSiteMode.Lax,
                    SecurePolicy = CookieSecurePolicy.SameAsRequest 
            };
                 
                options.ExpireTimeSpan = TimeSpan.FromHours(TokenConfig.MinExpiredHour);
                options.LoginPath = new PathString(AccountUrl.AccountLogin);
                options.ReturnUrlParameter = "RequestPath";
                options.SlidingExpiration = true;
            });
            services.AddCors(options =>
            {
                options.AddPolicy(name: "MyPolicy",
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:3000/")
                                .WithMethods("GET", "PUT", "POST", "DELETE", "OPTIONS");
                        builder.AllowAnyOrigin()
                         .AllowAnyHeader()
                         .AllowAnyMethod();
                    });
                options.AddPolicy(name: "MyPolicy2",
                   builder =>
                   {
                       builder.WithOrigins(UrlApi.ApiUrl)
                                .WithMethods("GET", "PUT", "POST", "DELETE", "OPTIONS");
                       builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                   });
                options.AddPolicy(name: "MyPolicy",
                   builder =>
                   {
                       builder.WithOrigins("https://hr.karobean.com/")
                                .WithMethods("GET", "PUT", "POST", "DELETE", "OPTIONS");
                       builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                   });
                options.AddPolicy(name: "MyPolicy",
                    builder =>
                    {
                        builder.WithOrigins("http://Test.karobean.com")
                                 .WithMethods("GET", "PUT", "POST", "DELETE", "OPTIONS");
                        builder.AllowAnyOrigin()
                         .AllowAnyHeader()
                         .AllowAnyMethod();
                    });
                options.AddPolicy(name: "MyPolicy",
                    builder =>
                    {
                        builder.WithOrigins("https://reacthr.vercel.app/")
                                 .WithMethods("GET", "PUT", "POST", "DELETE", "OPTIONS");
                        builder.AllowAnyOrigin()
                         .AllowAnyHeader()
                         .AllowAnyMethod();
                    });
            });
            return services;
        }
    }

}
