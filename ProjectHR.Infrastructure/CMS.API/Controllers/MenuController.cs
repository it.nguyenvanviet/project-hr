﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CMS.Core.ConstString;
using CMS.Core.Domain.ApiModel.ResponseModel;
using CMS.Core.Domain.Menu;
using CMS.Infrastructure.Interfaces.Accounts;
using CMS.Infrastructure.Interfaces.Menu;
using CMS.Infrastructure.Mapping.Interfaces.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CMS.API.Controllers
{
    [Authorize]
    public class menuController : apiController
    {
        private readonly IMenuRepository _menuRepository;
        private readonly IProjectLoggingRepository _projectLoggingRepository;
        private readonly IRolesRepository _rolesRepository;
        public menuController(IMenuRepository menuRepository, IProjectLoggingRepository projectLoggingRepository, IRolesRepository rolesRepository)
        {
            _menuRepository = menuRepository;
            _projectLoggingRepository = projectLoggingRepository;
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Get all Menus
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route(nameof(getmenus))]
        public ActionResult<IEnumerable<MenuViewModel>> getmenus()
        {
            var data = _menuRepository.Getall().Select(item => new MenuViewModel{
                ID = item.ID,
                Content = item.Content,
                Name = item.Name,
                IconClass = item.IconClass,
                Link = item.Link,
                Sort = item.Sort,
                ParentID = item.ParentID,
                Status = item.Status,
                Type = item.Type
            }).ToArray();
            return data;
        }

        /// <summary>
        /// Get menu by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<Menus> getmenus([FromRoute] int id)
        {
            var data = _menuRepository.GetByID(id);
            if (data == null)
            {
                return NotFound();
            }
            return data;
        }

        /// <summary>
        /// get menutype by userid
        /// </summary>  
        [HttpGet(nameof(getmenutype))]
        public ActionResult<IEnumerable<MenuType>> getmenutype()
        {
            var data = _menuRepository.GetMenuTypes();
            return data.ToList();
        }

        /// <summary>
        /// get menutype by userid
        /// </summary> 
        /// <returns></returns>
        [HttpGet(nameof(getmenutypebyuseid) + "/{userid}")]
        public ActionResult<IEnumerable<MenuType>> getmenutypebyuseid([FromRoute] string userid) 
        {
            var data = _menuRepository.GetMenuTypeByUserID(userid);
            return data.ToList();
        }

        /// <summary>
        /// get menutype by userid
        /// </summary> 
        /// <returns></returns>
        [HttpGet(nameof(getmenubyuseid) + "/{userid}")]
        public ActionResult<IEnumerable<Menus>> getmenubyuseid([FromRoute] string userid)
        {
            var data = _menuRepository.GetMenusByUserID(userid);
            return data.ToList();
        }


        /// <summary>
        /// Get role based on menu id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/role")]
        public object getrole([FromRoute] int id)
        {
            var rs = new ReponseModelView();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            try
            {
                var data = _rolesRepository.getByMenuID(id);
                if (data != null)
                {
                    rs.Result = data.Select(a=>new { id = a.ID, name = a.Name });
                    rs.Status = 1;
                }
                else
                {
                    rs.Result = "Not Found";
                }
            }
            catch (Exception e)
            {
                rs.Error = e.Message;
            }
            return (rs);
        }

        /// <summary>
        /// Get role based on menu id
        /// </summary> 
        /// <returns></returns>
        [HttpGet("position")]
        public object getposition()
        {
            var rs = new ReponseModelView();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            try
            {
                var data = _menuRepository.GetMenuPosition();
                if (data.Count() > 0)
                {
                    rs.Result = data;
                    rs.Status = 1;
                }
                else
                {
                    rs.Result = "Not Found";
                }
            }
            catch (Exception e)
            {
                rs.Error = e.Message;
            }
            return (rs);
        }

        /// <summary>
        /// Create Menu
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route(nameof(create))]
        public IActionResult create(Menus model)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            else
            {
                string result = "";
                var status = _menuRepository.Insert(model);
                switch (status)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status, result });
            }
        }

        /// <summary>
        /// Update menu information
        /// </summary> 
        /// <returns></returns>
        [HttpPut("{id}")] 
        public IActionResult update(Menus model, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            else
            {
                model.ID = id;
                string result = "";
                var status = _menuRepository.Update(model);
                switch(status)
                {
                    case 0: result = ConstString.UpdateFail ; break; 
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status,  result });
            }
        }

        /// <summary>
        /// Update vị trí và cha con
        /// </summary>
        /// <param name="par"></param>
        /// <returns></returns> 
        [HttpPut]
        [Route(nameof(updateposition))]
        public IActionResult updateposition([FromBody] RootMenu par )
        {
            var rs = new ReponseModelView();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            try
            {
                if (!String.IsNullOrEmpty(par.List))
                {
                    par.List_menu = JsonConvert.DeserializeObject<List<ItemMenu>>(par.List);

                    var data = _menuRepository.ChangePositionAsync(par);
                    if (data > 0)
                    {
                        rs.Status = 1;
                        rs.Result = ConstString.UpdateSuccess;
                    }
                    else
                        rs.Result = ConstString.UpdateFail;
                    
                }
                else
                {
                    rs.Error = ConstString.UpdateFail ;
                }

            }
            catch (Exception e)
            {
                rs.Error = e.Message;
            }
            stopwatch.Stop();
            rs.Elapsed = stopwatch.ElapsedMilliseconds;
            return Ok(rs);
        }

        /// <summary>
        /// Delete a menu
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Bool</returns>
        [HttpDelete("{id}")]
        public IActionResult delete([FromRoute] int id)
        {
            if (id < 0)
            {
                return BadRequest();
            }
            var isSuccessed = _menuRepository.Delete(id) > 0;
            return Ok(isSuccessed);
        }

        /// <summary>
        /// Cập nhật Menu
        /// </summary>
        /// <param name="par"></param>
        /// <returns></returns>
        [HttpPut]
        [Route(nameof(updaterole))]
        public IActionResult updaterole([FromBody] MenuRoleParaUpdate par)
        {
            var rs = new ReponseModelView();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            try
            {
                int countsuccess = 0;
                var menurole = new MenuRoleVM
                {
                    MenuID = par.ID,
                    ListRole = string.Join(",", par.Listroleid),
                };
                var data = _menuRepository.UpdateRoleMenu(menurole);
                if (data > 0)
                {
                    countsuccess++;
                }
                if (countsuccess > 0)
                {
                    rs.Result = ConstString.UpdateSuccess;
                    rs.Status = 1;
                }
                else
                {
                    rs.Result = ConstString.UpdateFail;
                }
            }
            catch (Exception e)
            {
                rs.Error = e.Message;
            }
            return Ok(rs);
        }
        
    }
}