﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Employee;
using CMS.Infrastructure.Mapping.Interfaces.Accounts.Departs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.API.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class departmentController : apiController
    {
        #region Repository

       
        private readonly IDepartmentRepository _departmentRepository;
        public departmentController(IDepartmentRepository departmentRepository)
        {
            _departmentRepository = departmentRepository;
        }

        #endregion

        #region Function

       
        /// <summary>
        /// Lấy danh sách phòng ban
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(nameof(GetDepartments))]
        public async Task<IList<Department>> GetDepartments(DepartmentGetModel model)
        {
            return await _departmentRepository.getDepartments(model);
        }

        /// <summary>
        /// Lấy chi tiết Phòng ban theo ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet(nameof(GetDepartmentByID) + "{id}")]
        public async Task<Department> GetDepartmentByID(int id)
        {
            return await _departmentRepository.getDepartmentByID(id);
        }

        /// <summary>
        /// Tạo Phong ban
        /// </summary>
        /// <param name="dep"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> POST(DepartmentInsertModel dep)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(dep);
            }
            dep.Status = 1;
            string result = "";
            var rs = await _departmentRepository.insert(dep);
            if (rs > 0)
            { 
                result = ConstString.UpdateSuccess;
                rs = 1;
            }
            else
            {
                result = ConstString.UpdateFail;
            }     
            return Ok(new { status = rs, result });
        }
        
        /// <summary>
        /// Điều chỉnh thông tin phòng ban
        /// </summary>
        /// <param name="spe"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PUT(DepartmentUpdateModel spe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(spe);
            }
            spe.Status = 1;
            string result = "";
            var rs = await _departmentRepository.update(spe);
            switch (rs)
            {
                case 0: result = ConstString.UpdateFail; break;
                case 1: result = ConstString.UpdateSuccess; break;
            }
            return Ok(new { status = rs, result });
        }
        
        /// <summary>
        /// Xóa phòng ban
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DELETE(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            string result = "";
            var rs = await _departmentRepository.delete(id);
            switch (rs)
            {
                case 1: result = ConstString.UpdateSuccess; break;
                case 0: result = ConstString.UpdateFail; break;
                case -1: result = "Có team đang ở Phòng ban này,không thể cập nhật, vui lòng kiểm tra lại"; break;
            }
            return Ok(new { status = rs, result });
        }
        #endregion
    }
}
