﻿using CMS.Core.Domain.Blog;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Infrastructure.Mapping.Interfaces.Blog;
using Microsoft.AspNetCore.Authorization;
using CMS.Core.ConstString;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CMS.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class blogController : apiController
    {
        #region repository
        private readonly IBlogRepository _blogRepository;
        public blogController(IBlogRepository blogRepository)
        {
            _blogRepository = blogRepository;
        }
        #endregion
        
        /// <summary>
        /// Lấy thông tin bài viết theo ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<BlogViewModel> Get(int ID)
        {
            return await _blogRepository.GetByID(ID);
        }

        /// <summary>
        /// Lấy thông tin bài viết theo id nhân viên ( người tạo )
        /// </summary>
        /// <param name="empid">ID nhân viên</param>
        /// <returns></returns>
        [HttpGet(nameof(getbyempid))]
        public async Task<IEnumerable<BlogViewModel>> getbyempid(int empid)
        {
            return await _blogRepository.GetBlogs(empid);
        }

        /// <summary>
        /// Tạo bài viết
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost] 
        public async Task<IActionResult> Post([FromBody] BlogInsertModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            model.Status = 1; 
            var rs = await _blogRepository.Insert(model);
            string result = "";
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }

        /// <summary>
        /// Chính sửa bài viết
        /// </summary>
        /// <param name="id">ID bài viết</param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] BlogUpdateModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            model.ID = id;
            var rs = await _blogRepository.Update(model);
            string result = "";
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }

       /// <summary>
       /// Xóa bài viết
       /// </summary>
       /// <param name="id"></param>
       /// <param name="model"></param>
       /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id, [FromBody] BlogDeleteModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            model.ID = id;
            var rs = await _blogRepository.Delete(model);
            string result = "";
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
    }
}
