﻿using CMS.Core.Domain.Projects;
using CMS.Infrastructure.Interfaces.Project;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class projectController : ControllerBase
    {
        private readonly IProjectsRepository _projectsRepository;
        public projectController(IProjectsRepository projectsRepository)
        {
            _projectsRepository = projectsRepository;
        }
        [HttpGet(nameof(getmenubyuseid) + "/{userid}")]
        public ActionResult<IEnumerable<ProjectViewModel>> getmenubyuseid([FromRoute] string userid)
        {
            var data = _projectsRepository.getListProjects(userid);
            return data.ToList();
        }
        [HttpGet(nameof(getProjectType))]
        public ActionResult<IEnumerable<ProjectType>> getProjectType()
        {
            var data = _projectsRepository.GetProjectType();
            return data.ToList();
        }
    }
}
