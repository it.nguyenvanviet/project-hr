﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CMS.Core.ConstString;
using CMS.Core.Domain.Employee;
using CMS.Core.Domain.Team.TeamViewModel;
using CMS.Infrastructure.Interfaces.Accounts;
using CMS.Infrastructure.Interfaces.Employee;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMS.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class employeeController : apiController
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IEmployeeInfoRepository _employeeinfoRepository;
        private readonly IRolesRepository _rolesRepository;
        public employeeController(IEmployeeRepository employeeRepository,
            IEmployeeInfoRepository employeeinfoRepository,
            IRolesRepository rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _employeeinfoRepository = employeeinfoRepository;
            _rolesRepository = rolesRepository;
        }
        #region Get



        /// <summary>
        /// Tìm kiếm thông tin Nhân viên
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost(nameof(search))]
        public IEnumerable<Employees> search(EmpSearchModel model)
        {
            var emp = _employeeRepository.search(model);
            return emp;
        }


        /// <summary>
        /// Lấy thông tin nhân viên qua ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public Employees Get(int ID)
        {
            return _employeeRepository.getByID(ID);
        }

        /// <summary>
        /// Lấy thông tin nhân viên qua userid
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet(nameof(getinfobyuserid))]
        public async Task<object> getinfobyuserid(string userid)
        {
            if (!String.IsNullOrEmpty(userid))
                return await _employeeRepository.getByUserIDAsync(userid);
            else
                return BadRequest();
        }
        
        /// <summary>
        /// Lấy thông tin liên lạc từ userid
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet(nameof(getbasicinfocontact))]
        public async Task<object> getbasicinfocontact(string userid)
        {
            if (!String.IsNullOrEmpty(userid))
                return await _employeeRepository.getBasicInfoUser(userid);
            else
                return BadRequest();
        }

        /// <summary>
        /// Lấy danh sách MXH theo id nhân viên
        /// </summary>
        /// <param name="empid"></param>
        /// <returns></returns>
        [HttpGet(nameof(getsocial))]
        public object getsocial(int empid)
        {
            return _employeeRepository.getSocialsByEmpID(empid);
        }
        /// <summary>
        /// Lấy danh sách chuyên ngành theo id nhân viên
        /// </summary>
        /// <param name="empid"></param>
        /// <returns></returns>
        [HttpGet(nameof(getspecialism))]
        public object getspecialism(int empid)
        {
            return _employeeRepository.getSpecialismByEmpID(empid);
        }
        /// <summary>
        /// Lấy thông tin học vấn theo id nhân viên
        /// </summary>
        /// <param name="empid"></param>
        /// <returns></returns>
        [HttpGet(nameof(geteducation))]
        public object geteducation(int empid)
        {
            return _employeeRepository.getEducationsByEmpID(empid);
        }
        /// <summary>
        /// Lấy danh sách kinh nghiệm làm việc theo id nhân viên
        /// </summary>
        /// <param name="empid"></param>
        /// <returns></returns>
        [HttpGet(nameof(getexperiences))]
        public object getexperiences(int empid)
        {
            return _employeeRepository.getExperiencesByEmpID(empid);
        }
        /// <summary>
        /// Lấy danh sách quyền theo id nhân viên
        /// </summary>
        /// <param name="empid"></param>
        /// <returns></returns>
        [HttpGet(nameof(getrole))]
        public async Task<object> getrole(int empid)
        {
            return await _rolesRepository.getAllRolesbyEmpIDAsync(empid);
        }

        /// <summary>
        /// Lấy danh sách MXH theo userid
        /// </summary>     
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpGet(nameof(getsocialbyuserid))]
        public object getsocialbyuserid(string userID)
        {
            return _employeeRepository.getSocialsByUserID(userID);
        }
        /// <summary>
        /// Lấy danh sách chuyên ngành theo userid
        /// </summary>     
        /// <param name="userID"></param>
        /// <returns></returns>
        [HttpGet(nameof(getspecialismbyuserid))]
        public object getspecialismbyuserid(string userID)
        {
            return _employeeRepository.getSpecialismByUserID(userID);
        }

        [HttpGet(nameof(geteducationbyuserid))]
        public object geteducationbyuserid(string userID)
        {
            return _employeeRepository.getEducationsByUserID(userID);
        }

        [HttpGet(nameof(getexperiencesbyuserid))]
        public object getexperiencesbyuserid(string userID)
        {
            return _employeeRepository.getExperiencesByUserID(userID);
        }

        [HttpGet(nameof(getskillbyuserid))]
        public object getskillbyuserid(string userID)
        {
            return _employeeRepository.getSkillsByUserID(userID);
        }
         

        #endregion

        #region Insert, Update, Delete


        /// <summary>
        /// Add and edit employee
        /// </summary>
        /// <returns></returns>
        [HttpPost(nameof(create))]
        public async Task<IActionResult> create(EmpRegisViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            emp.Status = 1;
            emp.Rank = 0;
            emp.Experience = 0;
            emp.JoinDate = DateTime.Now;
            var rs = await _employeeRepository.insertAsync(emp);
            if (rs > 0)
                return Ok(emp);
            return BadRequest();
        }
        /// <summary>
        /// Update nhân viên
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> edit(EmpUpdateViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeRepository.updateAsync(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }

        /// <summary>
        /// Update Avatar, Basic Info, Role
        /// </summary>
        /// <returns></returns> 
        [HttpPut(nameof(updateavartar))]
        public async Task<IActionResult> updateavartar(EmpUpdateAvatarViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeRepository.updateAvatar(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpPut(nameof(updatebasicinfo))]
        public async Task<IActionResult> updatebasicinfo(EmpUpdateViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeRepository.updateBasicInfoAsync(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpPut(nameof(updaterole))]
        public async Task<IActionResult> updaterole(EmpUpdateRoleViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeRepository.updateRole(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        
        #endregion

        #region info emp
        /// <summary>
        /// Add education, experience, skill, social, specialism, portfolio
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>

        [HttpPost(nameof(addeducation))]
        public async Task<IActionResult> addeducation(EmpAdd_EducationViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.addEducation(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }

        [HttpPost(nameof(addexperiences))]
        public async Task<IActionResult> addexperiences(EmpAdd_ExperienceViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.addExperience(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }

        [HttpPost(nameof(addsocial))]
        public async Task<IActionResult> addsocial(EmpAdd_SocialViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.addSocial(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }

        [HttpPost(nameof(addskill))]
        public async Task<IActionResult> addskill(EmpAdd_SkillViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.addSkill(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }

        [HttpPost(nameof(addspecialism))]
        public async Task<IActionResult> addspecialism(EmpAdd_SpecialismViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.addSpecialism(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpPost(nameof(addportfolio))]
        public async Task<IActionResult> addportfolio(EmpAdd_PortfolioViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.addPortfolio(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }

        [HttpPut(nameof(editeducation))] 
        public async Task<IActionResult> editeducation(EmpUpdate_EducationViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.editEducation(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpPut(nameof(editexperience))] 
        public async Task<IActionResult> editexperience(EmpUpdate_ExperienceViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.editExperience(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpPut(nameof(editsocial))] 
        public async Task<IActionResult> editsocial(EmpUpdate_SocialViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.editSocial(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpPut(nameof(editskill))] 
        public async Task<IActionResult> editskill(EmpUpdate_SkillViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.editSkill(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpPut(nameof(editspecialism))] 
        public async Task<IActionResult> editspecialism(EmpUpdate_SpecialismViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.editSpecialism(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpPut(nameof(editportfolio))]
        public async Task<IActionResult> editportfolio(EmpUpdate_PortfolioViewModel emp)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(emp);
            }
            string result = "";
            var rs = await _employeeinfoRepository.editPortfolio(emp);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }

        [HttpDelete("education/{id}")]
        public async Task<IActionResult> deleteeducation([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(id);
            }
            string result = "";
            var rs = await _employeeinfoRepository.deleteEducation(id);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpDelete("experience/{id}")]
        public async Task<IActionResult> deleteexperience([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(id);
            }
            string result = "";
            var rs = await _employeeinfoRepository.deleteExperience(id);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpDelete("skill/{id}")]
        public async Task<IActionResult> deleteskill([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(id);
            }
            string result = "";
            var rs = await _employeeinfoRepository.deleteSkill(id);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpDelete("social/{id}")]
        public async Task<IActionResult> deletesocial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(id);
            }
            string result = "";
            var rs = await _employeeinfoRepository.deleteSocial(id);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpDelete("specialism/{id}")]
        public async Task<IActionResult> deletespecialism([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(id);
            }
            string result = "";
            var rs = await _employeeinfoRepository.deleteSpecialism(id);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpDelete("portfolio/{id}")]
        public async Task<IActionResult> deleteportfolio([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(id);
            }
            string result = "";
            var rs = await _employeeinfoRepository.deletePortfolio(id);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }

        #endregion

        #region team 
        /// <summary>
        /// Get employee 
        /// </summary> 
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet(nameof(getteamleader)+"/{departid}")]
        public IEnumerable<TeamMemberViewModel> getteamleader([FromRoute] int departid = 0)
        {
            var emp = _employeeRepository.getteamleader(departid); 
            return emp;
        }
        /// <summary>
        /// Get employee 
        /// </summary> 
        /// <returns></returns>
        [HttpGet(nameof(getteammember) + "/{departid}")]
        public IEnumerable<TeamMemberViewModel> getteammember([FromRoute] int departid = 0)
        {
            var emp = _employeeRepository.getteammember(departid);
            return emp;
        }
        #endregion


    }
}