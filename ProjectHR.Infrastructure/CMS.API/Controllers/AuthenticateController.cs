﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using CMS.Core.ConstString;
using CMS.Core.Domain.Account;
using CMS.Infrastructure.Interfaces.Account;
using CMS.Infrastructure.Interfaces.Accounts;
using CMS.Infrastructure.Interfaces.Employee;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace CMS.API.Controllers
{
    [Authorize(Policy = Policy.AdminRole)]
    public class authenticateController : apiController
    {
        #region Repository


        private readonly IAccountRepository _accountRepository;
        private readonly IRolesRepository _rolesRepository;
        private readonly IConfiguration _configuration;

        public authenticateController(
            IAccountRepository accountRepository,
            IRolesRepository rolesRepository,
            IConfiguration configuration)
        {
            _accountRepository = accountRepository;
            _rolesRepository = rolesRepository;
            _configuration = configuration;
        }
        #endregion

        #region Login 

        /// <summary>
        /// Api for login 
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/authenticate/login
        ///     {
        ///        "username":"admin",
        ///        "password":"admin",
        ///     }
        ///
        /// </remarks> 
        /// <param name="model" required="true" schema="testttt">Id of the Absence Reason being updated.</param> 
        /// <returns></returns> 
        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModelApi model)
        {
            var user = await _accountRepository.FindByNameAsync(model.Username);
            var userdata = await _accountRepository.Login(model.Username, model.Password);
            if (user != null && userdata != null)
            {
                var userRoles = await _rolesRepository.getAllRolesbyUserIDAsync(user.ID);

                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole.Name));
                }

                var signingCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"])), SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(TokenConfig.MinExpiredHour),
                    claims: authClaims,
                    signingCredentials: signingCredentials
                    );
                var tokenstring = new JwtSecurityTokenHandler().WriteToken(token);
                var tokenobject = new
                {
                    token = tokenstring,
                    user = userdata,
                    expiration = token.ValidTo
                };
                if (user != null && userdata.EmpID != null)
                    return Ok(tokenobject);
                else
                    return Unauthorized(new { error = "Tài khoản hoặc mật khẩu không chính xác, Vui lòng thử lại." });
            }
            return Unauthorized();

        }
        [AllowAnonymous]
        [HttpPost]
        [Route("refreshToken")]
        public IActionResult RefreshToken([FromBody] TokenRequest model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            var principal = getClaimsPrincipalOldToken(model.Token);
            var signingCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"])), SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                     issuer: _configuration["JWT:ValidIssuer"],
                     audience: _configuration["JWT:ValidAudience"],
                     expires: DateTime.Now.AddHours(TokenConfig.MinExpiredHour),
                     claims: principal.Claims,
                     signingCredentials: signingCredentials
                     );
            var tokenstring = new JwtSecurityTokenHandler().WriteToken(token);
            var tokenobject = new
            {
                newtoken = tokenstring,
                expiration = token.ValidTo
            };
            if (principal.Claims.Count() > 0)
                return Ok(tokenobject);
            else
                return Unauthorized(new { error = "Token không chính xác!, Vui lòng thử lại." });
        }

        private ClaimsPrincipal getClaimsPrincipalOldToken(string token)
        {
            var mySecret = _configuration["JWT:Secret"];
            var mySecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(mySecret));
            var myIssuer = _configuration["JWT:ValidIssuer"];
            var myAudience = _configuration["JWT:ValidAudience"];
            var tokenHandler = new JwtSecurityTokenHandler();

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidIssuer = myIssuer,
                ValidAudience = myAudience,
                IssuerSigningKey = mySecurityKey
            };
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");
            return principal;
        } 
        #endregion

        #region  get role


        /// <summary>
        /// Get all Roles
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route(nameof(getrole))]
        public object getrole()
        {
            var listrole = _rolesRepository.getall().Select(a => new { id = a.ID, name = a.Name });
            return listrole;
        }

        #endregion
    }
}