﻿using CMS.Infrastructure.Mapping.Interfaces.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace CMS.API.Controllers
{
   
    [EnableCors("MyPolicy")]
    [ApiController]
    [Route("api/[controller]")]
    public abstract class apiController : ControllerBase
    { }
    
}