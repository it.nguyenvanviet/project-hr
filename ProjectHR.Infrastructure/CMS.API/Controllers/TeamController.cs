using CMS.Core.ConstString;
using CMS.Core.Domain.Team;
using CMS.Core.Domain.Team.Parameter;
using CMS.Core.Domain.Team.TeamViewModel;
using CMS.Infrastructure.Interfaces.Team;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class teamController : apiController
    {
        private readonly ITeamRepository _teamrepository;
        public teamController(ITeamRepository teamrepository)
        {
            _teamrepository = teamrepository;
        }
        
        [HttpPost(nameof(GetTeams))]
        public IList<TeamSearchViewModel> GetTeams(TeamSearchModel model)
        {
            return  _teamrepository.GetTeamsByFilter(model);
        }
        [HttpGet("{id}")]
        public TeamSearchViewModel GET(int id)
        {
            return _teamrepository.GetByID(id);
        }

        #region insert update delete

        [HttpPost]
        public async Task<IActionResult> POST(TeamInsertModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            model.Status = 1;
            string result = "";
            var rs = await _teamrepository.Insert(model);
            if (rs > 0)
            { 
                result = ConstString.UpdateSuccess;
                rs = 1;
            }
            else
            {
                result = ConstString.UpdateFail;
            }     
            return Ok(new { status = rs, result });
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PUT(TeamUpdateModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            } 
            string result = "";
            var rs = await _teamrepository.Update(model);
            switch (rs)
            {
                case 0: result = ConstString.UpdateFail; break;
                case 1: result = ConstString.UpdateSuccess; break;
            }
            return Ok(new { status = rs, result });
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DELETE(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            string result = "";
            var rs = await _teamrepository.Delete(id);
            switch (rs)
            {
                case 1: result = ConstString.UpdateSuccess; break;
                case 0: result = ConstString.UpdateFail; break;
                case -1: result = "Có team đang ở Phòng ban này,không thể cập nhật, vui lòng kiểm tra lại"; break;
            }
            return Ok(new { status = rs, result });
        }
        #endregion
    }
}
