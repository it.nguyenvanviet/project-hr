﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMS.Core.ConstString;
using CMS.Core.Domain.Setting;
using CMS.Infrastructure.Mapping.Interfaces.Settings;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CMS.API.Controllers
{
    [Authorize(Policy = Policy.AdminRole)]
    [Route("api/[controller]")]
    [ApiController]
    public class settingController : apiController
    {
        private readonly ISettingsRepository _settingRepository;
        public settingController(ISettingsRepository settingsRepository)
        {
            _settingRepository = settingsRepository;
        }
        [HttpGet]
        [Route(nameof(getsetting))]
        public IEnumerable<Setting> getsetting()
        {
            var data = _settingRepository.GetSettings().Select(item => new Setting
            {
                ID = item.ID,
                Name = item.Name,
                Status = item.Status,
                Type = item.Type,
                CreateBy = item.CreateBy,
                CreateDate = item.CreateDate,
                ModifyBy = item.ModifyBy,
                ModifyDate = item.ModifyDate,
                Contents = item.Contents
            }).ToArray();
            return data;
        } 

        [HttpPut]
        public IActionResult update(Setting model, int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            else
            {
                model.ID = id;
                string result = "";
                var status = _settingRepository.UpdateSettings(model);
                switch (status)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status, result });
            }
        }
    }
}