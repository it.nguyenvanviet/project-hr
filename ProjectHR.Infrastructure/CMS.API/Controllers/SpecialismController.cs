﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Employee;
using CMS.Infrastructure.Interfaces.Accounts;
using CMS.Infrastructure.Interfaces.Employee;
using CMS.Infrastructure.Mapping.Interfaces.Specialisms;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class specialismController : apiController
    {
        private readonly ISpecialismRepository _specialismRepository;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IRolesRepository _rolesRepository;
        public specialismController(
            IEmployeeRepository employeeRepository,
            IRolesRepository rolesRepository,
            ISpecialismRepository specialismRepository
        )
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
            _specialismRepository = specialismRepository;
        }
        #region Get

        [HttpGet]
        public IEnumerable<Specialism> Get()
        {
            return _specialismRepository.GetSpecialisms();
        }

        #endregion

        #region insert update delete

        [HttpPost]
        public async Task<IActionResult> POST(SpecialismInsertModel spe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(spe);
            }
            spe.Status = 1;
            string result = "";
            var rs = await _specialismRepository.Create(spe);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PUT(SpecialismUpdateModel spe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(spe);
            }
            spe.Status = 1;
            string result = "";
            var rs = await _specialismRepository.Update(spe);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DELETE(SpecialismDeleteModel spe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(spe);
            } 
            string result = "";
            var rs = await _specialismRepository.Delete(spe);
            if (rs > 0)
            {
                switch (rs)
                {
                    case 0: result = ConstString.UpdateFail; break;
                    case 1: result = ConstString.UpdateSuccess; break;
                }
                return Ok(new { status = rs, result });
            }
            return BadRequest();
        }
        #endregion
    }
}
