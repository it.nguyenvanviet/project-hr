﻿using CMS.Core.ConstString;
using CMS.Core.Domain.Leave;
using CMS.Infrastructure.Mapping.Interfaces.Leave;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class leaveController : apiController
    {
        private readonly ILeaveRepository _leaverepository;
        public leaveController(ILeaveRepository leaverepository)
        {
            _leaverepository = leaverepository;
        }

        [HttpGet(nameof(GetCountLeaveRequest))]
        public async Task<CountLeaveRequest> GetCountLeaveRequest([FromRoute] DateTime? FromDate, DateTime? ToDate)
        {
            return await _leaverepository.getCountLeaveRequest(FromDate, ToDate);
        }

        [HttpPost(nameof(GetListLeaveRequest))]
        public async Task<IList<LeaveViewModel>> GetListLeaveRequest([FromBody] LeaveSearchParamModel model)
        {
            return await _leaverepository.getListLeaveRequest(model);
        }

        [HttpGet("{id}")]
        public async Task<LeaveModel> GET(int id)
        {
            return await _leaverepository.getByID(id);
        }

        #region insert update delete

        [HttpPost]
        public async Task<IActionResult> POST(LeaveInsertModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            model.Status = 1;
            string result = string.Empty;
            var rs = await _leaverepository.createLeaveRequest(model);
            if (rs > 0)
            {
                result = ConstString.UpdateSuccess;
                rs = 1;
            }
            else
            {
                result = ConstString.UpdateFail;
            }
            return Ok(new { status = rs, result });
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PUT(LeaveUpdateModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            string result = string.Empty;
            var rs = await _leaverepository.updateLeaveRequest(model);
            switch (rs)
            {
                case 0: result = ConstString.UpdateFail; break;
                case 1: result = ConstString.UpdateSuccess; break;
            }
            return Ok(new { status = rs, result });
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DELETE(int id, string UpdateBy)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            string result = string.Empty;
            var rs = await _leaverepository.deleteLeaveRequest(id,UpdateBy);
            switch (rs)
            {
                case 1: result = ConstString.UpdateSuccess; break;
                case 0: result = ConstString.UpdateFail; break; 
            }
            return Ok(new { status = rs, result });
        }
        [HttpPut(nameof(ChangeApproved))]
        public async Task<IActionResult> ChangeApproved(ChangeLeaveApproved model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(model);
            }
            string result = string.Empty;
            var rs = await _leaverepository.changeApproved(model);
            switch (rs)
            {
                case 0: result = ConstString.UpdateFail; break;
                case 1: result = ConstString.UpdateSuccess; break;
            }
            return Ok(new { status = rs, result });
        }

        #endregion
    }
}
